## Author
Divya Anam
Date: January 03, 2020
## Version

| Version | Information |
|  -----  |    -----    |
|  1.0.0  |  Online Food Ordering System|

## The Repository
The repository includes the basic system for online food ordering with Laravel+Vue as integrated as a single project.

## Installation Steps
1. Clone the project.
2. Change directory to the project.
3. Create .env file if not exist.
4. Copy env.example contents to your .env file.
5. Create a new database and change the database name in the .env file.
6. Run php artisan migrate:refresh --seed to seed the data in the database.
5. Generate app key using 'php artisan generate:key' command.
6. Install composer using 'composer install' command.
7. Install node modules using 'npm install' command.
8. Run 'npm run development' command for compiling assets. 

## Assumptions

1. Integrated Vue+Laravel as a Single Project
2. The Menu Options are contained in a single array, i.e the menu items are not sub divided as per the the food item type like Desserts,Beverages,etc.
3. Each restaurant has a single cuisine type.

## Update Log


## License
Copyright (c) twoiq LLP. All rights reserved.