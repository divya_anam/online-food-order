<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Restaurant'],function(){
    Route::resource('restaurant','RestaurantController',['only'=>['index','show']]);
});
Route::group(['namespace'=>'Order'],function(){
    Route::post('/order/make-payment','OrderController@makePayment')->name('order.make-payment');
    Route::resource('order','OrderController',['only'=>['store']]);
});

Route::get('/{any}', 'SPA\SpaController@index')->where('any', '.*');



