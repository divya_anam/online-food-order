<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Restaurant\Restaurant;
use App\Repositories\Restaurant\RestaurantRepository;
use App\Models\Restaurant\MenuOption;
use Log;
use Exception;

class RestaurantController extends Controller
{
    protected $restaurant_repo;
    public function __construct(RestaurantRepository $restaurant_repo)
    {
        $this->restaurant_repo = $restaurant_repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $records = $this->getPaginationRecordCount(request());
        $search = empty($request->input('search')) ?  '' : $request->input('search');
        $restaurants = $this->restaurant_repo->searchRestaurant($search);
        try{ 
            $restaurants = $restaurants->get()->toArray();
            return $this->responseSuccessWithData(['restaurants'=>$restaurants]);
        } catch(Exception $e) {
            Log::error('Error while listing restaurant =>',[$e->getMessage()]);
            return abort(503);
        }
    }

    public function show($id)
    {
        $restaurant=Restaurant::findOrFail($id);
        $menu_options=MenuOption::where('restaurant_id',$id)->get()->toArray();
        return $this->responseSuccessWithData(['restaurant'=>$restaurant,'menu_options'=>$menu_options]);
    }
}
