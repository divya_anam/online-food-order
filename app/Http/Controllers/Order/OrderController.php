<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;
use App\Http\Requests\Requests\Order\OrderRequest;
use DB;
use Log;

class OrderController extends Controller
{

    protected $order_repo;
    public function __construct(OrderRepository $order_repo)
    {
        $this->order_repo=$order_repo;
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $attributes=$request->all();
            $result = $this->order_repo->addOrder($attributes);
            if(!$result) {
                DB::rollBack();
                return $this->responseFail('Unable to create order. Please contact system admin.', ['title' => 'Sorry']);
            }
            DB::commit();
            return $this->responseSuccessWithData([
                'message' => 'Order Created Successfully',
                'title' => 'Great',
                'order_id'=>$result->id
            ]);
        } catch(Exception $e) {
            DB::rollBack();
            Log::error('Error while creating order =>',[$e->getMessage()]);
            return $this->responseFail('Unable to create order. Please contact system admin.', ['title' => 'Sorry']);
        }
    }

    public function makePayment(Request $request){
        try {
            DB::beginTransaction();
            $attributes=$request->all();
            $result = $this->order_repo->addPayment($attributes);
            if(!$result) {
                DB::rollBack();
                return $this->responseFail('Unable to complete payment. Please contact system admin.', ['title' => 'Sorry']);
            }
            DB::commit();
            return $this->responseSuccessWithData([
                'message' => 'Payment Successful',
                'title' => 'Great',
                'order_id'=>$result->id
            ]);
        } catch(Exception $e) {
            DB::rollBack();
            Log::error('Payment Failed =>',[$e->getMessage()]);
            return $this->responseFail('Unable to complete payment. Please contact system admin.', ['title' => 'Sorry']);
        } 
    }
}
