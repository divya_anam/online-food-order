<?php

namespace App\Http\Controllers\SPA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpaController extends Controller
{  //
    public function index()
    {
        return view('welcome');
    }
}