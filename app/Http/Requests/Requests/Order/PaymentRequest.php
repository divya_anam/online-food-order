<?php

namespace App\Http\Requests\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required',
            'payment_type'=>'required',
            'card_number'=>'required',
            'card_name'=>'required',
            'month'=>'required',
            'year'=>'required',
            'cvv'=>'required'
        ];
    }
}
