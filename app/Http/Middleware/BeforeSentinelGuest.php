<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class BeforeSentinelGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Sentinel::getUser();
        if($user!=null){
            if($request->ajax()){
                   return response()->json(['result'=>false,'title'=>'Attention!','message'=>__('messages.auth.already_logged_in')],200)->setCallback($request->input('callback'));
           }
           else{

               return redirect(route('home'));
           }
        }
        return $next($request);
    }
}
