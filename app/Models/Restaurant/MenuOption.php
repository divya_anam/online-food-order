<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;


class MenuOption extends Model
{
    protected $fillable = [
        'restaurant_id',
        'item_name',
        'item_price',
	];
}
