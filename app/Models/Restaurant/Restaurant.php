<?php

namespace App\Models\Restaurant;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Encrypted;

class Restaurant extends Model
{
	use Encrypted;
    protected $fillable = [
		// 'image_url',
        'name',
        'cuisine',
        'location'
	];

	public function menuOptions(){
		return $this->hasMany('App\Models\Resturant\MenuOptions','restaurant_id','id');
	}

	public function order(){
		return $this->hasMany('App\Models\Order\Order','restaurant_id','id');
	}
}
