<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Encrypted;

class Order extends Model
{
    //
    use Encrypted;
    protected $fillable =[
        'restaurant_id',
        'menu_items',
        'total'
    ];

    public function restaurant(){
        return $this->belongsTo('App\Models\Restaurant\Restaurant','restaurant_id','id');
    }

    public function payment(){
        return $this->hasOne('App\Models\Order\Payment','order_id','id');
    }
}
