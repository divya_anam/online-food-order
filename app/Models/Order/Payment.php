<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Encrypted;

class Payment extends Model
{
    //
    use Encrypted;
    protected $fillable =[
        'order_id',
        'payment_type',
        'card_number',
        'card_name',
        'month',
        'year',
        'cvv'
    ];

    public function order(){
        return $this->belongsTo('App\Models\Order\Order','order_id','id');
    }
}
