<?php
namespace App\Repositories\Restaurant;

use App\Repositories\EloquentDBRepository;

class MenuOptionRepository extends EloquentDBRepository {
    
    protected $model = 'App\Models\Restaurant\MenuOption';
   
    public function __construct(){
        parent::__construct();
    }

    public function addMenuOptions($attributes){
        $data = [
            'restaurant_id'=>$attributes['restaurant_id'],
            'item_name'=>$attributes['item_name'],
            'item_price'=>$attributes['item_price'],
        ];
        $this->model->create($data);
    }
}