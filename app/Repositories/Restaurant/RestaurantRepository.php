<?php
namespace App\Repositories\Restaurant;

use App\Repositories\EloquentDBRepository;
use App\Models\Restaurant\Restaurant;
class RestaurantRepository extends EloquentDBRepository {

    protected $model = 'App\Models\Restaurant\Restaurant';
       
    public function __construct(){
        parent::__construct();
    }

    public function addRestaurant($attributes){
        $data = [
            'name'=>$attributes['name'],
            'cuisine'=>$attributes['cuisine'],
            'location'=>$attributes['location'],
        ];
        $this->model->create($data);
    }

    public function searchRestaurant($search){
        $restaurants = (new Restaurant)->newQuery();
        $restaurants = $restaurants->where('cuisine','like','%'.$search.'%')->orWhere('location','like','%'.$search.'%');
        return $restaurants;
    }
}