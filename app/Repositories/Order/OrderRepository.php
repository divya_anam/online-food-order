<?php
namespace App\Repositories\Order;

use App\Repositories\EloquentDBRepository;
use App\Models\Order\Payment;
use Log;

class OrderRepository extends EloquentDBRepository {

    protected $model = 'App\Models\Order\Order';
       
    public function __construct(){
        parent::__construct();
    }

    public function addOrder($attributes){
        $data = [
            'restaurant_id'=>$attributes['restaurant_id'],
            'menu_items'=>json_encode($attributes['menu_items']),
            'total'=>$attributes['total']
        ];
        $result=$this->model->create($data);
        return $result;
    }

    public function addPayment($attributes){
        $data = [
            'order_id'=>$attributes['order_id'],
            'payment_type'=>$attributes['payment_type'],
            'card_number'=>$attributes['card_number'],
            'card_name'=>$attributes['card_name'],
            'month'=>$attributes['month'],
            'year'=>$attributes['year'],
            'cvv'=>$attributes['cvv']
        ];
        $result=Payment::create($data);
        return $result;
    }
}