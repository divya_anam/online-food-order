var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

!function () {
    "use strict";
    function e(e) {
        return JSON.parse(JSON.stringify(e));
    }function t(e) {
        for (var t = y(e); "ÿà" <= t[1].slice(0, 2) && t[1].slice(0, 2) <= "ÿï";) {
            t = [t[0]].concat(t.slice(2));
        }return t.join("");
    }function a(e) {
        return s(">" + p("B", e.length), e);
    }function i(e) {
        return s(">" + p("H", e.length), e);
    }function n(e) {
        return s(">" + p("L", e.length), e);
    }function r(e, t, r) {
        var o,
            l,
            m,
            y,
            c = "",
            S = "";if ("Byte" == t) o = e.length, 4 >= o ? S = a(e) + p("\x00", 4 - o) : (S = s(">L", [r]), c = a(e));else if ("Short" == t) o = e.length, 2 >= o ? S = i(e) + p("\x00\x00", 2 - o) : (S = s(">L", [r]), c = i(e));else if ("Long" == t) o = e.length, 1 >= o ? S = n(e) : (S = s(">L", [r]), c = n(e));else if ("Ascii" == t) l = e + "\x00", o = l.length, o > 4 ? (S = s(">L", [r]), c = l) : S = l + p("\x00", 4 - o);else if ("Rational" == t) {
            if ("number" == typeof e[0]) o = 1, m = e[0], y = e[1], l = s(">L", [m]) + s(">L", [y]);else {
                o = e.length, l = "";for (var f = 0; o > f; f++) {
                    m = e[f][0], y = e[f][1], l += s(">L", [m]) + s(">L", [y]);
                }
            }S = s(">L", [r]), c = l;
        } else if ("SRational" == t) {
            if ("number" == typeof e[0]) o = 1, m = e[0], y = e[1], l = s(">l", [m]) + s(">l", [y]);else {
                o = e.length, l = "";for (var f = 0; o > f; f++) {
                    m = e[f][0], y = e[f][1], l += s(">l", [m]) + s(">l", [y]);
                }
            }S = s(">L", [r]), c = l;
        } else "Undefined" == t && (o = e.length, o > 4 ? (S = s(">L", [r]), c = e) : S = e + p("\x00", 4 - o));var h = s(">L", [o]);return [h, S, c];
    }function o(e, t, a) {
        var i,
            n = 8,
            o = Object.keys(e).length,
            l = s(">H", [o]);i = ["0th", "1st"].indexOf(t) > -1 ? 2 + 12 * o + 4 : 2 + 12 * o;var m,
            p = "",
            y = "";for (var m in e) {
            if ("string" == typeof m && (m = parseInt(m)), !("0th" == t && [34665, 34853].indexOf(m) > -1 || "Exif" == t && 40965 == m || "1st" == t && [513, 514].indexOf(m) > -1)) {
                var c = e[m],
                    S = s(">H", [m]),
                    f = u[t][m].type,
                    h = s(">H", [g[f]]);"number" == typeof c && (c = [c]);var d = n + i + a + y.length,
                    P = r(c, f, d),
                    C = P[0],
                    R = P[1],
                    L = P[2];p += S + h + C + R, y += L;
            }
        }return [l + p, y];
    }function l(e) {
        var t, a;if ("ÿØ" == e.slice(0, 2)) t = y(e), a = c(t), a ? this.tiftag = a.slice(10) : this.tiftag = null;else if (["II", "MM"].indexOf(e.slice(0, 2)) > -1) this.tiftag = e;else {
            if ("Exif" != e.slice(0, 4)) throw "Given file is neither JPEG nor TIFF.";this.tiftag = e.slice(6);
        }
    }function s(e, t) {
        if (!(t instanceof Array)) throw "'pack' error. Got invalid type argument.";if (e.length - 1 != t.length) throw "'pack' error. " + (e.length - 1) + " marks, " + t.length + " elements.";var a;if ("<" == e[0]) a = !0;else {
            if (">" != e[0]) throw "";a = !1;
        }for (var i = "", n = 1, r = null, o = null, l = null; o = e[n];) {
            if ("b" == o.toLowerCase()) {
                if (r = t[n - 1], "b" == o && 0 > r && (r += 256), r > 255 || 0 > r) throw "'pack' error.";l = String.fromCharCode(r);
            } else if ("H" == o) {
                if (r = t[n - 1], r > 65535 || 0 > r) throw "'pack' error.";l = String.fromCharCode(Math.floor(r % 65536 / 256)) + String.fromCharCode(r % 256), a && (l = l.split("").reverse().join(""));
            } else {
                if ("l" != o.toLowerCase()) throw "'pack' error.";if (r = t[n - 1], "l" == o && 0 > r && (r += 4294967296), r > 4294967295 || 0 > r) throw "'pack' error.";l = String.fromCharCode(Math.floor(r / 16777216)) + String.fromCharCode(Math.floor(r % 16777216 / 65536)) + String.fromCharCode(Math.floor(r % 65536 / 256)) + String.fromCharCode(r % 256), a && (l = l.split("").reverse().join(""));
            }i += l, n += 1;
        }return i;
    }function m(e, t) {
        if ("string" != typeof t) throw "'unpack' error. Got invalid type argument.";for (var a = 0, i = 1; i < e.length; i++) {
            if ("b" == e[i].toLowerCase()) a += 1;else if ("h" == e[i].toLowerCase()) a += 2;else {
                if ("l" != e[i].toLowerCase()) throw "'unpack' error. Got invalid mark.";a += 4;
            }
        }if (a != t.length) throw "'unpack' error. Mismatch between symbol and string length. " + a + ":" + t.length;var n;if ("<" == e[0]) n = !0;else {
            if (">" != e[0]) throw "'unpack' error.";n = !1;
        }for (var r = [], o = 0, l = 1, s = null, m = null, p = null, y = ""; m = e[l];) {
            if ("b" == m.toLowerCase()) p = 1, y = t.slice(o, o + p), s = y.charCodeAt(0), "b" == m && s >= 128 && (s -= 256);else if ("H" == m) p = 2, y = t.slice(o, o + p), n && (y = y.split("").reverse().join("")), s = 256 * y.charCodeAt(0) + y.charCodeAt(1);else {
                if ("l" != m.toLowerCase()) throw "'unpack' error. " + m;p = 4, y = t.slice(o, o + p), n && (y = y.split("").reverse().join("")), s = 16777216 * y.charCodeAt(0) + 65536 * y.charCodeAt(1) + 256 * y.charCodeAt(2) + y.charCodeAt(3), "l" == m && s >= 2147483648 && (s -= 4294967296);
            }r.push(s), o += p, l += 1;
        }return r;
    }function p(e, t) {
        for (var a = "", i = 0; t > i; i++) {
            a += e;
        }return a;
    }function y(e) {
        if ("ÿØ" != e.slice(0, 2)) throw "Given data isn't JPEG.";for (var t = 2, a = ["ÿØ"];;) {
            if ("ÿÚ" == e.slice(t, t + 2)) {
                a.push(e.slice(t));break;
            }var i = m(">H", e.slice(t + 2, t + 4))[0],
                n = t + i + 2;if (a.push(e.slice(t, n)), t = n, t >= e.length) throw "Wrong JPEG data.";
        }return a;
    }function c(e) {
        for (var t, a = 0; a < e.length; a++) {
            if (t = e[a], "ÿá" == t.slice(0, 2) && "Exif\x00\x00" == t.slice(4, 10)) return t;
        }return null;
    }function S(e, t) {
        return "ÿà" == e[1].slice(0, 2) && "ÿá" == e[2].slice(0, 2) && "Exif\x00\x00" == e[2].slice(4, 10) ? t ? (e[2] = t, e = ["ÿØ"].concat(e.slice(2))) : e = null == t ? e.slice(0, 2).concat(e.slice(3)) : e.slice(0).concat(e.slice(2)) : "ÿà" == e[1].slice(0, 2) ? t && (e[1] = t) : "ÿá" == e[1].slice(0, 2) && "Exif\x00\x00" == e[1].slice(4, 10) ? t ? e[1] = t : null == t && (e = e.slice(0).concat(e.slice(2))) : t && (e = [e[0], t].concat(e.slice(1))), e.join("");
    }var f = {};if (f.version = "1.03", f.remove = function (e) {
        var t = !1;if ("ÿØ" == e.slice(0, 2)) ;else {
            if ("data:image/jpeg;base64," != e.slice(0, 23) && "data:image/jpg;base64," != e.slice(0, 22)) throw "Given data is not jpeg.";e = d(e.split(",")[1]), t = !0;
        }var a = y(e);if ("ÿá" == a[1].slice(0, 2) && "Exif\x00\x00" == a[1].slice(4, 10)) a = [a[0]].concat(a.slice(2));else {
            if ("ÿá" != a[2].slice(0, 2) || "Exif\x00\x00" != a[2].slice(4, 10)) throw "Exif not found.";a = a.slice(0, 2).concat(a.slice(3));
        }var i = a.join("");return t && (i = "data:image/jpeg;base64," + h(i)), i;
    }, f.insert = function (e, t) {
        var a = !1;if ("Exif\x00\x00" != e.slice(0, 6)) throw "Given data is not exif.";if ("ÿØ" == t.slice(0, 2)) ;else {
            if ("data:image/jpeg;base64," != t.slice(0, 23) && "data:image/jpg;base64," != t.slice(0, 22)) throw "Given data is not jpeg.";t = d(t.split(",")[1]), a = !0;
        }var i = "ÿá" + s(">H", [e.length + 2]) + e,
            n = y(t),
            r = S(n, i);return a && (r = "data:image/jpeg;base64," + h(r)), r;
    }, f.load = function (e) {
        var t;if ("string" != typeof e) throw "'load' gots invalid type argument.";if ("ÿØ" == e.slice(0, 2)) t = e;else if ("data:image/jpeg;base64," == e.slice(0, 23) || "data:image/jpg;base64," == e.slice(0, 22)) t = d(e.split(",")[1]);else {
            if ("Exif" != e.slice(0, 4)) throw "'load' gots invalid file data.";t = e.slice(6);
        }var a = { "0th": {}, Exif: {}, GPS: {}, Interop: {}, "1st": {}, thumbnail: null },
            i = new l(t);if (null === i.tiftag) return a;"II" == i.tiftag.slice(0, 2) ? i.endian_mark = "<" : i.endian_mark = ">";var n = m(i.endian_mark + "L", i.tiftag.slice(4, 8))[0];a["0th"] = i.get_ifd(n, "0th");var r = a["0th"].first_ifd_pointer;if (delete a["0th"].first_ifd_pointer, 34665 in a["0th"] && (n = a["0th"][34665], a.Exif = i.get_ifd(n, "Exif")), 34853 in a["0th"] && (n = a["0th"][34853], a.GPS = i.get_ifd(n, "GPS")), 40965 in a.Exif && (n = a.Exif[40965], a.Interop = i.get_ifd(n, "Interop")), "\x00\x00\x00\x00" != r && (n = m(i.endian_mark + "L", r)[0], a["1st"] = i.get_ifd(n, "1st"), 513 in a["1st"] && 514 in a["1st"])) {
            var o = a["1st"][513] + a["1st"][514],
                s = i.tiftag.slice(a["1st"][513], o);a.thumbnail = s;
        }return a;
    }, f.dump = function (a) {
        var i,
            n,
            r,
            l,
            m,
            p = 8,
            y = e(a),
            c = "Exif\x00\x00MM\x00*\x00\x00\x00\b",
            S = !1,
            h = !1,
            d = !1,
            u = !1;i = "0th" in y ? y["0th"] : {}, "Exif" in y && Object.keys(y.Exif).length || "Interop" in y && Object.keys(y.Interop).length ? (i[34665] = 1, S = !0, n = y.Exif, "Interop" in y && Object.keys(y.Interop).length ? (n[40965] = 1, d = !0, r = y.Interop) : Object.keys(n).indexOf(f.ExifIFD.InteroperabilityTag.toString()) > -1 && delete n[40965]) : Object.keys(i).indexOf(f.ImageIFD.ExifTag.toString()) > -1 && delete i[34665], "GPS" in y && Object.keys(y.GPS).length ? (i[f.ImageIFD.GPSTag] = 1, h = !0, l = y.GPS) : Object.keys(i).indexOf(f.ImageIFD.GPSTag.toString()) > -1 && delete i[f.ImageIFD.GPSTag], "1st" in y && "thumbnail" in y && null != y.thumbnail && (u = !0, y["1st"][513] = 1, y["1st"][514] = 1, m = y["1st"]);var P,
            C,
            R,
            L,
            x,
            I = o(i, "0th", 0),
            D = I[0].length + 12 * S + 12 * h + 4 + I[1].length,
            G = "",
            A = 0,
            v = "",
            b = 0,
            T = "",
            k = 0,
            w = "";if (S && (P = o(n, "Exif", D), A = P[0].length + 12 * d + P[1].length), h && (C = o(l, "GPS", D + A), v = C.join(""), b = v.length), d) {
            var F = D + A + b;R = o(r, "Interop", F), T = R.join(""), k = T.length;
        }if (u) {
            var F = D + A + b + k;if (L = o(m, "1st", F), x = t(y.thumbnail), x.length > 64e3) throw "Given thumbnail is too large. max 64kB";
        }var B = "",
            E = "",
            M = "",
            O = "\x00\x00\x00\x00";if (S) {
            var N = p + D,
                U = s(">L", [N]),
                _ = 34665,
                H = s(">H", [_]),
                j = s(">H", [g.Long]),
                V = s(">L", [1]);B = H + j + V + U;
        }if (h) {
            var N = p + D + A,
                U = s(">L", [N]),
                _ = 34853,
                H = s(">H", [_]),
                j = s(">H", [g.Long]),
                V = s(">L", [1]);E = H + j + V + U;
        }if (d) {
            var N = p + D + A + b,
                U = s(">L", [N]),
                _ = 40965,
                H = s(">H", [_]),
                j = s(">H", [g.Long]),
                V = s(">L", [1]);M = H + j + V + U;
        }if (u) {
            var N = p + D + A + b + k;O = s(">L", [N]);var J = N + L[0].length + 24 + 4 + L[1].length,
                X = "\x00\x00\x00\x00" + s(">L", [J]),
                z = "\x00\x00\x00\x00" + s(">L", [x.length]);w = L[0] + X + z + "\x00\x00\x00\x00" + L[1] + x;
        }var Y = I[0] + B + E + O + I[1];return S && (G = P[0] + M + P[1]), c + Y + G + v + T + w;
    }, l.prototype = { get_ifd: function get_ifd(e, t) {
            var a,
                i = {},
                n = m(this.endian_mark + "H", this.tiftag.slice(e, e + 2))[0],
                r = e + 2;a = ["0th", "1st"].indexOf(t) > -1 ? "Image" : t;for (var o = 0; n > o; o++) {
                e = r + 12 * o;var l = m(this.endian_mark + "H", this.tiftag.slice(e, e + 2))[0],
                    s = m(this.endian_mark + "H", this.tiftag.slice(e + 2, e + 4))[0],
                    p = m(this.endian_mark + "L", this.tiftag.slice(e + 4, e + 8))[0],
                    y = this.tiftag.slice(e + 8, e + 12),
                    c = [s, p, y];l in u[a] && (i[l] = this.convert_value(c));
            }return "0th" == t && (e = r + 12 * n, i.first_ifd_pointer = this.tiftag.slice(e, e + 4)), i;
        }, convert_value: function convert_value(e) {
            var t,
                a = null,
                i = e[0],
                n = e[1],
                r = e[2];if (1 == i) n > 4 ? (t = m(this.endian_mark + "L", r)[0], a = m(this.endian_mark + p("B", n), this.tiftag.slice(t, t + n))) : a = m(this.endian_mark + p("B", n), r.slice(0, n));else if (2 == i) n > 4 ? (t = m(this.endian_mark + "L", r)[0], a = this.tiftag.slice(t, t + n - 1)) : a = r.slice(0, n - 1);else if (3 == i) n > 2 ? (t = m(this.endian_mark + "L", r)[0], a = m(this.endian_mark + p("H", n), this.tiftag.slice(t, t + 2 * n))) : a = m(this.endian_mark + p("H", n), r.slice(0, 2 * n));else if (4 == i) n > 1 ? (t = m(this.endian_mark + "L", r)[0], a = m(this.endian_mark + p("L", n), this.tiftag.slice(t, t + 4 * n))) : a = m(this.endian_mark + p("L", n), r);else if (5 == i) {
                if (t = m(this.endian_mark + "L", r)[0], n > 1) {
                    a = [];for (var o = 0; n > o; o++) {
                        a.push([m(this.endian_mark + "L", this.tiftag.slice(t + 8 * o, t + 4 + 8 * o))[0], m(this.endian_mark + "L", this.tiftag.slice(t + 4 + 8 * o, t + 8 + 8 * o))[0]]);
                    }
                } else a = [m(this.endian_mark + "L", this.tiftag.slice(t, t + 4))[0], m(this.endian_mark + "L", this.tiftag.slice(t + 4, t + 8))[0]];
            } else if (7 == i) n > 4 ? (t = m(this.endian_mark + "L", r)[0], a = this.tiftag.slice(t, t + n)) : a = r.slice(0, n);else {
                if (10 != i) throw "Exif might be wrong. Got incorrect value type to decode. type:" + i;if (t = m(this.endian_mark + "L", r)[0], n > 1) {
                    a = [];for (var o = 0; n > o; o++) {
                        a.push([m(this.endian_mark + "l", this.tiftag.slice(t + 8 * o, t + 4 + 8 * o))[0], m(this.endian_mark + "l", this.tiftag.slice(t + 4 + 8 * o, t + 8 + 8 * o))[0]]);
                    }
                } else a = [m(this.endian_mark + "l", this.tiftag.slice(t, t + 4))[0], m(this.endian_mark + "l", this.tiftag.slice(t + 4, t + 8))[0]];
            }return a instanceof Array && 1 == a.length ? a[0] : a;
        } }, "undefined" != typeof window && "function" == typeof window.btoa) var h = window.btoa;if ("undefined" == typeof h) var h = function h(e) {
        for (var t, a, i, n, r, o, l, s = "", m = 0, p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; m < e.length;) {
            t = e.charCodeAt(m++), a = e.charCodeAt(m++), i = e.charCodeAt(m++), n = t >> 2, r = (3 & t) << 4 | a >> 4, o = (15 & a) << 2 | i >> 6, l = 63 & i, isNaN(a) ? o = l = 64 : isNaN(i) && (l = 64), s = s + p.charAt(n) + p.charAt(r) + p.charAt(o) + p.charAt(l);
        }return s;
    };if ("undefined" != typeof window && "function" == typeof window.atob) var d = window.atob;if ("undefined" == typeof d) var d = function d(e) {
        var t,
            a,
            i,
            n,
            r,
            o,
            l,
            s = "",
            m = 0,
            p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";for (e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""); m < e.length;) {
            n = p.indexOf(e.charAt(m++)), r = p.indexOf(e.charAt(m++)), o = p.indexOf(e.charAt(m++)), l = p.indexOf(e.charAt(m++)), t = n << 2 | r >> 4, a = (15 & r) << 4 | o >> 2, i = (3 & o) << 6 | l, s += String.fromCharCode(t), 64 != o && (s += String.fromCharCode(a)), 64 != l && (s += String.fromCharCode(i));
        }return s;
    };var g = { Byte: 1, Ascii: 2, Short: 3, Long: 4, Rational: 5, Undefined: 7, SLong: 9, SRational: 10 },
        u = { Image: { 11: { name: "ProcessingSoftware", type: "Ascii" }, 254: { name: "NewSubfileType", type: "Long" }, 255: { name: "SubfileType", type: "Short" }, 256: { name: "ImageWidth", type: "Long" }, 257: { name: "ImageLength", type: "Long" }, 258: { name: "BitsPerSample", type: "Short" }, 259: { name: "Compression", type: "Short" }, 262: { name: "PhotometricInterpretation", type: "Short" }, 263: { name: "Threshholding", type: "Short" }, 264: { name: "CellWidth", type: "Short" }, 265: { name: "CellLength", type: "Short" }, 266: { name: "FillOrder", type: "Short" }, 269: { name: "DocumentName", type: "Ascii" }, 270: { name: "ImageDescription", type: "Ascii" }, 271: { name: "Make", type: "Ascii" }, 272: { name: "Model", type: "Ascii" }, 273: { name: "StripOffsets", type: "Long" }, 274: { name: "Orientation", type: "Short" }, 277: { name: "SamplesPerPixel", type: "Short" }, 278: { name: "RowsPerStrip", type: "Long" }, 279: { name: "StripByteCounts", type: "Long" }, 282: { name: "XResolution", type: "Rational" }, 283: { name: "YResolution", type: "Rational" }, 284: { name: "PlanarConfiguration", type: "Short" }, 290: { name: "GrayResponseUnit", type: "Short" }, 291: { name: "GrayResponseCurve", type: "Short" }, 292: { name: "T4Options", type: "Long" }, 293: { name: "T6Options", type: "Long" }, 296: { name: "ResolutionUnit", type: "Short" }, 301: { name: "TransferFunction", type: "Short" }, 305: { name: "Software", type: "Ascii" }, 306: { name: "DateTime", type: "Ascii" }, 315: { name: "Artist", type: "Ascii" }, 316: { name: "HostComputer", type: "Ascii" }, 317: { name: "Predictor", type: "Short" }, 318: { name: "WhitePoint", type: "Rational" }, 319: { name: "PrimaryChromaticities", type: "Rational" }, 320: { name: "ColorMap", type: "Short" }, 321: { name: "HalftoneHints", type: "Short" }, 322: { name: "TileWidth", type: "Short" }, 323: { name: "TileLength", type: "Short" }, 324: { name: "TileOffsets", type: "Short" }, 325: { name: "TileByteCounts", type: "Short" }, 330: { name: "SubIFDs", type: "Long" }, 332: { name: "InkSet", type: "Short" }, 333: { name: "InkNames", type: "Ascii" }, 334: { name: "NumberOfInks", type: "Short" }, 336: { name: "DotRange", type: "Byte" }, 337: { name: "TargetPrinter", type: "Ascii" }, 338: { name: "ExtraSamples", type: "Short" }, 339: { name: "SampleFormat", type: "Short" }, 340: { name: "SMinSampleValue", type: "Short" }, 341: { name: "SMaxSampleValue", type: "Short" }, 342: { name: "TransferRange", type: "Short" }, 343: { name: "ClipPath", type: "Byte" }, 344: { name: "XClipPathUnits", type: "Long" }, 345: { name: "YClipPathUnits", type: "Long" }, 346: { name: "Indexed", type: "Short" }, 347: { name: "JPEGTables", type: "Undefined" }, 351: { name: "OPIProxy", type: "Short" }, 512: { name: "JPEGProc", type: "Long" }, 513: { name: "JPEGInterchangeFormat", type: "Long" }, 514: { name: "JPEGInterchangeFormatLength", type: "Long" }, 515: { name: "JPEGRestartInterval", type: "Short" }, 517: { name: "JPEGLosslessPredictors", type: "Short" }, 518: { name: "JPEGPointTransforms", type: "Short" }, 519: { name: "JPEGQTables", type: "Long" }, 520: { name: "JPEGDCTables", type: "Long" }, 521: { name: "JPEGACTables", type: "Long" }, 529: { name: "YCbCrCoefficients", type: "Rational" }, 530: { name: "YCbCrSubSampling", type: "Short" }, 531: { name: "YCbCrPositioning", type: "Short" }, 532: { name: "ReferenceBlackWhite", type: "Rational" }, 700: { name: "XMLPacket", type: "Byte" }, 18246: { name: "Rating", type: "Short" }, 18249: { name: "RatingPercent", type: "Short" }, 32781: { name: "ImageID", type: "Ascii" }, 33421: { name: "CFARepeatPatternDim", type: "Short" }, 33422: { name: "CFAPattern", type: "Byte" }, 33423: { name: "BatteryLevel", type: "Rational" }, 33432: { name: "Copyright", type: "Ascii" }, 33434: { name: "ExposureTime", type: "Rational" }, 34377: { name: "ImageResources", type: "Byte" }, 34665: { name: "ExifTag", type: "Long" }, 34675: { name: "InterColorProfile", type: "Undefined" }, 34853: { name: "GPSTag", type: "Long" }, 34857: { name: "Interlace", type: "Short" }, 34858: { name: "TimeZoneOffset", type: "Long" }, 34859: { name: "SelfTimerMode", type: "Short" }, 37387: { name: "FlashEnergy", type: "Rational" }, 37388: { name: "SpatialFrequencyResponse", type: "Undefined" }, 37389: { name: "Noise", type: "Undefined" }, 37390: { name: "FocalPlaneXResolution", type: "Rational" }, 37391: { name: "FocalPlaneYResolution", type: "Rational" }, 37392: { name: "FocalPlaneResolutionUnit", type: "Short" }, 37393: { name: "ImageNumber", type: "Long" }, 37394: { name: "SecurityClassification", type: "Ascii" }, 37395: { name: "ImageHistory", type: "Ascii" }, 37397: { name: "ExposureIndex", type: "Rational" }, 37398: { name: "TIFFEPStandardID", type: "Byte" }, 37399: { name: "SensingMethod", type: "Short" }, 40091: { name: "XPTitle", type: "Byte" }, 40092: { name: "XPComment", type: "Byte" }, 40093: { name: "XPAuthor", type: "Byte" }, 40094: { name: "XPKeywords", type: "Byte" }, 40095: { name: "XPSubject", type: "Byte" }, 50341: { name: "PrintImageMatching", type: "Undefined" }, 50706: { name: "DNGVersion", type: "Byte" }, 50707: { name: "DNGBackwardVersion", type: "Byte" }, 50708: { name: "UniqueCameraModel", type: "Ascii" }, 50709: { name: "LocalizedCameraModel", type: "Byte" }, 50710: { name: "CFAPlaneColor", type: "Byte" }, 50711: { name: "CFALayout", type: "Short" }, 50712: { name: "LinearizationTable", type: "Short" }, 50713: { name: "BlackLevelRepeatDim", type: "Short" }, 50714: { name: "BlackLevel", type: "Rational" }, 50715: { name: "BlackLevelDeltaH", type: "SRational" }, 50716: { name: "BlackLevelDeltaV", type: "SRational" }, 50717: { name: "WhiteLevel", type: "Short" }, 50718: { name: "DefaultScale", type: "Rational" }, 50719: { name: "DefaultCropOrigin", type: "Short" }, 50720: { name: "DefaultCropSize", type: "Short" }, 50721: { name: "ColorMatrix1", type: "SRational" }, 50722: { name: "ColorMatrix2", type: "SRational" }, 50723: { name: "CameraCalibration1", type: "SRational" }, 50724: { name: "CameraCalibration2", type: "SRational" }, 50725: { name: "ReductionMatrix1", type: "SRational" }, 50726: { name: "ReductionMatrix2", type: "SRational" }, 50727: { name: "AnalogBalance", type: "Rational" }, 50728: { name: "AsShotNeutral", type: "Short" }, 50729: { name: "AsShotWhiteXY", type: "Rational" }, 50730: { name: "BaselineExposure", type: "SRational" }, 50731: { name: "BaselineNoise", type: "Rational" }, 50732: { name: "BaselineSharpness", type: "Rational" }, 50733: { name: "BayerGreenSplit", type: "Long" }, 50734: { name: "LinearResponseLimit", type: "Rational" }, 50735: { name: "CameraSerialNumber", type: "Ascii" }, 50736: { name: "LensInfo", type: "Rational" }, 50737: { name: "ChromaBlurRadius", type: "Rational" }, 50738: { name: "AntiAliasStrength", type: "Rational" }, 50739: { name: "ShadowScale", type: "SRational" }, 50740: { name: "DNGPrivateData", type: "Byte" }, 50741: { name: "MakerNoteSafety", type: "Short" }, 50778: { name: "CalibrationIlluminant1", type: "Short" }, 50779: { name: "CalibrationIlluminant2", type: "Short" }, 50780: { name: "BestQualityScale", type: "Rational" }, 50781: { name: "RawDataUniqueID", type: "Byte" }, 50827: { name: "OriginalRawFileName", type: "Byte" }, 50828: { name: "OriginalRawFileData", type: "Undefined" }, 50829: { name: "ActiveArea", type: "Short" }, 50830: { name: "MaskedAreas", type: "Short" }, 50831: { name: "AsShotICCProfile", type: "Undefined" }, 50832: { name: "AsShotPreProfileMatrix", type: "SRational" }, 50833: { name: "CurrentICCProfile", type: "Undefined" }, 50834: { name: "CurrentPreProfileMatrix", type: "SRational" }, 50879: { name: "ColorimetricReference", type: "Short" }, 50931: { name: "CameraCalibrationSignature", type: "Byte" }, 50932: { name: "ProfileCalibrationSignature", type: "Byte" }, 50934: { name: "AsShotProfileName", type: "Byte" }, 50935: { name: "NoiseReductionApplied", type: "Rational" }, 50936: { name: "ProfileName", type: "Byte" }, 50937: { name: "ProfileHueSatMapDims", type: "Long" }, 50938: { name: "ProfileHueSatMapData1", type: "Float" }, 50939: { name: "ProfileHueSatMapData2", type: "Float" }, 50940: { name: "ProfileToneCurve", type: "Float" }, 50941: { name: "ProfileEmbedPolicy", type: "Long" }, 50942: { name: "ProfileCopyright", type: "Byte" }, 50964: { name: "ForwardMatrix1", type: "SRational" }, 50965: { name: "ForwardMatrix2", type: "SRational" }, 50966: { name: "PreviewApplicationName", type: "Byte" }, 50967: { name: "PreviewApplicationVersion", type: "Byte" }, 50968: { name: "PreviewSettingsName", type: "Byte" }, 50969: { name: "PreviewSettingsDigest", type: "Byte" }, 50970: { name: "PreviewColorSpace", type: "Long" }, 50971: { name: "PreviewDateTime", type: "Ascii" }, 50972: { name: "RawImageDigest", type: "Undefined" }, 50973: { name: "OriginalRawFileDigest", type: "Undefined" }, 50974: { name: "SubTileBlockSize", type: "Long" }, 50975: { name: "RowInterleaveFactor", type: "Long" }, 50981: { name: "ProfileLookTableDims", type: "Long" }, 50982: { name: "ProfileLookTableData", type: "Float" }, 51008: { name: "OpcodeList1", type: "Undefined" }, 51009: { name: "OpcodeList2", type: "Undefined" }, 51022: { name: "OpcodeList3", type: "Undefined" } }, Exif: { 33434: { name: "ExposureTime", type: "Rational" }, 33437: { name: "FNumber", type: "Rational" }, 34850: { name: "ExposureProgram", type: "Short" }, 34852: { name: "SpectralSensitivity", type: "Ascii" }, 34855: { name: "ISOSpeedRatings", type: "Short" }, 34856: { name: "OECF", type: "Undefined" }, 34864: { name: "SensitivityType", type: "Short" }, 34865: { name: "StandardOutputSensitivity", type: "Long" }, 34866: { name: "RecommendedExposureIndex", type: "Long" }, 34867: { name: "ISOSpeed", type: "Long" }, 34868: { name: "ISOSpeedLatitudeyyy", type: "Long" }, 34869: { name: "ISOSpeedLatitudezzz", type: "Long" }, 36864: { name: "ExifVersion", type: "Undefined" }, 36867: { name: "DateTimeOriginal", type: "Ascii" }, 36868: { name: "DateTimeDigitized", type: "Ascii" }, 37121: { name: "ComponentsConfiguration", type: "Undefined" }, 37122: { name: "CompressedBitsPerPixel", type: "Rational" }, 37377: { name: "ShutterSpeedValue", type: "SRational" }, 37378: { name: "ApertureValue", type: "Rational" }, 37379: { name: "BrightnessValue", type: "SRational" }, 37380: { name: "ExposureBiasValue", type: "SRational" }, 37381: { name: "MaxApertureValue", type: "Rational" }, 37382: { name: "SubjectDistance", type: "Rational" }, 37383: { name: "MeteringMode", type: "Short" }, 37384: { name: "LightSource", type: "Short" }, 37385: { name: "Flash", type: "Short" }, 37386: { name: "FocalLength", type: "Rational" }, 37396: { name: "SubjectArea", type: "Short" }, 37500: { name: "MakerNote", type: "Undefined" }, 37510: { name: "UserComment", type: "Ascii" }, 37520: { name: "SubSecTime", type: "Ascii" }, 37521: { name: "SubSecTimeOriginal", type: "Ascii" }, 37522: { name: "SubSecTimeDigitized", type: "Ascii" }, 40960: { name: "FlashpixVersion", type: "Undefined" }, 40961: { name: "ColorSpace", type: "Short" }, 40962: { name: "PixelXDimension", type: "Long" }, 40963: { name: "PixelYDimension", type: "Long" }, 40964: { name: "RelatedSoundFile", type: "Ascii" }, 40965: { name: "InteroperabilityTag", type: "Long" }, 41483: { name: "FlashEnergy", type: "Rational" }, 41484: { name: "SpatialFrequencyResponse", type: "Undefined" }, 41486: { name: "FocalPlaneXResolution", type: "Rational" }, 41487: { name: "FocalPlaneYResolution", type: "Rational" }, 41488: { name: "FocalPlaneResolutionUnit", type: "Short" }, 41492: { name: "SubjectLocation", type: "Short" }, 41493: { name: "ExposureIndex", type: "Rational" }, 41495: { name: "SensingMethod", type: "Short" }, 41728: { name: "FileSource", type: "Undefined" }, 41729: { name: "SceneType", type: "Undefined" }, 41730: { name: "CFAPattern", type: "Undefined" }, 41985: { name: "CustomRendered", type: "Short" }, 41986: { name: "ExposureMode", type: "Short" }, 41987: { name: "WhiteBalance", type: "Short" }, 41988: { name: "DigitalZoomRatio", type: "Rational" }, 41989: { name: "FocalLengthIn35mmFilm", type: "Short" }, 41990: { name: "SceneCaptureType", type: "Short" }, 41991: { name: "GainControl", type: "Short" }, 41992: { name: "Contrast", type: "Short" }, 41993: { name: "Saturation", type: "Short" }, 41994: { name: "Sharpness", type: "Short" }, 41995: { name: "DeviceSettingDescription", type: "Undefined" }, 41996: { name: "SubjectDistanceRange", type: "Short" }, 42016: { name: "ImageUniqueID", type: "Ascii" }, 42032: { name: "CameraOwnerName", type: "Ascii" }, 42033: { name: "BodySerialNumber", type: "Ascii" }, 42034: { name: "LensSpecification", type: "Rational" }, 42035: { name: "LensMake", type: "Ascii" }, 42036: { name: "LensModel", type: "Ascii" }, 42037: { name: "LensSerialNumber", type: "Ascii" }, 42240: { name: "Gamma", type: "Rational" } }, GPS: { 0: { name: "GPSVersionID", type: "Byte" }, 1: { name: "GPSLatitudeRef", type: "Ascii" }, 2: { name: "GPSLatitude", type: "Rational" }, 3: { name: "GPSLongitudeRef", type: "Ascii" }, 4: { name: "GPSLongitude", type: "Rational" }, 5: { name: "GPSAltitudeRef", type: "Byte" }, 6: { name: "GPSAltitude", type: "Rational" }, 7: { name: "GPSTimeStamp", type: "Rational" }, 8: { name: "GPSSatellites", type: "Ascii" }, 9: { name: "GPSStatus", type: "Ascii" }, 10: { name: "GPSMeasureMode", type: "Ascii" }, 11: { name: "GPSDOP", type: "Rational" }, 12: { name: "GPSSpeedRef", type: "Ascii" }, 13: { name: "GPSSpeed", type: "Rational" }, 14: { name: "GPSTrackRef", type: "Ascii" }, 15: { name: "GPSTrack", type: "Rational" }, 16: { name: "GPSImgDirectionRef", type: "Ascii" }, 17: { name: "GPSImgDirection", type: "Rational" }, 18: { name: "GPSMapDatum", type: "Ascii" }, 19: { name: "GPSDestLatitudeRef", type: "Ascii" }, 20: { name: "GPSDestLatitude", type: "Rational" }, 21: { name: "GPSDestLongitudeRef", type: "Ascii" }, 22: { name: "GPSDestLongitude", type: "Rational" }, 23: { name: "GPSDestBearingRef", type: "Ascii" }, 24: { name: "GPSDestBearing", type: "Rational" }, 25: { name: "GPSDestDistanceRef", type: "Ascii" }, 26: { name: "GPSDestDistance", type: "Rational" }, 27: { name: "GPSProcessingMethod", type: "Undefined" }, 28: { name: "GPSAreaInformation", type: "Undefined" }, 29: { name: "GPSDateStamp", type: "Ascii" }, 30: { name: "GPSDifferential", type: "Short" }, 31: { name: "GPSHPositioningError", type: "Rational" } }, Interop: { 1: { name: "InteroperabilityIndex", type: "Ascii" } } };u["0th"] = u.Image, u["1st"] = u.Image, f.TAGS = u, f.ImageIFD = { ProcessingSoftware: 11, NewSubfileType: 254, SubfileType: 255, ImageWidth: 256, ImageLength: 257, BitsPerSample: 258, Compression: 259, PhotometricInterpretation: 262, Threshholding: 263, CellWidth: 264, CellLength: 265, FillOrder: 266, DocumentName: 269, ImageDescription: 270, Make: 271, Model: 272, StripOffsets: 273, Orientation: 274, SamplesPerPixel: 277, RowsPerStrip: 278, StripByteCounts: 279, XResolution: 282, YResolution: 283, PlanarConfiguration: 284, GrayResponseUnit: 290, GrayResponseCurve: 291, T4Options: 292, T6Options: 293, ResolutionUnit: 296, TransferFunction: 301, Software: 305, DateTime: 306, Artist: 315, HostComputer: 316, Predictor: 317, WhitePoint: 318, PrimaryChromaticities: 319, ColorMap: 320, HalftoneHints: 321, TileWidth: 322, TileLength: 323, TileOffsets: 324, TileByteCounts: 325, SubIFDs: 330, InkSet: 332, InkNames: 333, NumberOfInks: 334, DotRange: 336, TargetPrinter: 337, ExtraSamples: 338, SampleFormat: 339, SMinSampleValue: 340, SMaxSampleValue: 341, TransferRange: 342, ClipPath: 343, XClipPathUnits: 344, YClipPathUnits: 345, Indexed: 346, JPEGTables: 347, OPIProxy: 351, JPEGProc: 512, JPEGInterchangeFormat: 513, JPEGInterchangeFormatLength: 514, JPEGRestartInterval: 515, JPEGLosslessPredictors: 517, JPEGPointTransforms: 518, JPEGQTables: 519, JPEGDCTables: 520, JPEGACTables: 521, YCbCrCoefficients: 529, YCbCrSubSampling: 530, YCbCrPositioning: 531, ReferenceBlackWhite: 532, XMLPacket: 700, Rating: 18246, RatingPercent: 18249, ImageID: 32781, CFARepeatPatternDim: 33421, CFAPattern: 33422, BatteryLevel: 33423, Copyright: 33432, ExposureTime: 33434, ImageResources: 34377, ExifTag: 34665, InterColorProfile: 34675, GPSTag: 34853, Interlace: 34857, TimeZoneOffset: 34858, SelfTimerMode: 34859, FlashEnergy: 37387, SpatialFrequencyResponse: 37388, Noise: 37389, FocalPlaneXResolution: 37390, FocalPlaneYResolution: 37391, FocalPlaneResolutionUnit: 37392, ImageNumber: 37393, SecurityClassification: 37394, ImageHistory: 37395, ExposureIndex: 37397, TIFFEPStandardID: 37398, SensingMethod: 37399, XPTitle: 40091, XPComment: 40092, XPAuthor: 40093, XPKeywords: 40094, XPSubject: 40095, PrintImageMatching: 50341, DNGVersion: 50706, DNGBackwardVersion: 50707, UniqueCameraModel: 50708, LocalizedCameraModel: 50709, CFAPlaneColor: 50710, CFALayout: 50711, LinearizationTable: 50712, BlackLevelRepeatDim: 50713, BlackLevel: 50714, BlackLevelDeltaH: 50715, BlackLevelDeltaV: 50716, WhiteLevel: 50717, DefaultScale: 50718, DefaultCropOrigin: 50719, DefaultCropSize: 50720, ColorMatrix1: 50721, ColorMatrix2: 50722, CameraCalibration1: 50723, CameraCalibration2: 50724, ReductionMatrix1: 50725, ReductionMatrix2: 50726, AnalogBalance: 50727, AsShotNeutral: 50728, AsShotWhiteXY: 50729, BaselineExposure: 50730, BaselineNoise: 50731, BaselineSharpness: 50732, BayerGreenSplit: 50733, LinearResponseLimit: 50734, CameraSerialNumber: 50735, LensInfo: 50736, ChromaBlurRadius: 50737, AntiAliasStrength: 50738, ShadowScale: 50739, DNGPrivateData: 50740, MakerNoteSafety: 50741, CalibrationIlluminant1: 50778, CalibrationIlluminant2: 50779, BestQualityScale: 50780, RawDataUniqueID: 50781, OriginalRawFileName: 50827, OriginalRawFileData: 50828, ActiveArea: 50829, MaskedAreas: 50830, AsShotICCProfile: 50831, AsShotPreProfileMatrix: 50832, CurrentICCProfile: 50833, CurrentPreProfileMatrix: 50834, ColorimetricReference: 50879, CameraCalibrationSignature: 50931, ProfileCalibrationSignature: 50932, AsShotProfileName: 50934, NoiseReductionApplied: 50935, ProfileName: 50936, ProfileHueSatMapDims: 50937, ProfileHueSatMapData1: 50938, ProfileHueSatMapData2: 50939, ProfileToneCurve: 50940, ProfileEmbedPolicy: 50941, ProfileCopyright: 50942, ForwardMatrix1: 50964, ForwardMatrix2: 50965, PreviewApplicationName: 50966, PreviewApplicationVersion: 50967, PreviewSettingsName: 50968, PreviewSettingsDigest: 50969, PreviewColorSpace: 50970, PreviewDateTime: 50971, RawImageDigest: 50972, OriginalRawFileDigest: 50973, SubTileBlockSize: 50974, RowInterleaveFactor: 50975, ProfileLookTableDims: 50981, ProfileLookTableData: 50982, OpcodeList1: 51008, OpcodeList2: 51009, OpcodeList3: 51022, NoiseProfile: 51041 }, f.ExifIFD = { ExposureTime: 33434, FNumber: 33437, ExposureProgram: 34850, SpectralSensitivity: 34852, ISOSpeedRatings: 34855, OECF: 34856, SensitivityType: 34864, StandardOutputSensitivity: 34865, RecommendedExposureIndex: 34866, ISOSpeed: 34867, ISOSpeedLatitudeyyy: 34868, ISOSpeedLatitudezzz: 34869, ExifVersion: 36864, DateTimeOriginal: 36867, DateTimeDigitized: 36868, ComponentsConfiguration: 37121, CompressedBitsPerPixel: 37122, ShutterSpeedValue: 37377, ApertureValue: 37378, BrightnessValue: 37379, ExposureBiasValue: 37380, MaxApertureValue: 37381, SubjectDistance: 37382, MeteringMode: 37383, LightSource: 37384, Flash: 37385, FocalLength: 37386, SubjectArea: 37396, MakerNote: 37500, UserComment: 37510, SubSecTime: 37520, SubSecTimeOriginal: 37521, SubSecTimeDigitized: 37522, FlashpixVersion: 40960, ColorSpace: 40961, PixelXDimension: 40962, PixelYDimension: 40963, RelatedSoundFile: 40964, InteroperabilityTag: 40965, FlashEnergy: 41483, SpatialFrequencyResponse: 41484, FocalPlaneXResolution: 41486, FocalPlaneYResolution: 41487, FocalPlaneResolutionUnit: 41488, SubjectLocation: 41492, ExposureIndex: 41493, SensingMethod: 41495, FileSource: 41728, SceneType: 41729, CFAPattern: 41730, CustomRendered: 41985, ExposureMode: 41986, WhiteBalance: 41987, DigitalZoomRatio: 41988, FocalLengthIn35mmFilm: 41989, SceneCaptureType: 41990, GainControl: 41991, Contrast: 41992, Saturation: 41993, Sharpness: 41994, DeviceSettingDescription: 41995, SubjectDistanceRange: 41996, ImageUniqueID: 42016, CameraOwnerName: 42032, BodySerialNumber: 42033, LensSpecification: 42034, LensMake: 42035, LensModel: 42036, LensSerialNumber: 42037, Gamma: 42240 }, f.GPSIFD = { GPSVersionID: 0, GPSLatitudeRef: 1, GPSLatitude: 2, GPSLongitudeRef: 3, GPSLongitude: 4, GPSAltitudeRef: 5, GPSAltitude: 6, GPSTimeStamp: 7, GPSSatellites: 8, GPSStatus: 9, GPSMeasureMode: 10, GPSDOP: 11, GPSSpeedRef: 12, GPSSpeed: 13, GPSTrackRef: 14, GPSTrack: 15, GPSImgDirectionRef: 16, GPSImgDirection: 17, GPSMapDatum: 18, GPSDestLatitudeRef: 19, GPSDestLatitude: 20, GPSDestLongitudeRef: 21, GPSDestLongitude: 22, GPSDestBearingRef: 23, GPSDestBearing: 24, GPSDestDistanceRef: 25, GPSDestDistance: 26, GPSProcessingMethod: 27, GPSAreaInformation: 28, GPSDateStamp: 29, GPSDifferential: 30, GPSHPositioningError: 31 }, f.InteropIFD = { InteroperabilityIndex: 1 }, f.GPSHelper = { degToDmsRational: function degToDmsRational(e) {
            var t = e % 1 * 60,
                a = t % 1 * 60,
                i = Math.floor(e),
                n = Math.floor(t),
                r = Math.round(100 * a);return [[i, 1], [n, 1], [r, 100]];
        } }, "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = f), exports.piexif = f) : window.piexif = f;
}();

!function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(t) : "undefined" != typeof module && void 0 !== module.exports ? module.exports = t() : window.KvSortable = t();
}(function () {
    "use strict";
    function t(e, n) {
        if (!e || !e.nodeType || 1 !== e.nodeType) throw "KvSortable: `el` must be HTMLElement, and not " + {}.toString.call(e);this.el = e, this.options = n = g({}, n), e[U] = this;var i = { group: Math.random(), sort: !0, disabled: !1, store: null, handle: null, scroll: !0, scrollSensitivity: 30, scrollSpeed: 10, draggable: /[uo]l/i.test(e.nodeName) ? "li" : ">*", ghostClass: "kvsortable-ghost", chosenClass: "kvsortable-chosen", dragClass: "kvsortable-drag", ignore: "a, img", filter: null, preventOnFilter: !0, animation: 0, setData: function setData(t, e) {
                t.setData("Text", e.textContent);
            }, dropBubble: !1, dragoverBubble: !1, dataIdAttr: "data-id", delay: 0, forceFallback: !1, fallbackClass: "kvsortable-fallback", fallbackOnBody: !1, fallbackTolerance: 0, fallbackOffset: { x: 0, y: 0 }, supportPointer: !1 !== t.supportPointer };for (var r in i) {
            !(r in n) && (n[r] = i[r]);
        }rt(n);for (var a in this) {
            "_" === a.charAt(0) && "function" == typeof this[a] && (this[a] = this[a].bind(this));
        }this.nativeDraggable = !n.forceFallback && Z, o(e, "mousedown", this._onTapStart), o(e, "touchstart", this._onTapStart), n.supportPointer && o(e, "pointerdown", this._onTapStart), this.nativeDraggable && (o(e, "dragover", this), o(e, "dragenter", this)), ot.push(this._onDragOver), n.store && this.sort(n.store.get(this));
    }function e(t, e) {
        "clone" !== t.lastPullMode && (e = !0), w && w.state !== e && (a(w, "display", e ? "none" : ""), e || w.state && (t.options.group.revertClone ? (T.insertBefore(w, C), t._animate(_, w)) : T.insertBefore(w, _)), w.state = e);
    }function n(t, e, n) {
        if (t) {
            n = n || W;do {
                if (">*" === e && t.parentNode === n || f(t, e)) return t;
            } while (t = function (t) {
                var e = t.host;return e && e.nodeType ? e : t.parentNode;
            }(t));
        }return null;
    }function o(t, e, n) {
        t.addEventListener(e, n, G);
    }function i(t, e, n) {
        t.removeEventListener(e, n, G);
    }function r(t, e, n) {
        if (t) if (t.classList) t.classList[n ? "add" : "remove"](e);else {
            var o = (" " + t.className + " ").replace(L, " ").replace(" " + e + " ", " ");t.className = (o + (n ? " " + e : "")).replace(L, " ");
        }
    }function a(t, e, n) {
        var o = t && t.style;if (o) {
            if (void 0 === n) return W.defaultView && W.defaultView.getComputedStyle ? n = W.defaultView.getComputedStyle(t, "") : t.currentStyle && (n = t.currentStyle), void 0 === e ? n : n[e];e in o || (e = "-webkit-" + e), o[e] = n + ("string" == typeof n ? "" : "px");
        }
    }function s(t, e, n) {
        if (t) {
            var o = t.getElementsByTagName(e),
                i = 0,
                r = o.length;if (n) for (; i < r; i++) {
                n(o[i], i);
            }return o;
        }return [];
    }function l(t, e, n, o, i, r, a, s) {
        t = t || e[U];var l = W.createEvent("Event"),
            c = t.options,
            d = "on" + n.charAt(0).toUpperCase() + n.substr(1);l.initEvent(n, !0, !0), l.to = i || e, l.from = r || e, l.item = o || e, l.clone = w, l.oldIndex = a, l.newIndex = s, e.dispatchEvent(l), c[d] && c[d].call(t, l);
    }function c(t, e, n, o, i, r, a, s) {
        var l,
            c,
            d = t[U],
            h = d.options.onMove;return (l = W.createEvent("Event")).initEvent("move", !0, !0), l.to = e, l.from = t, l.dragged = n, l.draggedRect = o, l.related = i || e, l.relatedRect = r || e.getBoundingClientRect(), l.willInsertAfter = s, t.dispatchEvent(l), h && (c = h.call(d, l, a)), c;
    }function d(t) {
        t.draggable = !1;
    }function h() {
        $ = !1;
    }function u(t, e) {
        var n = 0;if (!t || !t.parentNode) return -1;for (; t && (t = t.previousElementSibling);) {
            "TEMPLATE" === t.nodeName.toUpperCase() || ">*" !== e && !f(t, e) || n++;
        }return n;
    }function f(t, e) {
        if (t) {
            var n = (e = e.split(".")).shift().toUpperCase(),
                o = new RegExp("\\s(" + e.join("|") + ")(?=\\s)", "g");return !("" !== n && t.nodeName.toUpperCase() != n || e.length && ((" " + t.className + " ").match(o) || []).length != e.length);
        }return !1;
    }function p(t, e) {
        var n, o;return function () {
            void 0 === n && (n = arguments, o = this, q(function () {
                1 === n.length ? t.call(o, n[0]) : t.apply(o, n), n = void 0;
            }, e));
        };
    }function g(t, e) {
        if (t && e) for (var n in e) {
            e.hasOwnProperty(n) && (t[n] = e[n]);
        }return t;
    }function v(t) {
        return z && z.dom ? z.dom(t).cloneNode(!0) : V ? V(t).clone(!0)[0] : t.cloneNode(!0);
    }function m(t) {
        return q(t, 0);
    }function b(t) {
        return clearTimeout(t);
    }if ("undefined" == typeof window || !window.document) return function () {
        throw new Error("KvSortable.js requires a window with a document");
    };var _,
        D,
        y,
        w,
        T,
        C,
        S,
        E,
        k,
        x,
        N,
        B,
        P,
        Y,
        O,
        X,
        I,
        R,
        A,
        M,
        j = {},
        L = /\s+/g,
        F = /left|right|inline/,
        U = "KvSortable" + new Date().getTime(),
        H = window,
        W = H.document,
        K = H.parseInt,
        q = H.setTimeout,
        V = H.jQuery || H.Zepto,
        z = H.Polymer,
        G = !1,
        Q = !1,
        Z = "draggable" in W.createElement("div"),
        J = function (t) {
        return !navigator.userAgent.match(/(?:Trident.*rv[ :]?11\.|msie)/i) && (t = W.createElement("x"), t.style.cssText = "pointer-events:auto", "auto" === t.style.pointerEvents);
    }(),
        $ = !1,
        tt = Math.abs,
        et = Math.min,
        nt = [],
        ot = [],
        it = p(function (t, e, n) {
        if (n && e.scroll) {
            var o,
                i,
                r,
                a,
                s,
                l,
                c = n[U],
                d = e.scrollSensitivity,
                h = e.scrollSpeed,
                u = t.clientX,
                f = t.clientY,
                p = window.innerWidth,
                g = window.innerHeight;if (k !== n && (E = e.scroll, k = n, x = e.scrollFn, !0 === E)) {
                E = n;do {
                    if (E.offsetWidth < E.scrollWidth || E.offsetHeight < E.scrollHeight) break;
                } while (E = E.parentNode);
            }E && (o = E, i = E.getBoundingClientRect(), r = (tt(i.right - u) <= d) - (tt(i.left - u) <= d), a = (tt(i.bottom - f) <= d) - (tt(i.top - f) <= d)), r || a || (a = (g - f <= d) - (f <= d), ((r = (p - u <= d) - (u <= d)) || a) && (o = H)), j.vx === r && j.vy === a && j.el === o || (j.el = o, j.vx = r, j.vy = a, clearInterval(j.pid), o && (j.pid = setInterval(function () {
                if (l = a ? a * h : 0, s = r ? r * h : 0, "function" == typeof x) return x.call(c, s, l, t);o === H ? H.scrollTo(H.pageXOffset + s, H.pageYOffset + l) : (o.scrollTop += l, o.scrollLeft += s);
            }, 24)));
        }
    }, 30),
        rt = function rt(t) {
        function e(t, e) {
            return void 0 !== t && !0 !== t || (t = n.name), "function" == typeof t ? t : function (n, o) {
                var i = o.options.group.name;return e ? t : t && (t.join ? t.indexOf(i) > -1 : i == t);
            };
        }var n = {},
            o = t.group;o && "object" == (typeof o === "undefined" ? "undefined" : _typeof(o)) || (o = { name: o }), n.name = o.name, n.checkPull = e(o.pull, !0), n.checkPut = e(o.put), n.revertClone = o.revertClone, t.group = n;
    };try {
        window.addEventListener("test", null, Object.defineProperty({}, "passive", { get: function get() {
                G = { capture: !1, passive: Q = !1 };
            } }));
    } catch (t) {}return t.prototype = { constructor: t, _onTapStart: function _onTapStart(t) {
            var e,
                o = this,
                i = this.el,
                r = this.options,
                a = r.preventOnFilter,
                s = t.type,
                c = t.touches && t.touches[0],
                d = (c || t).target,
                h = t.target.shadowRoot && t.path && t.path[0] || d,
                f = r.filter;if (function (t) {
                for (var e = t.getElementsByTagName("input"), n = e.length; n--;) {
                    var o = e[n];o.checked && nt.push(o);
                }
            }(i), !_ && !(/mousedown|pointerdown/.test(s) && 0 !== t.button || r.disabled) && !h.isContentEditable && (d = n(d, r.draggable, i)) && S !== d) {
                if (e = u(d, r.draggable), "function" == typeof f) {
                    if (f.call(this, t, d, this)) return l(o, h, "filter", d, i, i, e), void (a && t.preventDefault());
                } else if (f && (f = f.split(",").some(function (t) {
                    if (t = n(h, t.trim(), i)) return l(o, t, "filter", d, i, i, e), !0;
                }))) return void (a && t.preventDefault());r.handle && !n(h, r.handle, i) || this._prepareDragStart(t, c, d, e);
            }
        }, _prepareDragStart: function _prepareDragStart(t, e, n, i) {
            var a,
                c = this,
                h = c.el,
                u = c.options,
                f = h.ownerDocument;n && !_ && n.parentNode === h && (R = t, T = h, D = (_ = n).parentNode, C = _.nextSibling, S = n, X = u.group, Y = i, this._lastX = (e || t).clientX, this._lastY = (e || t).clientY, _.style["will-change"] = "all", a = function a() {
                c._disableDelayedDrag(), _.draggable = c.nativeDraggable, r(_, u.chosenClass, !0), c._triggerDragStart(t, e), l(c, T, "choose", _, T, T, Y);
            }, u.ignore.split(",").forEach(function (t) {
                s(_, t.trim(), d);
            }), o(f, "mouseup", c._onDrop), o(f, "touchend", c._onDrop), o(f, "touchcancel", c._onDrop), o(f, "selectstart", c), u.supportPointer && o(f, "pointercancel", c._onDrop), u.delay ? (o(f, "mouseup", c._disableDelayedDrag), o(f, "touchend", c._disableDelayedDrag), o(f, "touchcancel", c._disableDelayedDrag), o(f, "mousemove", c._disableDelayedDrag), o(f, "touchmove", c._disableDelayedDrag), u.supportPointer && o(f, "pointermove", c._disableDelayedDrag), c._dragStartTimer = q(a, u.delay)) : a());
        }, _disableDelayedDrag: function _disableDelayedDrag() {
            var t = this.el.ownerDocument;clearTimeout(this._dragStartTimer), i(t, "mouseup", this._disableDelayedDrag), i(t, "touchend", this._disableDelayedDrag), i(t, "touchcancel", this._disableDelayedDrag), i(t, "mousemove", this._disableDelayedDrag), i(t, "touchmove", this._disableDelayedDrag), i(t, "pointermove", this._disableDelayedDrag);
        }, _triggerDragStart: function _triggerDragStart(t, e) {
            (e = e || ("touch" == t.pointerType ? t : null)) ? (R = { target: _, clientX: e.clientX, clientY: e.clientY }, this._onDragStart(R, "touch")) : this.nativeDraggable ? (o(_, "dragend", this), o(T, "dragstart", this._onDragStart)) : this._onDragStart(R, !0);try {
                W.selection ? m(function () {
                    W.selection.empty();
                }) : window.getSelection().removeAllRanges();
            } catch (t) {}
        }, _dragStarted: function _dragStarted() {
            if (T && _) {
                var e = this.options;r(_, e.ghostClass, !0), r(_, e.dragClass, !1), t.active = this, l(this, T, "start", _, T, T, Y);
            } else this._nulling();
        }, _emulateDragOver: function _emulateDragOver() {
            if (A) {
                if (this._lastX === A.clientX && this._lastY === A.clientY) return;this._lastX = A.clientX, this._lastY = A.clientY, J || a(y, "display", "none");var t = W.elementFromPoint(A.clientX, A.clientY),
                    e = t,
                    n = ot.length;if (t && t.shadowRoot && (e = t = t.shadowRoot.elementFromPoint(A.clientX, A.clientY)), e) do {
                    if (e[U]) {
                        for (; n--;) {
                            ot[n]({ clientX: A.clientX, clientY: A.clientY, target: t, rootEl: e });
                        }break;
                    }t = e;
                } while (e = e.parentNode);J || a(y, "display", "");
            }
        }, _onTouchMove: function _onTouchMove(e) {
            if (R) {
                var n = this.options,
                    o = n.fallbackTolerance,
                    i = n.fallbackOffset,
                    r = e.touches ? e.touches[0] : e,
                    s = r.clientX - R.clientX + i.x,
                    l = r.clientY - R.clientY + i.y,
                    c = e.touches ? "translate3d(" + s + "px," + l + "px,0)" : "translate(" + s + "px," + l + "px)";if (!t.active) {
                    if (o && et(tt(r.clientX - this._lastX), tt(r.clientY - this._lastY)) < o) return;this._dragStarted();
                }this._appendGhost(), M = !0, A = r, a(y, "webkitTransform", c), a(y, "mozTransform", c), a(y, "msTransform", c), a(y, "transform", c), e.preventDefault();
            }
        }, _appendGhost: function _appendGhost() {
            if (!y) {
                var t,
                    e = _.getBoundingClientRect(),
                    n = a(_),
                    o = this.options;r(y = _.cloneNode(!0), o.ghostClass, !1), r(y, o.fallbackClass, !0), r(y, o.dragClass, !0), a(y, "top", e.top - K(n.marginTop, 10)), a(y, "left", e.left - K(n.marginLeft, 10)), a(y, "width", e.width), a(y, "height", e.height), a(y, "opacity", "0.8"), a(y, "position", "fixed"), a(y, "zIndex", "100000"), a(y, "pointerEvents", "none"), o.fallbackOnBody && W.body.appendChild(y) || T.appendChild(y), t = y.getBoundingClientRect(), a(y, "width", 2 * e.width - t.width), a(y, "height", 2 * e.height - t.height);
            }
        }, _onDragStart: function _onDragStart(t, e) {
            var n = this,
                i = t.dataTransfer,
                s = n.options;n._offUpEvents(), X.checkPull(n, n, _, t) && ((w = v(_)).draggable = !1, w.style["will-change"] = "", a(w, "display", "none"), r(w, n.options.chosenClass, !1), n._cloneId = m(function () {
                T.insertBefore(w, _), l(n, T, "clone", _);
            })), r(_, s.dragClass, !0), e ? ("touch" === e ? (o(W, "touchmove", n._onTouchMove), o(W, "touchend", n._onDrop), o(W, "touchcancel", n._onDrop), s.supportPointer && (o(W, "pointermove", n._onTouchMove), o(W, "pointerup", n._onDrop))) : (o(W, "mousemove", n._onTouchMove), o(W, "mouseup", n._onDrop)), n._loopId = setInterval(n._emulateDragOver, 50)) : (i && (i.effectAllowed = "move", s.setData && s.setData.call(n, i, _)), o(W, "drop", n), n._dragStartId = m(n._dragStarted));
        }, _onDragOver: function _onDragOver(o) {
            var i,
                r,
                s,
                l,
                d = this.el,
                u = this.options,
                f = u.group,
                p = t.active,
                g = X === f,
                v = !1,
                m = u.sort;if (void 0 !== o.preventDefault && (o.preventDefault(), !u.dragoverBubble && o.stopPropagation()), !_.animated && (M = !0, p && !u.disabled && (g ? m || (l = !T.contains(_)) : I === this || (p.lastPullMode = X.checkPull(this, p, _, o)) && f.checkPut(this, p, _, o)) && (void 0 === o.rootEl || o.rootEl === this.el))) {
                if (it(o, u, this.el), $) return;if (i = n(o.target, u.draggable, d), r = _.getBoundingClientRect(), I !== this && (I = this, v = !0), l) return e(p, !0), D = T, void (w || C ? T.insertBefore(_, w || C) : m || T.appendChild(_));if (0 === d.children.length || d.children[0] === y || d === o.target && function (t, e) {
                    var n = t.lastElementChild.getBoundingClientRect();return e.clientY - (n.top + n.height) > 5 || e.clientX - (n.left + n.width) > 5;
                }(d, o)) {
                    if (0 !== d.children.length && d.children[0] !== y && d === o.target && (i = d.lastElementChild), i) {
                        if (i.animated) return;s = i.getBoundingClientRect();
                    }e(p, g), !1 !== c(T, d, _, r, i, s, o) && (_.contains(d) || (d.appendChild(_), D = d), this._animate(r, _), i && this._animate(s, i));
                } else if (i && !i.animated && i !== _ && void 0 !== i.parentNode[U]) {
                    N !== i && (N = i, B = a(i), P = a(i.parentNode));var b = (s = i.getBoundingClientRect()).right - s.left,
                        S = s.bottom - s.top,
                        E = F.test(B.cssFloat + B.display) || "flex" == P.display && 0 === P["flex-direction"].indexOf("row"),
                        k = i.offsetWidth > _.offsetWidth,
                        x = i.offsetHeight > _.offsetHeight,
                        Y = (E ? (o.clientX - s.left) / b : (o.clientY - s.top) / S) > .5,
                        O = i.nextElementSibling,
                        R = !1;if (E) {
                        var A = _.offsetTop,
                            j = i.offsetTop;R = A === j ? i.previousElementSibling === _ && !k || Y && k : i.previousElementSibling === _ || _.previousElementSibling === i ? (o.clientY - s.top) / S > .5 : j > A;
                    } else v || (R = O !== _ && !x || Y && x);var L = c(T, d, _, r, i, s, o, R);!1 !== L && (1 !== L && -1 !== L || (R = 1 === L), $ = !0, q(h, 30), e(p, g), _.contains(d) || (R && !O ? d.appendChild(_) : i.parentNode.insertBefore(_, R ? O : i)), D = _.parentNode, this._animate(r, _), this._animate(s, i));
                }
            }
        }, _animate: function _animate(t, e) {
            var n = this.options.animation;if (n) {
                var o = e.getBoundingClientRect();1 === t.nodeType && (t = t.getBoundingClientRect()), a(e, "transition", "none"), a(e, "transform", "translate3d(" + (t.left - o.left) + "px," + (t.top - o.top) + "px,0)"), e.offsetWidth, a(e, "transition", "all " + n + "ms"), a(e, "transform", "translate3d(0,0,0)"), clearTimeout(e.animated), e.animated = q(function () {
                    a(e, "transition", ""), a(e, "transform", ""), e.animated = !1;
                }, n);
            }
        }, _offUpEvents: function _offUpEvents() {
            var t = this.el.ownerDocument;i(W, "touchmove", this._onTouchMove), i(W, "pointermove", this._onTouchMove), i(t, "mouseup", this._onDrop), i(t, "touchend", this._onDrop), i(t, "pointerup", this._onDrop), i(t, "touchcancel", this._onDrop), i(t, "pointercancel", this._onDrop), i(t, "selectstart", this);
        }, _onDrop: function _onDrop(e) {
            var n = this.el,
                o = this.options;clearInterval(this._loopId), clearInterval(j.pid), clearTimeout(this._dragStartTimer), b(this._cloneId), b(this._dragStartId), i(W, "mouseover", this), i(W, "mousemove", this._onTouchMove), this.nativeDraggable && (i(W, "drop", this), i(n, "dragstart", this._onDragStart)), this._offUpEvents(), e && (M && (e.preventDefault(), !o.dropBubble && e.stopPropagation()), y && y.parentNode && y.parentNode.removeChild(y), T !== D && "clone" === t.active.lastPullMode || w && w.parentNode && w.parentNode.removeChild(w), _ && (this.nativeDraggable && i(_, "dragend", this), d(_), _.style["will-change"] = "", r(_, this.options.ghostClass, !1), r(_, this.options.chosenClass, !1), l(this, T, "unchoose", _, D, T, Y), T !== D ? (O = u(_, o.draggable)) >= 0 && (l(null, D, "add", _, D, T, Y, O), l(this, T, "remove", _, D, T, Y, O), l(null, D, "sort", _, D, T, Y, O), l(this, T, "sort", _, D, T, Y, O)) : _.nextSibling !== C && (O = u(_, o.draggable)) >= 0 && (l(this, T, "update", _, D, T, Y, O), l(this, T, "sort", _, D, T, Y, O)), t.active && (null != O && -1 !== O || (O = Y), l(this, T, "end", _, D, T, Y, O), this.save()))), this._nulling();
        }, _nulling: function _nulling() {
            T = _ = D = y = C = w = S = E = k = R = A = M = O = N = B = I = X = t.active = null, nt.forEach(function (t) {
                t.checked = !0;
            }), nt.length = 0;
        }, handleEvent: function handleEvent(t) {
            switch (t.type) {case "drop":case "dragend":
                    this._onDrop(t);break;case "dragover":case "dragenter":
                    _ && (this._onDragOver(t), function (t) {
                        t.dataTransfer && (t.dataTransfer.dropEffect = "move"), t.preventDefault();
                    }(t));break;case "mouseover":
                    this._onDrop(t);break;case "selectstart":
                    t.preventDefault();}
        }, toArray: function toArray() {
            for (var t, e = [], o = this.el.children, i = 0, r = o.length, a = this.options; i < r; i++) {
                n(t = o[i], a.draggable, this.el) && e.push(t.getAttribute(a.dataIdAttr) || function (t) {
                    for (var e = t.tagName + t.className + t.src + t.href + t.textContent, n = e.length, o = 0; n--;) {
                        o += e.charCodeAt(n);
                    }return o.toString(36);
                }(t));
            }return e;
        }, sort: function sort(t) {
            var e = {},
                o = this.el;this.toArray().forEach(function (t, i) {
                var r = o.children[i];n(r, this.options.draggable, o) && (e[t] = r);
            }, this), t.forEach(function (t) {
                e[t] && (o.removeChild(e[t]), o.appendChild(e[t]));
            });
        }, save: function save() {
            var t = this.options.store;t && t.set(this);
        }, closest: function closest(t, e) {
            return n(t, e || this.options.draggable, this.el);
        }, option: function option(t, e) {
            var n = this.options;if (void 0 === e) return n[t];n[t] = e, "group" === t && rt(n);
        }, destroy: function destroy() {
            var t = this.el;t[U] = null, i(t, "mousedown", this._onTapStart), i(t, "touchstart", this._onTapStart), i(t, "pointerdown", this._onTapStart), this.nativeDraggable && (i(t, "dragover", this), i(t, "dragenter", this)), Array.prototype.forEach.call(t.querySelectorAll("[draggable]"), function (t) {
                t.removeAttribute("draggable");
            }), ot.splice(ot.indexOf(this._onDragOver), 1), this._onDrop(), this.el = t = null;
        } }, o(W, "touchmove", function (e) {
        t.active && e.preventDefault();
    }), t.utils = { on: o, off: i, css: a, find: s, is: function is(t, e) {
            return !!n(t, e, t);
        }, extend: g, throttle: p, closest: n, toggleClass: r, clone: v, index: u, nextTick: m, cancelNextTick: b }, t.create = function (e, n) {
        return new t(e, n);
    }, t.version = "1.7.0", t;
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery);
}(function (t) {
    "use strict";
    t.fn.kvsortable = function (e) {
        var n,
            o = arguments;return this.each(function () {
            var i = t(this),
                r = i.data("kvsortable");r || !(e instanceof Object) && e || (r = new KvSortable(this, e), i.data("kvsortable", r)), r && ("widget" === e ? n = r : "destroy" === e ? (r.destroy(), i.removeData("kvsortable")) : "function" == typeof r[e] ? n = r[e].apply(r, [].slice.call(o, 1)) : e in r.options && (n = r.option.apply(r, o)));
        }), void 0 === n ? this : n;
    };
});

(function (e) {
    "use strict";
    var t = typeof window === "undefined" ? null : window;if (typeof define === "function" && define.amd) {
        define(function () {
            return e(t);
        });
    } else if (typeof module !== "undefined") {
        module.exports = e(t);
    } else {
        t.DOMPurify = e(t);
    }
})(function e(t) {
    "use strict";
    var r = function r(t) {
        return e(t);
    };r.version = "0.7.4";if (!t || !t.document || t.document.nodeType !== 9) {
        r.isSupported = false;return r;
    }var n = t.document;var a = n;var i = t.DocumentFragment;var o = t.HTMLTemplateElement;var l = t.NodeFilter;var s = t.NamedNodeMap || t.MozNamedAttrMap;var f = t.Text;var c = t.Comment;var u = t.DOMParser;if (typeof o === "function") {
        var d = n.createElement("template");if (d.content && d.content.ownerDocument) {
            n = d.content.ownerDocument;
        }
    }var m = n.implementation;var p = n.createNodeIterator;var h = n.getElementsByTagName;var v = n.createDocumentFragment;var g = a.importNode;var y = {};r.isSupported = typeof m.createHTMLDocument !== "undefined" && n.documentMode !== 9;var b = function b(e, t) {
        var r = t.length;while (r--) {
            if (typeof t[r] === "string") {
                t[r] = t[r].toLowerCase();
            }e[t[r]] = true;
        }return e;
    };var T = function T(e) {
        var t = {};var r;for (r in e) {
            if (e.hasOwnProperty(r)) {
                t[r] = e[r];
            }
        }return t;
    };var x = null;var k = b({}, ["a", "abbr", "acronym", "address", "area", "article", "aside", "audio", "b", "bdi", "bdo", "big", "blink", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "content", "data", "datalist", "dd", "decorator", "del", "details", "dfn", "dir", "div", "dl", "dt", "element", "em", "fieldset", "figcaption", "figure", "font", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "i", "img", "input", "ins", "kbd", "label", "legend", "li", "main", "map", "mark", "marquee", "menu", "menuitem", "meter", "nav", "nobr", "ol", "optgroup", "option", "output", "p", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "section", "select", "shadow", "small", "source", "spacer", "span", "strike", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "time", "tr", "track", "tt", "u", "ul", "var", "video", "wbr", "svg", "altglyph", "altglyphdef", "altglyphitem", "animatecolor", "animatemotion", "animatetransform", "circle", "clippath", "defs", "desc", "ellipse", "filter", "font", "g", "glyph", "glyphref", "hkern", "image", "line", "lineargradient", "marker", "mask", "metadata", "mpath", "path", "pattern", "polygon", "polyline", "radialgradient", "rect", "stop", "switch", "symbol", "text", "textpath", "title", "tref", "tspan", "view", "vkern", "feBlend", "feColorMatrix", "feComponentTransfer", "feComposite", "feConvolveMatrix", "feDiffuseLighting", "feDisplacementMap", "feFlood", "feFuncA", "feFuncB", "feFuncG", "feFuncR", "feGaussianBlur", "feMerge", "feMergeNode", "feMorphology", "feOffset", "feSpecularLighting", "feTile", "feTurbulence", "math", "menclose", "merror", "mfenced", "mfrac", "mglyph", "mi", "mlabeledtr", "mmuliscripts", "mn", "mo", "mover", "mpadded", "mphantom", "mroot", "mrow", "ms", "mpspace", "msqrt", "mystyle", "msub", "msup", "msubsup", "mtable", "mtd", "mtext", "mtr", "munder", "munderover", "#text"]);var A = null;var w = b({}, ["accept", "action", "align", "alt", "autocomplete", "background", "bgcolor", "border", "cellpadding", "cellspacing", "checked", "cite", "class", "clear", "color", "cols", "colspan", "coords", "datetime", "default", "dir", "disabled", "download", "enctype", "face", "for", "headers", "height", "hidden", "high", "href", "hreflang", "id", "ismap", "label", "lang", "list", "loop", "low", "max", "maxlength", "media", "method", "min", "multiple", "name", "noshade", "novalidate", "nowrap", "open", "optimum", "pattern", "placeholder", "poster", "preload", "pubdate", "radiogroup", "readonly", "rel", "required", "rev", "reversed", "rows", "rowspan", "spellcheck", "scope", "selected", "shape", "size", "span", "srclang", "start", "src", "step", "style", "summary", "tabindex", "title", "type", "usemap", "valign", "value", "width", "xmlns", "accent-height", "accumulate", "additivive", "alignment-baseline", "ascent", "attributename", "attributetype", "azimuth", "basefrequency", "baseline-shift", "begin", "bias", "by", "clip", "clip-path", "clip-rule", "color", "color-interpolation", "color-interpolation-filters", "color-profile", "color-rendering", "cx", "cy", "d", "dx", "dy", "diffuseconstant", "direction", "display", "divisor", "dur", "edgemode", "elevation", "end", "fill", "fill-opacity", "fill-rule", "filter", "flood-color", "flood-opacity", "font-family", "font-size", "font-size-adjust", "font-stretch", "font-style", "font-variant", "font-weight", "fx", "fy", "g1", "g2", "glyph-name", "glyphref", "gradientunits", "gradienttransform", "image-rendering", "in", "in2", "k", "k1", "k2", "k3", "k4", "kerning", "keypoints", "keysplines", "keytimes", "lengthadjust", "letter-spacing", "kernelmatrix", "kernelunitlength", "lighting-color", "local", "marker-end", "marker-mid", "marker-start", "markerheight", "markerunits", "markerwidth", "maskcontentunits", "maskunits", "max", "mask", "mode", "min", "numoctaves", "offset", "operator", "opacity", "order", "orient", "orientation", "origin", "overflow", "paint-order", "path", "pathlength", "patterncontentunits", "patterntransform", "patternunits", "points", "preservealpha", "r", "rx", "ry", "radius", "refx", "refy", "repeatcount", "repeatdur", "restart", "result", "rotate", "scale", "seed", "shape-rendering", "specularconstant", "specularexponent", "spreadmethod", "stddeviation", "stitchtiles", "stop-color", "stop-opacity", "stroke-dasharray", "stroke-dashoffset", "stroke-linecap", "stroke-linejoin", "stroke-miterlimit", "stroke-opacity", "stroke", "stroke-width", "surfacescale", "targetx", "targety", "transform", "text-anchor", "text-decoration", "text-rendering", "textlength", "u1", "u2", "unicode", "values", "viewbox", "visibility", "vert-adv-y", "vert-origin-x", "vert-origin-y", "word-spacing", "wrap", "writing-mode", "xchannelselector", "ychannelselector", "x", "x1", "x2", "y", "y1", "y2", "z", "zoomandpan", "accent", "accentunder", "bevelled", "close", "columnsalign", "columnlines", "columnspan", "denomalign", "depth", "display", "displaystyle", "fence", "frame", "largeop", "length", "linethickness", "lspace", "lquote", "mathbackground", "mathcolor", "mathsize", "mathvariant", "maxsize", "minsize", "movablelimits", "notation", "numalign", "open", "rowalign", "rowlines", "rowspacing", "rowspan", "rspace", "rquote", "scriptlevel", "scriptminsize", "scriptsizemultiplier", "selection", "separator", "separators", "stretchy", "subscriptshift", "supscriptshift", "symmetric", "voffset", "xlink:href", "xml:id", "xlink:title", "xml:space", "xmlns:xlink"]);var E = null;var S = null;var M = true;var O = false;var L = false;var D = false;var N = /\{\{[\s\S]*|[\s\S]*\}\}/gm;var _ = /<%[\s\S]*|[\s\S]*%>/gm;var C = false;var z = false;var R = false;var F = false;var H = true;var B = true;var W = b({}, ["audio", "head", "math", "script", "style", "svg", "video"]);var j = b({}, ["audio", "video", "img", "source"]);var G = b({}, ["alt", "class", "for", "id", "label", "name", "pattern", "placeholder", "summary", "title", "value", "style", "xmlns"]);var I = null;var q = n.createElement("form");var P = function P(e) {
        if ((typeof e === "undefined" ? "undefined" : _typeof(e)) !== "object") {
            e = {};
        }x = "ALLOWED_TAGS" in e ? b({}, e.ALLOWED_TAGS) : k;A = "ALLOWED_ATTR" in e ? b({}, e.ALLOWED_ATTR) : w;E = "FORBID_TAGS" in e ? b({}, e.FORBID_TAGS) : {};S = "FORBID_ATTR" in e ? b({}, e.FORBID_ATTR) : {};M = e.ALLOW_DATA_ATTR !== false;O = e.ALLOW_UNKNOWN_PROTOCOLS || false;L = e.SAFE_FOR_JQUERY || false;D = e.SAFE_FOR_TEMPLATES || false;C = e.WHOLE_DOCUMENT || false;z = e.RETURN_DOM || false;R = e.RETURN_DOM_FRAGMENT || false;F = e.RETURN_DOM_IMPORT || false;H = e.SANITIZE_DOM !== false;B = e.KEEP_CONTENT !== false;if (D) {
            M = false;
        }if (R) {
            z = true;
        }if (e.ADD_TAGS) {
            if (x === k) {
                x = T(x);
            }b(x, e.ADD_TAGS);
        }if (e.ADD_ATTR) {
            if (A === w) {
                A = T(A);
            }b(A, e.ADD_ATTR);
        }if (B) {
            x["#text"] = true;
        }if (Object && "freeze" in Object) {
            Object.freeze(e);
        }I = e;
    };var U = function U(e) {
        try {
            e.parentNode.removeChild(e);
        } catch (t) {
            e.outerHTML = "";
        }
    };var V = function V(e) {
        var t, r;try {
            t = new u().parseFromString(e, "text/html");
        } catch (n) {}if (!t) {
            t = m.createHTMLDocument("");r = t.body;r.parentNode.removeChild(r.parentNode.firstElementChild);r.outerHTML = e;
        }if (typeof t.getElementsByTagName === "function") {
            return t.getElementsByTagName(C ? "html" : "body")[0];
        }return h.call(t, C ? "html" : "body")[0];
    };var K = function K(e) {
        return p.call(e.ownerDocument || e, e, l.SHOW_ELEMENT | l.SHOW_COMMENT | l.SHOW_TEXT, function () {
            return l.FILTER_ACCEPT;
        }, false);
    };var J = function J(e) {
        if (e instanceof f || e instanceof c) {
            return false;
        }if (typeof e.nodeName !== "string" || typeof e.textContent !== "string" || typeof e.removeChild !== "function" || !(e.attributes instanceof s) || typeof e.removeAttribute !== "function" || typeof e.setAttribute !== "function") {
            return true;
        }return false;
    };var Q = function Q(e) {
        var t, r;re("beforeSanitizeElements", e, null);if (J(e)) {
            U(e);return true;
        }t = e.nodeName.toLowerCase();re("uponSanitizeElement", e, { tagName: t });if (!x[t] || E[t]) {
            if (B && !W[t] && typeof e.insertAdjacentHTML === "function") {
                try {
                    e.insertAdjacentHTML("AfterEnd", e.innerHTML);
                } catch (n) {}
            }U(e);return true;
        }if (L && !e.firstElementChild && (!e.content || !e.content.firstElementChild)) {
            e.innerHTML = e.textContent.replace(/</g, "&lt;");
        }if (D && e.nodeType === 3) {
            r = e.textContent;r = r.replace(N, " ");r = r.replace(_, " ");e.textContent = r;
        }re("afterSanitizeElements", e, null);return false;
    };var X = /^data-[\w.\u00B7-\uFFFF-]/;var Y = /^(?:(?:(?:f|ht)tps?|mailto|tel):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i;var Z = /^(?:\w+script|data):/i;var $ = /[\x00-\x20\xA0\u1680\u180E\u2000-\u2029\u205f\u3000]/g;var ee = function ee(e) {
        var r, a, i, o, l, s, f, c;re("beforeSanitizeAttributes", e, null);s = e.attributes;if (!s) {
            return;
        }f = { attrName: "", attrValue: "", keepAttr: true };c = s.length;while (c--) {
            r = s[c];a = r.name;i = r.value;o = a.toLowerCase();f.attrName = o;f.attrValue = i;f.keepAttr = true;re("uponSanitizeAttribute", e, f);i = f.attrValue;if (o === "name" && e.nodeName === "IMG" && s.id) {
                l = s.id;s = Array.prototype.slice.apply(s);e.removeAttribute("id");e.removeAttribute(a);if (s.indexOf(l) > c) {
                    e.setAttribute("id", l.value);
                }
            } else {
                if (a === "id") {
                    e.setAttribute(a, "");
                }e.removeAttribute(a);
            }if (!f.keepAttr) {
                continue;
            }if (H && (o === "id" || o === "name") && (i in t || i in n || i in q)) {
                continue;
            }if (D) {
                i = i.replace(N, " ");i = i.replace(_, " ");
            }if (A[o] && !S[o] && (G[o] || Y.test(i.replace($, "")) || o === "src" && i.indexOf("data:") === 0 && j[e.nodeName.toLowerCase()]) || M && X.test(o) || O && !Z.test(i.replace($, ""))) {
                try {
                    e.setAttribute(a, i);
                } catch (u) {}
            }
        }re("afterSanitizeAttributes", e, null);
    };var te = function te(e) {
        var t;var r = K(e);re("beforeSanitizeShadowDOM", e, null);while (t = r.nextNode()) {
            re("uponSanitizeShadowNode", t, null);if (Q(t)) {
                continue;
            }if (t.content instanceof i) {
                te(t.content);
            }ee(t);
        }re("afterSanitizeShadowDOM", e, null);
    };var re = function re(e, t, n) {
        if (!y[e]) {
            return;
        }y[e].forEach(function (e) {
            e.call(r, t, n, I);
        });
    };r.sanitize = function (e, n) {
        var o, l, s, f, c;if (!e) {
            e = "";
        }if (typeof e !== "string") {
            if (typeof e.toString !== "function") {
                throw new TypeError("toString is not a function");
            } else {
                e = e.toString();
            }
        }if (!r.isSupported) {
            if (_typeof(t.toStaticHTML) === "object" || typeof t.toStaticHTML === "function") {
                return t.toStaticHTML(e);
            }return e;
        }P(n);if (!z && !C && e.indexOf("<") === -1) {
            return e;
        }o = V(e);if (!o) {
            return z ? null : "";
        }f = K(o);while (l = f.nextNode()) {
            if (l.nodeType === 3 && l === s) {
                continue;
            }if (Q(l)) {
                continue;
            }if (l.content instanceof i) {
                te(l.content);
            }ee(l);s = l;
        }if (z) {
            if (R) {
                c = v.call(o.ownerDocument);while (o.firstChild) {
                    c.appendChild(o.firstChild);
                }
            } else {
                c = o;
            }if (F) {
                c = g.call(a, c, true);
            }return c;
        }return C ? o.outerHTML : o.innerHTML;
    };r.addHook = function (e, t) {
        if (typeof t !== "function") {
            return;
        }y[e] = y[e] || [];y[e].push(t);
    };r.removeHook = function (e) {
        if (y[e]) {
            y[e].pop();
        }
    };r.removeHooks = function (e) {
        if (y[e]) {
            y[e] = [];
        }
    };r.removeAllHooks = function () {
        y = [];
    };return r;
});

/*!
 * bootstrap-fileinput v4.4.8
 * http://plugins.krajee.com/file-input
 *
 * Author: Kartik Visweswaran
 * Copyright: 2014 - 2018, Kartik Visweswaran, Krajee.com
 *
 * Licensed under the BSD 3-Clause
 * https://github.com/kartik-v/bootstrap-fileinput/blob/master/LICENSE.md
 */!function (e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = e(require("jquery")) : e(window.jQuery);
}(function (e) {
    "use strict";
    e.fn.fileinputLocales = {}, e.fn.fileinputThemes = {}, String.prototype.setTokens = function (e) {
        var t,
            i,
            a = this.toString();for (t in e) {
            e.hasOwnProperty(t) && (i = new RegExp("{" + t + "}", "g"), a = a.replace(i, e[t]));
        }return a;
    };var t, i;t = { FRAMES: ".kv-preview-thumb", SORT_CSS: "file-sortable", OBJECT_PARAMS: '<param name="controller" value="true" />\n<param name="allowFullScreen" value="true" />\n<param name="allowScriptAccess" value="always" />\n<param name="autoPlay" value="false" />\n<param name="autoStart" value="false" />\n<param name="quality" value="high" />\n', DEFAULT_PREVIEW: '<div class="file-preview-other">\n<span class="{previewFileIconClass}">{previewFileIcon}</span>\n</div>', MODAL_ID: "kvFileinputModal", MODAL_EVENTS: ["show", "shown", "hide", "hidden", "loaded"], objUrl: window.URL || window.webkitURL, compare: function compare(e, t, i) {
            return void 0 !== e && (i ? e === t : e.match(t));
        }, isIE: function isIE(e) {
            if ("Microsoft Internet Explorer" !== navigator.appName) return !1;if (10 === e) return new RegExp("msie\\s" + e, "i").test(navigator.userAgent);var t,
                i = document.createElement("div");return i.innerHTML = "<!--[if IE " + e + "]> <i></i> <![endif]-->", t = i.getElementsByTagName("i").length, document.body.appendChild(i), i.parentNode.removeChild(i), t;
        }, initModal: function initModal(t) {
            var i = e("body");i.length && t.appendTo(i);
        }, isEmpty: function isEmpty(t, i) {
            return void 0 === t || null === t || 0 === t.length || i && "" === e.trim(t);
        }, isArray: function isArray(e) {
            return Array.isArray(e) || "[object Array]" === Object.prototype.toString.call(e);
        }, ifSet: function ifSet(e, t, i) {
            return i = i || "", t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && e in t ? t[e] : i;
        }, cleanArray: function cleanArray(e) {
            return e instanceof Array || (e = []), e.filter(function (e) {
                return void 0 !== e && null !== e;
            });
        }, spliceArray: function spliceArray(t, i, a) {
            var r,
                n,
                o = 0,
                l = [];if (!(t instanceof Array)) return [];for (n = e.extend(!0, [], t), a && n.reverse(), r = 0; r < n.length; r++) {
                r !== i && (l[o] = n[r], o++);
            }return a && l.reverse(), l;
        }, getNum: function getNum(e, t) {
            return t = t || 0, "number" == typeof e ? e : ("string" == typeof e && (e = parseFloat(e)), isNaN(e) ? t : e);
        }, hasFileAPISupport: function hasFileAPISupport() {
            return !(!window.File || !window.FileReader);
        }, hasDragDropSupport: function hasDragDropSupport() {
            var e = document.createElement("div");return !t.isIE(9) && (void 0 !== e.draggable || void 0 !== e.ondragstart && void 0 !== e.ondrop);
        }, hasFileUploadSupport: function hasFileUploadSupport() {
            return t.hasFileAPISupport() && window.FormData;
        }, hasBlobSupport: function hasBlobSupport() {
            try {
                return !!window.Blob && Boolean(new Blob());
            } catch (e) {
                return !1;
            }
        }, hasArrayBufferViewSupport: function hasArrayBufferViewSupport() {
            try {
                return 100 === new Blob([new Uint8Array(100)]).size;
            } catch (e) {
                return !1;
            }
        }, dataURI2Blob: function dataURI2Blob(e) {
            var i,
                a,
                r,
                n,
                o,
                l,
                s = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder,
                d = t.hasBlobSupport(),
                c = (d || s) && window.atob && window.ArrayBuffer && window.Uint8Array;if (!c) return null;for (i = e.split(",")[0].indexOf("base64") >= 0 ? atob(e.split(",")[1]) : decodeURIComponent(e.split(",")[1]), a = new ArrayBuffer(i.length), r = new Uint8Array(a), n = 0; n < i.length; n += 1) {
                r[n] = i.charCodeAt(n);
            }return o = e.split(",")[0].split(":")[1].split(";")[0], d ? new Blob([t.hasArrayBufferViewSupport() ? r : a], { type: o }) : (l = new s(), l.append(a), l.getBlob(o));
        }, arrayBuffer2String: function arrayBuffer2String(e) {
            if (window.TextDecoder) return new TextDecoder("utf-8").decode(e);var t,
                i,
                a,
                r,
                n = Array.prototype.slice.apply(new Uint8Array(e)),
                o = "",
                l = 0;for (t = n.length; t > l;) {
                switch (i = n[l++], i >> 4) {case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:
                        o += String.fromCharCode(i);break;case 12:case 13:
                        a = n[l++], o += String.fromCharCode((31 & i) << 6 | 63 & a);break;case 14:
                        a = n[l++], r = n[l++], o += String.fromCharCode((15 & i) << 12 | (63 & a) << 6 | (63 & r) << 0);}
            }return o;
        }, isHtml: function isHtml(e) {
            var t = document.createElement("div");t.innerHTML = e;for (var i = t.childNodes, a = i.length; a--;) {
                if (1 === i[a].nodeType) return !0;
            }return !1;
        }, isSvg: function isSvg(e) {
            return e.match(/^\s*<\?xml/i) && (e.match(/<!DOCTYPE svg/i) || e.match(/<svg/i));
        }, getMimeType: function getMimeType(e, t, i) {
            switch (e) {case "ffd8ffe0":case "ffd8ffe1":case "ffd8ffe2":
                    return "image/jpeg";case "89504E47":
                    return "image/png";case "47494638":
                    return "image/gif";case "49492a00":
                    return "image/tiff";case "52494646":
                    return "image/webp";case "66747970":
                    return "video/3gp";case "4f676753":
                    return "video/ogg";case "1a45dfa3":
                    return "video/mkv";case "000001ba":case "000001b3":
                    return "video/mpeg";case "3026b275":
                    return "video/wmv";case "25504446":
                    return "application/pdf";case "25215053":
                    return "application/ps";case "504b0304":case "504b0506":case "504b0508":
                    return "application/zip";case "377abcaf":
                    return "application/7z";case "75737461":
                    return "application/tar";case "7801730d":
                    return "application/dmg";default:
                    switch (e.substring(0, 6)) {case "435753":
                            return "application/x-shockwave-flash";case "494433":
                            return "audio/mp3";case "425a68":
                            return "application/bzip";default:
                            switch (e.substring(0, 4)) {case "424d":
                                    return "image/bmp";case "fffb":
                                    return "audio/mp3";case "4d5a":
                                    return "application/exe";case "1f9d":case "1fa0":
                                    return "application/zip";case "1f8b":
                                    return "application/gzip";default:
                                    return t && !t.match(/[^\u0000-\u007f]/) ? "application/text-plain" : i;}}}
        }, addCss: function addCss(e, t) {
            e.removeClass(t).addClass(t);
        }, getElement: function getElement(i, a, r) {
            return t.isEmpty(i) || t.isEmpty(i[a]) ? r : e(i[a]);
        }, uniqId: function uniqId() {
            return Math.round(new Date().getTime()) + "_" + Math.round(100 * Math.random());
        }, htmlEncode: function htmlEncode(e) {
            return e.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;");
        }, replaceTags: function replaceTags(t, i) {
            var a = t;return i ? (e.each(i, function (e, t) {
                "function" == typeof t && (t = t()), a = a.split(e).join(t);
            }), a) : a;
        }, cleanMemory: function cleanMemory(e) {
            var i = e.is("img") ? e.attr("src") : e.find("source").attr("src");t.objUrl.revokeObjectURL(i);
        }, findFileName: function findFileName(e) {
            var t = e.lastIndexOf("/");return -1 === t && (t = e.lastIndexOf("\\")), e.split(e.substring(t, t + 1)).pop();
        }, checkFullScreen: function checkFullScreen() {
            return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
        }, toggleFullScreen: function toggleFullScreen(e) {
            var i = document,
                a = i.documentElement;a && e && !t.checkFullScreen() ? a.requestFullscreen ? a.requestFullscreen() : a.msRequestFullscreen ? a.msRequestFullscreen() : a.mozRequestFullScreen ? a.mozRequestFullScreen() : a.webkitRequestFullscreen && a.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT) : i.exitFullscreen ? i.exitFullscreen() : i.msExitFullscreen ? i.msExitFullscreen() : i.mozCancelFullScreen ? i.mozCancelFullScreen() : i.webkitExitFullscreen && i.webkitExitFullscreen();
        }, moveArray: function moveArray(t, i, a, r) {
            var n = e.extend(!0, [], t);if (r && n.reverse(), a >= n.length) for (var o = a - n.length; o-- + 1;) {
                n.push(void 0);
            }return n.splice(a, 0, n.splice(i, 1)[0]), r && n.reverse(), n;
        }, cleanZoomCache: function cleanZoomCache(e) {
            var t = e.closest(".kv-zoom-cache-theme");t.length || (t = e.closest(".kv-zoom-cache")), t.remove();
        }, setOrientation: function setOrientation(e, t) {
            var i,
                a,
                r,
                n = new DataView(e),
                o = 0,
                l = 1;if (65496 !== n.getUint16(o) || e.length < 2) return void (t && t());for (o += 2, i = n.byteLength; i - 2 > o;) {
                switch (a = n.getUint16(o), o += 2, a) {case 65505:
                        r = n.getUint16(o), i = r - o, o += 2;break;case 274:
                        l = n.getUint16(o + 6, !1), i = 0;}
            }t && t(l);
        }, validateOrientation: function validateOrientation(e, i) {
            if (window.FileReader && window.DataView) {
                var a,
                    r = new FileReader();r.onloadend = function () {
                    a = r.result, t.setOrientation(a, i);
                }, r.readAsArrayBuffer(e);
            }
        }, adjustOrientedImage: function adjustOrientedImage(e, t) {
            var i, a, r;if (e.hasClass("is-portrait-gt4")) {
                if (t) return void e.css({ width: e.parent().height() });e.css({ height: "auto", width: e.height() }), i = e.parent().offset().top, a = e.offset().top, r = i - a, e.css("margin-top", r);
            }
        }, closeButton: function closeButton(e) {
            return e = e ? "close " + e : "close", '<button type="button" class="' + e + '" aria-label="Close">\n  <span aria-hidden="true">&times;</span>\n</button>';
        } }, i = function i(_i, a) {
        var r = this;r.$element = e(_i), r.$parent = r.$element.parent(), r._validate() && (r.isPreviewable = t.hasFileAPISupport(), r.isIE9 = t.isIE(9), r.isIE10 = t.isIE(10), (r.isPreviewable || r.isIE9) && (r._init(a), r._listen()), r.$element.removeClass("file-loading"));
    }, i.prototype = { constructor: i, _cleanup: function _cleanup() {
            var e = this;e.reader = null, e.formdata = {}, e.uploadCount = 0, e.uploadStatus = {}, e.uploadLog = [], e.uploadAsyncCount = 0, e.loadedImages = [], e.totalImagesCount = 0, e.ajaxRequests = [], e.clearStack(), e.fileInputCleared = !1, e.fileBatchCompleted = !0, e.isPreviewable || (e.showPreview = !1), e.isError = !1, e.ajaxAborted = !1, e.cancelling = !1;
        }, _init: function _init(i, a) {
            var r,
                n,
                o,
                l,
                s = this,
                d = s.$element;s.options = i, e.each(i, function (e, i) {
                switch (e) {case "minFileCount":case "maxFileCount":case "minFileSize":case "maxFileSize":case "maxFilePreviewSize":case "resizeImageQuality":case "resizeIfSizeMoreThan":case "progressUploadThreshold":case "initialPreviewCount":case "zoomModalHeight":case "minImageHeight":case "maxImageHeight":case "minImageWidth":case "maxImageWidth":
                        s[e] = t.getNum(i);break;default:
                        s[e] = i;}
            }), s.rtl && (l = s.previewZoomButtonIcons.prev, s.previewZoomButtonIcons.prev = s.previewZoomButtonIcons.next, s.previewZoomButtonIcons.next = l), a || s._cleanup(), s.$form = d.closest("form"), s._initTemplateDefaults(), s.uploadFileAttr = t.isEmpty(d.attr("name")) ? "file_data" : d.attr("name"), o = s._getLayoutTemplate("progress"), s.progressTemplate = o.replace("{class}", s.progressClass), s.progressCompleteTemplate = o.replace("{class}", s.progressCompleteClass), s.progressErrorTemplate = o.replace("{class}", s.progressErrorClass), s.dropZoneEnabled = t.hasDragDropSupport() && s.dropZoneEnabled, s.isDisabled = d.attr("disabled") || d.attr("readonly"), s.isDisabled && d.attr("disabled", !0), s.isAjaxUpload = t.hasFileUploadSupport() && !t.isEmpty(s.uploadUrl), s.isClickable = s.browseOnZoneClick && s.showPreview && (s.isAjaxUpload && s.dropZoneEnabled || !t.isEmpty(s.defaultPreviewContent)), s.slug = "function" == typeof i.slugCallback ? i.slugCallback : s._slugDefault, s.mainTemplate = s.showCaption ? s._getLayoutTemplate("main1") : s._getLayoutTemplate("main2"), s.captionTemplate = s._getLayoutTemplate("caption"), s.previewGenericTemplate = s._getPreviewTemplate("generic"), !s.imageCanvas && s.resizeImage && (s.maxImageWidth || s.maxImageHeight) && (s.imageCanvas = document.createElement("canvas"), s.imageCanvasContext = s.imageCanvas.getContext("2d")), t.isEmpty(d.attr("id")) && d.attr("id", t.uniqId()), s.namespace = ".fileinput_" + d.attr("id").replace(/-/g, "_"), void 0 === s.$container ? s.$container = s._createContainer() : s._refreshContainer(), n = s.$container, s.$dropZone = n.find(".file-drop-zone"), s.$progress = n.find(".kv-upload-progress"), s.$btnUpload = n.find(".fileinput-upload"), s.$captionContainer = t.getElement(i, "elCaptionContainer", n.find(".file-caption")), s.$caption = t.getElement(i, "elCaptionText", n.find(".file-caption-name")), t.isEmpty(s.msgPlaceholder) || (r = d.attr("multiple") ? s.filePlural : s.fileSingle, s.$caption.attr("placeholder", s.msgPlaceholder.replace("{files}", r))), s.$captionIcon = s.$captionContainer.find(".file-caption-icon"), s.$previewContainer = t.getElement(i, "elPreviewContainer", n.find(".file-preview")), s.$preview = t.getElement(i, "elPreviewImage", n.find(".file-preview-thumbnails")), s.$previewStatus = t.getElement(i, "elPreviewStatus", n.find(".file-preview-status")), s.$errorContainer = t.getElement(i, "elErrorContainer", s.$previewContainer.find(".kv-fileinput-error")), s._validateDisabled(), t.isEmpty(s.msgErrorClass) || t.addCss(s.$errorContainer, s.msgErrorClass), a || (s.$errorContainer.hide(), s.previewInitId = "preview-" + t.uniqId(), s._initPreviewCache(), s._initPreview(!0), s._initPreviewActions(), s._setFileDropZoneTitle(), s.$parent.hasClass("file-loading") && (s.$container.insertBefore(s.$parent), s.$parent.remove())), d.attr("disabled") && s.disable(), s._initZoom(), s.hideThumbnailContent && t.addCss(s.$preview, "hide-content");
        }, _initTemplateDefaults: function _initTemplateDefaults() {
            var i,
                a,
                r,
                n,
                o,
                l,
                s,
                d,
                c,
                p,
                u,
                f,
                m,
                v,
                g,
                h,
                w,
                _,
                b,
                C,
                y,
                x,
                T,
                E,
                S,
                k,
                F,
                P,
                I,
                A,
                z,
                D,
                $,
                j,
                U,
                B,
                O,
                R,
                L,
                M = this;i = '{preview}\n<div class="kv-upload-progress kv-hidden"></div><div class="clearfix"></div>\n<div class="input-group {class}">\n  {caption}\n<div class="input-group-btn input-group-append">\n      {remove}\n      {cancel}\n      {upload}\n      {browse}\n    </div>\n</div>', a = '{preview}\n<div class="kv-upload-progress kv-hidden"></div>\n<div class="clearfix"></div>\n{remove}\n{cancel}\n{upload}\n{browse}\n', r = '<div class="file-preview {class}">\n    {close}    <div class="{dropClass}">\n    <div class="file-preview-thumbnails">\n    </div>\n    <div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div>\n    <div class="kv-fileinput-error"></div>\n    </div>\n</div>', o = t.closeButton("fileinput-remove"), n = '<i class="glyphicon glyphicon-file"></i>', l = '<div class="file-caption form-control {class}" tabindex="500">\n  <span class="file-caption-icon"></span>\n  <input class="file-caption-name" onkeydown="return false;" onpaste="return false;">\n</div>', s = '<button type="{type}" tabindex="500" title="{title}" class="{css}" {status}>{icon} {label}</button>', d = '<a href="{href}" tabindex="500" title="{title}" class="{css}" {status}>{icon} {label}</a>', c = '<div tabindex="500" class="{css}" {status}>{icon} {label}</div>', p = '<div id="' + t.MODAL_ID + '" class="file-zoom-dialog modal fade" tabindex="-1" aria-labelledby="' + t.MODAL_ID + 'Label"></div>', u = '<div class="modal-dialog modal-lg{rtl}" role="document">\n  <div class="modal-content">\n    <div class="modal-header">\n      <h5 class="modal-title">{heading}</h5>\n      <span class="kv-zoom-title"></span>\n      <div class="kv-zoom-actions">{toggleheader}{fullscreen}{borderless}{close}</div>\n    </div>\n    <div class="modal-body">\n      <div class="floating-buttons"></div>\n      <div class="kv-zoom-body file-zoom-content {zoomFrameClass}"></div>\n{prev} {next}\n    </div>\n  </div>\n</div>\n', f = '<div class="progress">\n    <div class="{class}" role="progressbar" aria-valuenow="{percent}" aria-valuemin="0" aria-valuemax="100" style="width:{percent}%;">\n        {status}\n     </div>\n</div>', m = " <samp>({sizeText})</samp>", v = '<div class="file-thumbnail-footer">\n    <div class="file-footer-caption" title="{caption}">\n        <div class="file-caption-info">{caption}</div>\n        <div class="file-size-info">{size}</div>\n    </div>\n    {progress}\n{indicator}\n{actions}\n</div>', g = '<div class="file-actions">\n    <div class="file-footer-buttons">\n        {download} {upload} {delete} {zoom} {other}    </div>\n</div>\n{drag}\n<div class="clearfix"></div>', h = '<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}" {dataUrl}{dataKey}>{removeIcon}</button>\n', w = '<button type="button" class="kv-file-upload {uploadClass}" title="{uploadTitle}">{uploadIcon}</button>', _ = '<a class="kv-file-download {downloadClass}" title="{downloadTitle}" href="{downloadUrl}" download="{caption}" target="_blank">{downloadIcon}</a>', b = '<button type="button" class="kv-file-zoom {zoomClass}" title="{zoomTitle}">{zoomIcon}</button>', C = '<span class="file-drag-handle {dragClass}" title="{dragTitle}">{dragIcon}</span>', y = '<div class="file-upload-indicator" title="{indicatorTitle}">{indicator}</div>', x = '<div class="file-preview-frame {frameClass}" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}"', T = x + '><div class="kv-file-content">\n', E = x + ' title="{caption}"><div class="kv-file-content">\n', S = "</div>{footer}\n</div>\n", k = "{content}\n", O = " {style}", F = '<div class="kv-preview-data file-preview-html" title="{caption}"' + O + ">{data}</div>\n", P = '<img src="{data}" class="file-preview-image kv-preview-data" title="{caption}" alt="{caption}"' + O + ">\n", I = '<textarea class="kv-preview-data file-preview-text" title="{caption}" readonly' + O + ">{data}</textarea>\n", A = '<iframe class="kv-preview-data file-preview-office" src="https://docs.google.com/gview?url={data}&embedded=true"' + O + "></iframe>", z = '<video class="kv-preview-data file-preview-video" controls' + O + '>\n<source src="{data}" type="{type}">\n' + t.DEFAULT_PREVIEW + "\n</video>\n", D = '<!--suppress ALL --><audio class="kv-preview-data file-preview-audio" controls' + O + '>\n<source src="{data}" type="{type}">\n' + t.DEFAULT_PREVIEW + "\n</audio>\n", $ = '<embed class="kv-preview-data file-preview-flash" src="{data}" type="application/x-shockwave-flash"' + O + ">\n", U = '<embed class="kv-preview-data file-preview-pdf" src="{data}" type="application/pdf"' + O + ">\n", j = '<object class="kv-preview-data file-preview-object file-object {typeCss}" data="{data}" type="{type}"' + O + '>\n<param name="movie" value="{caption}" />\n' + t.OBJECT_PARAMS + " " + t.DEFAULT_PREVIEW + "\n</object>\n", B = '<div class="kv-preview-data file-preview-other-frame"' + O + ">\n" + t.DEFAULT_PREVIEW + "\n</div>\n", R = '<div class="kv-zoom-cache" style="display:none">{zoomContent}</div>', L = { width: "100%", height: "100%", "min-height": "480px" }, M.defaults = { layoutTemplates: { main1: i, main2: a, preview: r, close: o, fileIcon: n, caption: l, modalMain: p, modal: u, progress: f, size: m, footer: v, indicator: y, actions: g, actionDelete: h, actionUpload: w, actionDownload: _, actionZoom: b, actionDrag: C, btnDefault: s, btnLink: d, btnBrowse: c, zoomCache: R }, previewMarkupTags: { tagBefore1: T, tagBefore2: E, tagAfter: S }, previewContentTemplates: { generic: k, html: F, image: P, text: I, office: A, video: z, audio: D, flash: $, object: j, pdf: U, other: B }, allowedPreviewTypes: ["image", "html", "text", "video", "audio", "flash", "pdf", "object"], previewTemplates: {}, previewSettings: { image: { width: "auto", height: "auto", "max-width": "100%", "max-height": "100%" }, html: { width: "213px", height: "160px" }, text: { width: "213px", height: "160px" }, office: { width: "213px", height: "160px" }, video: { width: "213px", height: "160px" }, audio: { width: "100%", height: "30px" }, flash: { width: "213px", height: "160px" }, object: { width: "213px", height: "160px" }, pdf: { width: "213px", height: "160px" }, other: { width: "213px", height: "160px" } }, previewSettingsSmall: { image: { width: "auto", height: "auto", "max-width": "100%", "max-height": "100%" }, html: { width: "100%", height: "160px" }, text: { width: "100%", height: "160px" }, office: { width: "100%", height: "160px" }, video: { width: "100%", height: "auto" }, audio: { width: "100%", height: "30px" }, flash: { width: "100%", height: "auto" }, object: { width: "100%", height: "auto" }, pdf: { width: "100%", height: "160px" }, other: { width: "100%", height: "160px" } }, previewZoomSettings: { image: { width: "auto", height: "auto", "max-width": "100%", "max-height": "100%" }, html: L, text: L, office: { width: "100%", height: "100%", "max-width": "100%", "min-height": "480px" }, video: { width: "auto", height: "100%", "max-width": "100%" }, audio: { width: "100%", height: "30px" }, flash: { width: "auto", height: "480px" }, object: { width: "auto", height: "100%", "max-width": "100%", "min-height": "480px" }, pdf: L, other: { width: "auto", height: "100%", "min-height": "480px" } }, fileTypeSettings: { image: function image(e, i) {
                        return t.compare(e, "image.*") && !t.compare(e, /(tiff?|wmf)$/i) || t.compare(i, /\.(gif|png|jpe?g)$/i);
                    }, html: function html(e, i) {
                        return t.compare(e, "text/html") || t.compare(i, /\.(htm|html)$/i);
                    }, office: function office(e, i) {
                        return t.compare(e, /(word|excel|powerpoint|office|iwork-pages|tiff?)$/i) || t.compare(i, /\.(rtf|docx?|xlsx?|pptx?|pps|potx?|ods|odt|pages|ai|dxf|ttf|tiff?|wmf|e?ps)$/i);
                    }, text: function text(e, i) {
                        return t.compare(e, "text.*") || t.compare(i, /\.(xml|javascript)$/i) || t.compare(i, /\.(txt|md|csv|nfo|ini|json|php|js|css)$/i);
                    }, video: function video(e, i) {
                        return t.compare(e, "video.*") && (t.compare(e, /(ogg|mp4|mp?g|mov|webm|3gp)$/i) || t.compare(i, /\.(og?|mp4|webm|mp?g|mov|3gp)$/i));
                    }, audio: function audio(e, i) {
                        return t.compare(e, "audio.*") && (t.compare(i, /(ogg|mp3|mp?g|wav)$/i) || t.compare(i, /\.(og?|mp3|mp?g|wav)$/i));
                    }, flash: function flash(e, i) {
                        return t.compare(e, "application/x-shockwave-flash", !0) || t.compare(i, /\.(swf)$/i);
                    }, pdf: function pdf(e, i) {
                        return t.compare(e, "application/pdf", !0) || t.compare(i, /\.(pdf)$/i);
                    }, object: function object() {
                        return !0;
                    }, other: function other() {
                        return !0;
                    } }, fileActionSettings: { showRemove: !0, showUpload: !0, showDownload: !0, showZoom: !0, showDrag: !0, removeIcon: '<i class="glyphicon glyphicon-trash"></i>', removeClass: "btn btn-sm btn-kv btn-default btn-outline-secondary", removeErrorClass: "btn btn-sm btn-kv btn-danger", removeTitle: "Remove file", uploadIcon: '<i class="glyphicon glyphicon-upload"></i>', uploadClass: "btn btn-sm btn-kv btn-default btn-outline-secondary", uploadTitle: "Upload file", uploadRetryIcon: '<i class="glyphicon glyphicon-repeat"></i>', uploadRetryTitle: "Retry upload", downloadIcon: '<i class="glyphicon glyphicon-download"></i>', downloadClass: "btn btn-sm btn-kv btn-default btn-outline-secondary", downloadTitle: "Download file", zoomIcon: '<i class="glyphicon glyphicon-zoom-in"></i>', zoomClass: "btn btn-sm btn-kv btn-default btn-outline-secondary", zoomTitle: "View Details", dragIcon: '<i class="glyphicon glyphicon-move"></i>', dragClass: "text-info", dragTitle: "Move / Rearrange", dragSettings: {}, indicatorNew: '<i class="glyphicon glyphicon-plus-sign text-warning"></i>', indicatorSuccess: '<i class="glyphicon glyphicon-ok-sign text-success"></i>', indicatorError: '<i class="glyphicon glyphicon-exclamation-sign text-danger"></i>', indicatorLoading: '<i class="glyphicon glyphicon-hourglass text-muted"></i>', indicatorNewTitle: "Not uploaded yet", indicatorSuccessTitle: "Uploaded", indicatorErrorTitle: "Upload Error", indicatorLoadingTitle: "Uploading ..." } }, e.each(M.defaults, function (t, i) {
                return "allowedPreviewTypes" === t ? void (void 0 === M.allowedPreviewTypes && (M.allowedPreviewTypes = i)) : void (M[t] = e.extend(!0, {}, i, M[t]));
            }), M._initPreviewTemplates();
        }, _initPreviewTemplates: function _initPreviewTemplates() {
            var i,
                a = this,
                r = a.defaults,
                n = a.previewMarkupTags,
                o = n.tagAfter;e.each(r.previewContentTemplates, function (e, r) {
                t.isEmpty(a.previewTemplates[e]) && (i = n.tagBefore2, "generic" !== e && "image" !== e && "html" !== e && "text" !== e || (i = n.tagBefore1), a.previewTemplates[e] = i + r + o);
            });
        }, _initPreviewCache: function _initPreviewCache() {
            var i = this;i.previewCache = { data: {}, init: function init() {
                    var e = i.initialPreview;e.length > 0 && !t.isArray(e) && (e = e.split(i.initialPreviewDelimiter)), i.previewCache.data = { content: e, config: i.initialPreviewConfig, tags: i.initialPreviewThumbTags };
                }, count: function count() {
                    return i.previewCache.data && i.previewCache.data.content ? i.previewCache.data.content.length : 0;
                }, get: function get(a, r) {
                    var n,
                        o,
                        l,
                        s,
                        d,
                        c,
                        p,
                        u = "init_" + a,
                        f = i.previewCache.data,
                        m = f.config[a],
                        v = f.content[a],
                        g = i.previewInitId + "-" + u,
                        h = t.ifSet("previewAsData", m, i.initialPreviewAsData),
                        w = function w(e, a, r, n, o, l, s, d, c) {
                        return d = " file-preview-initial " + t.SORT_CSS + (d ? " " + d : ""), i._generatePreviewTemplate(e, a, r, n, o, !1, null, d, l, s, c);
                    };return v ? (r = void 0 === r ? !0 : r, l = t.ifSet("type", m, i.initialPreviewFileType || "generic"), d = t.ifSet("filename", m, t.ifSet("caption", m)), c = t.ifSet("filetype", m, l), s = i.previewCache.footer(a, r, m && m.size || null), p = t.ifSet("frameClass", m), n = h ? w(l, v, d, c, g, s, u, p) : w("generic", v, d, c, g, s, u, p, l).setTokens({ content: f.content[a] }), f.tags.length && f.tags[a] && (n = t.replaceTags(n, f.tags[a])), t.isEmpty(m) || t.isEmpty(m.frameAttr) || (o = e(document.createElement("div")).html(n), o.find(".file-preview-initial").attr(m.frameAttr), n = o.html(), o.remove()), n) : "";
                }, add: function add(e, a, r, n) {
                    var o,
                        l = i.previewCache.data;return t.isArray(e) || (e = e.split(i.initialPreviewDelimiter)), n ? (o = l.content.push(e) - 1, l.config[o] = a, l.tags[o] = r) : (o = e.length - 1, l.content = e, l.config = a, l.tags = r), i.previewCache.data = l, o;
                }, set: function set(e, a, r, n) {
                    var o,
                        l,
                        s = i.previewCache.data;if (e && e.length && (t.isArray(e) || (e = e.split(i.initialPreviewDelimiter)), l = e.filter(function (e) {
                        return null !== e;
                    }), l.length)) {
                        if (void 0 === s.content && (s.content = []), void 0 === s.config && (s.config = []), void 0 === s.tags && (s.tags = []), n) {
                            for (o = 0; o < e.length; o++) {
                                e[o] && s.content.push(e[o]);
                            }for (o = 0; o < a.length; o++) {
                                a[o] && s.config.push(a[o]);
                            }for (o = 0; o < r.length; o++) {
                                r[o] && s.tags.push(r[o]);
                            }
                        } else s.content = e, s.config = a, s.tags = r;i.previewCache.data = s;
                    }
                }, unset: function unset(e) {
                    var a = i.previewCache.count(),
                        r = i.reversePreviewOrder;if (a) {
                        if (1 === a) return i.previewCache.data.content = [], i.previewCache.data.config = [], i.previewCache.data.tags = [], i.initialPreview = [], i.initialPreviewConfig = [], void (i.initialPreviewThumbTags = []);i.previewCache.data.content = t.spliceArray(i.previewCache.data.content, e, r), i.previewCache.data.config = t.spliceArray(i.previewCache.data.config, e, r), i.previewCache.data.tags = t.spliceArray(i.previewCache.data.tags, e, r);
                    }
                }, out: function out() {
                    var e,
                        t,
                        a,
                        r = "",
                        n = i.previewCache.count();if (0 === n) return { content: "", caption: "" };for (t = 0; n > t; t++) {
                        a = i.previewCache.get(t), r = i.reversePreviewOrder ? a + r : r + a;
                    }return e = i._getMsgSelected(n), { content: r, caption: e };
                }, footer: function footer(e, a, r) {
                    var n = i.previewCache.data || {};if (t.isEmpty(n.content)) return "";(t.isEmpty(n.config) || t.isEmpty(n.config[e])) && (n.config[e] = {}), a = void 0 === a ? !0 : a;var o,
                        l = n.config[e],
                        s = t.ifSet("caption", l),
                        d = t.ifSet("width", l, "auto"),
                        c = t.ifSet("url", l, !1),
                        p = t.ifSet("key", l, null),
                        u = i.fileActionSettings,
                        f = i.initialPreviewShowDelete || !1,
                        m = l.downloadUrl || i.initialPreviewDownloadUrl || "",
                        v = l.filename || l.caption || "",
                        g = !!m,
                        h = t.ifSet("showRemove", l, t.ifSet("showRemove", u, f)),
                        w = t.ifSet("showDownload", l, t.ifSet("showDownload", u, g)),
                        _ = t.ifSet("showZoom", l, t.ifSet("showZoom", u, !0)),
                        b = t.ifSet("showDrag", l, t.ifSet("showDrag", u, !0)),
                        C = c === !1 && a;return w = w && l.downloadUrl !== !1 && !!m, o = i._renderFileActions(!1, w, h, _, b, C, c, p, !0, m, v), i._getLayoutTemplate("footer").setTokens({ progress: i._renderThumbProgress(), actions: o, caption: s, size: i._getSize(r), width: d, indicator: "" });
                } }, i.previewCache.init();
        }, _handler: function _handler(e, t, i) {
            var a = this,
                r = a.namespace,
                n = t.split(" ").join(r + " ") + r;e && e.length && e.off(n).on(n, i);
        }, _log: function _log(e) {
            var t = this,
                i = t.$element.attr("id");i && (e = '"' + i + '": ' + e), "undefined" != typeof window.console.log ? window.console.log(e) : window.alert(e);
        }, _validate: function _validate() {
            var e = this,
                t = "file" === e.$element.attr("type");return t || e._log('The input "type" must be set to "file" for initializing the "bootstrap-fileinput" plugin.'), t;
        }, _errorsExist: function _errorsExist() {
            var t,
                i = this,
                a = i.$errorContainer.find("li");return a.length ? !0 : (t = e(document.createElement("div")).html(i.$errorContainer.html()), t.find(".kv-error-close").remove(), t.find("ul").remove(), !!e.trim(t.text()).length);
        }, _errorHandler: function _errorHandler(e, t) {
            var i = this,
                a = e.target.error,
                r = function r(e) {
                i._showError(e.replace("{name}", t));
            };r(a.code === a.NOT_FOUND_ERR ? i.msgFileNotFound : a.code === a.SECURITY_ERR ? i.msgFileSecured : a.code === a.NOT_READABLE_ERR ? i.msgFileNotReadable : a.code === a.ABORT_ERR ? i.msgFilePreviewAborted : i.msgFilePreviewError);
        }, _addError: function _addError(e) {
            var t = this,
                i = t.$errorContainer;e && i.length && (i.html(t.errorCloseButton + e), t._handler(i.find(".kv-error-close"), "click", function () {
                setTimeout(function () {
                    t.showPreview && !t.getFrames().length && t.clear(), i.fadeOut("slow");
                }, 10);
            }));
        }, _setValidationError: function _setValidationError(e) {
            var i = this;e = (e ? e + " " : "") + "has-error", i.$container.removeClass(e).addClass("has-error"), t.addCss(i.$captionContainer, "is-invalid");
        }, _resetErrors: function _resetErrors(e) {
            var t = this,
                i = t.$errorContainer;t.isError = !1, t.$container.removeClass("has-error"), t.$captionContainer.removeClass("is-invalid"), i.html(""), e ? i.fadeOut("slow") : i.hide();
        }, _showFolderError: function _showFolderError(e) {
            var t,
                i = this,
                a = i.$errorContainer;e && (t = i.msgFoldersNotAllowed.replace("{n}", e), i._addError(t), i._setValidationError(), a.fadeIn(800), i._raise("filefoldererror", [e, t]));
        }, _showUploadError: function _showUploadError(e, t, i) {
            var a = this,
                r = a.$errorContainer,
                n = i || "fileuploaderror",
                o = t && t.id ? '<li data-file-id="' + t.id + '">' + e + "</li>" : "<li>" + e + "</li>";return 0 === r.find("ul").length ? a._addError("<ul>" + o + "</ul>") : r.find("ul").append(o), r.fadeIn(800), a._raise(n, [t, e]), a._setValidationError("file-input-new"), !0;
        }, _showError: function _showError(e, t, i) {
            var a = this,
                r = a.$errorContainer,
                n = i || "fileerror";return t = t || {}, t.reader = a.reader, a._addError(e), r.fadeIn(800), a._raise(n, [t, e]), a.isAjaxUpload || a._clearFileInput(), a._setValidationError("file-input-new"), a.$btnUpload.attr("disabled", !0), !0;
        }, _noFilesError: function _noFilesError(e) {
            var t = this,
                i = t.minFileCount > 1 ? t.filePlural : t.fileSingle,
                a = t.msgFilesTooLess.replace("{n}", t.minFileCount).replace("{files}", i),
                r = t.$errorContainer;t._addError(a), t.isError = !0, t._updateFileDetails(0), r.fadeIn(800), t._raise("fileerror", [e, a]), t._clearFileInput(), t._setValidationError();
        }, _parseError: function _parseError(t, i, a, r) {
            var n,
                o = this,
                l = e.trim(a + ""),
                s = void 0 !== i.responseJSON && void 0 !== i.responseJSON.error ? i.responseJSON.error : i.responseText;return o.cancelling && o.msgUploadAborted && (l = o.msgUploadAborted), o.showAjaxErrorDetails && s && (s = e.trim(s.replace(/\n\s*\n/g, "\n")), n = s.length ? "<pre>" + s + "</pre>" : "", l += l ? n : s), l || (l = o.msgAjaxError.replace("{operation}", t)), o.cancelling = !1, r ? "<b>" + r + ": </b>" + l : l;
        }, _parseFileType: function _parseFileType(e, i) {
            var a,
                r,
                n,
                o,
                l = this,
                s = l.allowedPreviewTypes || [];if ("application/text-plain" === e) return "text";for (o = 0; o < s.length; o++) {
                if (n = s[o], a = l.fileTypeSettings[n], r = a(e, i) ? n : "", !t.isEmpty(r)) return r;
            }return "other";
        }, _getPreviewIcon: function _getPreviewIcon(t) {
            var i,
                a = this,
                r = null;return t && t.indexOf(".") > -1 && (i = t.split(".").pop(), a.previewFileIconSettings && (r = a.previewFileIconSettings[i] || a.previewFileIconSettings[i.toLowerCase()] || null), a.previewFileExtSettings && e.each(a.previewFileExtSettings, function (e, t) {
                return a.previewFileIconSettings[e] && t(i) ? void (r = a.previewFileIconSettings[e]) : void 0;
            })), r;
        }, _parseFilePreviewIcon: function _parseFilePreviewIcon(e, t) {
            var i = this,
                a = i._getPreviewIcon(t) || i.previewFileIcon,
                r = e;return r.indexOf("{previewFileIcon}") > -1 && (r = r.setTokens({ previewFileIconClass: i.previewFileIconClass, previewFileIcon: a })), r;
        }, _raise: function _raise(t, i) {
            var a = this,
                r = e.Event(t);if (void 0 !== i ? a.$element.trigger(r, i) : a.$element.trigger(r), r.isDefaultPrevented() || r.result === !1) return !1;switch (t) {case "filebatchuploadcomplete":case "filebatchuploadsuccess":case "fileuploaded":case "fileclear":case "filecleared":case "filereset":case "fileerror":case "filefoldererror":case "fileuploaderror":case "filebatchuploaderror":case "filedeleteerror":case "filecustomerror":case "filesuccessremove":
                    break;default:
                    a.ajaxAborted || (a.ajaxAborted = r.result);}return !0;
        }, _listenFullScreen: function _listenFullScreen(e) {
            var t,
                i,
                a = this,
                r = a.$modal;r && r.length && (t = r && r.find(".btn-fullscreen"), i = r && r.find(".btn-borderless"), t.length && i.length && (t.removeClass("active").attr("aria-pressed", "false"), i.removeClass("active").attr("aria-pressed", "false"), e ? t.addClass("active").attr("aria-pressed", "true") : i.addClass("active").attr("aria-pressed", "true"), r.hasClass("file-zoom-fullscreen") ? a._maximizeZoomDialog() : e ? a._maximizeZoomDialog() : i.removeClass("active").attr("aria-pressed", "false")));
        }, _listen: function _listen() {
            var i,
                a = this,
                r = a.$element,
                n = a.$form,
                o = a.$container;a._handler(r, "change", e.proxy(a._change, a)), a.showBrowse && a._handler(a.$btnFile, "click", e.proxy(a._browse, a)), a._handler(o.find(".fileinput-remove:not([disabled])"), "click", e.proxy(a.clear, a)), a._handler(o.find(".fileinput-cancel"), "click", e.proxy(a.cancel, a)), a._initDragDrop(), a._handler(n, "reset", e.proxy(a.clear, a)), a.isAjaxUpload || a._handler(n, "submit", e.proxy(a._submitForm, a)), a._handler(a.$container.find(".fileinput-upload"), "click", e.proxy(a._uploadClick, a)), a._handler(e(window), "resize", function () {
                a._listenFullScreen(screen.width === window.innerWidth && screen.height === window.innerHeight);
            }), i = "webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange", a._handler(e(document), i, function () {
                a._listenFullScreen(t.checkFullScreen());
            }), a._autoFitContent(), a._initClickable();
        }, _autoFitContent: function _autoFitContent() {
            var t,
                i = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
                a = this,
                r = 400 > i ? a.previewSettingsSmall || a.defaults.previewSettingsSmall : a.previewSettings || a.defaults.previewSettings;e.each(r, function (e, i) {
                t = ".file-preview-frame .file-preview-" + e, a.$preview.find(t + ".kv-preview-data," + t + " .kv-preview-data").css(i);
            });
        }, _initClickable: function _initClickable() {
            var i,
                a = this;a.isClickable && (i = a.isAjaxUpload ? a.$dropZone : a.$preview.find(".file-default-preview"), t.addCss(i, "clickable"), i.attr("tabindex", -1), a._handler(i, "click", function (t) {
                var r = e(t.target);i.find(".kv-fileinput-error:visible").length || r.parents(".file-preview-thumbnails").length && !r.parents(".file-default-preview").length || (a.$element.trigger("click"), i.blur());
            }));
        }, _initDragDrop: function _initDragDrop() {
            var t = this,
                i = t.$dropZone;t.isAjaxUpload && t.dropZoneEnabled && t.showPreview && (t._handler(i, "dragenter dragover", e.proxy(t._zoneDragEnter, t)), t._handler(i, "dragleave", e.proxy(t._zoneDragLeave, t)), t._handler(i, "drop", e.proxy(t._zoneDrop, t)), t._handler(e(document), "dragenter dragover drop", t._zoneDragDropInit));
        }, _zoneDragDropInit: function _zoneDragDropInit(e) {
            e.stopPropagation(), e.preventDefault();
        }, _zoneDragEnter: function _zoneDragEnter(i) {
            var a = this,
                r = e.inArray("Files", i.originalEvent.dataTransfer.types) > -1;return a._zoneDragDropInit(i), a.isDisabled || !r ? (i.originalEvent.dataTransfer.effectAllowed = "none", void (i.originalEvent.dataTransfer.dropEffect = "none")) : void t.addCss(a.$dropZone, "file-highlighted");
        }, _zoneDragLeave: function _zoneDragLeave(e) {
            var t = this;t._zoneDragDropInit(e), t.isDisabled || t.$dropZone.removeClass("file-highlighted");
        }, _zoneDrop: function _zoneDrop(e) {
            var i = this;e.preventDefault(), i.isDisabled || t.isEmpty(e.originalEvent.dataTransfer.files) || (i._change(e, "dragdrop"), i.$dropZone.removeClass("file-highlighted"));
        }, _uploadClick: function _uploadClick(e) {
            var i,
                a = this,
                r = a.$container.find(".fileinput-upload"),
                n = !r.hasClass("disabled") && t.isEmpty(r.attr("disabled"));if (!e || !e.isDefaultPrevented()) {
                if (!a.isAjaxUpload) return void (n && "submit" !== r.attr("type") && (i = r.closest("form"), i.length && i.trigger("submit"), e.preventDefault()));e.preventDefault(), n && a.upload();
            }
        }, _submitForm: function _submitForm() {
            var e = this;return e._isFileSelectionValid() && !e._abort({});
        }, _clearPreview: function _clearPreview() {
            var i = this,
                a = i.$preview,
                r = i.showUploadedThumbs ? i.getFrames(":not(.file-preview-success)") : i.getFrames();r.each(function () {
                var i = e(this);i.remove(), t.cleanZoomCache(a.find("#zoom-" + i.attr("id")));
            }), i.getFrames().length && i.showPreview || i._resetUpload(), i._validateDefaultPreview();
        }, _initSortable: function _initSortable() {
            var i,
                a = this,
                r = a.$preview,
                n = "." + t.SORT_CSS,
                o = a.reversePreviewOrder;window.KvSortable && 0 !== r.find(n).length && (i = { handle: ".drag-handle-init", dataIdAttr: "data-preview-id", scroll: !1, draggable: n, onSort: function onSort(i) {
                    var r = i.oldIndex,
                        n = i.newIndex,
                        l = 0;a.initialPreview = t.moveArray(a.initialPreview, r, n, o), a.initialPreviewConfig = t.moveArray(a.initialPreviewConfig, r, n, o), a.previewCache.init(), a.getFrames(".file-preview-initial").each(function () {
                        e(this).attr("data-fileindex", "init_" + l), l++;
                    }), a._raise("filesorted", { previewId: e(i.item).attr("id"), oldIndex: r, newIndex: n, stack: a.initialPreviewConfig });
                } }, r.data("kvsortable") && r.kvsortable("destroy"), e.extend(!0, i, a.fileActionSettings.dragSettings), r.kvsortable(i));
        }, _setPreviewContent: function _setPreviewContent(e) {
            var t = this;t.$preview.html(e), t._autoFitContent();
        }, _initPreview: function _initPreview(e) {
            var i,
                a = this,
                r = a.initialCaption || "";return a.previewCache.count() ? (i = a.previewCache.out(), r = e && a.initialCaption ? a.initialCaption : i.caption, a._setPreviewContent(i.content), a._setInitThumbAttr(), a._setCaption(r), a._initSortable(), void (t.isEmpty(i.content) || a.$container.removeClass("file-input-new"))) : (a._clearPreview(), void (e ? a._setCaption(r) : a._initCaption()));
        }, _getZoomButton: function _getZoomButton(e) {
            var t = this,
                i = t.previewZoomButtonIcons[e],
                a = t.previewZoomButtonClasses[e],
                r = ' title="' + (t.previewZoomButtonTitles[e] || "") + '" ',
                n = r + ("close" === e ? ' data-dismiss="modal" aria-hidden="true"' : "");return "fullscreen" !== e && "borderless" !== e && "toggleheader" !== e || (n += ' data-toggle="button" aria-pressed="false" autocomplete="off"'), '<button type="button" class="' + a + " btn-" + e + '"' + n + ">" + i + "</button>";
        }, _getModalContent: function _getModalContent() {
            var e = this;return e._getLayoutTemplate("modal").setTokens({ rtl: e.rtl ? " kv-rtl" : "", zoomFrameClass: e.frameClass, heading: e.msgZoomModalHeading, prev: e._getZoomButton("prev"), next: e._getZoomButton("next"), toggleheader: e._getZoomButton("toggleheader"), fullscreen: e._getZoomButton("fullscreen"), borderless: e._getZoomButton("borderless"), close: e._getZoomButton("close") });
        }, _listenModalEvent: function _listenModalEvent(e) {
            var i = this,
                a = i.$modal,
                r = function r(e) {
                return { sourceEvent: e, previewId: a.data("previewId"), modal: a };
            };a.on(e + ".bs.modal", function (n) {
                var o = a.find(".btn-fullscreen"),
                    l = a.find(".btn-borderless");i._raise("filezoom" + e, r(n)), "shown" === e && (l.removeClass("active").attr("aria-pressed", "false"), o.removeClass("active").attr("aria-pressed", "false"), a.hasClass("file-zoom-fullscreen") && (i._maximizeZoomDialog(), t.checkFullScreen() ? o.addClass("active").attr("aria-pressed", "true") : l.addClass("active").attr("aria-pressed", "true")));
            });
        }, _initZoom: function _initZoom() {
            var i,
                a = this,
                r = a._getLayoutTemplate("modalMain"),
                n = "#" + t.MODAL_ID;a.showPreview && (a.$modal = e(n), a.$modal && a.$modal.length || (i = e(document.createElement("div")).html(r).insertAfter(a.$container), a.$modal = e(n).insertBefore(i), i.remove()), t.initModal(a.$modal), a.$modal.html(a._getModalContent()), e.each(t.MODAL_EVENTS, function (e, t) {
                a._listenModalEvent(t);
            }));
        }, _initZoomButtons: function _initZoomButtons() {
            var t,
                i,
                a = this,
                r = a.$modal.data("previewId") || "",
                n = a.getFrames().toArray(),
                o = n.length,
                l = a.$modal.find(".btn-prev"),
                s = a.$modal.find(".btn-next");return n.length < 2 ? (l.hide(), void s.hide()) : (l.show(), s.show(), void (o && (t = e(n[0]), i = e(n[o - 1]), l.removeAttr("disabled"), s.removeAttr("disabled"), t.length && t.attr("id") === r && l.attr("disabled", !0), i.length && i.attr("id") === r && s.attr("disabled", !0))));
        }, _maximizeZoomDialog: function _maximizeZoomDialog() {
            var t = this,
                i = t.$modal,
                a = i.find(".modal-header:visible"),
                r = i.find(".modal-footer:visible"),
                n = i.find(".modal-body"),
                o = e(window).height(),
                l = 0;i.addClass("file-zoom-fullscreen"), a && a.length && (o -= a.outerHeight(!0)), r && r.length && (o -= r.outerHeight(!0)), n && n.length && (l = n.outerHeight(!0) - n.height(), o -= l), i.find(".kv-zoom-body").height(o);
        }, _resizeZoomDialog: function _resizeZoomDialog(e) {
            var i = this,
                a = i.$modal,
                r = a.find(".btn-fullscreen"),
                n = a.find(".btn-borderless");if (a.hasClass("file-zoom-fullscreen")) t.toggleFullScreen(!1), e ? r.hasClass("active") || (a.removeClass("file-zoom-fullscreen"), i._resizeZoomDialog(!0), n.hasClass("active") && n.removeClass("active").attr("aria-pressed", "false")) : r.hasClass("active") ? r.removeClass("active").attr("aria-pressed", "false") : (a.removeClass("file-zoom-fullscreen"), i.$modal.find(".kv-zoom-body").css("height", i.zoomModalHeight));else {
                if (!e) return void i._maximizeZoomDialog();t.toggleFullScreen(!0);
            }a.focus();
        }, _setZoomContent: function _setZoomContent(i, a) {
            var r,
                n,
                o,
                l,
                s,
                d,
                c,
                p,
                u,
                f,
                m = this,
                v = i.attr("id"),
                g = m.$modal,
                h = g.find(".btn-prev"),
                w = g.find(".btn-next"),
                _ = g.find(".btn-fullscreen"),
                b = g.find(".btn-borderless"),
                C = g.find(".btn-toggleheader"),
                y = m.$preview.find("#zoom-" + v);n = y.attr("data-template") || "generic", r = y.find(".kv-file-content"), o = r.length ? r.html() : "", u = i.data("caption") || "", f = i.data("size") || "", l = u + " " + f, g.find(".kv-zoom-title").attr("title", e("<div/>").html(l).text()).html(l), s = g.find(".kv-zoom-body"), g.removeClass("kv-single-content"), a ? (p = s.addClass("file-thumb-loading").clone().insertAfter(s), s.html(o).hide(), p.fadeOut("fast", function () {
                s.fadeIn("fast", function () {
                    s.removeClass("file-thumb-loading");
                }), p.remove();
            })) : s.html(o), c = m.previewZoomSettings[n], c && (d = s.find(".kv-preview-data"), t.addCss(d, "file-zoom-detail"), e.each(c, function (e, t) {
                d.css(e, t), (d.attr("width") && "width" === e || d.attr("height") && "height" === e) && d.removeAttr(e);
            })), g.data("previewId", v);var x = s.find("img");x.length && t.adjustOrientedImage(x, !0), m._handler(h, "click", function () {
                m._zoomSlideShow("prev", v);
            }), m._handler(w, "click", function () {
                m._zoomSlideShow("next", v);
            }), m._handler(_, "click", function () {
                m._resizeZoomDialog(!0);
            }), m._handler(b, "click", function () {
                m._resizeZoomDialog(!1);
            }), m._handler(C, "click", function () {
                var e,
                    t = g.find(".modal-header"),
                    i = g.find(".modal-body .floating-buttons"),
                    a = t.find(".kv-zoom-actions"),
                    r = function r(e) {
                    var i = m.$modal.find(".kv-zoom-body"),
                        a = m.zoomModalHeight;g.hasClass("file-zoom-fullscreen") && (a = i.outerHeight(!0), e || (a -= t.outerHeight(!0))), i.css("height", e ? a + e : a);
                };t.is(":visible") ? (e = t.outerHeight(!0), t.slideUp("slow", function () {
                    a.find(".btn").appendTo(i), r(e);
                })) : (i.find(".btn").appendTo(a), t.slideDown("slow", function () {
                    r();
                })), g.focus();
            }), m._handler(g, "keydown", function (e) {
                var t = e.which || e.keyCode;37 !== t || h.attr("disabled") || m._zoomSlideShow("prev", v), 39 !== t || w.attr("disabled") || m._zoomSlideShow("next", v);
            });
        }, _zoomPreview: function _zoomPreview(e) {
            var i,
                a = this,
                r = a.$modal;if (!e.length) throw "Cannot zoom to detailed preview!";t.initModal(r), r.html(a._getModalContent()), i = e.closest(t.FRAMES), a._setZoomContent(i), r.modal("show"), a._initZoomButtons();
        }, _zoomSlideShow: function _zoomSlideShow(t, i) {
            var a,
                r,
                n,
                o = this,
                l = o.$modal.find(".kv-zoom-actions .btn-" + t),
                s = o.getFrames().toArray(),
                d = s.length;if (!l.attr("disabled")) {
                for (r = 0; d > r; r++) {
                    if (e(s[r]).attr("id") === i) {
                        n = "prev" === t ? r - 1 : r + 1;break;
                    }
                }0 > n || n >= d || !s[n] || (a = e(s[n]), a.length && o._setZoomContent(a, !0), o._initZoomButtons(), o._raise("filezoom" + t, { previewId: i, modal: o.$modal }));
            }
        }, _initZoomButton: function _initZoomButton() {
            var t = this;t.$preview.find(".kv-file-zoom").each(function () {
                var i = e(this);t._handler(i, "click", function () {
                    t._zoomPreview(i);
                });
            });
        }, _clearObjects: function _clearObjects(t) {
            t.find("video audio").each(function () {
                this.pause(), e(this).remove();
            }), t.find("img object div").each(function () {
                e(this).remove();
            });
        }, _clearFileInput: function _clearFileInput() {
            var i,
                a,
                r,
                n = this,
                o = n.$element;n.fileInputCleared = !0, t.isEmpty(o.val()) || (n.isIE9 || n.isIE10 ? (i = o.closest("form"), a = e(document.createElement("form")), r = e(document.createElement("div")), o.before(r), i.length ? i.after(a) : r.after(a), a.append(o).trigger("reset"), r.before(o).remove(), a.remove()) : o.val(""));
        }, _resetUpload: function _resetUpload() {
            var e = this;e.uploadCache = { content: [], config: [], tags: [], append: !0 }, e.uploadCount = 0, e.uploadStatus = {}, e.uploadLog = [], e.uploadAsyncCount = 0, e.loadedImages = [], e.totalImagesCount = 0, e.$btnUpload.removeAttr("disabled"), e._setProgress(0), e.$progress.hide(), e._resetErrors(!1), e.ajaxAborted = !1, e.ajaxRequests = [], e._resetCanvas(), e.cacheInitialPreview = {}, e.overwriteInitial && (e.initialPreview = [], e.initialPreviewConfig = [], e.initialPreviewThumbTags = [], e.previewCache.data = { content: [], config: [], tags: [] });
        }, _resetCanvas: function _resetCanvas() {
            var e = this;e.canvas && e.imageCanvasContext && e.imageCanvasContext.clearRect(0, 0, e.canvas.width, e.canvas.height);
        }, _hasInitialPreview: function _hasInitialPreview() {
            var e = this;return !e.overwriteInitial && e.previewCache.count();
        }, _resetPreview: function _resetPreview() {
            var e,
                t,
                i = this;i.previewCache.count() ? (e = i.previewCache.out(), i._setPreviewContent(e.content), i._setInitThumbAttr(), t = i.initialCaption ? i.initialCaption : e.caption, i._setCaption(t)) : (i._clearPreview(), i._initCaption()), i.showPreview && (i._initZoom(), i._initSortable());
        }, _clearDefaultPreview: function _clearDefaultPreview() {
            var e = this;e.$preview.find(".file-default-preview").remove();
        }, _validateDefaultPreview: function _validateDefaultPreview() {
            var e = this;e.showPreview && !t.isEmpty(e.defaultPreviewContent) && (e._setPreviewContent('<div class="file-default-preview">' + e.defaultPreviewContent + "</div>"), e.$container.removeClass("file-input-new"), e._initClickable());
        }, _resetPreviewThumbs: function _resetPreviewThumbs(e) {
            var t,
                i = this;return e ? (i._clearPreview(), void i.clearStack()) : void (i._hasInitialPreview() ? (t = i.previewCache.out(), i._setPreviewContent(t.content), i._setInitThumbAttr(), i._setCaption(t.caption), i._initPreviewActions()) : i._clearPreview());
        }, _getLayoutTemplate: function _getLayoutTemplate(e) {
            var i = this,
                a = i.layoutTemplates[e];return t.isEmpty(i.customLayoutTags) ? a : t.replaceTags(a, i.customLayoutTags);
        }, _getPreviewTemplate: function _getPreviewTemplate(e) {
            var i = this,
                a = i.previewTemplates[e];return t.isEmpty(i.customPreviewTags) ? a : t.replaceTags(a, i.customPreviewTags);
        }, _getOutData: function _getOutData(e, t, i) {
            var a = this;return e = e || {}, t = t || {}, i = i || a.filestack.slice(0) || {}, { form: a.formdata, files: i, filenames: a.filenames, filescount: a.getFilesCount(), extra: a._getExtraData(), response: t, reader: a.reader, jqXHR: e };
        }, _getMsgSelected: function _getMsgSelected(e) {
            var t = this,
                i = 1 === e ? t.fileSingle : t.filePlural;return e > 0 ? t.msgSelected.replace("{n}", e).replace("{files}", i) : t.msgNoFilesSelected;
        }, _getFrame: function _getFrame(t) {
            var i = this,
                a = e("#" + t);return a.length ? a : (i._log('Invalid thumb frame with id: "' + t + '".'), null);
        }, _getThumbs: function _getThumbs(e) {
            return e = e || "", this.getFrames(":not(.file-preview-initial)" + e);
        }, _getExtraData: function _getExtraData(e, t) {
            var i = this,
                a = i.uploadExtraData;return "function" == typeof i.uploadExtraData && (a = i.uploadExtraData(e, t)), a;
        }, _initXhr: function _initXhr(e, t, i) {
            var a = this;return e.upload && e.upload.addEventListener("progress", function (e) {
                var r = 0,
                    n = e.total,
                    o = e.loaded || e.position;e.lengthComputable && (r = Math.floor(o / n * 100)), t ? a._setAsyncUploadStatus(t, r, i) : a._setProgress(r);
            }, !1), e;
        }, _mergeAjaxCallback: function _mergeAjaxCallback(e, t, i) {
            var a,
                r = this,
                n = r.ajaxSettings,
                o = r.mergeAjaxCallbacks;"delete" === i && (n = r.ajaxDeleteSettings, o = r.mergeAjaxDeleteCallbacks), a = n[e], o && "function" == typeof a ? "before" === o ? n[e] = function () {
                a.apply(this, arguments), t.apply(this, arguments);
            } : n[e] = function () {
                t.apply(this, arguments), a.apply(this, arguments);
            } : n[e] = t, "delete" === i ? r.ajaxDeleteSettings = n : r.ajaxSettings = n;
        }, _ajaxSubmit: function _ajaxSubmit(t, i, a, r, n, o) {
            var l,
                s = this;s._raise("filepreajax", [n, o]) && (s._uploadExtra(n, o), s._mergeAjaxCallback("beforeSend", t), s._mergeAjaxCallback("success", i), s._mergeAjaxCallback("complete", a), s._mergeAjaxCallback("error", r), l = e.extend(!0, {}, { xhr: function xhr() {
                    var t = e.ajaxSettings.xhr();return s._initXhr(t, n, s.getFileStack().length);
                }, url: o && s.uploadUrlThumb ? s.uploadUrlThumb : s.uploadUrl, type: "POST", dataType: "json", data: s.formdata, cache: !1, processData: !1, contentType: !1 }, s.ajaxSettings), s.ajaxRequests.push(e.ajax(l)));
        }, _mergeArray: function _mergeArray(e, i) {
            var a = this,
                r = t.cleanArray(a[e]),
                n = t.cleanArray(i);a[e] = r.concat(n);
        }, _initUploadSuccess: function _initUploadSuccess(i, a, r) {
            var n,
                o,
                l,
                s,
                d,
                c,
                p,
                u,
                f,
                m = this;m.showPreview && "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && !e.isEmptyObject(i) && void 0 !== i.initialPreview && i.initialPreview.length > 0 && (m.hasInitData = !0, c = i.initialPreview || [], p = i.initialPreviewConfig || [], u = i.initialPreviewThumbTags || [], n = void 0 === i.append || i.append, c.length > 0 && !t.isArray(c) && (c = c.split(m.initialPreviewDelimiter)), m._mergeArray("initialPreview", c), m._mergeArray("initialPreviewConfig", p), m._mergeArray("initialPreviewThumbTags", u), void 0 !== a ? r ? (f = a.attr("data-fileindex"), m.uploadCache.content[f] = c[0], m.uploadCache.config[f] = p[0] || [], m.uploadCache.tags[f] = u[0] || [], m.uploadCache.append = n) : (l = m.previewCache.add(c, p[0], u[0], n), o = m.previewCache.get(l, !1), s = e(document.createElement("div")).html(o).hide().insertAfter(a), d = s.find(".kv-zoom-cache"), d && d.length && d.insertAfter(a), a.fadeOut("slow", function () {
                var e = s.find(".file-preview-frame");e && e.length && e.insertBefore(a).fadeIn("slow").css("display:inline-block"), m._initPreviewActions(), m._clearFileInput(), t.cleanZoomCache(m.$preview.find("#zoom-" + a.attr("id"))), a.remove(), s.remove(), m._initSortable();
            })) : (m.previewCache.set(c, p, u, n), m._initPreview(), m._initPreviewActions()));
        }, _initSuccessThumbs: function _initSuccessThumbs() {
            var i = this;i.showPreview && i._getThumbs(t.FRAMES + ".file-preview-success").each(function () {
                var a = e(this),
                    r = i.$preview,
                    n = a.find(".kv-file-remove");n.removeAttr("disabled"), i._handler(n, "click", function () {
                    var e = a.attr("id"),
                        n = i._raise("filesuccessremove", [e, a.attr("data-fileindex")]);t.cleanMemory(a), n !== !1 && a.fadeOut("slow", function () {
                        t.cleanZoomCache(r.find("#zoom-" + e)), a.remove(), i.getFrames().length || i.reset();
                    });
                });
            });
        }, _checkAsyncComplete: function _checkAsyncComplete() {
            var t,
                i,
                a = this;for (i = 0; i < a.filestack.length; i++) {
                if (a.filestack[i] && (t = a.previewInitId + "-" + i, -1 === e.inArray(t, a.uploadLog))) return !1;
            }return a.uploadAsyncCount === a.uploadLog.length;
        }, _uploadExtra: function _uploadExtra(t, i) {
            var a = this,
                r = a._getExtraData(t, i);0 !== r.length && e.each(r, function (e, t) {
                a.formdata.append(e, t);
            });
        }, _uploadSingle: function _uploadSingle(i, a) {
            var r,
                n,
                o,
                l,
                s,
                d,
                c,
                p,
                u,
                f,
                m,
                v = this,
                g = v.getFileStack().length,
                h = new FormData(),
                w = v.previewInitId + "-" + i,
                _ = v.filestack.length > 0 || !e.isEmptyObject(v.uploadExtraData),
                b = e("#" + w).find(".file-thumb-progress"),
                C = { id: w, index: i };v.formdata = h, v.showPreview && (n = e("#" + w + ":not(.file-preview-initial)"), l = n.find(".kv-file-upload"), s = n.find(".kv-file-remove"), b.show()), 0 === g || !_ || l && l.hasClass("disabled") || v._abort(C) || (m = function m(e, t) {
                d || v.updateStack(e, void 0), v.uploadLog.push(t), v._checkAsyncComplete() && (v.fileBatchCompleted = !0);
            }, o = function o() {
                var e,
                    i,
                    a,
                    r = v.uploadCache,
                    n = 0,
                    o = v.cacheInitialPreview;v.fileBatchCompleted && (o && o.content && (n = o.content.length), setTimeout(function () {
                    var l = 0 === v.getFileStack(!0).length;if (v.showPreview) {
                        if (v.previewCache.set(r.content, r.config, r.tags, r.append), n) {
                            for (i = 0; i < r.content.length; i++) {
                                a = i + n, o.content[a] = r.content[i], o.config.length && (o.config[a] = r.config[i]), o.tags.length && (o.tags[a] = r.tags[i]);
                            }v.initialPreview = t.cleanArray(o.content), v.initialPreviewConfig = t.cleanArray(o.config), v.initialPreviewThumbTags = t.cleanArray(o.tags);
                        } else v.initialPreview = r.content, v.initialPreviewConfig = r.config, v.initialPreviewThumbTags = r.tags;v.cacheInitialPreview = {}, v.hasInitData && (v._initPreview(), v._initPreviewActions());
                    }v.unlock(l), l && v._clearFileInput(), e = v.$preview.find(".file-preview-initial"), v.uploadAsync && e.length && (t.addCss(e, t.SORT_CSS), v._initSortable()), v._raise("filebatchuploadcomplete", [v.filestack, v._getExtraData()]), v.uploadCount = 0, v.uploadStatus = {}, v.uploadLog = [], v._setProgress(101), v.ajaxAborted = !1;
                }, 100));
            }, c = function c(o) {
                r = v._getOutData(o), v.fileBatchCompleted = !1, a || (v.ajaxAborted = !1), v.showPreview && (n.hasClass("file-preview-success") || (v._setThumbStatus(n, "Loading"), t.addCss(n, "file-uploading")), l.attr("disabled", !0), s.attr("disabled", !0)), a || v.lock(), v._raise("filepreupload", [r, w, i]), e.extend(!0, C, r), v._abort(C) && (o.abort(), a || (v._setThumbStatus(n, "New"), n.removeClass("file-uploading"), l.removeAttr("disabled"), s.removeAttr("disabled"), v.unlock()), v._setProgressCancelled());
            }, p = function p(o, s, c) {
                var p = v.showPreview && n.attr("id") ? n.attr("id") : w;r = v._getOutData(c, o), e.extend(!0, C, r), setTimeout(function () {
                    t.isEmpty(o) || t.isEmpty(o.error) ? (v.showPreview && (v._setThumbStatus(n, "Success"), l.hide(), v._initUploadSuccess(o, n, a), v._setProgress(101, b)), v._raise("fileuploaded", [r, p, i]), a ? m(i, p) : v.updateStack(i, void 0)) : (d = !0, v._showUploadError(o.error, C), v._setPreviewError(n, i, v.filestack[i], v.retryErrorUploads), v.retryErrorUploads || l.hide(), a && m(i, p), v._setProgress(101, e("#" + p).find(".file-thumb-progress"), v.msgUploadError));
                }, 100);
            }, u = function u() {
                setTimeout(function () {
                    v.showPreview && (l.removeAttr("disabled"), s.removeAttr("disabled"), n.removeClass("file-uploading")), a ? o() : (v.unlock(!1), v._clearFileInput()), v._initSuccessThumbs();
                }, 100);
            }, f = function f(t, r, o) {
                var s = v.ajaxOperations.uploadThumb,
                    c = v._parseError(s, t, o, a && v.filestack[i].name ? v.filestack[i].name : null);d = !0, setTimeout(function () {
                    a && m(i, w), v.uploadStatus[w] = 100, v._setPreviewError(n, i, v.filestack[i], v.retryErrorUploads), v.retryErrorUploads || l.hide(), e.extend(!0, C, v._getOutData(t)), v._setProgress(101, b, v.msgAjaxProgressError.replace("{operation}", s)), v._setProgress(101, e("#" + w).find(".file-thumb-progress"), v.msgUploadError), v._showUploadError(c, C);
                }, 100);
            }, h.append(v.uploadFileAttr, v.filestack[i], v.filenames[i]), h.append("file_id", i), v._ajaxSubmit(c, p, u, f, w, i));
        }, _uploadBatch: function _uploadBatch() {
            var i,
                a,
                r,
                n,
                o,
                l = this,
                s = l.filestack,
                d = s.length,
                c = {},
                p = l.filestack.length > 0 || !e.isEmptyObject(l.uploadExtraData);l.formdata = new FormData(), 0 !== d && p && !l._abort(c) && (o = function o() {
                e.each(s, function (e) {
                    l.updateStack(e, void 0);
                }), l._clearFileInput();
            }, i = function i(_i2) {
                l.lock();var a = l._getOutData(_i2);l.ajaxAborted = !1, l.showPreview && l._getThumbs().each(function () {
                    var i = e(this),
                        a = i.find(".kv-file-upload"),
                        r = i.find(".kv-file-remove");i.hasClass("file-preview-success") || (l._setThumbStatus(i, "Loading"), t.addCss(i, "file-uploading")), a.attr("disabled", !0), r.attr("disabled", !0);
                }), l._raise("filebatchpreupload", [a]), l._abort(a) && (_i2.abort(), l._getThumbs().each(function () {
                    var t = e(this),
                        i = t.find(".kv-file-upload"),
                        a = t.find(".kv-file-remove");t.hasClass("file-preview-loading") && (l._setThumbStatus(t, "New"), t.removeClass("file-uploading")), i.removeAttr("disabled"), a.removeAttr("disabled");
                }), l._setProgressCancelled());
            }, a = function a(i, _a, r) {
                var n = l._getOutData(r, i),
                    s = 0,
                    d = l._getThumbs(":not(.file-preview-success)"),
                    c = t.isEmpty(i) || t.isEmpty(i.errorkeys) ? [] : i.errorkeys;t.isEmpty(i) || t.isEmpty(i.error) ? (l._raise("filebatchuploadsuccess", [n]), o(), l.showPreview ? (d.each(function () {
                    var t = e(this);l._setThumbStatus(t, "Success"), t.removeClass("file-uploading"), t.find(".kv-file-upload").hide().removeAttr("disabled");
                }), l._initUploadSuccess(i)) : l.reset(), l._setProgress(101)) : (l.showPreview && (d.each(function () {
                    var t = e(this),
                        i = t.attr("data-fileindex");t.removeClass("file-uploading"), t.find(".kv-file-upload").removeAttr("disabled"), t.find(".kv-file-remove").removeAttr("disabled"), 0 === c.length || -1 !== e.inArray(s, c) ? (l._setPreviewError(t, i, l.filestack[i], l.retryErrorUploads), l.retryErrorUploads || (t.find(".kv-file-upload").hide(), l.updateStack(i, void 0))) : (t.find(".kv-file-upload").hide(), l._setThumbStatus(t, "Success"), l.updateStack(i, void 0)), t.hasClass("file-preview-error") && !l.retryErrorUploads || s++;
                }), l._initUploadSuccess(i)), l._showUploadError(i.error, n, "filebatchuploaderror"), l._setProgress(101, l.$progress, l.msgUploadError));
            }, n = function n() {
                l.unlock(), l._initSuccessThumbs(), l._clearFileInput(), l._raise("filebatchuploadcomplete", [l.filestack, l._getExtraData()]);
            }, r = function r(t, i, a) {
                var r = l._getOutData(t),
                    n = l.ajaxOperations.uploadBatch,
                    o = l._parseError(n, t, a);l._showUploadError(o, r, "filebatchuploaderror"), l.uploadFileCount = d - 1, l.showPreview && (l._getThumbs().each(function () {
                    var t = e(this),
                        i = t.attr("data-fileindex");t.removeClass("file-uploading"), void 0 !== l.filestack[i] && l._setPreviewError(t);
                }), l._getThumbs().removeClass("file-uploading"), l._getThumbs(" .kv-file-upload").removeAttr("disabled"), l._getThumbs(" .kv-file-delete").removeAttr("disabled"), l._setProgress(101, l.$progress, l.msgAjaxProgressError.replace("{operation}", n)));
            }, e.each(s, function (e, i) {
                t.isEmpty(s[e]) || l.formdata.append(l.uploadFileAttr, i, l.filenames[e]);
            }), l._ajaxSubmit(i, a, n, r));
        }, _uploadExtraOnly: function _uploadExtraOnly() {
            var e,
                i,
                a,
                r,
                n = this,
                o = {};n.formdata = new FormData(), n._abort(o) || (e = function e(_e) {
                n.lock();var t = n._getOutData(_e);n._raise("filebatchpreupload", [t]), n._setProgress(50), o.data = t, o.xhr = _e, n._abort(o) && (_e.abort(), n._setProgressCancelled());
            }, i = function i(e, _i3, a) {
                var r = n._getOutData(a, e);t.isEmpty(e) || t.isEmpty(e.error) ? (n._raise("filebatchuploadsuccess", [r]), n._clearFileInput(), n._initUploadSuccess(e), n._setProgress(101)) : n._showUploadError(e.error, r, "filebatchuploaderror");
            }, a = function a() {
                n.unlock(), n._clearFileInput(), n._raise("filebatchuploadcomplete", [n.filestack, n._getExtraData()]);
            }, r = function r(e, t, i) {
                var a = n._getOutData(e),
                    r = n.ajaxOperations.uploadExtra,
                    l = n._parseError(r, e, i);o.data = a, n._showUploadError(l, a, "filebatchuploaderror"), n._setProgress(101, n.$progress, n.msgAjaxProgressError.replace("{operation}", r));
            }, n._ajaxSubmit(e, i, a, r));
        }, _deleteFileIndex: function _deleteFileIndex(i) {
            var a = this,
                r = i.attr("data-fileindex"),
                n = a.reversePreviewOrder;"init_" === r.substring(0, 5) && (r = parseInt(r.replace("init_", "")), a.initialPreview = t.spliceArray(a.initialPreview, r, n), a.initialPreviewConfig = t.spliceArray(a.initialPreviewConfig, r, n), a.initialPreviewThumbTags = t.spliceArray(a.initialPreviewThumbTags, r, n), a.getFrames().each(function () {
                var t = e(this),
                    i = t.attr("data-fileindex");"init_" === i.substring(0, 5) && (i = parseInt(i.replace("init_", "")), i > r && (i--, t.attr("data-fileindex", "init_" + i)));
            }), a.uploadAsync && (a.cacheInitialPreview = a.getPreview()));
        }, _initFileActions: function _initFileActions() {
            var i = this,
                a = i.$preview;i.showPreview && (i._initZoomButton(), i.getFrames(" .kv-file-remove").each(function () {
                var r,
                    n,
                    o,
                    l,
                    s = e(this),
                    d = s.closest(t.FRAMES),
                    c = d.attr("id"),
                    p = d.attr("data-fileindex");i._handler(s, "click", function () {
                    return l = i._raise("filepreremove", [c, p]), l !== !1 && i._validateMinCount() ? (r = d.hasClass("file-preview-error"), t.cleanMemory(d), void d.fadeOut("slow", function () {
                        t.cleanZoomCache(a.find("#zoom-" + c)), i.updateStack(p, void 0), i._clearObjects(d), d.remove(), c && r && i.$errorContainer.find('li[data-file-id="' + c + '"]').fadeOut("fast", function () {
                            e(this).remove(), i._errorsExist() || i._resetErrors();
                        }), i._clearFileInput();var l = i.getFileStack(!0),
                            s = i.previewCache.count(),
                            u = l.length,
                            f = i.showPreview && i.getFrames().length;0 !== u || 0 !== s || f ? (n = s + u, o = n > 1 ? i._getMsgSelected(n) : l[0] ? i._getFileNames()[0] : "", i._setCaption(o)) : i.reset(), i._raise("fileremoved", [c, p]);
                    })) : !1;
                });
            }), i.getFrames(" .kv-file-upload").each(function () {
                var a = e(this);i._handler(a, "click", function () {
                    var e = a.closest(t.FRAMES),
                        r = e.attr("data-fileindex");i.$progress.hide(), e.hasClass("file-preview-error") && !i.retryErrorUploads || i._uploadSingle(r, !1);
                });
            }));
        }, _initPreviewActions: function _initPreviewActions() {
            var i = this,
                a = i.$preview,
                r = i.deleteExtraData || {},
                n = t.FRAMES + " .kv-file-remove",
                o = i.fileActionSettings,
                l = o.removeClass,
                s = o.removeErrorClass,
                d = function d() {
                var e = i.isAjaxUpload ? i.previewCache.count() : i.$element.get(0).files.length;a.find(t.FRAMES).length || e || (i._setCaption(""), i.reset(), i.initialCaption = "");
            };i._initZoomButton(), a.find(n).each(function () {
                var n,
                    o,
                    c,
                    p = e(this),
                    u = p.data("url") || i.deleteUrl,
                    f = p.data("key");if (!t.isEmpty(u) && void 0 !== f) {
                    var m,
                        v,
                        g,
                        h,
                        w = p.closest(t.FRAMES),
                        _ = i.previewCache.data,
                        b = w.attr("data-fileindex");b = parseInt(b.replace("init_", "")), g = t.isEmpty(_.config) && t.isEmpty(_.config[b]) ? null : _.config[b], h = t.isEmpty(g) || t.isEmpty(g.extra) ? r : g.extra, "function" == typeof h && (h = h()), v = { id: p.attr("id"), key: f, extra: h }, n = function n(e) {
                        i.ajaxAborted = !1, i._raise("filepredelete", [f, e, h]), i._abort() ? e.abort() : (p.removeClass(s), t.addCss(w, "file-uploading"), t.addCss(p, "disabled " + l));
                    }, o = function o(e, r, n) {
                        var o, c;return t.isEmpty(e) || t.isEmpty(e.error) ? (w.removeClass("file-uploading").addClass("file-deleted"), void w.fadeOut("slow", function () {
                            b = parseInt(w.attr("data-fileindex").replace("init_", "")), i.previewCache.unset(b), i._deleteFileIndex(w), o = i.previewCache.count(), c = o > 0 ? i._getMsgSelected(o) : "", i._setCaption(c), i._raise("filedeleted", [f, n, h]), t.cleanZoomCache(a.find("#zoom-" + w.attr("id"))), i._clearObjects(w), w.remove(), d();
                        })) : (v.jqXHR = n, v.response = e, i._showError(e.error, v, "filedeleteerror"), w.removeClass("file-uploading"), p.removeClass("disabled " + l).addClass(s), void d());
                    }, c = function c(e, t, a) {
                        var r = i.ajaxOperations.deleteThumb,
                            n = i._parseError(r, e, a);v.jqXHR = e, v.response = {}, i._showError(n, v, "filedeleteerror"), w.removeClass("file-uploading"), p.removeClass("disabled " + l).addClass(s), d();
                    }, i._mergeAjaxCallback("beforeSend", n, "delete"), i._mergeAjaxCallback("success", o, "delete"), i._mergeAjaxCallback("error", c, "delete"), m = e.extend(!0, {}, { url: u, type: "POST", dataType: "json", data: e.extend(!0, {}, { key: f }, h) }, i.ajaxDeleteSettings), i._handler(p, "click", function () {
                        return i._validateMinCount() ? (i.ajaxAborted = !1, i._raise("filebeforedelete", [f, h]), void (i.ajaxAborted instanceof Promise ? i.ajaxAborted.then(function (t) {
                            t || e.ajax(m);
                        }) : i.ajaxAborted || e.ajax(m))) : !1;
                    });
                }
            });
        }, _hideFileIcon: function _hideFileIcon() {
            var e = this;e.overwriteInitial && e.$captionContainer.removeClass("icon-visible");
        }, _showFileIcon: function _showFileIcon() {
            var e = this;t.addCss(e.$captionContainer, "icon-visible");
        }, _getSize: function _getSize(t) {
            var i,
                a,
                r,
                n = this,
                o = parseFloat(t),
                l = n.fileSizeGetter;return e.isNumeric(t) && e.isNumeric(o) ? ("function" == typeof l ? r = l(o) : 0 === o ? r = "0.00 B" : (i = Math.floor(Math.log(o) / Math.log(1024)), a = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], r = 1 * (o / Math.pow(1024, i)).toFixed(2) + " " + a[i]), n._getLayoutTemplate("size").replace("{sizeText}", r)) : "";
        }, _generatePreviewTemplate: function _generatePreviewTemplate(i, a, r, n, o, l, s, d, c, p, u) {
            var f,
                m,
                v = this,
                g = v.slug(r),
                h = "",
                w = "",
                _ = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
                b = 400 > _ ? v.previewSettingsSmall[i] || v.defaults.previewSettingsSmall[i] : v.previewSettings[i] || v.defaults.previewSettings[i],
                C = c || v._renderFileFooter(g, s, "auto", l),
                y = v._getPreviewIcon(r),
                x = "type-default",
                T = y && v.preferIconicPreview,
                E = y && v.preferIconicZoomPreview;return b && e.each(b, function (e, t) {
                w += e + ":" + t + ";";
            }), m = function m(a, l, s, c) {
                var f = s ? "zoom-" + o : o,
                    m = v._getPreviewTemplate(a),
                    h = (d || "") + " " + c;return v.frameClass && (h = v.frameClass + " " + h), s && (h = h.replace(" " + t.SORT_CSS, "")), m = v._parseFilePreviewIcon(m, r), "text" === a && (l = t.htmlEncode(l)), "object" !== i || n || e.each(v.defaults.fileTypeSettings, function (e, t) {
                    "object" !== e && "other" !== e && t(r, n) && (x = "type-" + e);
                }), m.setTokens({ previewId: f, caption: g, frameClass: h, type: n, fileindex: p, typeCss: x, footer: C, data: l, template: u || i, style: w ? 'style="' + w + '"' : "" });
            }, p = p || o.slice(o.lastIndexOf("-") + 1), v.fileActionSettings.showZoom && (h = m(E ? "other" : i, a, !0, "kv-zoom-thumb")), h = "\n" + v._getLayoutTemplate("zoomCache").replace("{zoomContent}", h), f = m(T ? "other" : i, a, !1, "kv-preview-thumb"), f + h;
        }, _addToPreview: function _addToPreview(e, t) {
            var i = this;return i.reversePreviewOrder ? e.prepend(t) : e.append(t);
        }, _previewDefault: function _previewDefault(i, a, r) {
            var n = this,
                o = n.$preview;if (n.showPreview) {
                var l,
                    s = i ? i.name : "",
                    d = i ? i.type : "",
                    c = i.size || 0,
                    p = n.slug(s),
                    u = r === !0 && !n.isAjaxUpload,
                    f = t.objUrl.createObjectURL(i);n._clearDefaultPreview(), l = n._generatePreviewTemplate("other", f, s, d, a, u, c), n._addToPreview(o, l), n._setThumbAttr(a, p, c), r === !0 && n.isAjaxUpload && n._setThumbStatus(e("#" + a), "Error");
            }
        }, _previewFile: function _previewFile(e, i, a, r, n, o) {
            if (this.showPreview) {
                var l,
                    s = this,
                    d = i ? i.name : "",
                    c = o.type,
                    p = o.name,
                    u = s._parseFileType(c, d),
                    f = s.allowedPreviewTypes,
                    m = s.allowedPreviewMimeTypes,
                    v = s.$preview,
                    g = i.size || 0,
                    h = f && f.indexOf(u) >= 0,
                    w = m && -1 !== m.indexOf(c),
                    _ = "text" === u || "html" === u || "image" === u ? a.target.result : n;if ("html" === u && s.purifyHtml && window.DOMPurify && (_ = window.DOMPurify.sanitize(_)), h || w) {
                    l = s._generatePreviewTemplate(u, _, d, c, r, !1, g), s._clearDefaultPreview(), s._addToPreview(v, l);var b = v.find("#" + r + " img");b.length && s.autoOrientImage ? t.validateOrientation(i, function (e) {
                        if (!e) return void s._validateImage(r, p, c, g, _);var a = v.find("#zoom-" + r + " img"),
                            n = "rotate-" + e;e > 4 && (n += b.width() > b.height() ? " is-portrait-gt4" : " is-landscape-gt4"), t.addCss(b, n), t.addCss(a, n), s._raise("fileimageoriented", { $img: b, file: i }), s._validateImage(r, p, c, g, _), t.adjustOrientedImage(b);
                    }) : s._validateImage(r, p, c, g, _);
                } else s._previewDefault(i, r);s._setThumbAttr(r, p, g), s._initSortable();
            }
        }, _setThumbAttr: function _setThumbAttr(t, i, a) {
            var r = this,
                n = e("#" + t);n.length && (a = a && a > 0 ? r._getSize(a) : "", n.data({ caption: i, size: a }));
        }, _setInitThumbAttr: function _setInitThumbAttr() {
            var e,
                i,
                a,
                r,
                n = this,
                o = n.previewCache.data,
                l = n.previewCache.count();if (0 !== l) for (var s = 0; l > s; s++) {
                e = o.config[s], r = n.previewInitId + "-init_" + s, i = t.ifSet("caption", e, t.ifSet("filename", e)), a = t.ifSet("size", e), n._setThumbAttr(r, i, a);
            }
        }, _slugDefault: function _slugDefault(e) {
            return t.isEmpty(e) ? "" : String(e).replace(/[\[\]\/\{}:;#%=\(\)\*\+\?\\\^\$\|<>&"']/g, "_");
        }, _updateFileDetails: function _updateFileDetails(e) {
            var i = this,
                a = i.$element,
                r = i.getFileStack(),
                n = t.isIE(9) && t.findFileName(a.val()) || a[0].files[0] && a[0].files[0].name || r.length && r[0].name || "",
                o = i.slug(n),
                l = i.isAjaxUpload ? r.length : e,
                s = i.previewCache.count() + l,
                d = 1 === l ? o : i._getMsgSelected(s);i.isError ? (i.$previewContainer.removeClass("file-thumb-loading"), i.$previewStatus.html(""), i.$captionContainer.removeClass("icon-visible")) : i._showFileIcon(), i._setCaption(d, i.isError), i.$container.removeClass("file-input-new file-input-ajax-new"), 1 === arguments.length && i._raise("fileselect", [e, o]), i.previewCache.count() && i._initPreviewActions();
        }, _setThumbStatus: function _setThumbStatus(e, t) {
            var i = this;if (i.showPreview) {
                var a = "indicator" + t,
                    r = a + "Title",
                    n = "file-preview-" + t.toLowerCase(),
                    o = e.find(".file-upload-indicator"),
                    l = i.fileActionSettings;e.removeClass("file-preview-success file-preview-error file-preview-loading"), "Success" === t && e.find(".file-drag-handle").remove(), o.html(l[a]), o.attr("title", l[r]), e.addClass(n), "Error" !== t || i.retryErrorUploads || e.find(".kv-file-upload").attr("disabled", !0);
            }
        }, _setProgressCancelled: function _setProgressCancelled() {
            var e = this;e._setProgress(101, e.$progress, e.msgCancelled);
        }, _setProgress: function _setProgress(e, i, a) {
            var r,
                n = this,
                o = Math.min(e, 100),
                l = n.progressUploadThreshold,
                s = 100 >= e ? n.progressTemplate : n.progressCompleteTemplate,
                d = 100 > o ? n.progressTemplate : a ? n.progressErrorTemplate : s;i = i || n.$progress, t.isEmpty(d) || (r = l && o > l && 100 >= e ? d.setTokens({ percent: l, status: n.msgUploadThreshold }) : d.setTokens({ percent: o, status: e > 100 ? n.msgUploadEnd : o + "%" }), i.html(r), a && i.find('[role="progressbar"]').html(a));
        }, _setFileDropZoneTitle: function _setFileDropZoneTitle() {
            var e,
                i = this,
                a = i.$container.find(".file-drop-zone"),
                r = i.dropZoneTitle;i.isClickable && (e = t.isEmpty(i.$element.attr("multiple")) ? i.fileSingle : i.filePlural, r += i.dropZoneClickTitle.replace("{files}", e)), a.find("." + i.dropZoneTitleClass).remove(), i.isAjaxUpload && i.showPreview && 0 !== a.length && !(i.getFileStack().length > 0) && i.dropZoneEnabled && (0 === a.find(t.FRAMES).length && t.isEmpty(i.defaultPreviewContent) && a.prepend('<div class="' + i.dropZoneTitleClass + '">' + r + "</div>"), i.$container.removeClass("file-input-new"), t.addCss(i.$container, "file-input-ajax-new"));
        }, _setAsyncUploadStatus: function _setAsyncUploadStatus(t, i, a) {
            var r = this,
                n = 0;r._setProgress(i, e("#" + t).find(".file-thumb-progress")), r.uploadStatus[t] = i, e.each(r.uploadStatus, function (e, t) {
                n += t;
            }), r._setProgress(Math.floor(n / a));
        }, _validateMinCount: function _validateMinCount() {
            var e = this,
                t = e.isAjaxUpload ? e.getFileStack().length : e.$element.get(0).files.length;return e.validateInitialCount && e.minFileCount > 0 && e._getFileCount(t - 1) < e.minFileCount ? (e._noFilesError({}), !1) : !0;
        }, _getFileCount: function _getFileCount(e) {
            var t = this,
                i = 0;return t.validateInitialCount && !t.overwriteInitial && (i = t.previewCache.count(), e += i), e;
        }, _getFileId: function _getFileId(e) {
            var t,
                i = this,
                a = i.generateFileId;return "function" == typeof a ? a(e, event) : e ? (t = String(e.webkitRelativePath || e.fileName || e.name || null), t ? e.size + "-" + t.replace(/[^0-9a-zA-Z_-]/gim, "") : null) : null;
        }, _getFileName: function _getFileName(e) {
            return e && e.name ? this.slug(e.name) : void 0;
        }, _getFileIds: function _getFileIds(e) {
            var t = this;return t.fileids.filter(function (t) {
                return e ? void 0 !== t : void 0 !== t && null !== t;
            });
        }, _getFileNames: function _getFileNames(e) {
            var t = this;return t.filenames.filter(function (t) {
                return e ? void 0 !== t : void 0 !== t && null !== t;
            });
        }, _setPreviewError: function _setPreviewError(e, t, i, a) {
            var r = this;if (void 0 !== t && r.updateStack(t, i), r.showPreview) {
                if (r.removeFromPreviewOnError && !a) return void e.remove();r._setThumbStatus(e, "Error"), r._refreshUploadButton(e, a);
            }
        }, _refreshUploadButton: function _refreshUploadButton(e, t) {
            var i = this,
                a = e.find(".kv-file-upload"),
                r = i.fileActionSettings,
                n = r.uploadIcon,
                o = r.uploadTitle;a.length && (t && (n = r.uploadRetryIcon, o = r.uploadRetryTitle), a.attr("title", o).html(n));
        }, _checkDimensions: function _checkDimensions(e, i, a, r, n, o, l) {
            var s,
                d,
                c,
                p,
                u = this,
                f = "Small" === i ? "min" : "max",
                m = u[f + "Image" + o];!t.isEmpty(m) && a.length && (c = a[0], d = "Width" === o ? c.naturalWidth || c.width : c.naturalHeight || c.height, p = "Small" === i ? d >= m : m >= d, p || (s = u["msgImage" + o + i].setTokens({ name: n, size: m }), u._showUploadError(s, l), u._setPreviewError(r, e, null)));
        }, _validateImage: function _validateImage(t, i, a, r, n) {
            var o,
                l,
                s,
                d,
                c = this,
                p = c.$preview,
                u = p.find("#" + t),
                f = u.attr("data-fileindex"),
                m = u.find("img");i = i || "Untitled", m.one("load", function () {
                l = u.width(), s = p.width(), l > s && m.css("width", "100%"), o = { ind: f, id: t }, c._checkDimensions(f, "Small", m, u, i, "Width", o), c._checkDimensions(f, "Small", m, u, i, "Height", o), c.resizeImage || (c._checkDimensions(f, "Large", m, u, i, "Width", o), c._checkDimensions(f, "Large", m, u, i, "Height", o)), c._raise("fileimageloaded", [t]);try {
                    d = window.piexif ? window.piexif.load(n) : null;
                } catch (e) {
                    d = null;
                }c.loadedImages.push({ ind: f, img: m, thumb: u, pid: t, typ: a, siz: r, validated: !1, imgData: n, exifObj: d }), u.data("exif", d), c._validateAllImages();
            }).one("error", function () {
                c._raise("fileimageloaderror", [t]);
            }).each(function () {
                this.complete ? e(this).trigger("load") : this.error && e(this).trigger("error");
            });
        }, _validateAllImages: function _validateAllImages() {
            var e,
                t,
                i,
                a = this,
                r = { val: 0 },
                n = a.loadedImages.length,
                o = a.resizeIfSizeMoreThan;if (n === a.totalImagesCount && (a._raise("fileimagesloaded"), a.resizeImage)) for (e = 0; e < a.loadedImages.length; e++) {
                t = a.loadedImages[e], t.validated || (i = t.siz, i && i > 1e3 * o && a._getResizedImage(t, r, n), a.loadedImages[e].validated = !0);
            }
        }, _getResizedImage: function _getResizedImage(i, a, r) {
            var n,
                o,
                l,
                s,
                d,
                c,
                p,
                u = this,
                f = e(i.img)[0],
                m = f.naturalWidth,
                v = f.naturalHeight,
                g = 1,
                h = u.maxImageWidth || m,
                w = u.maxImageHeight || v,
                _ = !(!m || !v),
                b = u.imageCanvas,
                C = u.imageCanvasContext,
                y = i.typ,
                x = i.pid,
                T = i.ind,
                E = i.thumb,
                S = i.exifObj;if (d = function d(e, t, i) {
                u.isAjaxUpload ? u._showUploadError(e, t, i) : u._showError(e, t, i), u._setPreviewError(E, T);
            }, (!u.filestack[T] || !_ || h >= m && w >= v) && (_ && u.filestack[T] && u._raise("fileimageresized", [x, T]), a.val++, a.val === r && u._raise("fileimagesresized"), !_)) return void d(u.msgImageResizeError, { id: x, index: T }, "fileimageresizeerror");y = y || u.resizeDefaultImageType, o = m > h, l = v > w, g = "width" === u.resizePreference ? o ? h / m : l ? w / v : 1 : l ? w / v : o ? h / m : 1, u._resetCanvas(), m *= g, v *= g, b.width = m, b.height = v;try {
                C.drawImage(f, 0, 0, m, v), s = b.toDataURL(y, u.resizeQuality), S && (p = window.piexif.dump(S), s = window.piexif.insert(p, s)), n = t.dataURI2Blob(s), u.filestack[T] = n, u._raise("fileimageresized", [x, T]), a.val++, a.val === r && u._raise("fileimagesresized", [void 0, void 0]), n instanceof Blob || d(u.msgImageResizeError, { id: x, index: T }, "fileimageresizeerror");
            } catch (k) {
                a.val++, a.val === r && u._raise("fileimagesresized", [void 0, void 0]), c = u.msgImageResizeException.replace("{errors}", k.message), d(c, { id: x, index: T }, "fileimageresizeexception");
            }
        }, _initBrowse: function _initBrowse(e) {
            var t = this;t.showBrowse ? (t.$btnFile = e.find(".btn-file"), t.$btnFile.append(t.$element)) : t.$element.hide();
        }, _initCaption: function _initCaption() {
            var e = this,
                i = e.initialCaption || "";return e.overwriteInitial || t.isEmpty(i) ? (e.$caption.val(""), !1) : (e._setCaption(i), !0);
        }, _setCaption: function _setCaption(i, a) {
            var r,
                n,
                o,
                l,
                s,
                d = this,
                c = d.getFileStack();if (d.$caption.length) {
                if (d.$captionContainer.removeClass("icon-visible"), a) r = e("<div>" + d.msgValidationError + "</div>").text(), l = c.length, s = l ? 1 === l && c[0] ? d._getFileNames()[0] : d._getMsgSelected(l) : d._getMsgSelected(d.msgNo), n = t.isEmpty(i) ? s : i, o = '<span class="' + d.msgValidationErrorClass + '">' + d.msgValidationErrorIcon + "</span>";else {
                    if (t.isEmpty(i)) return;r = e("<div>" + i + "</div>").text(), n = r, o = d._getLayoutTemplate("fileIcon");
                }d.$captionContainer.addClass("icon-visible"), d.$caption.attr("title", r).val(n), d.$captionIcon.html(o);
            }
        }, _createContainer: function _createContainer() {
            var t = this,
                i = { "class": "file-input file-input-new" + (t.rtl ? " kv-rtl" : "") },
                a = e(document.createElement("div")).attr(i).html(t._renderMain());return t.$element.before(a), t._initBrowse(a), t.theme && a.addClass("theme-" + t.theme), a;
        }, _refreshContainer: function _refreshContainer() {
            var e = this,
                t = e.$container;t.before(e.$element), t.html(e._renderMain()), e._initBrowse(t), e._validateDisabled();
        }, _validateDisabled: function _validateDisabled() {
            var e = this;e.$caption.attr({ readonly: e.isDisabled });
        }, _renderMain: function _renderMain() {
            var e = this,
                t = e.isAjaxUpload && e.dropZoneEnabled ? " file-drop-zone" : "file-drop-disabled",
                i = e.showClose ? e._getLayoutTemplate("close") : "",
                a = e.showPreview ? e._getLayoutTemplate("preview").setTokens({ "class": e.previewClass, dropClass: t }) : "",
                r = e.isDisabled ? e.captionClass + " file-caption-disabled" : e.captionClass,
                n = e.captionTemplate.setTokens({ "class": r + " kv-fileinput-caption" });return e.mainTemplate.setTokens({ "class": e.mainClass + (!e.showBrowse && e.showCaption ? " no-browse" : ""), preview: a, close: i, caption: n, upload: e._renderButton("upload"), remove: e._renderButton("remove"), cancel: e._renderButton("cancel"), browse: e._renderButton("browse") });
        }, _renderButton: function _renderButton(e) {
            var i = this,
                a = i._getLayoutTemplate("btnDefault"),
                r = i[e + "Class"],
                n = i[e + "Title"],
                o = i[e + "Icon"],
                l = i[e + "Label"],
                s = i.isDisabled ? " disabled" : "",
                d = "button";switch (e) {case "remove":
                    if (!i.showRemove) return "";break;case "cancel":
                    if (!i.showCancel) return "";r += " kv-hidden";break;case "upload":
                    if (!i.showUpload) return "";i.isAjaxUpload && !i.isDisabled ? a = i._getLayoutTemplate("btnLink").replace("{href}", i.uploadUrl) : d = "submit";break;case "browse":
                    if (!i.showBrowse) return "";a = i._getLayoutTemplate("btnBrowse");break;default:
                    return "";}return r += "browse" === e ? " btn-file" : " fileinput-" + e + " fileinput-" + e + "-button", t.isEmpty(l) || (l = ' <span class="' + i.buttonLabelClass + '">' + l + "</span>"), a.setTokens({ type: d, css: r, title: n, status: s, icon: o, label: l });
        }, _renderThumbProgress: function _renderThumbProgress() {
            var e = this;return '<div class="file-thumb-progress kv-hidden">' + e.progressTemplate.setTokens({ percent: "0", status: e.msgUploadBegin }) + "</div>";
        }, _renderFileFooter: function _renderFileFooter(e, i, a, r) {
            var n,
                o = this,
                l = o.fileActionSettings,
                s = l.showRemove,
                d = l.showDrag,
                c = l.showUpload,
                p = l.showZoom,
                u = o._getLayoutTemplate("footer"),
                f = o._getLayoutTemplate("indicator"),
                m = r ? l.indicatorError : l.indicatorNew,
                v = r ? l.indicatorErrorTitle : l.indicatorNewTitle,
                g = f.setTokens({ indicator: m, indicatorTitle: v });return i = o._getSize(i), n = o.isAjaxUpload ? u.setTokens({ actions: o._renderFileActions(c, !1, s, p, d, !1, !1, !1), caption: e, size: i, width: a, progress: o._renderThumbProgress(), indicator: g }) : u.setTokens({ actions: o._renderFileActions(!1, !1, !1, p, d, !1, !1, !1), caption: e, size: i, width: a, progress: "", indicator: g }), n = t.replaceTags(n, o.previewThumbTags);
        }, _renderFileActions: function _renderFileActions(e, t, i, a, r, n, o, l, s, d, c) {
            if (!(e || t || i || a || r)) return "";var p,
                u = this,
                f = o === !1 ? "" : ' data-url="' + o + '"',
                m = l === !1 ? "" : ' data-key="' + l + '"',
                v = "",
                g = "",
                h = "",
                w = "",
                _ = "",
                b = u._getLayoutTemplate("actions"),
                C = u.fileActionSettings,
                y = u.otherActionButtons.setTokens({ dataKey: m, key: l }),
                x = n ? C.removeClass + " disabled" : C.removeClass;return i && (v = u._getLayoutTemplate("actionDelete").setTokens({ removeClass: x, removeIcon: C.removeIcon, removeTitle: C.removeTitle, dataUrl: f, dataKey: m, key: l })), e && (g = u._getLayoutTemplate("actionUpload").setTokens({ uploadClass: C.uploadClass, uploadIcon: C.uploadIcon, uploadTitle: C.uploadTitle })), t && (h = u._getLayoutTemplate("actionDownload").setTokens({ downloadClass: C.downloadClass, downloadIcon: C.downloadIcon, downloadTitle: C.downloadTitle, downloadUrl: d || u.initialPreviewDownloadUrl }), h = h.setTokens({ filename: c, key: l })), a && (w = u._getLayoutTemplate("actionZoom").setTokens({ zoomClass: C.zoomClass, zoomIcon: C.zoomIcon, zoomTitle: C.zoomTitle })), r && s && (p = "drag-handle-init " + C.dragClass, _ = u._getLayoutTemplate("actionDrag").setTokens({ dragClass: p, dragTitle: C.dragTitle, dragIcon: C.dragIcon })), b.setTokens({ "delete": v, upload: g, download: h, zoom: w, drag: _, other: y });
        }, _browse: function _browse(e) {
            var t = this;t._raise("filebrowse"), e && e.isDefaultPrevented() || (t.isError && !t.isAjaxUpload && t.clear(), t.$captionContainer.focus());
        }, _filterDuplicate: function _filterDuplicate(e, t, i) {
            var a = this,
                r = a._getFileId(e);r && i && i.indexOf(r) > -1 || (i || (i = []), t.push(e), i.push(r));
        }, _change: function _change(i) {
            var a = this,
                r = a.$element;if (!a.isAjaxUpload && t.isEmpty(r.val()) && a.fileInputCleared) return void (a.fileInputCleared = !1);a.fileInputCleared = !1;var n,
                o,
                l,
                s,
                d = [],
                c = arguments.length > 1,
                p = a.isAjaxUpload,
                u = c ? i.originalEvent.dataTransfer.files : r.get(0).files,
                f = a.filestack.length,
                m = t.isEmpty(r.attr("multiple")),
                v = m && f > 0,
                g = 0,
                h = a._getFileIds(),
                w = function w(t, i, r, n) {
                var o = e.extend(!0, {}, a._getOutData({}, {}, u), { id: r, index: n }),
                    l = { id: r, index: n, file: i, files: u };return a.isAjaxUpload ? a._showUploadError(t, o) : a._showError(t, l);
            };if (a.reader = null, a._resetUpload(), a._hideFileIcon(), a.isAjaxUpload && a.$container.find(".file-drop-zone ." + a.dropZoneTitleClass).remove(), c ? e.each(u, function (e, t) {
                t && !t.type && void 0 !== t.size && t.size % 4096 === 0 ? g++ : a._filterDuplicate(t, d, h);
            }) : (u = i.target && void 0 === i.target.files ? i.target.value ? [{ name: i.target.value.replace(/^.+\\/, "") }] : [] : i.target.files || {}, p ? e.each(u, function (e, t) {
                a._filterDuplicate(t, d, h);
            }) : d = u), t.isEmpty(d) || 0 === d.length) return p || a.clear(), a._showFolderError(g), void a._raise("fileselectnone");if (a._resetErrors(), s = d.length, o = a._getFileCount(a.isAjaxUpload ? a.getFileStack().length + s : s), a.maxFileCount > 0 && o > a.maxFileCount) {
                if (!a.autoReplace || s > a.maxFileCount) return l = a.autoReplace && s > a.maxFileCount ? s : o, n = a.msgFilesTooMany.replace("{m}", a.maxFileCount).replace("{n}", l), a.isError = w(n, null, null, null), a.$captionContainer.removeClass("icon-visible"), a._setCaption("", !0), void a.$container.removeClass("file-input-new file-input-ajax-new");o > a.maxFileCount && a._resetPreviewThumbs(p);
            } else !p || v ? (a._resetPreviewThumbs(!1), v && a.clearStack()) : !p || 0 !== f || a.previewCache.count() && !a.overwriteInitial || a._resetPreviewThumbs(!0);a.isPreviewable ? a.readFiles(d) : a._updateFileDetails(1), a._showFolderError(g);
        }, _abort: function _abort(t) {
            var i,
                a = this;return a.ajaxAborted && "object" == _typeof(a.ajaxAborted) && void 0 !== a.ajaxAborted.message ? (i = e.extend(!0, {}, a._getOutData(), t), i.abortData = a.ajaxAborted.data || {}, i.abortMessage = a.ajaxAborted.message, a._setProgress(101, a.$progress, a.msgCancelled), a._showUploadError(a.ajaxAborted.message, i, "filecustomerror"), a.cancel(), !0) : !!a.ajaxAborted;
        }, _resetFileStack: function _resetFileStack() {
            var i = this,
                a = 0,
                r = [],
                n = [],
                o = [];i._getThumbs().each(function () {
                var l,
                    s = e(this),
                    d = s.attr("data-fileindex"),
                    c = i.filestack[d],
                    p = s.attr("id");"-1" !== d && -1 !== d && (void 0 !== c ? (r[a] = c, n[a] = i._getFileName(c), o[a] = i._getFileId(c), s.attr({ id: i.previewInitId + "-" + a, "data-fileindex": a }), a++) : (l = "uploaded-" + t.uniqId(), s.attr({ id: l, "data-fileindex": "-1" }), i.$preview.find("#zoom-" + p).attr("id", "zoom-" + l)));
            }), i.filestack = r, i.filenames = n, i.fileids = o;
        }, _isFileSelectionValid: function _isFileSelectionValid(e) {
            var t = this;return e = e || 0, t.required && !t.getFilesCount() ? (t.$errorContainer.html(""), t._showUploadError(t.msgFileRequired), !1) : t.minFileCount > 0 && t._getFileCount(e) < t.minFileCount ? (t._noFilesError({}), !1) : !0;
        }, clearStack: function clearStack() {
            var e = this;return e.filestack = [], e.filenames = [], e.fileids = [], e.$element;
        }, updateStack: function updateStack(e, t) {
            var i = this;return i.filestack[e] = t, i.filenames[e] = i._getFileName(t), i.fileids[e] = t && i._getFileId(t) || null, i.$element;
        }, addToStack: function addToStack(e) {
            var t = this;return t.filestack.push(e), t.filenames.push(t._getFileName(e)), t.fileids.push(t._getFileId(e)), t.$element;
        }, getFileStack: function getFileStack(e) {
            var t = this;return t.filestack.filter(function (t) {
                return e ? void 0 !== t : void 0 !== t && null !== t;
            });
        }, getFilesCount: function getFilesCount() {
            var e = this,
                t = e.isAjaxUpload ? e.getFileStack().length : e.$element.get(0).files.length;return e._getFileCount(t);
        }, readFiles: function readFiles(i) {
            this.reader = new FileReader();var _a2,
                r = this,
                n = r.$element,
                o = r.$preview,
                l = r.reader,
                s = r.$previewContainer,
                d = r.$previewStatus,
                c = r.msgLoading,
                p = r.msgProgress,
                u = r.previewInitId,
                f = i.length,
                m = r.fileTypeSettings,
                v = r.filestack.length,
                g = r.allowedFileTypes,
                h = g ? g.length : 0,
                w = r.allowedFileExtensions,
                _ = t.isEmpty(w) ? "" : w.join(", "),
                b = r.maxFilePreviewSize && parseFloat(r.maxFilePreviewSize),
                C = o.length && (!b || isNaN(b)),
                y = function y(t, n, o, l) {
                var s,
                    d = e.extend(!0, {}, r._getOutData({}, {}, i), { id: o, index: l }),
                    c = { id: o, index: l, file: n, files: i };r._previewDefault(n, o, !0), r.isAjaxUpload ? (r.addToStack(void 0), setTimeout(function () {
                    _a2(l + 1);
                }, 100)) : f = 0, r._initFileActions(), s = e("#" + o), s.find(".kv-file-upload").hide(), r.removeFromPreviewOnError && s.remove(), r.isError = r.isAjaxUpload ? r._showUploadError(t, d) : r._showError(t, c), r._updateFileDetails(f);
            };r.loadedImages = [], r.totalImagesCount = 0, e.each(i, function (e, t) {
                var i = r.fileTypeSettings.image;i && i(t.type) && r.totalImagesCount++;
            }), _a2 = function a(x) {
                if (t.isEmpty(n.attr("multiple")) && (f = 1), x >= f) return r.isAjaxUpload && r.filestack.length > 0 ? r._raise("filebatchselected", [r.getFileStack()]) : r._raise("filebatchselected", [i]), s.removeClass("file-thumb-loading"), void d.html("");var T,
                    E,
                    S,
                    k,
                    F,
                    P,
                    I,
                    A,
                    z,
                    D,
                    $,
                    j,
                    U = v + x,
                    B = u + "-" + U,
                    O = i[x],
                    R = m.text,
                    L = m.image,
                    M = m.html,
                    Z = O.name ? r.slug(O.name) : "",
                    N = (O.size || 0) / 1e3,
                    H = "",
                    V = t.objUrl.createObjectURL(O),
                    W = 0,
                    q = "",
                    K = 0,
                    Y = function Y() {
                    var e = p.setTokens({ index: x + 1, files: f, percent: 50, name: Z });setTimeout(function () {
                        d.html(e), r._updateFileDetails(f), _a2(x + 1);
                    }, 100), r._raise("fileloaded", [O, B, x, l]);
                };if (h > 0) for (E = 0; h > E; E++) {
                    P = g[E], I = r.msgFileTypes[P] || P, q += 0 === E ? I : ", " + I;
                }if (Z === !1) return void _a2(x + 1);if (0 === Z.length) return S = r.msgInvalidFileName.replace("{name}", t.htmlEncode(O.name)), void y(S, O, B, x);if (t.isEmpty(w) || (H = new RegExp("\\.(" + w.join("|") + ")$", "i")), T = N.toFixed(2), r.maxFileSize > 0 && N > r.maxFileSize) return S = r.msgSizeTooLarge.setTokens({ name: Z, size: T, maxSize: r.maxFileSize }), void y(S, O, B, x);if (null !== r.minFileSize && N <= t.getNum(r.minFileSize)) return S = r.msgSizeTooSmall.setTokens({ name: Z, size: T, minSize: r.minFileSize }), void y(S, O, B, x);if (!t.isEmpty(g) && t.isArray(g)) {
                    for (E = 0; E < g.length; E += 1) {
                        k = g[E], A = m[k], W += A && "function" == typeof A && A(O.type, O.name) ? 1 : 0;
                    }if (0 === W) return S = r.msgInvalidFileType.setTokens({ name: Z, types: q }), void y(S, O, B, x);
                }return 0 !== W || t.isEmpty(w) || !t.isArray(w) || t.isEmpty(H) || (F = t.compare(Z, H), W += t.isEmpty(F) ? 0 : F.length, 0 !== W) ? r.showPreview ? !C && N > b ? (r.addToStack(O), s.addClass("file-thumb-loading"), r._previewDefault(O, B), r._initFileActions(), r._updateFileDetails(f), void _a2(x + 1)) : (o.length && void 0 !== FileReader ? (z = R(O.type, Z), D = M(O.type, Z), $ = L(O.type, Z), d.html(c.replace("{index}", x + 1).replace("{files}", f)), s.addClass("file-thumb-loading"), l.onerror = function (e) {
                    r._errorHandler(e, Z);
                }, l.onload = function (i) {
                    var a,
                        n,
                        o,
                        s,
                        d,
                        c,
                        p = [],
                        u = function u(e) {
                        var t = new FileReader();t.onerror = function (e) {
                            r._errorHandler(e, Z);
                        }, t.onload = function (e) {
                            r._previewFile(x, O, e, B, V, n), r._initFileActions(), Y();
                        }, e ? t.readAsText(O, r.textEncoding) : t.readAsDataURL(O);
                    };if (n = { name: Z, type: O.type }, e.each(m, function (e, t) {
                        "object" !== e && "other" !== e && t(O.type, Z) && K++;
                    }), 0 === K) {
                        for (o = new Uint8Array(i.target.result), E = 0; E < o.length; E++) {
                            s = o[E].toString(16), p.push(s);
                        }if (a = p.join("").toLowerCase().substring(0, 8), c = t.getMimeType(a, "", ""), t.isEmpty(c) && (d = t.arrayBuffer2String(l.result), c = t.isSvg(d) ? "image/svg+xml" : t.getMimeType(a, d, O.type)), n = { name: Z, type: c }, z = R(c, ""), D = M(c, ""), $ = L(c, ""), j = z || D, j || $) return void u(j);
                    }r._previewFile(x, O, i, B, V, n), r._initFileActions(), Y();
                }, l.onprogress = function (e) {
                    if (e.lengthComputable) {
                        var t = e.loaded / e.total * 100,
                            i = Math.ceil(t);S = p.setTokens({ index: x + 1, files: f, percent: i, name: Z }), setTimeout(function () {
                            d.html(S);
                        }, 100);
                    }
                }, z || D ? l.readAsText(O, r.textEncoding) : $ ? l.readAsDataURL(O) : l.readAsArrayBuffer(O)) : (r._previewDefault(O, B), setTimeout(function () {
                    _a2(x + 1), r._updateFileDetails(f);
                }, 100), r._raise("fileloaded", [O, B, x, l])), void r.addToStack(O)) : (r.isAjaxUpload && r.addToStack(O), setTimeout(function () {
                    _a2(x + 1), r._updateFileDetails(f);
                }, 100), void r._raise("fileloaded", [O, B, x, l])) : (S = r.msgInvalidFileExtension.setTokens({ name: Z, extensions: _ }), void y(S, O, B, x));
            }, _a2(0), r._updateFileDetails(f, !1);
        }, lock: function lock() {
            var e = this;return e._resetErrors(), e.disable(), e.showRemove && e.$container.find(".fileinput-remove").hide(), e.showCancel && e.$container.find(".fileinput-cancel").show(), e._raise("filelock", [e.filestack, e._getExtraData()]), e.$element;
        }, unlock: function unlock(e) {
            var t = this;return void 0 === e && (e = !0), t.enable(), t.showCancel && t.$container.find(".fileinput-cancel").hide(), t.showRemove && t.$container.find(".fileinput-remove").show(), e && t._resetFileStack(), t._raise("fileunlock", [t.filestack, t._getExtraData()]), t.$element;
        }, cancel: function cancel() {
            var t,
                i = this,
                a = i.ajaxRequests,
                r = a.length;if (r > 0) for (t = 0; r > t; t += 1) {
                i.cancelling = !0, a[t].abort();
            }return i._setProgressCancelled(), i._getThumbs().each(function () {
                var t = e(this),
                    a = t.attr("data-fileindex");t.removeClass("file-uploading"), void 0 !== i.filestack[a] && (t.find(".kv-file-upload").removeClass("disabled").removeAttr("disabled"), t.find(".kv-file-remove").removeClass("disabled").removeAttr("disabled")), i.unlock();
            }), i.$element;
        }, clear: function clear() {
            var i,
                a = this;if (a._raise("fileclear")) return a.$btnUpload.removeAttr("disabled"), a._getThumbs().find("video,audio,img").each(function () {
                t.cleanMemory(e(this));
            }), a._resetUpload(), a.clearStack(), a._clearFileInput(), a._resetErrors(!0), a._hasInitialPreview() ? (a._showFileIcon(), a._resetPreview(), a._initPreviewActions(), a.$container.removeClass("file-input-new")) : (a._getThumbs().each(function () {
                a._clearObjects(e(this));
            }), a.isAjaxUpload && (a.previewCache.data = {}), a.$preview.html(""), i = !a.overwriteInitial && a.initialCaption.length > 0 ? a.initialCaption : "", a.$caption.attr("title", "").val(i), t.addCss(a.$container, "file-input-new"), a._validateDefaultPreview()), 0 === a.$container.find(t.FRAMES).length && (a._initCaption() || a.$captionContainer.removeClass("icon-visible")), a._hideFileIcon(), a._raise("filecleared"), a.$captionContainer.focus(), a._setFileDropZoneTitle(), a.$element;
        }, reset: function reset() {
            var e = this;if (e._raise("filereset")) return e._resetPreview(), e.$container.find(".fileinput-filename").text(""), t.addCss(e.$container, "file-input-new"), (e.getFrames().length || e.isAjaxUpload && e.dropZoneEnabled) && e.$container.removeClass("file-input-new"), e.clearStack(), e.formdata = {}, e._setFileDropZoneTitle(), e.$element;
        }, disable: function disable() {
            var e = this;return e.isDisabled = !0, e._raise("filedisabled"), e.$element.attr("disabled", "disabled"), e.$container.find(".kv-fileinput-caption").addClass("file-caption-disabled"), e.$container.find(".fileinput-remove, .fileinput-upload, .file-preview-frame button").attr("disabled", !0), t.addCss(e.$container.find(".btn-file"), "disabled"), e._initDragDrop(), e.$element;
        }, enable: function enable() {
            var e = this;return e.isDisabled = !1, e._raise("fileenabled"), e.$element.removeAttr("disabled"), e.$container.find(".kv-fileinput-caption").removeClass("file-caption-disabled"), e.$container.find(".fileinput-remove, .fileinput-upload, .file-preview-frame button").removeAttr("disabled"), e.$container.find(".btn-file").removeClass("disabled"), e._initDragDrop(), e.$element;
        }, upload: function upload() {
            var i,
                a,
                r,
                n = this,
                o = n.getFileStack().length,
                l = !e.isEmptyObject(n._getExtraData());if (n.isAjaxUpload && !n.isDisabled && n._isFileSelectionValid(o)) {
                if (n._resetUpload(), 0 === o && !l) return void n._showUploadError(n.msgUploadEmpty);if (n.$progress.show(), n.uploadCount = 0, n.uploadStatus = {}, n.uploadLog = [], n.lock(), n._setProgress(2), 0 === o && l) return void n._uploadExtraOnly();if (r = n.filestack.length, n.hasInitData = !1, !n.uploadAsync) return n._uploadBatch(), n.$element;for (a = n._getOutData(), n._raise("filebatchpreupload", [a]), n.fileBatchCompleted = !1, n.uploadCache = { content: [], config: [], tags: [], append: !0 }, n.uploadAsyncCount = n.getFileStack().length, i = 0; r > i; i++) {
                    n.uploadCache.content[i] = null, n.uploadCache.config[i] = null, n.uploadCache.tags[i] = null;
                }for (n.$preview.find(".file-preview-initial").removeClass(t.SORT_CSS), n._initSortable(), n.cacheInitialPreview = n.getPreview(), i = 0; r > i; i++) {
                    n.filestack[i] && n._uploadSingle(i, !0);
                }
            }
        }, destroy: function destroy() {
            var t = this,
                i = t.$form,
                a = t.$container,
                r = t.$element,
                n = t.namespace;return e(document).off(n), e(window).off(n), i && i.length && i.off(n), t.isAjaxUpload && t._clearFileInput(), t._cleanup(), t._initPreviewCache(), r.insertBefore(a).off(n).removeData(), a.off().remove(), r;
        }, refresh: function refresh(i, a) {
            var r = this,
                n = r.$element;return i = "object" != (typeof i === "undefined" ? "undefined" : _typeof(i)) || t.isEmpty(i) ? r.options : e.extend(!0, {}, r.options, i), r._init(i, !0), r._listen(), a && n.trigger("change" + r.namespace), n;
        }, zoom: function zoom(e) {
            var i = this,
                a = i._getFrame(e),
                r = i.$modal;a && (t.initModal(r), r.html(i._getModalContent()), i._setZoomContent(a), r.modal("show"), i._initZoomButtons());
        }, getExif: function getExif(e) {
            var t = this,
                i = t._getFrame(e);return i && i.data("exif") || null;
        }, getFrames: function getFrames(i) {
            var a,
                r = this;return i = i || "", a = r.$preview.find(t.FRAMES + i), r.reversePreviewOrder && (a = e(a.get().reverse())), a;
        }, getPreview: function getPreview() {
            var e = this;return { content: e.initialPreview, config: e.initialPreviewConfig, tags: e.initialPreviewThumbTags };
        } }, e.fn.fileinput = function (a) {
        if (t.hasFileAPISupport() || t.isIE(9)) {
            var r = Array.apply(null, arguments),
                n = [];switch (r.shift(), this.each(function () {
                var o,
                    l = e(this),
                    s = l.data("fileinput"),
                    d = "object" == (typeof a === "undefined" ? "undefined" : _typeof(a)) && a,
                    c = d.theme || l.data("theme"),
                    p = {},
                    u = {},
                    f = d.language || l.data("language") || e.fn.fileinput.defaults.language || "en";s || (c && (u = e.fn.fileinputThemes[c] || {}), "en" === f || t.isEmpty(e.fn.fileinputLocales[f]) || (p = e.fn.fileinputLocales[f] || {}), o = e.extend(!0, {}, e.fn.fileinput.defaults, u, e.fn.fileinputLocales.en, p, d, l.data()), s = new i(this, o), l.data("fileinput", s)), "string" == typeof a && n.push(s[a].apply(s, r));
            }), n.length) {case 0:
                    return this;case 1:
                    return n[0];default:
                    return n;}
        }
    }, e.fn.fileinput.defaults = { language: "en", showCaption: !0, showBrowse: !0, showPreview: !0, showRemove: !0, showUpload: !0, showCancel: !0, showClose: !0, showUploadedThumbs: !0, browseOnZoneClick: !1, autoReplace: !1, autoOrientImage: !0, required: !1, rtl: !1, hideThumbnailContent: !1, generateFileId: null, previewClass: "", captionClass: "", frameClass: "krajee-default", mainClass: "file-caption-main", mainTemplate: null, purifyHtml: !0, fileSizeGetter: null, initialCaption: "", initialPreview: [], initialPreviewDelimiter: "*$$*", initialPreviewAsData: !1, initialPreviewFileType: "image", initialPreviewConfig: [], initialPreviewThumbTags: [], previewThumbTags: {}, initialPreviewShowDelete: !0, initialPreviewDownloadUrl: "", removeFromPreviewOnError: !1, deleteUrl: "", deleteExtraData: {}, overwriteInitial: !0, previewZoomButtonIcons: { prev: '<i class="glyphicon glyphicon-triangle-left"></i>', next: '<i class="glyphicon glyphicon-triangle-right"></i>', toggleheader: '<i class="glyphicon glyphicon-resize-vertical"></i>', fullscreen: '<i class="glyphicon glyphicon-fullscreen"></i>', borderless: '<i class="glyphicon glyphicon-resize-full"></i>', close: '<i class="glyphicon glyphicon-remove"></i>' }, previewZoomButtonClasses: { prev: "btn btn-navigate", next: "btn btn-navigate", toggleheader: "btn btn-sm btn-kv btn-default btn-outline-secondary", fullscreen: "btn btn-sm btn-kv btn-default btn-outline-secondary", borderless: "btn btn-sm btn-kv btn-default btn-outline-secondary", close: "btn btn-sm btn-kv btn-default btn-outline-secondary" }, preferIconicPreview: !1, preferIconicZoomPreview: !1, allowedPreviewTypes: void 0, allowedPreviewMimeTypes: null, allowedFileTypes: null, allowedFileExtensions: null, defaultPreviewContent: null, customLayoutTags: {}, customPreviewTags: {}, previewFileIcon: '<i class="glyphicon glyphicon-file"></i>', previewFileIconClass: "file-other-icon", previewFileIconSettings: {}, previewFileExtSettings: {}, buttonLabelClass: "hidden-xs", browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>&nbsp;', browseClass: "btn btn-primary", removeIcon: '<i class="glyphicon glyphicon-trash"></i>', removeClass: "btn btn-default btn-secondary", cancelIcon: '<i class="glyphicon glyphicon-ban-circle"></i>', cancelClass: "btn btn-default btn-secondary", uploadIcon: '<i class="glyphicon glyphicon-upload"></i>', uploadClass: "btn btn-default btn-secondary", uploadUrl: null, uploadUrlThumb: null, uploadAsync: !0, uploadExtraData: {}, zoomModalHeight: 480, minImageWidth: null, minImageHeight: null, maxImageWidth: null, maxImageHeight: null, resizeImage: !1, resizePreference: "width", resizeQuality: .92, resizeDefaultImageType: "image/jpeg", resizeIfSizeMoreThan: 0, minFileSize: 0, maxFileSize: 0, maxFilePreviewSize: 25600, minFileCount: 0, maxFileCount: 0, validateInitialCount: !1, msgValidationErrorClass: "text-danger", msgValidationErrorIcon: '<i class="glyphicon glyphicon-exclamation-sign"></i> ', msgErrorClass: "file-error-message", progressThumbClass: "progress-bar bg-success progress-bar-success progress-bar-striped active", progressClass: "progress-bar bg-success progress-bar-success progress-bar-striped active", progressCompleteClass: "progress-bar bg-success progress-bar-success", progressErrorClass: "progress-bar bg-danger progress-bar-danger", progressUploadThreshold: 99, previewFileType: "image", elCaptionContainer: null, elCaptionText: null, elPreviewContainer: null, elPreviewImage: null, elPreviewStatus: null, elErrorContainer: null, errorCloseButton: t.closeButton("kv-error-close"), slugCallback: null, dropZoneEnabled: !0, dropZoneTitleClass: "file-drop-zone-title", fileActionSettings: {}, otherActionButtons: "", textEncoding: "UTF-8", ajaxSettings: {}, ajaxDeleteSettings: {}, showAjaxErrorDetails: !0, mergeAjaxCallbacks: !1, mergeAjaxDeleteCallbacks: !1, retryErrorUploads: !0, reversePreviewOrder: !1 }, e.fn.fileinputLocales.en = { fileSingle: "file", filePlural: "files", browseLabel: "Browse &hellip;", removeLabel: "Remove", removeTitle: "Clear selected files", cancelLabel: "Cancel", cancelTitle: "Abort ongoing upload", uploadLabel: "Upload", uploadTitle: "Upload selected files", msgNo: "No", msgNoFilesSelected: "No files selected", msgCancelled: "Cancelled", msgPlaceholder: "Select {files}...", msgZoomModalHeading: "Detailed Preview", msgFileRequired: "You must select a file to upload.", msgSizeTooSmall: 'File "{name}" (<b>{size} KB</b>) is too small and must be larger than <b>{minSize} KB</b>.', msgSizeTooLarge: 'File "{name}" (<b>{size} KB</b>) exceeds maximum allowed upload size of <b>{maxSize} KB</b>.', msgFilesTooLess: "You must select at least <b>{n}</b> {files} to upload.", msgFilesTooMany: "Number of files selected for upload <b>({n})</b> exceeds maximum allowed limit of <b>{m}</b>.", msgFileNotFound: 'File "{name}" not found!', msgFileSecured: 'Security restrictions prevent reading the file "{name}".', msgFileNotReadable: 'File "{name}" is not readable.', msgFilePreviewAborted: 'File preview aborted for "{name}".', msgFilePreviewError: 'An error occurred while reading the file "{name}".', msgInvalidFileName: 'Invalid or unsupported characters in file name "{name}".', msgInvalidFileType: 'Invalid type for file "{name}". Only "{types}" files are supported.', msgInvalidFileExtension: 'Invalid extension for file "{name}". Only "{extensions}" files are supported.', msgFileTypes: { image: "image", html: "HTML", text: "text", video: "video", audio: "audio", flash: "flash", pdf: "PDF", object: "object" }, msgUploadAborted: "The file upload was aborted", msgUploadThreshold: "Processing...", msgUploadBegin: "Initializing...", msgUploadEnd: "Done", msgUploadEmpty: "No valid data available for upload.", msgUploadError: "Error", msgValidationError: "Validation Error", msgLoading: "Loading file {index} of {files} &hellip;", msgProgress: "Loading file {index} of {files} - {name} - {percent}% completed.", msgSelected: "{n} {files} selected", msgFoldersNotAllowed: "Drag & drop files only! {n} folder(s) dropped were skipped.", msgImageWidthSmall: 'Width of image file "{name}" must be at least {size} px.', msgImageHeightSmall: 'Height of image file "{name}" must be at least {size} px.', msgImageWidthLarge: 'Width of image file "{name}" cannot exceed {size} px.', msgImageHeightLarge: 'Height of image file "{name}" cannot exceed {size} px.', msgImageResizeError: "Could not get the image dimensions to resize.", msgImageResizeException: "Error while resizing the image.<pre>{errors}</pre>", msgAjaxError: "Something went wrong with the {operation} operation. Please try again later!", msgAjaxProgressError: "{operation} failed", ajaxOperations: { deleteThumb: "file delete", uploadThumb: "file upload", uploadBatch: "batch file upload", uploadExtra: "form data upload" }, dropZoneTitle: "Drag & drop files here &hellip;", dropZoneClickTitle: "<br>(or click to select {files})", previewZoomButtonTitles: { prev: "View previous file", next: "View next file", toggleheader: "Toggle header", fullscreen: "Toggle full screen", borderless: "Toggle borderless mode", close: "Close detailed preview" } }, e.fn.fileinput.Constructor = i, e(document).ready(function () {
        var t = e("input.file[type=file]");t.length && t.fileinput();
    });
});

/**
 * Bootstrap Multiselect (http://davidstutz.de/bootstrap-multiselect/)
 *
 * Apache License, Version 2.0:
 * Copyright (c) 2012 - 2018 David Stutz
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a
 * copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * BSD 3-Clause License:
 * Copyright (c) 2012 - 2018 David Stutz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    - Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *    - Neither the name of David Stutz nor the names of its contributors may be
 *      used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
(function (root, factory) {
    // check to see if 'knockout' AMD module is specified if using requirejs
    if (typeof define === 'function' && define.amd && typeof require === 'function' && typeof require.specified === 'function' && require.specified('knockout')) {

        // AMD. Register as an anonymous module.
        define(['jquery', 'knockout'], factory);
    } else {
        // Browser globals
        factory(root.jQuery, root.ko);
    }
})(this, function ($, ko) {
    "use strict"; // jshint ;_;

    if (typeof ko !== 'undefined' && ko.bindingHandlers && !ko.bindingHandlers.multiselect) {
        ko.bindingHandlers.multiselect = {
            after: ['options', 'value', 'selectedOptions', 'enable', 'disable'],

            init: function init(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var $element = $(element);
                var config = ko.toJS(valueAccessor());

                $element.multiselect(config);

                if (allBindings.has('options')) {
                    var options = allBindings.get('options');
                    if (ko.isObservable(options)) {
                        ko.computed({
                            read: function read() {
                                options();
                                setTimeout(function () {
                                    var ms = $element.data('multiselect');
                                    if (ms) ms.updateOriginalOptions(); //Not sure how beneficial this is.
                                    $element.multiselect('rebuild');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        });
                    }
                }

                //value and selectedOptions are two-way, so these will be triggered even by our own actions.
                //It needs some way to tell if they are triggered because of us or because of outside change.
                //It doesn't loop but it's a waste of processing.
                if (allBindings.has('value')) {
                    var value = allBindings.get('value');
                    if (ko.isObservable(value)) {
                        ko.computed({
                            read: function read() {
                                value();
                                setTimeout(function () {
                                    $element.multiselect('refresh');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    }
                }

                //Switched from arrayChange subscription to general subscription using 'refresh'.
                //Not sure performance is any better using 'select' and 'deselect'.
                if (allBindings.has('selectedOptions')) {
                    var selectedOptions = allBindings.get('selectedOptions');
                    if (ko.isObservable(selectedOptions)) {
                        ko.computed({
                            read: function read() {
                                selectedOptions();
                                setTimeout(function () {
                                    $element.multiselect('refresh');
                                }, 1);
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    }
                }

                var setEnabled = function setEnabled(enable) {
                    setTimeout(function () {
                        if (enable) $element.multiselect('enable');else $element.multiselect('disable');
                    });
                };

                if (allBindings.has('enable')) {
                    var enable = allBindings.get('enable');
                    if (ko.isObservable(enable)) {
                        ko.computed({
                            read: function read() {
                                setEnabled(enable());
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    } else {
                        setEnabled(enable);
                    }
                }

                if (allBindings.has('disable')) {
                    var disable = allBindings.get('disable');
                    if (ko.isObservable(disable)) {
                        ko.computed({
                            read: function read() {
                                setEnabled(!disable());
                            },
                            disposeWhenNodeIsRemoved: element
                        }).extend({ rateLimit: 100, notifyWhenChangesStop: true });
                    } else {
                        setEnabled(!disable);
                    }
                }

                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $element.multiselect('destroy');
                });
            },

            update: function update(element, valueAccessor, allBindings, viewModel, bindingContext) {
                var $element = $(element);
                var config = ko.toJS(valueAccessor());

                $element.multiselect('setOptions', config);
                $element.multiselect('rebuild');
            }
        };
    }

    function forEach(array, callback) {
        for (var index = 0; index < array.length; ++index) {
            callback(array[index], index);
        }
    }

    /**
     * Constructor to create a new multiselect using the given select.
     *
     * @param {jQuery} select
     * @param {Object} options
     * @returns {Multiselect}
     */
    function Multiselect(select, options) {

        this.$select = $(select);
        this.options = this.mergeOptions($.extend({}, options, this.$select.data()));

        // Placeholder via data attributes
        if (this.$select.attr("data-placeholder")) {
            this.options.nonSelectedText = this.$select.data("placeholder");
        }

        // Initialization.
        // We have to clone to create a new reference.
        this.originalOptions = this.$select.clone()[0].options;
        this.query = '';
        this.searchTimeout = null;
        this.lastToggledInput = null;

        this.options.multiple = this.$select.attr('multiple') === "multiple";
        this.options.onChange = $.proxy(this.options.onChange, this);
        this.options.onSelectAll = $.proxy(this.options.onSelectAll, this);
        this.options.onDeselectAll = $.proxy(this.options.onDeselectAll, this);
        this.options.onDropdownShow = $.proxy(this.options.onDropdownShow, this);
        this.options.onDropdownHide = $.proxy(this.options.onDropdownHide, this);
        this.options.onDropdownShown = $.proxy(this.options.onDropdownShown, this);
        this.options.onDropdownHidden = $.proxy(this.options.onDropdownHidden, this);
        this.options.onInitialized = $.proxy(this.options.onInitialized, this);
        this.options.onFiltering = $.proxy(this.options.onFiltering, this);

        // Build select all if enabled.
        this.buildContainer();
        this.buildButton();
        this.buildDropdown();
        this.buildReset();
        this.buildSelectAll();
        this.buildDropdownOptions();
        this.buildFilter();

        this.updateButtonText();
        this.updateSelectAll(true);

        if (this.options.enableClickableOptGroups && this.options.multiple) {
            this.updateOptGroups();
        }

        this.options.wasDisabled = this.$select.prop('disabled');
        if (this.options.disableIfEmpty && $('option', this.$select).length <= 0) {
            this.disable();
        }

        this.$select.wrap('<span class="multiselect-native-select" />').after(this.$container);
        this.options.onInitialized(this.$select, this.$container);
    }

    Multiselect.prototype = {

        defaults: {
            /**
             * Default text function will either print 'None selected' in case no
             * option is selected or a list of the selected options up to a length
             * of 3 selected options.
             *
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {String}
             */
            buttonText: function buttonText(options, select) {
                if (this.disabledText.length > 0 && (select.prop('disabled') || options.length == 0 && this.disableIfEmpty)) {

                    return this.disabledText;
                } else if (options.length === 0) {
                    return this.nonSelectedText;
                } else if (this.allSelectedText && options.length === $('option', $(select)).length && $('option', $(select)).length !== 1 && this.multiple) {

                    if (this.selectAllNumber) {
                        return this.allSelectedText + ' (' + options.length + ')';
                    } else {
                        return this.allSelectedText;
                    }
                } else if (this.numberDisplayed != 0 && options.length > this.numberDisplayed) {
                    return options.length + ' ' + this.nSelectedText;
                } else {
                    var selected = '';
                    var delimiter = this.delimiterText;

                    options.each(function () {
                        var label = $(this).attr('label') !== undefined ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });

                    return selected.substr(0, selected.length - this.delimiterText.length);
                }
            },
            /**
             * Updates the title of the button similar to the buttonText function.
             *
             * @param {jQuery} options
             * @param {jQuery} select
             * @returns {@exp;selected@call;substr}
             */
            buttonTitle: function buttonTitle(options, select) {
                if (options.length === 0) {
                    return this.nonSelectedText;
                } else {
                    var selected = '';
                    var delimiter = this.delimiterText;

                    options.each(function () {
                        var label = $(this).attr('label') !== undefined ? $(this).attr('label') : $(this).text();
                        selected += label + delimiter;
                    });
                    return selected.substr(0, selected.length - this.delimiterText.length);
                }
            },
            checkboxName: function checkboxName(option) {
                return false; // no checkbox name
            },
            /**
             * Create a label.
             *
             * @param {jQuery} element
             * @returns {String}
             */
            optionLabel: function optionLabel(element) {
                return $(element).attr('label') || $(element).text();
            },
            /**
             * Create a class.
             *
             * @param {jQuery} element
             * @returns {String}
             */
            optionClass: function optionClass(element) {
                return $(element).attr('class') || '';
            },
            /**
             * Triggered on change of the multiselect.
             *
             * Not triggered when selecting/deselecting options manually.
             *
             * @param {jQuery} option
             * @param {Boolean} checked
             */
            onChange: function onChange(option, checked) {},
            /**
             * Triggered when the dropdown is shown.
             *
             * @param {jQuery} event
             */
            onDropdownShow: function onDropdownShow(event) {},
            /**
             * Triggered when the dropdown is hidden.
             *
             * @param {jQuery} event
             */
            onDropdownHide: function onDropdownHide(event) {},
            /**
             * Triggered after the dropdown is shown.
             *
             * @param {jQuery} event
             */
            onDropdownShown: function onDropdownShown(event) {},
            /**
             * Triggered after the dropdown is hidden.
             *
             * @param {jQuery} event
             */
            onDropdownHidden: function onDropdownHidden(event) {},
            /**
             * Triggered on select all.
             */
            onSelectAll: function onSelectAll() {},
            /**
             * Triggered on deselect all.
             */
            onDeselectAll: function onDeselectAll() {},
            /**
             * Triggered after initializing.
             *
             * @param {jQuery} $select
             * @param {jQuery} $container
             */
            onInitialized: function onInitialized($select, $container) {},
            /**
             * Triggered on filtering.
             *
             * @param {jQuery} $filter
             */
            onFiltering: function onFiltering($filter) {},
            enableHTML: false,
            buttonClass: 'btn btn-default',
            inheritClass: false,
            buttonWidth: 'auto',
            buttonContainer: '<div class="btn-group" />',
            dropRight: false,
            dropUp: false,
            selectedClass: 'active',
            // Maximum height of the dropdown menu.
            // If maximum height is exceeded a scrollbar will be displayed.
            maxHeight: false,
            includeSelectAllOption: false,
            includeSelectAllIfMoreThan: 0,
            selectAllText: ' Select all',
            selectAllValue: 'multiselect-all',
            selectAllName: false,
            selectAllNumber: true,
            selectAllJustVisible: true,
            enableFiltering: false,
            enableCaseInsensitiveFiltering: false,
            enableFullValueFiltering: false,
            enableClickableOptGroups: false,
            enableCollapsibleOptGroups: false,
            collapseOptGroupsByDefault: false,
            filterPlaceholder: 'Search',
            // possible options: 'text', 'value', 'both'
            filterBehavior: 'text',
            includeFilterClearBtn: true,
            preventInputChangeEvent: false,
            nonSelectedText: 'None selected',
            nSelectedText: 'selected',
            allSelectedText: 'All selected',
            numberDisplayed: 3,
            disableIfEmpty: false,
            disabledText: '',
            delimiterText: ', ',
            includeResetOption: false,
            includeResetDivider: false,
            resetText: 'Reset',
            templates: {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item multiselect-filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text" /></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>',
                resetButton: '<li class="multiselect-reset text-center"><div class="input-group"><a class="btn btn-default btn-block"></a></div></li>'
            }
        },

        constructor: Multiselect,

        /**
         * Builds the container of the multiselect.
         */
        buildContainer: function buildContainer() {
            this.$container = $(this.options.buttonContainer);
            this.$container.on('show.bs.dropdown', this.options.onDropdownShow);
            this.$container.on('hide.bs.dropdown', this.options.onDropdownHide);
            this.$container.on('shown.bs.dropdown', this.options.onDropdownShown);
            this.$container.on('hidden.bs.dropdown', this.options.onDropdownHidden);
        },

        /**
         * Builds the button of the multiselect.
         */
        buildButton: function buildButton() {
            this.$button = $(this.options.templates.button).addClass(this.options.buttonClass);
            if (this.$select.attr('class') && this.options.inheritClass) {
                this.$button.addClass(this.$select.attr('class'));
            }
            // Adopt active state.
            if (this.$select.prop('disabled')) {
                this.disable();
            } else {
                this.enable();
            }

            // Manually add button width if set.
            if (this.options.buttonWidth && this.options.buttonWidth !== 'auto') {
                this.$button.css({
                    'width': '100%', //this.options.buttonWidth,
                    'overflow': 'hidden',
                    'text-overflow': 'ellipsis'
                });
                this.$container.css({
                    'width': this.options.buttonWidth
                });
            }

            // Keep the tab index from the select.
            var tabindex = this.$select.attr('tabindex');
            if (tabindex) {
                this.$button.attr('tabindex', tabindex);
            }

            this.$container.prepend(this.$button);
        },

        /**
         * Builds the ul representing the dropdown menu.
         */
        buildDropdown: function buildDropdown() {

            // Build ul.
            this.$ul = $(this.options.templates.ul);

            if (this.options.dropRight) {
                this.$ul.addClass('pull-right');
            }

            // Set max height of dropdown menu to activate auto scrollbar.
            if (this.options.maxHeight) {
                // TODO: Add a class for this option to move the css declarations.
                this.$ul.css({
                    'max-height': this.options.maxHeight + 'px',
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden'
                });
            }

            if (this.options.dropUp) {

                var height = Math.min(this.options.maxHeight, $('option[data-role!="divider"]', this.$select).length * 26 + $('option[data-role="divider"]', this.$select).length * 19 + (this.options.includeSelectAllOption ? 26 : 0) + (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering ? 44 : 0));
                var moveCalc = height + 34;

                this.$ul.css({
                    'max-height': height + 'px',
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden',
                    'margin-top': "-" + moveCalc + 'px'
                });
            }

            this.$container.append(this.$ul);
        },

        /**
         * Build the dropdown options and binds all necessary events.
         *
         * Uses createDivider and createOptionValue to create the necessary options.
         */
        buildDropdownOptions: function buildDropdownOptions() {

            this.$select.children().each($.proxy(function (index, element) {

                var $element = $(element);
                // Support optgroups and options without a group simultaneously.
                var tag = $element.prop('tagName').toLowerCase();

                if ($element.prop('value') === this.options.selectAllValue) {
                    return;
                }

                if (tag === 'optgroup') {
                    this.createOptgroup(element);
                } else if (tag === 'option') {

                    if ($element.data('role') === 'divider') {
                        this.createDivider();
                    } else {
                        this.createOptionValue(element);
                    }
                }

                // Other illegal tags will be ignored.
            }, this));

            // Bind the change event on the dropdown elements.
            $(this.$ul).off('change', 'li:not(.multiselect-group) input[type="checkbox"], li:not(.multiselect-group) input[type="radio"]');
            $(this.$ul).on('change', 'li:not(.multiselect-group) input[type="checkbox"], li:not(.multiselect-group) input[type="radio"]', $.proxy(function (event) {
                var $target = $(event.target);

                var checked = $target.prop('checked') || false;
                var isSelectAllOption = $target.val() === this.options.selectAllValue;

                // Apply or unapply the configured selected class.
                if (this.options.selectedClass) {
                    if (checked) {
                        $target.closest('li').addClass(this.options.selectedClass);
                    } else {
                        $target.closest('li').removeClass(this.options.selectedClass);
                    }
                }

                // Get the corresponding option.
                var value = $target.val();
                var $option = this.getOptionByValue(value);

                var $optionsNotThis = $('option', this.$select).not($option);
                var $checkboxesNotThis = $('input', this.$container).not($target);

                if (isSelectAllOption) {

                    if (checked) {
                        this.selectAll(this.options.selectAllJustVisible, true);
                    } else {
                        this.deselectAll(this.options.selectAllJustVisible, true);
                    }
                } else {
                    if (checked) {
                        $option.prop('selected', true);

                        if (this.options.multiple) {
                            // Simply select additional option.
                            $option.prop('selected', true);
                        } else {
                            // Unselect all other options and corresponding checkboxes.
                            if (this.options.selectedClass) {
                                $($checkboxesNotThis).closest('li').removeClass(this.options.selectedClass);
                            }

                            $($checkboxesNotThis).prop('checked', false);
                            $optionsNotThis.prop('selected', false);

                            // It's a single selection, so close.
                            this.$button.click();
                        }

                        if (this.options.selectedClass === "active") {
                            $optionsNotThis.closest("a").css("outline", "");
                        }
                    } else {
                        // Unselect option.
                        $option.prop('selected', false);
                    }

                    // To prevent select all from firing onChange: #575
                    this.options.onChange($option, checked);

                    // Do not update select all or optgroups on select all change!
                    this.updateSelectAll();

                    if (this.options.enableClickableOptGroups && this.options.multiple) {
                        this.updateOptGroups();
                    }
                }

                this.$select.change();
                this.updateButtonText();

                if (this.options.preventInputChangeEvent) {
                    return false;
                }
            }, this));

            $('li a', this.$ul).on('mousedown', function (e) {
                if (e.shiftKey) {
                    // Prevent selecting text by Shift+click
                    return false;
                }
            });

            $(this.$ul).on('touchstart click', 'li a', $.proxy(function (event) {
                event.stopPropagation();

                var $target = $(event.target);

                if (event.shiftKey && this.options.multiple) {
                    if ($target.is("label")) {
                        // Handles checkbox selection manually (see https://github.com/davidstutz/bootstrap-multiselect/issues/431)
                        event.preventDefault();
                        $target = $target.find("input");
                        $target.prop("checked", !$target.prop("checked"));
                    }
                    var checked = $target.prop('checked') || false;

                    if (this.lastToggledInput !== null && this.lastToggledInput !== $target) {
                        // Make sure we actually have a range
                        var from = this.$ul.find("li:visible").index($target.parents("li"));
                        var to = this.$ul.find("li:visible").index(this.lastToggledInput.parents("li"));

                        if (from > to) {
                            // Swap the indices
                            var tmp = to;
                            to = from;
                            from = tmp;
                        }

                        // Make sure we grab all elements since slice excludes the last index
                        ++to;

                        // Change the checkboxes and underlying options
                        var range = this.$ul.find("li").not(".multiselect-filter-hidden").slice(from, to).find("input");

                        range.prop('checked', checked);

                        if (this.options.selectedClass) {
                            range.closest('li').toggleClass(this.options.selectedClass, checked);
                        }

                        for (var i = 0, j = range.length; i < j; i++) {
                            var $checkbox = $(range[i]);

                            var $option = this.getOptionByValue($checkbox.val());

                            $option.prop('selected', checked);
                        }
                    }

                    // Trigger the select "change" event
                    $target.trigger("change");
                }

                // Remembers last clicked option
                if ($target.is("input") && !$target.closest("li").is(".multiselect-item")) {
                    this.lastToggledInput = $target;
                }

                $target.blur();
            }, this));

            // Keyboard support.
            this.$container.off('keydown.multiselect').on('keydown.multiselect', $.proxy(function (event) {
                if ($('input[type="text"]', this.$container).is(':focus')) {
                    return;
                }

                if (event.keyCode === 9 && this.$container.hasClass('open')) {
                    this.$button.click();
                } else {
                    var $items = $(this.$container).find("li:not(.divider):not(.disabled) a").filter(":visible");

                    if (!$items.length) {
                        return;
                    }

                    var index = $items.index($items.filter(':focus'));

                    // Navigation up.
                    if (event.keyCode === 38 && index > 0) {
                        index--;
                    }
                    // Navigate down.
                    else if (event.keyCode === 40 && index < $items.length - 1) {
                            index++;
                        } else if (!~index) {
                            index = 0;
                        }

                    var $current = $items.eq(index);
                    $current.focus();

                    if (event.keyCode === 32 || event.keyCode === 13) {
                        var $checkbox = $current.find('input');

                        $checkbox.prop("checked", !$checkbox.prop("checked"));
                        $checkbox.change();
                    }

                    event.stopPropagation();
                    event.preventDefault();
                }
            }, this));

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                $("li.multiselect-group input", this.$ul).on("change", $.proxy(function (event) {
                    event.stopPropagation();

                    var $target = $(event.target);
                    var checked = $target.prop('checked') || false;

                    var $li = $(event.target).closest('li');
                    var $group = $li.nextUntil("li.multiselect-group").not('.multiselect-filter-hidden').not('.disabled');

                    var $inputs = $group.find("input");

                    var values = [];
                    var $options = [];

                    if (this.options.selectedClass) {
                        if (checked) {
                            $li.addClass(this.options.selectedClass);
                        } else {
                            $li.removeClass(this.options.selectedClass);
                        }
                    }

                    $.each($inputs, $.proxy(function (index, input) {
                        var value = $(input).val();
                        var $option = this.getOptionByValue(value);

                        if (checked) {
                            $(input).prop('checked', true);
                            $(input).closest('li').addClass(this.options.selectedClass);

                            $option.prop('selected', true);
                        } else {
                            $(input).prop('checked', false);
                            $(input).closest('li').removeClass(this.options.selectedClass);

                            $option.prop('selected', false);
                        }

                        $options.push(this.getOptionByValue(value));
                    }, this));

                    // Cannot use select or deselect here because it would call updateOptGroups again.

                    this.options.onChange($options, checked);

                    this.$select.change();
                    this.updateButtonText();
                    this.updateSelectAll();
                }, this));
            }

            if (this.options.enableCollapsibleOptGroups && this.options.multiple) {
                $("li.multiselect-group .caret-container", this.$ul).on("click", $.proxy(function (event) {
                    var $li = $(event.target).closest('li');
                    var $inputs = $li.nextUntil("li.multiselect-group").not('.multiselect-filter-hidden');

                    var visible = true;
                    $inputs.each(function () {
                        visible = visible && !$(this).hasClass('multiselect-collapsible-hidden');
                    });

                    if (visible) {
                        $inputs.hide().addClass('multiselect-collapsible-hidden');
                    } else {
                        $inputs.show().removeClass('multiselect-collapsible-hidden');
                    }
                }, this));

                $("li.multiselect-all", this.$ul).css('background', '#f3f3f3').css('border-bottom', '1px solid #eaeaea');
                $("li.multiselect-all > a > label.checkbox", this.$ul).css('padding', '3px 20px 3px 35px');
                $("li.multiselect-group > a > input", this.$ul).css('margin', '4px 0px 5px -20px');
            }
        },

        /**
         * Create an option using the given select option.
         *
         * @param {jQuery} element
         */
        createOptionValue: function createOptionValue(element) {
            var $element = $(element);
            if ($element.is(':selected')) {
                $element.prop('selected', true);
            }

            // Support the label attribute on options.
            var label = this.options.optionLabel(element);
            var classes = this.options.optionClass(element);
            var value = $element.val();
            var inputType = this.options.multiple ? "checkbox" : "radio";

            var $li = $(this.options.templates.li);
            var $label = $('label', $li);
            $label.addClass(inputType);
            $label.attr("title", label);
            $li.addClass(classes);

            // Hide all children items when collapseOptGroupsByDefault is true
            if (this.options.collapseOptGroupsByDefault && $(element).parent().prop("tagName").toLowerCase() === "optgroup") {
                $li.addClass("multiselect-collapsible-hidden");
                $li.hide();
            }

            if (this.options.enableHTML) {
                $label.html(" " + label);
            } else {
                $label.text(" " + label);
            }

            var $checkbox = $('<input/>').attr('type', inputType);

            var name = this.options.checkboxName($element);
            if (name) {
                $checkbox.attr('name', name);
            }

            $label.prepend($checkbox);

            var selected = $element.prop('selected') || false;
            $checkbox.val(value);

            if (value === this.options.selectAllValue) {
                $li.addClass("multiselect-item multiselect-all");
                $checkbox.parent().parent().addClass('multiselect-all');
            }

            $label.attr('title', $element.attr('title'));

            this.$ul.append($li);

            if ($element.is(':disabled')) {
                $checkbox.attr('disabled', 'disabled').prop('disabled', true).closest('a').attr("tabindex", "-1").closest('li').addClass('disabled');
            }

            $checkbox.prop('checked', selected);

            if (selected && this.options.selectedClass) {
                $checkbox.closest('li').addClass(this.options.selectedClass);
            }
        },

        /**
         * Creates a divider using the given select option.
         *
         * @param {jQuery} element
         */
        createDivider: function createDivider(element) {
            var $divider = $(this.options.templates.divider);
            this.$ul.append($divider);
        },

        /**
         * Creates an optgroup.
         *
         * @param {jQuery} group
         */
        createOptgroup: function createOptgroup(group) {
            var label = $(group).attr("label");
            var value = $(group).attr("value");
            var $li = $('<li class="multiselect-item multiselect-group"><a href="javascript:void(0);"><label><b></b></label></a></li>');

            var classes = this.options.optionClass(group);
            $li.addClass(classes);

            if (this.options.enableHTML) {
                $('label b', $li).html(" " + label);
            } else {
                $('label b', $li).text(" " + label);
            }

            if (this.options.enableCollapsibleOptGroups && this.options.multiple) {
                $('a', $li).append('<span class="caret-container"><b class="caret"></b></span>');
            }

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                $('a label', $li).prepend('<input type="checkbox" value="' + value + '"/>');
            }

            if ($(group).is(':disabled')) {
                $li.addClass('disabled');
            }

            this.$ul.append($li);

            $("option", group).each($.proxy(function ($, group) {
                this.createOptionValue(group);
            }, this));
        },

        /**
         * Build the reset.
         *
         */
        buildReset: function buildReset() {
            if (this.options.includeResetOption) {

                // Check whether to add a divider after the reset.
                if (this.options.includeResetDivider) {
                    this.$ul.prepend($(this.options.templates.divider));
                }

                var $resetButton = $(this.options.templates.resetButton);

                if (this.options.enableHTML) {
                    $('a', $resetButton).html(this.options.resetText);
                } else {
                    $('a', $resetButton).text(this.options.resetText);
                }

                $('a', $resetButton).click($.proxy(function () {
                    this.clearSelection();
                }, this));

                this.$ul.prepend($resetButton);
            }
        },

        /**
         * Build the select all.
         *
         * Checks if a select all has already been created.
         */
        buildSelectAll: function buildSelectAll() {
            if (typeof this.options.selectAllValue === 'number') {
                this.options.selectAllValue = this.options.selectAllValue.toString();
            }

            var alreadyHasSelectAll = this.hasSelectAll();

            if (!alreadyHasSelectAll && this.options.includeSelectAllOption && this.options.multiple && $('option', this.$select).length > this.options.includeSelectAllIfMoreThan) {

                // Check whether to add a divider after the select all.
                if (this.options.includeSelectAllDivider) {
                    this.$ul.prepend($(this.options.templates.divider));
                }

                var $li = $(this.options.templates.li);
                $('label', $li).addClass("checkbox");

                if (this.options.enableHTML) {
                    $('label', $li).html(" " + this.options.selectAllText);
                } else {
                    $('label', $li).text(" " + this.options.selectAllText);
                }

                if (this.options.selectAllName) {
                    $('label', $li).prepend('<input type="checkbox" name="' + this.options.selectAllName + '" />');
                } else {
                    $('label', $li).prepend('<input type="checkbox" />');
                }

                var $checkbox = $('input', $li);
                $checkbox.val(this.options.selectAllValue);

                $li.addClass("multiselect-item multiselect-all");
                $checkbox.parent().parent().addClass('multiselect-all');

                this.$ul.prepend($li);

                $checkbox.prop('checked', false);
            }
        },

        /**
         * Builds the filter.
         */
        buildFilter: function buildFilter() {

            // Build filter if filtering OR case insensitive filtering is enabled and the number of options exceeds (or equals) enableFilterLength.
            if (this.options.enableFiltering || this.options.enableCaseInsensitiveFiltering) {
                var enableFilterLength = Math.max(this.options.enableFiltering, this.options.enableCaseInsensitiveFiltering);

                if (this.$select.find('option').length >= enableFilterLength) {

                    this.$filter = $(this.options.templates.filter);
                    $('input', this.$filter).attr('placeholder', this.options.filterPlaceholder);

                    // Adds optional filter clear button
                    if (this.options.includeFilterClearBtn) {
                        var clearBtn = $(this.options.templates.filterClearBtn);
                        clearBtn.on('click', $.proxy(function (event) {
                            clearTimeout(this.searchTimeout);

                            this.query = '';
                            this.$filter.find('.multiselect-search').val('');
                            $('li', this.$ul).show().removeClass('multiselect-filter-hidden');

                            this.updateSelectAll();

                            if (this.options.enableClickableOptGroups && this.options.multiple) {
                                this.updateOptGroups();
                            }
                        }, this));
                        this.$filter.find('.input-group').append(clearBtn);
                    }

                    this.$ul.prepend(this.$filter);

                    this.$filter.val(this.query).on('click', function (event) {
                        event.stopPropagation();
                    }).on('input keydown', $.proxy(function (event) {
                        // Cancel enter key default behaviour
                        if (event.which === 13) {
                            event.preventDefault();
                        }

                        // This is useful to catch "keydown" events after the browser has updated the control.
                        clearTimeout(this.searchTimeout);

                        this.searchTimeout = this.asyncFunction($.proxy(function () {

                            if (this.query !== event.target.value) {
                                this.query = event.target.value;

                                var currentGroup, currentGroupVisible;
                                $.each($('li', this.$ul), $.proxy(function (index, element) {
                                    var value = $('input', element).length > 0 ? $('input', element).val() : "";
                                    var text = $('label', element).text();

                                    var filterCandidate = '';
                                    if (this.options.filterBehavior === 'text') {
                                        filterCandidate = text;
                                    } else if (this.options.filterBehavior === 'value') {
                                        filterCandidate = value;
                                    } else if (this.options.filterBehavior === 'both') {
                                        filterCandidate = text + '\n' + value;
                                    }

                                    if (value !== this.options.selectAllValue && text) {

                                        // By default lets assume that element is not
                                        // interesting for this search.
                                        var showElement = false;

                                        if (this.options.enableCaseInsensitiveFiltering) {
                                            filterCandidate = filterCandidate.toLowerCase();
                                            this.query = this.query.toLowerCase();
                                        }

                                        if (this.options.enableFullValueFiltering && this.options.filterBehavior !== 'both') {
                                            var valueToMatch = filterCandidate.trim().substring(0, this.query.length);
                                            if (this.query.indexOf(valueToMatch) > -1) {
                                                showElement = true;
                                            }
                                        } else if (filterCandidate.indexOf(this.query) > -1) {
                                            showElement = true;
                                        }

                                        // Toggle current element (group or group item) according to showElement boolean.
                                        if (!showElement) {
                                            $(element).css('display', 'none');
                                            $(element).addClass('multiselect-filter-hidden');
                                        }
                                        if (showElement) {
                                            $(element).css('display', 'block');
                                            $(element).removeClass('multiselect-filter-hidden');
                                        }

                                        // Differentiate groups and group items.
                                        if ($(element).hasClass('multiselect-group')) {
                                            // Remember group status.
                                            currentGroup = element;
                                            currentGroupVisible = showElement;
                                        } else {
                                            // Show group name when at least one of its items is visible.
                                            if (showElement) {
                                                $(currentGroup).show().removeClass('multiselect-filter-hidden');
                                            }

                                            // Show all group items when group name satisfies filter.
                                            if (!showElement && currentGroupVisible) {
                                                $(element).show().removeClass('multiselect-filter-hidden');
                                            }
                                        }
                                    }
                                }, this));
                            }

                            this.updateSelectAll();

                            if (this.options.enableClickableOptGroups && this.options.multiple) {
                                this.updateOptGroups();
                            }

                            this.options.onFiltering(event.target);
                        }, this), 300, this);
                    }, this));
                }
            }
        },

        /**
         * Unbinds the whole plugin.
         */
        destroy: function destroy() {
            this.$container.remove();
            this.$select.show();

            // reset original state
            this.$select.prop('disabled', this.options.wasDisabled);

            this.$select.data('multiselect', null);
        },

        /**
         * Refreshs the multiselect based on the selected options of the select.
         */
        refresh: function refresh() {
            var inputs = {};
            $('li input', this.$ul).each(function () {
                inputs[$(this).val()] = $(this);
            });

            $('option', this.$select).each($.proxy(function (index, element) {
                var $elem = $(element);
                var $input = inputs[$(element).val()];

                if ($elem.is(':selected')) {
                    $input.prop('checked', true);

                    if (this.options.selectedClass) {
                        $input.closest('li').addClass(this.options.selectedClass);
                    }
                } else {
                    $input.prop('checked', false);

                    if (this.options.selectedClass) {
                        $input.closest('li').removeClass(this.options.selectedClass);
                    }
                }

                if ($elem.is(":disabled")) {
                    $input.attr('disabled', 'disabled').prop('disabled', true).closest('li').addClass('disabled');
                } else {
                    $input.prop('disabled', false).closest('li').removeClass('disabled');
                }
            }, this));

            this.updateButtonText();
            this.updateSelectAll();

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }
        },

        /**
         * Select all options of the given values.
         *
         * If triggerOnChange is set to true, the on change event is triggered if
         * and only if one value is passed.
         *
         * @param {Array} selectValues
         * @param {Boolean} triggerOnChange
         */
        select: function select(selectValues, triggerOnChange) {
            if (!$.isArray(selectValues)) {
                selectValues = [selectValues];
            }

            for (var i = 0; i < selectValues.length; i++) {
                var value = selectValues[i];

                if (value === null || value === undefined) {
                    continue;
                }

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if ($option === undefined || $checkbox === undefined) {
                    continue;
                }

                if (!this.options.multiple) {
                    this.deselectAll(false);
                }

                if (this.options.selectedClass) {
                    $checkbox.closest('li').addClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', true);
                $option.prop('selected', true);

                if (triggerOnChange) {
                    this.options.onChange($option, true);
                }
            }

            this.updateButtonText();
            this.updateSelectAll();

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }
        },

        /**
         * Clears all selected items.
         */
        clearSelection: function clearSelection() {
            this.deselectAll(false);
            this.updateButtonText();
            this.updateSelectAll();

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }
        },

        /**
         * Deselects all options of the given values.
         *
         * If triggerOnChange is set to true, the on change event is triggered, if
         * and only if one value is passed.
         *
         * @param {Array} deselectValues
         * @param {Boolean} triggerOnChange
         */
        deselect: function deselect(deselectValues, triggerOnChange) {
            if (!$.isArray(deselectValues)) {
                deselectValues = [deselectValues];
            }

            for (var i = 0; i < deselectValues.length; i++) {
                var value = deselectValues[i];

                if (value === null || value === undefined) {
                    continue;
                }

                var $option = this.getOptionByValue(value);
                var $checkbox = this.getInputByValue(value);

                if ($option === undefined || $checkbox === undefined) {
                    continue;
                }

                if (this.options.selectedClass) {
                    $checkbox.closest('li').removeClass(this.options.selectedClass);
                }

                $checkbox.prop('checked', false);
                $option.prop('selected', false);

                if (triggerOnChange) {
                    this.options.onChange($option, false);
                }
            }

            this.updateButtonText();
            this.updateSelectAll();

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }
        },

        /**
         * Selects all enabled & visible options.
         *
         * If justVisible is true or not specified, only visible options are selected.
         *
         * @param {Boolean} justVisible
         * @param {Boolean} triggerOnSelectAll
         */
        selectAll: function selectAll(justVisible, triggerOnSelectAll) {

            var justVisible = typeof justVisible === 'undefined' ? true : justVisible;
            var allLis = $("li:not(.divider):not(.disabled):not(.multiselect-group)", this.$ul);
            var visibleLis = $("li:not(.divider):not(.disabled):not(.multiselect-group):not(.multiselect-filter-hidden):not(.multiselect-collapisble-hidden)", this.$ul).filter(':visible');

            if (justVisible) {
                $('input:enabled', visibleLis).prop('checked', true);
                visibleLis.addClass(this.options.selectedClass);

                $('input:enabled', visibleLis).each($.proxy(function (index, element) {
                    var value = $(element).val();
                    var option = this.getOptionByValue(value);
                    $(option).prop('selected', true);
                }, this));
            } else {
                $('input:enabled', allLis).prop('checked', true);
                allLis.addClass(this.options.selectedClass);

                $('input:enabled', allLis).each($.proxy(function (index, element) {
                    var value = $(element).val();
                    var option = this.getOptionByValue(value);
                    $(option).prop('selected', true);
                }, this));
            }

            $('li input[value="' + this.options.selectAllValue + '"]', this.$ul).prop('checked', true);

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }

            if (triggerOnSelectAll) {
                this.options.onSelectAll();
            }
        },

        /**
         * Deselects all options.
         *
         * If justVisible is true or not specified, only visible options are deselected.
         *
         * @param {Boolean} justVisible
         */
        deselectAll: function deselectAll(justVisible, triggerOnDeselectAll) {

            var justVisible = typeof justVisible === 'undefined' ? true : justVisible;
            var allLis = $("li:not(.divider):not(.disabled):not(.multiselect-group)", this.$ul);
            var visibleLis = $("li:not(.divider):not(.disabled):not(.multiselect-group):not(.multiselect-filter-hidden):not(.multiselect-collapisble-hidden)", this.$ul).filter(':visible');

            if (justVisible) {
                $('input[type="checkbox"]:enabled', visibleLis).prop('checked', false);
                visibleLis.removeClass(this.options.selectedClass);

                $('input[type="checkbox"]:enabled', visibleLis).each($.proxy(function (index, element) {
                    var value = $(element).val();
                    var option = this.getOptionByValue(value);
                    $(option).prop('selected', false);
                }, this));
            } else {
                $('input[type="checkbox"]:enabled', allLis).prop('checked', false);
                allLis.removeClass(this.options.selectedClass);

                $('input[type="checkbox"]:enabled', allLis).each($.proxy(function (index, element) {
                    var value = $(element).val();
                    var option = this.getOptionByValue(value);
                    $(option).prop('selected', false);
                }, this));
            }

            $('li input[value="' + this.options.selectAllValue + '"]', this.$ul).prop('checked', false);

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }

            if (triggerOnDeselectAll) {
                this.options.onDeselectAll();
            }
        },

        /**
         * Rebuild the plugin.
         *
         * Rebuilds the dropdown, the filter and the select all option.
         */
        rebuild: function rebuild() {
            this.$ul.html('');

            // Important to distinguish between radios and checkboxes.
            this.options.multiple = this.$select.attr('multiple') === "multiple";

            this.buildSelectAll();
            this.buildDropdownOptions();
            this.buildFilter();

            this.updateButtonText();
            this.updateSelectAll(true);

            if (this.options.enableClickableOptGroups && this.options.multiple) {
                this.updateOptGroups();
            }

            if (this.options.disableIfEmpty && $('option', this.$select).length <= 0) {
                this.disable();
            } else {
                this.enable();
            }

            if (this.options.dropRight) {
                this.$ul.addClass('pull-right');
            }
        },

        /**
         * The provided data will be used to build the dropdown.
         */
        dataprovider: function dataprovider(_dataprovider) {

            var groupCounter = 0;
            var $select = this.$select.empty();

            $.each(_dataprovider, function (index, option) {
                var $tag;

                if ($.isArray(option.children)) {
                    // create optiongroup tag
                    groupCounter++;

                    $tag = $('<optgroup/>').attr({
                        label: option.label || 'Group ' + groupCounter,
                        disabled: !!option.disabled,
                        value: option.value
                    });

                    forEach(option.children, function (subOption) {
                        // add children option tags
                        var attributes = {
                            value: subOption.value,
                            label: subOption.label || subOption.value,
                            title: subOption.title,
                            selected: !!subOption.selected,
                            disabled: !!subOption.disabled
                        };

                        //Loop through attributes object and add key-value for each attribute
                        for (var key in subOption.attributes) {
                            attributes['data-' + key] = subOption.attributes[key];
                        }
                        //Append original attributes + new data attributes to option
                        $tag.append($('<option/>').attr(attributes));
                    });
                } else {

                    var attributes = {
                        'value': option.value,
                        'label': option.label || option.value,
                        'title': option.title,
                        'class': option['class'],
                        'selected': !!option['selected'],
                        'disabled': !!option['disabled']
                    };
                    //Loop through attributes object and add key-value for each attribute
                    for (var key in option.attributes) {
                        attributes['data-' + key] = option.attributes[key];
                    }
                    //Append original attributes + new data attributes to option
                    $tag = $('<option/>').attr(attributes);

                    $tag.text(option.label || option.value);
                }

                $select.append($tag);
            });

            this.rebuild();
        },

        /**
         * Enable the multiselect.
         */
        enable: function enable() {
            this.$select.prop('disabled', false);
            this.$button.prop('disabled', false).removeClass('disabled');
        },

        /**
         * Disable the multiselect.
         */
        disable: function disable() {
            this.$select.prop('disabled', true);
            this.$button.prop('disabled', true).addClass('disabled');
        },

        /**
         * Set the options.
         *
         * @param {Array} options
         */
        setOptions: function setOptions(options) {
            this.options = this.mergeOptions(options);
        },

        /**
         * Merges the given options with the default options.
         *
         * @param {Array} options
         * @returns {Array}
         */
        mergeOptions: function mergeOptions(options) {
            return $.extend(true, {}, this.defaults, this.options, options);
        },

        /**
         * Checks whether a select all checkbox is present.
         *
         * @returns {Boolean}
         */
        hasSelectAll: function hasSelectAll() {
            return $('li.multiselect-all', this.$ul).length > 0;
        },

        /**
         * Update opt groups.
         */
        updateOptGroups: function updateOptGroups() {
            var $groups = $('li.multiselect-group', this.$ul);
            var selectedClass = this.options.selectedClass;

            $groups.each(function () {
                var $options = $(this).nextUntil('li.multiselect-group').not('.multiselect-filter-hidden').not('.disabled');

                var checked = true;
                $options.each(function () {
                    var $input = $('input', this);

                    if (!$input.prop('checked')) {
                        checked = false;
                    }
                });

                if (selectedClass) {
                    if (checked) {
                        $(this).addClass(selectedClass);
                    } else {
                        $(this).removeClass(selectedClass);
                    }
                }

                $('input', this).prop('checked', checked);
            });
        },

        /**
         * Updates the select all checkbox based on the currently displayed and selected checkboxes.
         */
        updateSelectAll: function updateSelectAll(notTriggerOnSelectAll) {
            if (this.hasSelectAll()) {
                var allBoxes = $("li:not(.multiselect-item):not(.multiselect-filter-hidden):not(.multiselect-group):not(.disabled) input:enabled", this.$ul);
                var allBoxesLength = allBoxes.length;
                var checkedBoxesLength = allBoxes.filter(":checked").length;
                var selectAllLi = $("li.multiselect-all", this.$ul);
                var selectAllInput = selectAllLi.find("input");

                if (checkedBoxesLength > 0 && checkedBoxesLength === allBoxesLength) {
                    selectAllInput.prop("checked", true);
                    selectAllLi.addClass(this.options.selectedClass);
                } else {
                    selectAllInput.prop("checked", false);
                    selectAllLi.removeClass(this.options.selectedClass);
                }
            }
        },

        /**
         * Update the button text and its title based on the currently selected options.
         */
        updateButtonText: function updateButtonText() {
            var options = this.getSelected();

            // First update the displayed button text.
            if (this.options.enableHTML) {
                $('.multiselect .multiselect-selected-text', this.$container).html(this.options.buttonText(options, this.$select));
            } else {
                $('.multiselect .multiselect-selected-text', this.$container).text(this.options.buttonText(options, this.$select));
            }

            // Now update the title attribute of the button.
            $('.multiselect', this.$container).attr('title', this.options.buttonTitle(options, this.$select));
        },

        /**
         * Get all selected options.
         *
         * @returns {jQUery}
         */
        getSelected: function getSelected() {
            return $('option', this.$select).filter(":selected");
        },

        /**
         * Gets a select option by its value.
         *
         * @param {String} value
         * @returns {jQuery}
         */
        getOptionByValue: function getOptionByValue(value) {

            var options = $('option', this.$select);
            var valueToCompare = value.toString();

            for (var i = 0; i < options.length; i = i + 1) {
                var option = options[i];
                if (option.value === valueToCompare) {
                    return $(option);
                }
            }
        },

        /**
         * Get the input (radio/checkbox) by its value.
         *
         * @param {String} value
         * @returns {jQuery}
         */
        getInputByValue: function getInputByValue(value) {

            var checkboxes = $('li input:not(.multiselect-search)', this.$ul);
            var valueToCompare = value.toString();

            for (var i = 0; i < checkboxes.length; i = i + 1) {
                var checkbox = checkboxes[i];
                if (checkbox.value === valueToCompare) {
                    return $(checkbox);
                }
            }
        },

        /**
         * Used for knockout integration.
         */
        updateOriginalOptions: function updateOriginalOptions() {
            this.originalOptions = this.$select.clone()[0].options;
        },

        asyncFunction: function asyncFunction(callback, timeout, self) {
            var args = Array.prototype.slice.call(arguments, 3);
            return setTimeout(function () {
                callback.apply(self || window, args);
            }, timeout);
        },

        setAllSelectedText: function setAllSelectedText(allSelectedText) {
            this.options.allSelectedText = allSelectedText;
            this.updateButtonText();
        }
    };

    $.fn.multiselect = function (option, parameter, extraOptions) {
        return this.each(function () {
            var data = $(this).data('multiselect');
            var options = (typeof option === "undefined" ? "undefined" : _typeof(option)) === 'object' && option;

            // Initialize the multiselect.
            if (!data) {
                data = new Multiselect(this, options);
                $(this).data('multiselect', data);
            }

            // Call multiselect method.
            if (typeof option === 'string') {
                data[option](parameter, extraOptions);

                if (option === 'destroy') {
                    $(this).data('multiselect', false);
                }
            }
        });
    };

    $.fn.multiselect.Constructor = Multiselect;

    $(function () {
        $("select[data-role=multiselect]").multiselect();
    });
});

/** @license
 *
 *     Colour Palette Generator script.
 *     Copyright (c) 2014 Google Inc.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License"); you may
 *     not use this file except in compliance with the License.  You may
 *     obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *     implied.  See the License for the specific language governing
 *     permissions and limitations under the License.
 *
 * Furthermore, ColorBrewer colour schemes are covered by the following:
 *
 *     Copyright (c) 2002 Cynthia Brewer, Mark Harrower, and
 *                        The Pennsylvania State University.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License"); you may
 *     not use this file except in compliance with the License. You may obtain
 *     a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *     implied. See the License for the specific language governing
 *     permissions and limitations under the License.
 *
 *     Redistribution and use in source and binary forms, with or without
 *     modification, are permitted provided that the following conditions are
 *     met:
 *
 *     1. Redistributions as source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. The end-user documentation included with the redistribution, if any,
 *     must include the following acknowledgment: "This product includes color
 *     specifications and designs developed by Cynthia Brewer
 *     (http://colorbrewer.org/)." Alternately, this acknowledgment may appear
 *     in the software itself, if and wherever such third-party
 *     acknowledgments normally appear.
 *
 *     4. The name "ColorBrewer" must not be used to endorse or promote products
 *     derived from this software without prior written permission. For written
 *     permission, please contact Cynthia Brewer at cbrewer@psu.edu.
 *
 *     5. Products derived from this software may not be called "ColorBrewer",
 *     nor may "ColorBrewer" appear in their name, without prior written
 *     permission of Cynthia Brewer.
 *
 * Furthermore, Solarized colour schemes are covered by the following:
 *
 *     Copyright (c) 2011 Ethan Schoonover
 *
 *     Permission is hereby granted, free of charge, to any person obtaining
 *     a copy of this software and associated documentation files (the
 *     "Software"), to deal in the Software without restriction, including
 *     without limitation the rights to use, copy, modify, merge, publish,
 *     distribute, sublicense, and/or sell copies of the Software, and to
 *     permit persons to whom the Software is furnished to do so, subject to
 *     the following conditions:
 *
 *     The above copyright notice and this permission notice shall be included
 *     in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *     OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *     LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *     OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *     WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

'use strict';

var palette = function () {

    var proto = Array.prototype;
    var slice = function slice(arr, opt_begin, opt_end) {
        return proto.slice.apply(arr, proto.slice.call(arguments, 1));
    };

    var extend = function extend(arr, arr2) {
        return proto.push.apply(arr, arr2);
    };

    var function_type = _typeof(function () {});

    var INF = 1000000000; // As far as we're concerned, that's infinity. ;)


    /**
     * Generate a colour palette from given scheme.
     *
     * If scheme argument is not a function it is passed to palettes.listSchemes
     * function (along with the number argument).  This may result in an array
     * of more than one available scheme.  If that is the case, scheme at
     * opt_index position is taken.
     *
     * This allows using different palettes for different data without having to
     * name the schemes specifically, for example:
     *
     *     palette_for_foo = palette('sequential', 10, 0);
     *     palette_for_bar = palette('sequential', 10, 1);
     *     palette_for_baz = palette('sequential', 10, 2);
     *
     * @param {!palette.SchemeType|string|palette.Palette} scheme Scheme to
     *     generate palette for.  Either a function constructed with
     *     palette.Scheme object, or anything that palette.listSchemes accepts
     *     as name argument.
     * @param {number} number Number of colours to return.  If negative, absolute
     *     value is taken and colours will be returned in reverse order.
     * @param {number=} opt_index If scheme is a name of a group or an array and
     *     results in more than one scheme, index of the scheme to use.  The
     *     index wraps around.
     * @param {...*} varargs Additional arguments to pass to palette or colour
     *     generator (if the chosen scheme uses those).
     * @return {Array<string>} Array of abs(number) 'RRGGBB' strings or null if
     *     no matching scheme was found.
     */
    var palette = function palette(scheme, number, opt_index, varargs) {
        number |= 0;
        if (number == 0) {
            return [];
        }

        if ((typeof scheme === "undefined" ? "undefined" : _typeof(scheme)) !== function_type) {
            var arr = palette.listSchemes(
            /** @type {string|palette.Palette} */scheme, number);
            if (!arr.length) {
                return null;
            }
            scheme = arr[(opt_index || 0) % arr.length];
        }

        var args = slice(arguments, 2);
        args[0] = number;
        return scheme.apply(scheme, args);
    };

    /**
     * Returns a callable colour scheme object.
     *
     * Just after being created, the scheme has no colour palettes and no way of
     * generating any, thus generate method will return null.  To turn scheme
     * into a useful object, addPalette, addPalettes or setColorFunction methods
     * need to be used.
     *
     * To generate a colour palette with given number colours using function
     * returned by this method, just call it with desired number of colours.
     *
     * Since this function *returns* a callable object, it must *not* be used
     * with the new operator.
     *
     * @param {string} name Name of the scheme.
     * @param {string|!Array<string>=} opt_groups A group name or list of
     *     groups the scheme should be categorised under.  Three typical groups
     *     to use are 'qualitative', 'sequential' and 'diverging', but any
     *     groups may be created.
     * @return {!palette.SchemeType} A colour palette generator function, which
     *     in addition has methods and properties like a regular object.  Think
     *     of it as a callable object.
     */
    palette.Scheme = function (name, opt_groups) {
        /**
         * A map from a number to a colour palettes with given number of colours.
         * @type {!Object<number, palette.Palette>}
         */
        var palettes = {};

        /**
         * The biggest palette in palettes map.
         * @type {number}
         */
        var palettes_max = 0;

        /**
         * The smallest palette in palettes map.
         * @type {number}
         */
        var palettes_min = INF;

        var makeGenerator = function makeGenerator() {
            if (arguments.length <= 1) {
                return self.color_func.bind(self);
            } else {
                var args = slice(arguments);
                return function (x) {
                    args[0] = x;
                    return self.color_func.apply(self, args);
                };
            }
        };

        /**
         * Generate a colour palette from the scheme.
         *
         * If there was a palette added with addPalette (or addPalettes) with
         * enough colours, that palette will be used.  Otherwise, if colour
         * function has been set using setColorFunction method, that function will
         * be used to generate the palette.  Otherwise null is returned.
         *
         * @param {number} number Number of colours to return.  If negative,
         *     absolute value is taken and colours will be returned in reverse
         *     order.
         * @param {...*} varargs Additional arguments to pass to palette or colour
         *     generator (if the chosen scheme uses those).
         */
        var self = function self(number, varargs) {
            number |= 0;
            if (!number) {
                return [];
            }

            var _number = number;
            number = Math.abs(number);

            if (number <= palettes_max) {
                for (var i = Math.max(number, palettes_min); !(i in palettes); ++i) {
                    /* nop */
                }
                var colors = palettes[i];
                if (i > number) {
                    var take_head = 'shrinking_takes_head' in colors ? colors.shrinking_takes_head : self.shrinking_takes_head;
                    if (take_head) {
                        colors = colors.slice(0, number);
                        i = number;
                    } else {
                        return palette.generate(function (x) {
                            return colors[Math.round(x)];
                        }, _number, 0, colors.length - 1);
                    }
                }
                colors = colors.slice();
                if (_number < 0) {
                    colors.reverse();
                }
                return colors;
            } else if (self.color_func) {
                return palette.generate(makeGenerator.apply(self, arguments), _number, 0, 1, self.color_func_cyclic);
            } else {
                return null;
            }
        };

        /**
         * The name of the palette.
         * @type {string}
         */
        self.scheme_name = name;

        /**
         * A list of groups the palette belongs to.
         * @type {!Array<string>}
         */
        self.groups = opt_groups ? typeof opt_groups === 'string' ? [opt_groups] : opt_groups : [];

        /**
         * The biggest palette this scheme can generate.
         * @type {number}
         */
        self.max = 0;

        /**
         * The biggest palette this scheme can generate that is colour-blind
         * friendly.
         * @type {number}
         */
        self.cbf_max = INF;

        /**
         * Adds a colour palette to the colour scheme.
         *
         * @param {palette.Palette} palette An array of 'RRGGBB' strings
         *     representing the palette to add.
         * @param {boolean=} opt_is_cbf Whether the palette is colourblind friendly.
         */
        self.addPalette = function (palette, opt_is_cbf) {
            var len = palette.length;
            if (len) {
                palettes[len] = palette;
                palettes_min = Math.min(palettes_min, len);
                palettes_max = Math.max(palettes_max, len);
                self.max = Math.max(self.max, len);
                if (!opt_is_cbf && len != 1) {
                    self.cbf_max = Math.min(self.cbf_max, len - 1);
                }
            }
        };

        /**
         * Adds number of colour palettes to the colour scheme.
         *
         * @param {palette.PalettesList} palettes A map or an array of colour
         *     palettes to add.  If map, i.e.  object, is used, properties should
         *     use integer property names.
         * @param {number=} opt_max Size of the biggest palette in palettes set.
         *     If not set, palettes must have a length property which will be used.
         * @param {number=} opt_cbf_max Size of the biggest palette which is still
         *     colourblind friendly.  1 by default.
         */
        self.addPalettes = function (palettes, opt_max, opt_cbf_max) {
            opt_max = opt_max || palettes.length;
            for (var i = 0; i < opt_max; ++i) {
                if (i in palettes) {
                    self.addPalette(palettes[i], true);
                }
            }
            self.cbf_max = Math.min(self.cbf_max, opt_cbf_max || 1);
        };

        /**
         * Enable shrinking palettes taking head of the list of colours.
         *
         * When user requests n-colour palette but the smallest palette added with
         * addPalette (or addPalettes) is m-colour one (where n < m), n colours
         * across the palette will be returned.  For example:
         *     var ex = palette.Scheme('ex');
         *     ex.addPalette(['000000', 'bcbcbc', 'ffffff']);
         *     var pal = ex(2);
         *     // pal == ['000000', 'ffffff']
         *
         * This works for palettes where the distance between colours is
         * correlated to distance in the palette array, which is true in gradients
         * such as the one above.
         *
         * To turn this feature off shrinkByTakingHead can be set to true either
         * for all palettes in the scheme (if opt_idx is not given) or for palette
         * with given number of colours only.  In general, setting the option for
         * given palette overwrites whatever has been set for the scheme.  The
         * default, as described above, is false.
         *
         * Alternatively, the feature can be enabled by setting shrinking_takes_head
         * property for the palette Array or the scheme object.
         *
         * For example, all of the below give equivalent results:
         *     var pal = ['ff0000', '00ff00', '0000ff'];
         *
         *     var ex = palette.Scheme('ex');
         *     ex.addPalette(pal);               // ex(2) == ['ff0000', '0000ff']
         *     ex.shrinkByTakingHead(true);      // ex(2) == ['ff0000', '00ff00']
         *
         *     ex = palette.Scheme('ex');
         *     ex.addPalette(pal);               // ex(2) == ['ff0000', '0000ff']
         *     ex.shrinkByTakingHead(true, 3);   // ex(2) == ['ff0000', '00ff00']
         *
         *     ex = palette.Scheme('ex');
         *     ex.addPalette(pal);
         *     ex.addPalette(pal);               // ex(2) == ['ff0000', '0000ff']
         *     pal.shrinking_takes_head = true;  // ex(2) == ['ff0000', '00ff00']
         *
         * @param {boolean} enabled Whether to enable or disable the “shrinking
         *     takes head” feature.  It is disabled by default.
         * @param {number=} opt_idx If given, the “shrinking takes head” option
         *     for palette with given number of colours is set.  If such palette
         *     does not exist, nothing happens.
         */
        self.shrinkByTakingHead = function (enabled, opt_idx) {
            if (opt_idx !== void 0) {
                if (opt_idx in palettes) {
                    palettes[opt_idx].shrinking_takes_head = !!enabled;
                }
            } else {
                self.shrinking_takes_head = !!enabled;
            }
        };

        /**
         * Sets a colour generation function of the colour scheme.
         *
         * The function must accept a singe number argument whose value can be from
         * 0.0 to 1.0, and return a colour as an 'RRGGBB' string.  This function
         * will be used when generating palettes, i.e. if 11-colour palette is
         * requested, this function will be called with arguments 0.0, 0.1, …, 1.0.
         *
         * If the palette generated by the function is colourblind friendly,
         * opt_is_cbf should be set to true.
         *
         * In some cases, it is not desirable to reach 1.0 when generating
         * a palette.  This happens for hue-rainbows where the 0–1 range corresponds
         * to a 0°–360° range in hues, and since hue at 0° is the same as at 360°,
         * it's desired to stop short the end of the range when generating
         * a palette.  To accomplish this, opt_cyclic should be set to true.
         *
         * @param {palette.ColorFunction} func A colour generator function.
         * @param {boolean=} opt_is_cbf Whether palette generate with the function
         *     is colour-blind friendly.
         * @param {boolean=} opt_cyclic Whether colour at 0.0 is the same as the
         *     one at 1.0.
         */
        self.setColorFunction = function (func, opt_is_cbf, opt_cyclic) {
            self.color_func = func;
            self.color_func_cyclic = !!opt_cyclic;
            self.max = INF;
            if (!opt_is_cbf && self.cbf_max === INF) {
                self.cbf_max = 1;
            }
        };

        self.color = function (x, varargs) {
            if (self.color_func) {
                return self.color_func.apply(this, arguments);
            } else {
                return null;
            }
        };

        return self;
    };

    /**
     * Creates a new palette.Scheme and initialises it by calling addPalettes
     * method with the rest of the arguments.
     *
     * @param {string} name Name of the scheme.
     * @param {string|!Array<string>} groups A group name or list of group
     *     names the scheme belongs to.
     * @param {!Object<number, palette.Palette>|!Array<palette.Palette>}
     *     palettes A map or an array of colour palettes to add.  If map, i.e.
     *     object, is used, properties should use integer property names.
     * @param {number=} opt_max Size of the biggest palette in palettes set.
     *     If not set, palettes must have a length property which will be used.
     * @param {number=} opt_cbf_max Size of the biggest palette which is still
     *     colourblind friendly.  1 by default.
     * @return {!palette.SchemeType} A colour palette generator function, which
     *     in addition has methods and properties like a regular object.  Think
     *     of it as a callable object.
     */
    palette.Scheme.fromPalettes = function (name, groups, palettes, opt_max, opt_cbf_max) {
        var scheme = palette.Scheme(name, groups);
        scheme.addPalettes.apply(scheme, slice(arguments, 2));
        return scheme;
    };

    /**
     * Creates a new palette.Scheme and initialises it by calling
     * setColorFunction method with the rest of the arguments.
     *
     * @param {string} name Name of the scheme.
     * @param {string|!Array<string>} groups A group name or list of group
     *     names the scheme belongs to.
     * @param {palette.ColorFunction} func A colour generator function.
     * @param {boolean=} opt_is_cbf Whether palette generate with the function
     *     is colour-blind friendly.
     * @param {boolean=} opt_cyclic Whether colour at 0.0 is the same as the
     *     one at 1.0.
     * @return {!palette.SchemeType} A colour palette generator function, which
     *     in addition has methods and properties like a regular object.  Think
     *     of it as a callable object.
     */
    palette.Scheme.withColorFunction = function (name, groups, func, opt_is_cbf, opt_cyclic) {
        var scheme = palette.Scheme(name, groups);
        scheme.setColorFunction.apply(scheme, slice(arguments, 2));
        return scheme;
    };

    /**
     * A map of registered schemes.  Maps a scheme or group name to a list of
     * scheme objects.  Property name is either 'n-<name>' for single scheme
     * names or 'g-<name>' for scheme group names.
     *
     * @type {!Object<string, !Array<!Object>>}
     */
    var registered_schemes = {};

    /**
     * Registers a new colour scheme.
     *
     * @param {!palette.SchemeType} scheme The scheme to add.
     */
    palette.register = function (scheme) {
        registered_schemes['n-' + scheme.scheme_name] = [scheme];
        scheme.groups.forEach(function (g) {
            (registered_schemes['g-' + g] = registered_schemes['g-' + g] || []).push(scheme);
        });
        (registered_schemes['g-all'] = registered_schemes['g-all'] || []).push(scheme);
    };

    /**
     * List all schemes that match given name and number of colours.
     *
     * name argument can be either a string or an array of strings.  In the
     * former case, the function acts as if the argument was an array with name
     * as a single argument (i.e. “palette.listSchemes('foo')” is exactly the same
     * as “palette.listSchemes(['foo'])”).
     *
     * Each name can be either name of a palette (e.g. 'tol-sq' for Paul Tol's
     * sequential palette), or a name of a group (e.g. 'sequential' for all
     * sequential palettes).  Name can therefore map to a single scheme or
     * several schemes.
     *
     * Furthermore, name can be suffixed with '-cbf' to indicate that only
     * schemes that are colourblind friendly should be returned.  For example,
     * 'rainbow' returns a HSV rainbow scheme, but because it is not colourblind
     * friendly, 'rainbow-cbf' returns no schemes.
     *
     * Some schemes may produce colourblind friendly palettes for some number of
     * colours.  For example ColorBrewer's Dark2 scheme is colourblind friendly
     * if no more than 3 colours are generated.  If opt_number is not specified,
     * 'qualitative-cbf' will include 'cb-Dark2' but if opt_number is given as,
     * say, 5 it won't.
     *
     * Name can also be 'all' which will return all registered schemes.
     * Naturally, 'all-cbf' will return all colourblind friendly schemes.
     *
     * Schemes are added to the library using palette.register.  Schemes are
     * created using palette.Scheme function.  By default, the following schemes
     * are available:
     *
     *     Name            Description
     *     --------------  -----------------------------------------------------
     *     tol             Paul Tol's qualitative scheme, cbf, max 12 colours.
     *     tol-dv          Paul Tol's diverging scheme, cbf.
     *     tol-sq          Paul Tol's sequential scheme, cbf.
     *     tol-rainbow     Paul Tol's qualitative scheme, cbf.
     *
     *     rainbow         A rainbow palette.
     *
     *     cb-YlGn         ColorBrewer sequential schemes.
     *     cb-YlGnBu
     *     cb-GnBu
     *     cb-BuGn
     *     cb-PuBuGn
     *     cb-PuBu
     *     cb-BuPu
     *     cb-RdPu
     *     cb-PuRd
     *     cb-OrRd
     *     cb-YlOrRd
     *     cb-YlOrBr
     *     cb-Purples
     *     cb-Blues
     *     cb-Greens
     *     cb-Oranges
     *     cb-Reds
     *     cb-Greys
     *
     *     cb-PuOr         ColorBrewer diverging schemes.
     *     cb-BrBG
     *     cb-PRGn
     *     cb-PiYG
     *     cb-RdBu
     *     cb-RdGy
     *     cb-RdYlBu
     *     cb-Spectral
     *     cb-RdYlGn
     *
     *     cb-Accent       ColorBrewer qualitative schemes.
     *     cb-Dark2
     *     cb-Paired
     *     cb-Pastel1
     *     cb-Pastel2
     *     cb-Set1
     *     cb-Set2
     *     cb-Set3
     *
     *     sol-base        Solarized base colours.
     *     sol-accent      Solarized accent colours.
     *
     * The following groups are also available by default:
     *
     *     Name            Description
     *     --------------  -----------------------------------------------------
     *     all             All registered schemes.
     *     sequential      All sequential schemes.
     *     diverging       All diverging schemes.
     *     qualitative     All qualitative schemes.
     *     cb-sequential   All ColorBrewer sequential schemes.
     *     cb-diverging    All ColorBrewer diverging schemes.
     *     cb-qualitative  All ColorBrewer qualitative schemes.
     *
     * You can read more about Paul Tol's palettes at http://www.sron.nl/~pault/.
     * You can read more about ColorBrewer at http://colorbrewer2.org.
     *
     * @param {string|!Array<string>} name A name of a colour scheme, of
     *     a group of colour schemes, or an array of any of those.
     * @param {number=} opt_number When requesting only colourblind friendly
     *     schemes, number of colours the scheme must provide generating such
     *     that the palette is still colourblind friendly.  2 by default.
     * @return {!Array<!palette.SchemeType>} An array of colour scheme objects
     *     matching the criteria.  Sorted by scheme name.
     */
    palette.listSchemes = function (name, opt_number) {
        if (!opt_number) {
            opt_number = 2;
        } else if (opt_number < 0) {
            opt_number = -opt_number;
        }

        var ret = [];
        (typeof name === 'string' ? [name] : name).forEach(function (n) {
            var cbf = n.substring(n.length - 4) === '-cbf';
            if (cbf) {
                n = n.substring(0, n.length - 4);
            }
            var schemes = registered_schemes['g-' + n] || registered_schemes['n-' + n] || [];
            for (var i = 0, scheme; scheme = schemes[i]; ++i) {
                if ((cbf ? scheme.cbf : scheme.max) >= opt_number) {
                    ret.push(scheme);
                }
            }
        });

        ret.sort(function (a, b) {
            return a.scheme_name >= b.scheme_name ? a.scheme_name > b.scheme_name ? 1 : 0 : -1;
        });
        return ret;
    };

    /**
     * Generates a palette using given colour generating function.
     *
     * The color_func callback must accept a singe number argument whose value
     * can vary from 0.0 to 1.0 (or in general from opt_start to opt_end), and
     * return a colour as an 'RRGGBB' string.  This function will be used when
     * generating palettes, i.e. if 11-colour palette is requested, this
     * function will be called with arguments 0.0, 0.1, …, 1.0.
     *
     * In some cases, it is not desirable to reach 1.0 when generating
     * a palette.  This happens for hue-rainbows where the 0–1 range corresponds
     * to a 0°–360° range in hues, and since hue at 0° is the same as at 360°,
     * it's desired to stop short the end of the range when generating
     * a palette.  To accomplish this, opt_cyclic should be set to true.
     *
     * opt_start and opt_end may be used to change the range the colour
     * generation function is called with.  opt_end may be less than opt_start
     * which will case to traverse the range in reverse.  Another way to reverse
     * the palette is requesting negative number of colours.  The two methods do
     * not always lead to the same results (especially if opt_cyclic is set).
     *
     * @param {palette.ColorFunction} color_func A colours generating callback
     *     function.
     * @param {number} number Number of colours to generate in the palette.  If
     *     number is negative, colours in the palette will be reversed.  If only
     *     one colour is requested, colour at opt_start will be returned.
     * @param {number=} opt_start Optional starting point for the palette
     *     generation function.  Zero by default.
     * @param {number=} opt_end Optional ending point for the palette generation
     *     function.  One by default.
     * @param {boolean=} opt_cyclic If true, function will assume colour at
     *     point opt_start is the same as one at opt_end.
     * @return {palette.Palette} An array of 'RRGGBB' colours.
     */
    palette.generate = function (color_func, number, opt_start, opt_end, opt_cyclic) {
        if (Math.abs(number) < 1) {
            return [];
        }

        opt_start = opt_start === void 0 ? 0 : opt_start;
        opt_end = opt_end === void 0 ? 1 : opt_end;

        if (Math.abs(number) < 2) {
            return [color_func(opt_start)];
        }

        var i = Math.abs(number);
        var x = opt_start;
        var ret = [];
        var step = (opt_end - opt_start) / (opt_cyclic ? i : i - 1);

        for (; --i >= 0; x += step) {
            ret.push(color_func(x));
        }
        if (number < 0) {
            ret.reverse();
        }
        return ret;
    };

    /**
     * Clamps value to [0, 1] range.
     * @param {number} v Number to limit value of.
     * @return {number} If v is inside of [0, 1] range returns v, otherwise
     *     returns 0 or 1 depending which side of the range v is closer to.
     */
    var clamp = function clamp(v) {
        return v > 0 ? v < 1 ? v : 1 : 0;
    };

    /**
     * Converts r, g, b triple into RRGGBB hex representation.
     * @param {number} r Red value of the colour in the range [0, 1].
     * @param {number} g Green value of the colour in the range [0, 1].
     * @param {number} b Blue value of the colour in the range [0, 1].
     * @return {string} A lower-case RRGGBB representation of the colour.
     */
    palette.rgbColor = function (r, g, b) {
        return [r, g, b].map(function (v) {
            v = Number(Math.round(clamp(v) * 255)).toString(16);
            return v.length == 1 ? '0' + v : v;
        }).join('');
    };

    /**
     * Converts a linear r, g, b triple into RRGGBB hex representation.
     * @param {number} r Linear red value of the colour in the range [0, 1].
     * @param {number} g Linear green value of the colour in the range [0, 1].
     * @param {number} b Linear blue value of the colour in the range [0, 1].
     * @return {string} A lower-case RRGGBB representation of the colour.
     */
    palette.linearRgbColor = function (r, g, b) {
        // http://www.brucelindbloom.com/index.html?Eqn_XYZ_to_RGB.html
        return [r, g, b].map(function (v) {
            v = clamp(v);
            if (v <= 0.0031308) {
                v = 12.92 * v;
            } else {
                v = 1.055 * Math.pow(v, 1 / 2.4) - 0.055;
            }
            v = Number(Math.round(v * 255)).toString(16);
            return v.length == 1 ? '0' + v : v;
        }).join('');
    };

    /**
     * Converts an HSV colours to RRGGBB hex representation.
     * @param {number} h Hue in the range [0, 1].
     * @param {number=} opt_s Saturation in the range [0, 1].  One by default.
     * @param {number=} opt_v Value in the range [0, 1].  One by default.
     * @return {string} An RRGGBB representation of the colour.
     */
    palette.hsvColor = function (h, opt_s, opt_v) {
        h *= 6;
        var s = opt_s === void 0 ? 1 : clamp(opt_s);
        var v = opt_v === void 0 ? 1 : clamp(opt_v);
        var x = v * (1 - s * Math.abs(h % 2 - 1));
        var m = v * (1 - s);
        switch (Math.floor(h) % 6) {
            case 0:
                return palette.rgbColor(v, x, m);
            case 1:
                return palette.rgbColor(x, v, m);
            case 2:
                return palette.rgbColor(m, v, x);
            case 3:
                return palette.rgbColor(m, x, v);
            case 4:
                return palette.rgbColor(x, m, v);
            default:
                return palette.rgbColor(v, m, x);
        }
    };

    palette.register(palette.Scheme.withColorFunction('rainbow', 'qualitative', palette.hsvColor, false, true));

    return palette;
}();

/** @typedef {function(number): string} */
palette.ColorFunction;

/** @typedef {!Array<string>} */
palette.Palette;

/** @typedef {!Object<number, palette.Palette>|!Array<palette.Palette>} */
palette.PalettesList;

/**
 * @typedef {
 *   function(number, ...?): Array<string>|
 *   {
 *     scheme_name: string,
 *     groups: !Array<string>,
 *     max: number,
 *     cbf_max: number,
 *     addPalette: function(!Array<string>, boolean=),
 *     addPalettes: function(palette.PalettesList, number=, number=),
 *     shrinkByTakingHead: function(boolean, number=),
 *     setColorFunction: function(palette.ColorFunction, boolean=, boolean=),
 *     color: function(number, ...?): ?string}}
 */
palette.SchemeType;

/* Paul Tol's schemes start here. *******************************************/
/* See http://www.sron.nl/~pault/ */

(function () {
    var rgb = palette.rgbColor;

    /**
     * Calculates value of a polynomial at given point.
     * For example, poly(x, 1, 2, 3) calculates value of “1 + 2*x + 2*X^2”.
     * @param {number} x Value to calculate polynomial for.
     * @param {...number} varargs Coefficients of the polynomial specified in
     *     the order of rising powers of x including constant as the first
     *     variable argument.
     */
    var poly = function poly(x, varargs) {
        var i = arguments.length - 1,
            n = arguments[i];
        while (i > 1) {
            n = n * x + arguments[--i];
        }
        return n;
    };

    /**
     * Calculate approximate value of error function with maximum error of 0.0005.
     * See <https://en.wikipedia.org/wiki/Error_function>.
     * @param {number} x Argument of the error function.
     * @return {number} Value of error function for x.
     */
    var erf = function erf(x) {
        // https://en.wikipedia.org/wiki/Error_function#Approximation_with_elementary_functions
        // This produces a maximum error of 0.0005 which is more then we need.  In
        // the worst case, that error is multiplied by four and then divided by two
        // before being multiplied by 255, so in the end, the error is multiplied by
        // 510 which produces 0.255 which is less than a single colour step.
        var y = poly(Math.abs(x), 1, 0.278393, 0.230389, 0.000972, 0.078108);
        y *= y; // y^2
        y *= y; // y^4
        y = 1 - 1 / y;
        return x < 0 ? -y : y;
    };

    palette.register(palette.Scheme.fromPalettes('tol', 'qualitative', [['4477aa'], ['4477aa', 'cc6677'], ['4477aa', 'ddcc77', 'cc6677'], ['4477aa', '117733', 'ddcc77', 'cc6677'], ['332288', '88ccee', '117733', 'ddcc77', 'cc6677'], ['332288', '88ccee', '117733', 'ddcc77', 'cc6677', 'aa4499'], ['332288', '88ccee', '44aa99', '117733', 'ddcc77', 'cc6677', 'aa4499'], ['332288', '88ccee', '44aa99', '117733', '999933', 'ddcc77', 'cc6677', 'aa4499'], ['332288', '88ccee', '44aa99', '117733', '999933', 'ddcc77', 'cc6677', '882255', 'aa4499'], ['332288', '88ccee', '44aa99', '117733', '999933', 'ddcc77', '661100', 'cc6677', '882255', 'aa4499'], ['332288', '6699cc', '88ccee', '44aa99', '117733', '999933', 'ddcc77', '661100', 'cc6677', '882255', 'aa4499'], ['332288', '6699cc', '88ccee', '44aa99', '117733', '999933', 'ddcc77', '661100', 'cc6677', 'aa4466', '882255', 'aa4499']], 12, 12));

    /**
     * Calculates a colour along Paul Tol's sequential colours axis.
     * See <http://www.sron.nl/~pault/colourschemes.pdf> figure 7 and equation 1.
     * @param {number} x Position of the colour on the axis in the [0, 1] range.
     * @return {string} An RRGGBB representation of the colour.
     */
    palette.tolSequentialColor = function (x) {
        return rgb(1 - 0.392 * (1 + erf((x - 0.869) / 0.255)), 1.021 - 0.456 * (1 + erf((x - 0.527) / 0.376)), 1 - 0.493 * (1 + erf((x - 0.272) / 0.309)));
    };

    palette.register(palette.Scheme.withColorFunction('tol-sq', 'sequential', palette.tolSequentialColor, true));

    /**
     * Calculates a colour along Paul Tol's diverging colours axis.
     * See <http://www.sron.nl/~pault/colourschemes.pdf> figure 8 and equation 2.
     * @param {number} x Position of the colour on the axis in the [0, 1] range.
     * @return {string} An RRGGBB representation of the colour.
     */
    palette.tolDivergingColor = function (x) {
        var g = poly(x, 0.572, 1.524, -1.811) / poly(x, 1, -0.291, 0.1574);
        return rgb(poly(x, 0.235, -2.13, 26.92, -65.5, 63.5, -22.36), g * g, 1 / poly(x, 1.579, -4.03, 12.92, -31.4, 48.6, -23.36));
    };

    palette.register(palette.Scheme.withColorFunction('tol-dv', 'diverging', palette.tolDivergingColor, true));

    /**
     * Calculates a colour along Paul Tol's rainbow colours axis.
     * See <http://www.sron.nl/~pault/colourschemes.pdf> figure 13 and equation 3.
     * @param {number} x Position of the colour on the axis in the [0, 1] range.
     * @return {string} An RRGGBB representation of the colour.
     */
    palette.tolRainbowColor = function (x) {
        return rgb(poly(x, 0.472, -0.567, 4.05) / poly(x, 1, 8.72, -19.17, 14.1), poly(x, 0.108932, -1.22635, 27.284, -98.577, 163.3, -131.395, 40.634), 1 / poly(x, 1.97, 3.54, -68.5, 243, -297, 125));
    };

    palette.register(palette.Scheme.withColorFunction('tol-rainbow', 'qualitative', palette.tolRainbowColor, true));
})();

/* Solarized colour schemes start here. *************************************/
/* See http://ethanschoonover.com/solarized */

(function () {
    /*
     * Those are not really designed to be used in graphs, but we're keeping
     * them here in case someone cares.
     */
    palette.register(palette.Scheme.fromPalettes('sol-base', 'sequential', [['002b36', '073642', '586e75', '657b83', '839496', '93a1a1', 'eee8d5', 'fdf6e3']], 1, 8));
    palette.register(palette.Scheme.fromPalettes('sol-accent', 'qualitative', [['b58900', 'cb4b16', 'dc322f', 'd33682', '6c71c4', '268bd2', '2aa198', '859900']]));
})();

/* ColorBrewer colour schemes start here. ***********************************/
/* See http://colorbrewer2.org/ */

(function () {
    var schemes = {
        YlGn: {
            type: 'sequential',
            cbf: 42,
            3: ['f7fcb9', 'addd8e', '31a354'],
            4: ['ffffcc', 'c2e699', '78c679', '238443'],
            5: ['ffffcc', 'c2e699', '78c679', '31a354', '006837'],
            6: ['ffffcc', 'd9f0a3', 'addd8e', '78c679', '31a354', '006837'],
            7: ['ffffcc', 'd9f0a3', 'addd8e', '78c679', '41ab5d', '238443', '005a32'],
            8: ['ffffe5', 'f7fcb9', 'd9f0a3', 'addd8e', '78c679', '41ab5d', '238443', '005a32'],
            9: ['ffffe5', 'f7fcb9', 'd9f0a3', 'addd8e', '78c679', '41ab5d', '238443', '006837', '004529']
        },
        YlGnBu: {
            type: 'sequential',
            cbf: 42,
            3: ['edf8b1', '7fcdbb', '2c7fb8'],
            4: ['ffffcc', 'a1dab4', '41b6c4', '225ea8'],
            5: ['ffffcc', 'a1dab4', '41b6c4', '2c7fb8', '253494'],
            6: ['ffffcc', 'c7e9b4', '7fcdbb', '41b6c4', '2c7fb8', '253494'],
            7: ['ffffcc', 'c7e9b4', '7fcdbb', '41b6c4', '1d91c0', '225ea8', '0c2c84'],
            8: ['ffffd9', 'edf8b1', 'c7e9b4', '7fcdbb', '41b6c4', '1d91c0', '225ea8', '0c2c84'],
            9: ['ffffd9', 'edf8b1', 'c7e9b4', '7fcdbb', '41b6c4', '1d91c0', '225ea8', '253494', '081d58']
        },
        GnBu: {
            type: 'sequential',
            cbf: 42,
            3: ['e0f3db', 'a8ddb5', '43a2ca'],
            4: ['f0f9e8', 'bae4bc', '7bccc4', '2b8cbe'],
            5: ['f0f9e8', 'bae4bc', '7bccc4', '43a2ca', '0868ac'],
            6: ['f0f9e8', 'ccebc5', 'a8ddb5', '7bccc4', '43a2ca', '0868ac'],
            7: ['f0f9e8', 'ccebc5', 'a8ddb5', '7bccc4', '4eb3d3', '2b8cbe', '08589e'],
            8: ['f7fcf0', 'e0f3db', 'ccebc5', 'a8ddb5', '7bccc4', '4eb3d3', '2b8cbe', '08589e'],
            9: ['f7fcf0', 'e0f3db', 'ccebc5', 'a8ddb5', '7bccc4', '4eb3d3', '2b8cbe', '0868ac', '084081']
        },
        BuGn: {
            type: 'sequential',
            cbf: 42,
            3: ['e5f5f9', '99d8c9', '2ca25f'],
            4: ['edf8fb', 'b2e2e2', '66c2a4', '238b45'],
            5: ['edf8fb', 'b2e2e2', '66c2a4', '2ca25f', '006d2c'],
            6: ['edf8fb', 'ccece6', '99d8c9', '66c2a4', '2ca25f', '006d2c'],
            7: ['edf8fb', 'ccece6', '99d8c9', '66c2a4', '41ae76', '238b45', '005824'],
            8: ['f7fcfd', 'e5f5f9', 'ccece6', '99d8c9', '66c2a4', '41ae76', '238b45', '005824'],
            9: ['f7fcfd', 'e5f5f9', 'ccece6', '99d8c9', '66c2a4', '41ae76', '238b45', '006d2c', '00441b']
        },
        PuBuGn: {
            type: 'sequential',
            cbf: 42,
            3: ['ece2f0', 'a6bddb', '1c9099'],
            4: ['f6eff7', 'bdc9e1', '67a9cf', '02818a'],
            5: ['f6eff7', 'bdc9e1', '67a9cf', '1c9099', '016c59'],
            6: ['f6eff7', 'd0d1e6', 'a6bddb', '67a9cf', '1c9099', '016c59'],
            7: ['f6eff7', 'd0d1e6', 'a6bddb', '67a9cf', '3690c0', '02818a', '016450'],
            8: ['fff7fb', 'ece2f0', 'd0d1e6', 'a6bddb', '67a9cf', '3690c0', '02818a', '016450'],
            9: ['fff7fb', 'ece2f0', 'd0d1e6', 'a6bddb', '67a9cf', '3690c0', '02818a', '016c59', '014636']
        },
        PuBu: {
            type: 'sequential',
            cbf: 42,
            3: ['ece7f2', 'a6bddb', '2b8cbe'],
            4: ['f1eef6', 'bdc9e1', '74a9cf', '0570b0'],
            5: ['f1eef6', 'bdc9e1', '74a9cf', '2b8cbe', '045a8d'],
            6: ['f1eef6', 'd0d1e6', 'a6bddb', '74a9cf', '2b8cbe', '045a8d'],
            7: ['f1eef6', 'd0d1e6', 'a6bddb', '74a9cf', '3690c0', '0570b0', '034e7b'],
            8: ['fff7fb', 'ece7f2', 'd0d1e6', 'a6bddb', '74a9cf', '3690c0', '0570b0', '034e7b'],
            9: ['fff7fb', 'ece7f2', 'd0d1e6', 'a6bddb', '74a9cf', '3690c0', '0570b0', '045a8d', '023858']
        },
        BuPu: {
            type: 'sequential',
            cbf: 42,
            3: ['e0ecf4', '9ebcda', '8856a7'],
            4: ['edf8fb', 'b3cde3', '8c96c6', '88419d'],
            5: ['edf8fb', 'b3cde3', '8c96c6', '8856a7', '810f7c'],
            6: ['edf8fb', 'bfd3e6', '9ebcda', '8c96c6', '8856a7', '810f7c'],
            7: ['edf8fb', 'bfd3e6', '9ebcda', '8c96c6', '8c6bb1', '88419d', '6e016b'],
            8: ['f7fcfd', 'e0ecf4', 'bfd3e6', '9ebcda', '8c96c6', '8c6bb1', '88419d', '6e016b'],
            9: ['f7fcfd', 'e0ecf4', 'bfd3e6', '9ebcda', '8c96c6', '8c6bb1', '88419d', '810f7c', '4d004b']
        },
        RdPu: {
            type: 'sequential',
            cbf: 42,
            3: ['fde0dd', 'fa9fb5', 'c51b8a'],
            4: ['feebe2', 'fbb4b9', 'f768a1', 'ae017e'],
            5: ['feebe2', 'fbb4b9', 'f768a1', 'c51b8a', '7a0177'],
            6: ['feebe2', 'fcc5c0', 'fa9fb5', 'f768a1', 'c51b8a', '7a0177'],
            7: ['feebe2', 'fcc5c0', 'fa9fb5', 'f768a1', 'dd3497', 'ae017e', '7a0177'],
            8: ['fff7f3', 'fde0dd', 'fcc5c0', 'fa9fb5', 'f768a1', 'dd3497', 'ae017e', '7a0177'],
            9: ['fff7f3', 'fde0dd', 'fcc5c0', 'fa9fb5', 'f768a1', 'dd3497', 'ae017e', '7a0177', '49006a']
        },
        PuRd: {
            type: 'sequential',
            cbf: 42,
            3: ['e7e1ef', 'c994c7', 'dd1c77'],
            4: ['f1eef6', 'd7b5d8', 'df65b0', 'ce1256'],
            5: ['f1eef6', 'd7b5d8', 'df65b0', 'dd1c77', '980043'],
            6: ['f1eef6', 'd4b9da', 'c994c7', 'df65b0', 'dd1c77', '980043'],
            7: ['f1eef6', 'd4b9da', 'c994c7', 'df65b0', 'e7298a', 'ce1256', '91003f'],
            8: ['f7f4f9', 'e7e1ef', 'd4b9da', 'c994c7', 'df65b0', 'e7298a', 'ce1256', '91003f'],
            9: ['f7f4f9', 'e7e1ef', 'd4b9da', 'c994c7', 'df65b0', 'e7298a', 'ce1256', '980043', '67001f']
        },
        OrRd: {
            type: 'sequential',
            cbf: 42,
            3: ['fee8c8', 'fdbb84', 'e34a33'],
            4: ['fef0d9', 'fdcc8a', 'fc8d59', 'd7301f'],
            5: ['fef0d9', 'fdcc8a', 'fc8d59', 'e34a33', 'b30000'],
            6: ['fef0d9', 'fdd49e', 'fdbb84', 'fc8d59', 'e34a33', 'b30000'],
            7: ['fef0d9', 'fdd49e', 'fdbb84', 'fc8d59', 'ef6548', 'd7301f', '990000'],
            8: ['fff7ec', 'fee8c8', 'fdd49e', 'fdbb84', 'fc8d59', 'ef6548', 'd7301f', '990000'],
            9: ['fff7ec', 'fee8c8', 'fdd49e', 'fdbb84', 'fc8d59', 'ef6548', 'd7301f', 'b30000', '7f0000']
        },
        YlOrRd: {
            type: 'sequential',
            cbf: 42,
            3: ['ffeda0', 'feb24c', 'f03b20'],
            4: ['ffffb2', 'fecc5c', 'fd8d3c', 'e31a1c'],
            5: ['ffffb2', 'fecc5c', 'fd8d3c', 'f03b20', 'bd0026'],
            6: ['ffffb2', 'fed976', 'feb24c', 'fd8d3c', 'f03b20', 'bd0026'],
            7: ['ffffb2', 'fed976', 'feb24c', 'fd8d3c', 'fc4e2a', 'e31a1c', 'b10026'],
            8: ['ffffcc', 'ffeda0', 'fed976', 'feb24c', 'fd8d3c', 'fc4e2a', 'e31a1c', 'b10026'],
            9: ['ffffcc', 'ffeda0', 'fed976', 'feb24c', 'fd8d3c', 'fc4e2a', 'e31a1c', 'bd0026', '800026']
        },
        YlOrBr: {
            type: 'sequential',
            cbf: 42,
            3: ['fff7bc', 'fec44f', 'd95f0e'],
            4: ['ffffd4', 'fed98e', 'fe9929', 'cc4c02'],
            5: ['ffffd4', 'fed98e', 'fe9929', 'd95f0e', '993404'],
            6: ['ffffd4', 'fee391', 'fec44f', 'fe9929', 'd95f0e', '993404'],
            7: ['ffffd4', 'fee391', 'fec44f', 'fe9929', 'ec7014', 'cc4c02', '8c2d04'],
            8: ['ffffe5', 'fff7bc', 'fee391', 'fec44f', 'fe9929', 'ec7014', 'cc4c02', '8c2d04'],
            9: ['ffffe5', 'fff7bc', 'fee391', 'fec44f', 'fe9929', 'ec7014', 'cc4c02', '993404', '662506']
        },
        Purples: {
            type: 'sequential',
            cbf: 42,
            3: ['efedf5', 'bcbddc', '756bb1'],
            4: ['f2f0f7', 'cbc9e2', '9e9ac8', '6a51a3'],
            5: ['f2f0f7', 'cbc9e2', '9e9ac8', '756bb1', '54278f'],
            6: ['f2f0f7', 'dadaeb', 'bcbddc', '9e9ac8', '756bb1', '54278f'],
            7: ['f2f0f7', 'dadaeb', 'bcbddc', '9e9ac8', '807dba', '6a51a3', '4a1486'],
            8: ['fcfbfd', 'efedf5', 'dadaeb', 'bcbddc', '9e9ac8', '807dba', '6a51a3', '4a1486'],
            9: ['fcfbfd', 'efedf5', 'dadaeb', 'bcbddc', '9e9ac8', '807dba', '6a51a3', '54278f', '3f007d']
        },
        Blues: {
            type: 'sequential',
            cbf: 42,
            3: ['deebf7', '9ecae1', '3182bd'],
            4: ['eff3ff', 'bdd7e7', '6baed6', '2171b5'],
            5: ['eff3ff', 'bdd7e7', '6baed6', '3182bd', '08519c'],
            6: ['eff3ff', 'c6dbef', '9ecae1', '6baed6', '3182bd', '08519c'],
            7: ['eff3ff', 'c6dbef', '9ecae1', '6baed6', '4292c6', '2171b5', '084594'],
            8: ['f7fbff', 'deebf7', 'c6dbef', '9ecae1', '6baed6', '4292c6', '2171b5', '084594'],
            9: ['f7fbff', 'deebf7', 'c6dbef', '9ecae1', '6baed6', '4292c6', '2171b5', '08519c', '08306b']
        },
        Greens: {
            type: 'sequential',
            cbf: 42,
            3: ['e5f5e0', 'a1d99b', '31a354'],
            4: ['edf8e9', 'bae4b3', '74c476', '238b45'],
            5: ['edf8e9', 'bae4b3', '74c476', '31a354', '006d2c'],
            6: ['edf8e9', 'c7e9c0', 'a1d99b', '74c476', '31a354', '006d2c'],
            7: ['edf8e9', 'c7e9c0', 'a1d99b', '74c476', '41ab5d', '238b45', '005a32'],
            8: ['f7fcf5', 'e5f5e0', 'c7e9c0', 'a1d99b', '74c476', '41ab5d', '238b45', '005a32'],
            9: ['f7fcf5', 'e5f5e0', 'c7e9c0', 'a1d99b', '74c476', '41ab5d', '238b45', '006d2c', '00441b']
        },
        Oranges: {
            type: 'sequential',
            cbf: 42,
            3: ['fee6ce', 'fdae6b', 'e6550d'],
            4: ['feedde', 'fdbe85', 'fd8d3c', 'd94701'],
            5: ['feedde', 'fdbe85', 'fd8d3c', 'e6550d', 'a63603'],
            6: ['feedde', 'fdd0a2', 'fdae6b', 'fd8d3c', 'e6550d', 'a63603'],
            7: ['feedde', 'fdd0a2', 'fdae6b', 'fd8d3c', 'f16913', 'd94801', '8c2d04'],
            8: ['fff5eb', 'fee6ce', 'fdd0a2', 'fdae6b', 'fd8d3c', 'f16913', 'd94801', '8c2d04'],
            9: ['fff5eb', 'fee6ce', 'fdd0a2', 'fdae6b', 'fd8d3c', 'f16913', 'd94801', 'a63603', '7f2704']
        },
        Reds: {
            type: 'sequential',
            cbf: 42,
            3: ['fee0d2', 'fc9272', 'de2d26'],
            4: ['fee5d9', 'fcae91', 'fb6a4a', 'cb181d'],
            5: ['fee5d9', 'fcae91', 'fb6a4a', 'de2d26', 'a50f15'],
            6: ['fee5d9', 'fcbba1', 'fc9272', 'fb6a4a', 'de2d26', 'a50f15'],
            7: ['fee5d9', 'fcbba1', 'fc9272', 'fb6a4a', 'ef3b2c', 'cb181d', '99000d'],
            8: ['fff5f0', 'fee0d2', 'fcbba1', 'fc9272', 'fb6a4a', 'ef3b2c', 'cb181d', '99000d'],
            9: ['fff5f0', 'fee0d2', 'fcbba1', 'fc9272', 'fb6a4a', 'ef3b2c', 'cb181d', 'a50f15', '67000d']
        },
        Greys: {
            type: 'sequential',
            cbf: 42,
            3: ['f0f0f0', 'bdbdbd', '636363'],
            4: ['f7f7f7', 'cccccc', '969696', '525252'],
            5: ['f7f7f7', 'cccccc', '969696', '636363', '252525'],
            6: ['f7f7f7', 'd9d9d9', 'bdbdbd', '969696', '636363', '252525'],
            7: ['f7f7f7', 'd9d9d9', 'bdbdbd', '969696', '737373', '525252', '252525'],
            8: ['ffffff', 'f0f0f0', 'd9d9d9', 'bdbdbd', '969696', '737373', '525252', '252525'],
            9: ['ffffff', 'f0f0f0', 'd9d9d9', 'bdbdbd', '969696', '737373', '525252', '252525', '000000']
        },
        PuOr: {
            type: 'diverging',
            cbf: 42,
            3: ['f1a340', 'f7f7f7', '998ec3'],
            4: ['e66101', 'fdb863', 'b2abd2', '5e3c99'],
            5: ['e66101', 'fdb863', 'f7f7f7', 'b2abd2', '5e3c99'],
            6: ['b35806', 'f1a340', 'fee0b6', 'd8daeb', '998ec3', '542788'],
            7: ['b35806', 'f1a340', 'fee0b6', 'f7f7f7', 'd8daeb', '998ec3', '542788'],
            8: ['b35806', 'e08214', 'fdb863', 'fee0b6', 'd8daeb', 'b2abd2', '8073ac', '542788'],
            9: ['b35806', 'e08214', 'fdb863', 'fee0b6', 'f7f7f7', 'd8daeb', 'b2abd2', '8073ac', '542788'],
            10: ['7f3b08', 'b35806', 'e08214', 'fdb863', 'fee0b6', 'd8daeb', 'b2abd2', '8073ac', '542788', '2d004b'],
            11: ['7f3b08', 'b35806', 'e08214', 'fdb863', 'fee0b6', 'f7f7f7', 'd8daeb', 'b2abd2', '8073ac', '542788', '2d004b']
        },
        BrBG: {
            type: 'diverging',
            cbf: 42,
            3: ['d8b365', 'f5f5f5', '5ab4ac'],
            4: ['a6611a', 'dfc27d', '80cdc1', '018571'],
            5: ['a6611a', 'dfc27d', 'f5f5f5', '80cdc1', '018571'],
            6: ['8c510a', 'd8b365', 'f6e8c3', 'c7eae5', '5ab4ac', '01665e'],
            7: ['8c510a', 'd8b365', 'f6e8c3', 'f5f5f5', 'c7eae5', '5ab4ac', '01665e'],
            8: ['8c510a', 'bf812d', 'dfc27d', 'f6e8c3', 'c7eae5', '80cdc1', '35978f', '01665e'],
            9: ['8c510a', 'bf812d', 'dfc27d', 'f6e8c3', 'f5f5f5', 'c7eae5', '80cdc1', '35978f', '01665e'],
            10: ['543005', '8c510a', 'bf812d', 'dfc27d', 'f6e8c3', 'c7eae5', '80cdc1', '35978f', '01665e', '003c30'],
            11: ['543005', '8c510a', 'bf812d', 'dfc27d', 'f6e8c3', 'f5f5f5', 'c7eae5', '80cdc1', '35978f', '01665e', '003c30']
        },
        PRGn: {
            type: 'diverging',
            cbf: 42,
            3: ['af8dc3', 'f7f7f7', '7fbf7b'],
            4: ['7b3294', 'c2a5cf', 'a6dba0', '008837'],
            5: ['7b3294', 'c2a5cf', 'f7f7f7', 'a6dba0', '008837'],
            6: ['762a83', 'af8dc3', 'e7d4e8', 'd9f0d3', '7fbf7b', '1b7837'],
            7: ['762a83', 'af8dc3', 'e7d4e8', 'f7f7f7', 'd9f0d3', '7fbf7b', '1b7837'],
            8: ['762a83', '9970ab', 'c2a5cf', 'e7d4e8', 'd9f0d3', 'a6dba0', '5aae61', '1b7837'],
            9: ['762a83', '9970ab', 'c2a5cf', 'e7d4e8', 'f7f7f7', 'd9f0d3', 'a6dba0', '5aae61', '1b7837'],
            10: ['40004b', '762a83', '9970ab', 'c2a5cf', 'e7d4e8', 'd9f0d3', 'a6dba0', '5aae61', '1b7837', '00441b'],
            11: ['40004b', '762a83', '9970ab', 'c2a5cf', 'e7d4e8', 'f7f7f7', 'd9f0d3', 'a6dba0', '5aae61', '1b7837', '00441b']
        },
        PiYG: {
            type: 'diverging',
            cbf: 42,
            3: ['e9a3c9', 'f7f7f7', 'a1d76a'],
            4: ['d01c8b', 'f1b6da', 'b8e186', '4dac26'],
            5: ['d01c8b', 'f1b6da', 'f7f7f7', 'b8e186', '4dac26'],
            6: ['c51b7d', 'e9a3c9', 'fde0ef', 'e6f5d0', 'a1d76a', '4d9221'],
            7: ['c51b7d', 'e9a3c9', 'fde0ef', 'f7f7f7', 'e6f5d0', 'a1d76a', '4d9221'],
            8: ['c51b7d', 'de77ae', 'f1b6da', 'fde0ef', 'e6f5d0', 'b8e186', '7fbc41', '4d9221'],
            9: ['c51b7d', 'de77ae', 'f1b6da', 'fde0ef', 'f7f7f7', 'e6f5d0', 'b8e186', '7fbc41', '4d9221'],
            10: ['8e0152', 'c51b7d', 'de77ae', 'f1b6da', 'fde0ef', 'e6f5d0', 'b8e186', '7fbc41', '4d9221', '276419'],
            11: ['8e0152', 'c51b7d', 'de77ae', 'f1b6da', 'fde0ef', 'f7f7f7', 'e6f5d0', 'b8e186', '7fbc41', '4d9221', '276419']
        },
        RdBu: {
            type: 'diverging',
            cbf: 42,
            3: ['ef8a62', 'f7f7f7', '67a9cf'],
            4: ['ca0020', 'f4a582', '92c5de', '0571b0'],
            5: ['ca0020', 'f4a582', 'f7f7f7', '92c5de', '0571b0'],
            6: ['b2182b', 'ef8a62', 'fddbc7', 'd1e5f0', '67a9cf', '2166ac'],
            7: ['b2182b', 'ef8a62', 'fddbc7', 'f7f7f7', 'd1e5f0', '67a9cf', '2166ac'],
            8: ['b2182b', 'd6604d', 'f4a582', 'fddbc7', 'd1e5f0', '92c5de', '4393c3', '2166ac'],
            9: ['b2182b', 'd6604d', 'f4a582', 'fddbc7', 'f7f7f7', 'd1e5f0', '92c5de', '4393c3', '2166ac'],
            10: ['67001f', 'b2182b', 'd6604d', 'f4a582', 'fddbc7', 'd1e5f0', '92c5de', '4393c3', '2166ac', '053061'],
            11: ['67001f', 'b2182b', 'd6604d', 'f4a582', 'fddbc7', 'f7f7f7', 'd1e5f0', '92c5de', '4393c3', '2166ac', '053061']
        },
        RdGy: {
            type: 'diverging',
            cbf: 42,
            3: ['ef8a62', 'ffffff', '999999'],
            4: ['ca0020', 'f4a582', 'bababa', '404040'],
            5: ['ca0020', 'f4a582', 'ffffff', 'bababa', '404040'],
            6: ['b2182b', 'ef8a62', 'fddbc7', 'e0e0e0', '999999', '4d4d4d'],
            7: ['b2182b', 'ef8a62', 'fddbc7', 'ffffff', 'e0e0e0', '999999', '4d4d4d'],
            8: ['b2182b', 'd6604d', 'f4a582', 'fddbc7', 'e0e0e0', 'bababa', '878787', '4d4d4d'],
            9: ['b2182b', 'd6604d', 'f4a582', 'fddbc7', 'ffffff', 'e0e0e0', 'bababa', '878787', '4d4d4d'],
            10: ['67001f', 'b2182b', 'd6604d', 'f4a582', 'fddbc7', 'e0e0e0', 'bababa', '878787', '4d4d4d', '1a1a1a'],
            11: ['67001f', 'b2182b', 'd6604d', 'f4a582', 'fddbc7', 'ffffff', 'e0e0e0', 'bababa', '878787', '4d4d4d', '1a1a1a']
        },
        RdYlBu: {
            type: 'diverging',
            cbf: 42,
            3: ['fc8d59', 'ffffbf', '91bfdb'],
            4: ['d7191c', 'fdae61', 'abd9e9', '2c7bb6'],
            5: ['d7191c', 'fdae61', 'ffffbf', 'abd9e9', '2c7bb6'],
            6: ['d73027', 'fc8d59', 'fee090', 'e0f3f8', '91bfdb', '4575b4'],
            7: ['d73027', 'fc8d59', 'fee090', 'ffffbf', 'e0f3f8', '91bfdb', '4575b4'],
            8: ['d73027', 'f46d43', 'fdae61', 'fee090', 'e0f3f8', 'abd9e9', '74add1', '4575b4'],
            9: ['d73027', 'f46d43', 'fdae61', 'fee090', 'ffffbf', 'e0f3f8', 'abd9e9', '74add1', '4575b4'],
            10: ['a50026', 'd73027', 'f46d43', 'fdae61', 'fee090', 'e0f3f8', 'abd9e9', '74add1', '4575b4', '313695'],
            11: ['a50026', 'd73027', 'f46d43', 'fdae61', 'fee090', 'ffffbf', 'e0f3f8', 'abd9e9', '74add1', '4575b4', '313695']
        },
        Spectral: {
            type: 'diverging',
            cbf: 0,
            3: ['fc8d59', 'ffffbf', '99d594'],
            4: ['d7191c', 'fdae61', 'abdda4', '2b83ba'],
            5: ['d7191c', 'fdae61', 'ffffbf', 'abdda4', '2b83ba'],
            6: ['d53e4f', 'fc8d59', 'fee08b', 'e6f598', '99d594', '3288bd'],
            7: ['d53e4f', 'fc8d59', 'fee08b', 'ffffbf', 'e6f598', '99d594', '3288bd'],
            8: ['d53e4f', 'f46d43', 'fdae61', 'fee08b', 'e6f598', 'abdda4', '66c2a5', '3288bd'],
            9: ['d53e4f', 'f46d43', 'fdae61', 'fee08b', 'ffffbf', 'e6f598', 'abdda4', '66c2a5', '3288bd'],
            10: ['9e0142', 'd53e4f', 'f46d43', 'fdae61', 'fee08b', 'e6f598', 'abdda4', '66c2a5', '3288bd', '5e4fa2'],
            11: ['9e0142', 'd53e4f', 'f46d43', 'fdae61', 'fee08b', 'ffffbf', 'e6f598', 'abdda4', '66c2a5', '3288bd', '5e4fa2']
        },
        RdYlGn: {
            type: 'diverging',
            cbf: 0,
            3: ['fc8d59', 'ffffbf', '91cf60'],
            4: ['d7191c', 'fdae61', 'a6d96a', '1a9641'],
            5: ['d7191c', 'fdae61', 'ffffbf', 'a6d96a', '1a9641'],
            6: ['d73027', 'fc8d59', 'fee08b', 'd9ef8b', '91cf60', '1a9850'],
            7: ['d73027', 'fc8d59', 'fee08b', 'ffffbf', 'd9ef8b', '91cf60', '1a9850'],
            8: ['d73027', 'f46d43', 'fdae61', 'fee08b', 'd9ef8b', 'a6d96a', '66bd63', '1a9850'],
            9: ['d73027', 'f46d43', 'fdae61', 'fee08b', 'ffffbf', 'd9ef8b', 'a6d96a', '66bd63', '1a9850'],
            10: ['a50026', 'd73027', 'f46d43', 'fdae61', 'fee08b', 'd9ef8b', 'a6d96a', '66bd63', '1a9850', '006837'],
            11: ['a50026', 'd73027', 'f46d43', 'fdae61', 'fee08b', 'ffffbf', 'd9ef8b', 'a6d96a', '66bd63', '1a9850', '006837']
        },
        Accent: {
            type: 'qualitative',
            cbf: 0,
            3: ['7fc97f', 'beaed4', 'fdc086'],
            4: ['7fc97f', 'beaed4', 'fdc086', 'ffff99'],
            5: ['7fc97f', 'beaed4', 'fdc086', 'ffff99', '386cb0'],
            6: ['7fc97f', 'beaed4', 'fdc086', 'ffff99', '386cb0', 'f0027f'],
            7: ['7fc97f', 'beaed4', 'fdc086', 'ffff99', '386cb0', 'f0027f', 'bf5b17'],
            8: ['7fc97f', 'beaed4', 'fdc086', 'ffff99', '386cb0', 'f0027f', 'bf5b17', '666666']
        },
        Dark2: {
            type: 'qualitative',
            cbf: 3,
            3: ['1b9e77', 'd95f02', '7570b3'],
            4: ['1b9e77', 'd95f02', '7570b3', 'e7298a'],
            5: ['1b9e77', 'd95f02', '7570b3', 'e7298a', '66a61e'],
            6: ['1b9e77', 'd95f02', '7570b3', 'e7298a', '66a61e', 'e6ab02'],
            7: ['1b9e77', 'd95f02', '7570b3', 'e7298a', '66a61e', 'e6ab02', 'a6761d'],
            8: ['1b9e77', 'd95f02', '7570b3', 'e7298a', '66a61e', 'e6ab02', 'a6761d', '666666']
        },
        Paired: {
            type: 'qualitative',
            cbf: 4,
            3: ['a6cee3', '1f78b4', 'b2df8a'],
            4: ['a6cee3', '1f78b4', 'b2df8a', '33a02c'],
            5: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99'],
            6: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c'],
            7: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f'],
            8: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f', 'ff7f00'],
            9: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f', 'ff7f00', 'cab2d6'],
            10: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f', 'ff7f00', 'cab2d6', '6a3d9a'],
            11: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f', 'ff7f00', 'cab2d6', '6a3d9a', 'ffff99'],
            12: ['a6cee3', '1f78b4', 'b2df8a', '33a02c', 'fb9a99', 'e31a1c', 'fdbf6f', 'ff7f00', 'cab2d6', '6a3d9a', 'ffff99', 'b15928']
        },
        Pastel1: {
            type: 'qualitative',
            cbf: 0,
            3: ['fbb4ae', 'b3cde3', 'ccebc5'],
            4: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4'],
            5: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4', 'fed9a6'],
            6: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4', 'fed9a6', 'ffffcc'],
            7: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4', 'fed9a6', 'ffffcc', 'e5d8bd'],
            8: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4', 'fed9a6', 'ffffcc', 'e5d8bd', 'fddaec'],
            9: ['fbb4ae', 'b3cde3', 'ccebc5', 'decbe4', 'fed9a6', 'ffffcc', 'e5d8bd', 'fddaec', 'f2f2f2']
        },
        Pastel2: {
            type: 'qualitative',
            cbf: 0,
            3: ['b3e2cd', 'fdcdac', 'cbd5e8'],
            4: ['b3e2cd', 'fdcdac', 'cbd5e8', 'f4cae4'],
            5: ['b3e2cd', 'fdcdac', 'cbd5e8', 'f4cae4', 'e6f5c9'],
            6: ['b3e2cd', 'fdcdac', 'cbd5e8', 'f4cae4', 'e6f5c9', 'fff2ae'],
            7: ['b3e2cd', 'fdcdac', 'cbd5e8', 'f4cae4', 'e6f5c9', 'fff2ae', 'f1e2cc'],
            8: ['b3e2cd', 'fdcdac', 'cbd5e8', 'f4cae4', 'e6f5c9', 'fff2ae', 'f1e2cc', 'cccccc']
        },
        Set1: {
            type: 'qualitative',
            cbf: 0,
            3: ['e41a1c', '377eb8', '4daf4a'],
            4: ['e41a1c', '377eb8', '4daf4a', '984ea3'],
            5: ['e41a1c', '377eb8', '4daf4a', '984ea3', 'ff7f00'],
            6: ['e41a1c', '377eb8', '4daf4a', '984ea3', 'ff7f00', 'ffff33'],
            7: ['e41a1c', '377eb8', '4daf4a', '984ea3', 'ff7f00', 'ffff33', 'a65628'],
            8: ['e41a1c', '377eb8', '4daf4a', '984ea3', 'ff7f00', 'ffff33', 'a65628', 'f781bf'],
            9: ['e41a1c', '377eb8', '4daf4a', '984ea3', 'ff7f00', 'ffff33', 'a65628', 'f781bf', '999999']
        },
        Set2: {
            type: 'qualitative',
            cbf: 3,
            3: ['66c2a5', 'fc8d62', '8da0cb'],
            4: ['66c2a5', 'fc8d62', '8da0cb', 'e78ac3'],
            5: ['66c2a5', 'fc8d62', '8da0cb', 'e78ac3', 'a6d854'],
            6: ['66c2a5', 'fc8d62', '8da0cb', 'e78ac3', 'a6d854', 'ffd92f'],
            7: ['66c2a5', 'fc8d62', '8da0cb', 'e78ac3', 'a6d854', 'ffd92f', 'e5c494'],
            8: ['66c2a5', 'fc8d62', '8da0cb', 'e78ac3', 'a6d854', 'ffd92f', 'e5c494', 'b3b3b3']
        },
        Set3: {
            type: 'qualitative',
            cbf: 0,
            3: ['8dd3c7', 'ffffb3', 'bebada'],
            4: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072'],
            5: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3'],
            6: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462'],
            7: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69'],
            8: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69', 'fccde5'],
            9: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69', 'fccde5', 'd9d9d9'],
            10: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69', 'fccde5', 'd9d9d9', 'bc80bd'],
            11: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69', 'fccde5', 'd9d9d9', 'bc80bd', 'ccebc5'],
            12: ['8dd3c7', 'ffffb3', 'bebada', 'fb8072', '80b1d3', 'fdb462', 'b3de69', 'fccde5', 'd9d9d9', 'bc80bd', 'ccebc5', 'ffed6f']
        }
    };

    for (var name in schemes) {
        var scheme = schemes[name];
        scheme = palette.Scheme.fromPalettes('cb-' + name, [scheme.type, 'cb-' + scheme.type], scheme, 12, scheme.cbf);
        palette.register(scheme);
    }
})();