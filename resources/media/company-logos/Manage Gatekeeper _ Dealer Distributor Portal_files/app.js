var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var resetWarrantyRequestForm = function resetWarrantyRequestForm($form) {
    $old_serial = $form.find("#old_battery_serial_key");
    $new_serial = $form.find("#new_battery_serial_key");
    $reason = $form.find("#reason_for_replacement");
    var validator = $form.validate();
    $old_serial.val('');
    $new_serial.val('');
    $reason.val('').trigger('change');
    if (validator.settings.unhighlight) {
        validator.settings.unhighlight.call(validator, $old_serial[0], validator.settings.errorClass, validator.settings.validClass);
        validator.settings.unhighlight.call(validator, $new_serial[0], validator.settings.errorClass, validator.settings.validClass);
        validator.settings.unhighlight.call(validator, $reason[0], validator.settings.errorClass, validator.settings.validClass);
    }
    validator.hideThese(validator.errorsFor($old_serial[0]));
    validator.hideThese(validator.errorsFor($new_serial[0]));
    validator.hideThese(validator.errorsFor($reason[0]));
};

var CommonUserElement = function () {

    var intializeDatePicker = function intializeDatePicker() {

        var months = 12 * 17 + new Date().getMonth() + 1;

        $('.ats-date_of_birth').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            defaultViewDate: {
                year: new Date().getFullYear() - 18,
                month: '00',
                day: '01'
            },
            endDate: '-' + months.toString() + 'm'
        });

        $('.ats-date_of_join').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            setDate: 'now'
        });

        $('.ats-start-date').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate: "2016-01-01",
            setDate: 'now',
            zIndexOffset: 111111
        });
    };

    var intializeSelect2 = function intializeSelect2() {

        $('.ats-blood-group').select2({
            placeholder: "Select Blood Group"
        }).change(function () {
            $(this).valid();
        });

        $('.ats-department').select2({
            placeholder: "Select Department"
        }).change(function () {
            $(this).valid();
        });

        $('.ats-designation').select2({
            placeholder: "Select Designation"
        }).change(function () {
            $(this).valid();
        });

        $('#city_id').select2({
            placeholder: "Select City"
        }).change(function () {
            $(this).valid();
        });

        $('#distributor_id').select2({
            placeholder: "Select Distributor"
        }).change(function () {
            $(this).valid();
        });

        $('#category_id').select2({
            placeholder: "Select Battery Category"
        }).change(function () {
            $(this).valid();
        });

        $('.ats-state-select2').select2({
            placeholder: "Select State"
        }).change(function () {
            $(this).valid();
        });

        $("[name='reason_for_replacement']").select2({
            placeholder: "Select Reason For Battery Replacement"
        }).change(function (event) {
            $(this).valid();
            var $that = $("#reason_for_replacement_txt");
            var $form = $that.closest("form");
            var $container = $("#reason_for_replacement_container");
            if ($(this).val() == 'Other') {
                $container.show();
            } else {
                $container.hide();
                $that.val('');
                var validator = $form.validate();
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $that[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($that[0]));
            }
        });
    };

    return {
        init: function init() {
            intializeDatePicker();
            intializeSelect2();
        }
    };
}();

var displayAtsAlert = function displayAtsAlert(result, title, message) {
    if ($("#atsAlert").is(':visible')) {
        $("#atsAlert").animateCss('slideOutUp', function () {
            $("#atsAlert").hide();
            $("#atsTitle").empty();
            $("#atsAlertMsg").empty();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            if (result) {
                $("#atsAlert").removeClass("alert-info").addClass("alert-success");
            } else {
                $("#atsAlert").removeClass("alert-success").addClass("alert-info");
            }
            $("#atsAlertTitle").html(title);
            $("#atsAlertMsg").html(message);
            $("#atsAlert").show().animateCss('slideInDown');
        });
    } else {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        if (result) {
            $("#atsAlert").removeClass("alert-info").addClass("alert-success");
        } else {
            $("#atsAlert").removeClass("alert-success").addClass("alert-info");
        }
        $("#atsAlertTitle").html(title);
        $("#atsAlertMsg").html(message);
        $("#atsAlert").show().animateCss('slideInDown');
    }
};

var callHideAtsAlert = function callHideAtsAlert() {

    $("#atsAlertClose").trigger("click");
};

var hideAtsAlert = function hideAtsAlert() {

    $("#atsAlertClose").click(function () {
        $("#atsAlert").animateCss('slideOutUp', function () {
            $("#atsAlert").hide();
            $("#atsAlertTitle").empty();
            $("#atsAlertMsg").empty();
        });
    });
};

//== Class Initialization
jQuery(document).ready(function () {
    CommonUserElement.init();
    hideAtsAlert();
});

var invalidEmails = [];
var DDPApp = function () {

    $.fn.extend({
        animateCss: function animateCss(animationName, callback) {
            var animationEnd = function (el) {
                var animations = {
                    animation: 'animationend',
                    OAnimation: 'oAnimationEnd',
                    MozAnimation: 'mozAnimationEnd',
                    WebkitAnimation: 'webkitAnimationEnd'
                };

                for (var t in animations) {
                    if (el.style[t] !== undefined) {
                        return animations[t];
                    }
                }
            }(document.createElement('div'));

            this.addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);

                if (typeof callback === 'function') callback();
            });

            return this;
        }
    });

    var displaySidebarSelection = function displaySidebarSelection(data) {
        var pathname = window.location.href;
        pathname = pathname.replace(/\/$/, '').split('?')[0];
        $('li.m-menu__item  a').each(function (index, element) {
            var link = $(element).attr('href');
            if (pathname == link) {
                $(element).parent('li').addClass('m-menu__item--active');
                if ($(element).parents('ul').hasClass('m-menu__subnav')) {
                    $(element).parents('ul').parents('li').addClass('m-menu__item--expanded m-menu__item--open');
                }
            }
        });
    };

    var setSidebarPref = function setSidebarPref() {
        $("#m_aside_left_minimize_toggle").click(function () {
            if ($(this).hasClass("m-brand__toggler--active")) {
                Cookies.set("is_sidebar_toggle", "false");
            } else {
                Cookies.set("is_sidebar_toggle", "true");
            }
        });
    };

    var setupAppDefaults = function setupAppDefaults() {

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        jQuery.validator.setDefaults({
            errorElement: 'div', //default input error message container
            errorClass: 'form-control-feedback', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            errorPlacement: function errorPlacement(error, element) {
                // render error placement for each input type
                var group = $(element).closest('.m-form__group-sub').length > 0 ? $(element).closest('.m-form__group-sub') : $(element).closest('.m-form__group');
                var help = group.find('.m-form__help');

                if (group.find('.form-control-feedback').length !== 0) {
                    return;
                }

                if (help.length > 0) {
                    help.before(error);
                } else {
                    if ($(element).closest('.input-group').length > 0) {
                        $(element).closest('.input-group').after(error);
                    } else if ($(element).hasClass("m-select2")) {
                        $(element).parent().after(error);
                    } else {
                        if ($(element).is(':checkbox')) {
                            $(element).closest('.m-checkbox').find('>span').after(error);
                        } else {
                            $(element).after(error);
                        }
                    }
                }
            },

            highlight: function highlight(element) {
                // hightlight error inputs
                var group = $(element).closest('.m-form__group-sub').length > 0 ? $(element).closest('.m-form__group-sub') : $(element).closest('.m-form__group');

                group.addClass('has-danger'); // set error class to the control groupx
            },

            unhighlight: function unhighlight(element) {
                // revert the change done by hightlight
                var group = $(element).closest('.m-form__group-sub').length > 0 ? $(element).closest('.m-form__group-sub') : $(element).closest('.m-form__group');
                group.removeClass('has-danger'); // set error class to the control group
                group.find('.form-control-feedback').remove();
            },

            success: function success(label, element) {
                var group = $(label).closest('.m-form__group-sub').length > 0 ? $(label).closest('.m-form__group-sub') : $(label).closest('.m-form__group');
                group.removeClass('has-danger');
                group.find('.form-control-feedback').remove();
            }
        });

        $('form input').keypress(function (e) {
            if (e.which == 13) {
                if ($(this).closest('form').validate().form()) {
                    $(this).closest('form').find('.ats-submit').click();
                }
                return false;
            }
        });

        jQuery.each(["put", "delete"], function (i, method) {
            jQuery[method] = function (url, data, callback, type) {
                if (jQuery.isFunction(data)) {
                    type = type || callback;
                    callback = data;
                    data = undefined;
                }
                return jQuery.ajax({
                    url: url,
                    type: method,
                    dataType: type,
                    data: data,
                    success: callback
                });
            };
        });
    };

    var blankFunction = function blankFunction() {};
    var additionalValidation = function additionalValidation() {

        $.validator.addMethod("alpha", function (value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z]*$/);
        }, "Allow only alphabets.");

        $.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z0-9]*$/);
        }, "Allow only alphanumeric.");

        $.validator.addMethod("alphanumeric_dash", function (value, element) {
            return this.optional(element) || /^[a-zA-Z0-9.\-\_\s]+$/i.test(value);
        }, "Special characters are not allowed.");

        $.validator.addMethod("alphaNumericWithDot", function (value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z0-9.]*$/);
        }, "Search text is either valid alpha numeric string or valid amount.");

        $.validator.addMethod("validNumber", function (value, element) {
            return this.optional(element) || /^[0-9.]+$/i.test(value);
        }, "Please enter valid value.");

        $.validator.addMethod("EMAIL", function (value, element) {
            return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Enter email address in proper format.");

        $.validator.addMethod("checkForWhiteSpace", function (value, element, param) {
            var val = $(element).val();
            if (val && $.trim(val) == '') {
                return false;
            }
            return true;
        }, "Only white spaces are not allowed.");

        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || element.files[0].size / 1024 <= param;
        }, 'File size must be less than {0}.');

        $.validator.addMethod("spaceNotAllowed", function (value, element) {
            return this.optional(element) || value == value.match(/^\S*$/);
        }, "Blank space are not allowed.");

        $.validator.addMethod("gst_number", function (value, element) {
            return this.optional(element) || /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/i.test(value);
        }, "GST number is not in proper format.");

        $.validator.addMethod("pan_number", function (value, element) {
            return this.optional(element) || /^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/i.test(value);
        }, "PAN number is not in proper format.");

        $.validator.addMethod("aadhar_number", function (value, element) {
            return this.optional(element) || /^\d{12}$/i.test(value);
        }, "Aadhar number is not in proper format.");

        $.validator.addMethod("valid_serial_key", function (value, element) {
            return this.optional(element) || /^[0-9]{2}[0|1](0[1-9]|1[0-2])(1?[6-9]|[2-9][0-9])[0-9]{6}$/i.test(value);
        }, "Please enter valid serial key.");

        $.validator.addMethod("lr_no", function (value, element) {
            return this.optional(element) || /^[0-9]{5}\/[A-Z]{1}[0-9]{1}\/20(1?[6-9]|[2-9][0-9])$/i.test(value);
        }, "Please enter valid LR number.");

        $.validator.addMethod("po_no", function (value, element) {
            if (value == 'VERBAL') {
                return true;
            } else if (this.optional(element) || /^(SI\/PG|SI\/PM)\/[0-9]{3,20}\/(1?[6-9]|[2-9][0-9])$/i.test(value)) {
                return true;
            } else {
                if (this.optional(element) || /^(SI\/PG|SI\/PM)\/[0-9]{3,20}\/(1?[6-9]|[2-9][0-9])\-(1?[6-9]|[2-9][0-9])$/i.test(value)) {
                    var l = value.length;
                    return value.substring(l - 2, l) - value.substring(l - 5, l - 3) == 1;
                }
                return false;
            }
        }, "Please enter valid PO number.");

        $.validator.addMethod("invoice_number", function (value, element) {
            return this.optional(element) || /^(SI\/PG|SI\/PM)\/[0-9]{5,20}\/20(1?[6-9]|[2-9][0-9])$/i.test(value);
        }, "Please enter valid invoice number.");

        $.validator.addMethod("vehicle_number", function (value, element) {
            return this.optional(element) || /^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}$/i.test(value);
        }, "Please enter valid vehicle number.");

        $.validator.addMethod("greater_than_number", function (value, element, options) {
            var $fields = $(options[1], element.form);
            var $fieldsFirst = $fields.eq(0);
            var validator = $fieldsFirst.data("valid_req_grp") ? $fieldsFirst.data("valid_req_grp") : $.extend({}, this);
            var val = 0;
            $(options[1], element.form).each(function () {
                if ($(this).val() == null || $(this).val() == undefined || $(this).val() == '') {
                    val += 0;
                } else {
                    val += parseInt($(this).val());
                }
            });
            // Store the cloned validator for future validation
            $fieldsFirst.data("valid_req_grp", validator);
            // If element isn't being validated, run each require_from_group field's validation rules
            if (!$(element).data("being_validated")) {
                $fields.data("being_validated", true);
                $fields.each(function () {
                    validator.element(this);
                });
                $fields.data("being_validated", false);
            }
            var valid = parseFloat(val) > parseFloat(options[0]);
            return valid;
        }, $.validator.format('One of these must bigger than {0}'));

        $.validator.addMethod("comma_separated_emails", function (value, element) {
            if (this.optional(element)) {
                return true;
            }
            var mails = value.split(/,|;/);
            for (var i = 0; i < mails.length; i++) {
                // taken from the jquery validation internals
                // if (!/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(mails[i])) {
                if (!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/.test(mails[i])) {
                    invalidEmails[i] = $.trim(mails[i]);
                }
            }
            if (invalidEmails.length > 0) {
                return false;
            }
            return true;
        }, function () {
            var invalid = invalidEmails.filter(String);
            invalidEmails = [];
            if (invalid.length > 1) {
                var message = invalid.join() + ' are not valid email address';
            } else if (invalid.length == 1) {
                var message = invalid.join() + ' is not valid email address';
            } else {
                var message = 'Please specify a valid email address or a comma separated list of addresses';
            }
            return message;
        });
    };

    return {
        //main function to initiate the module
        init: function init() {
            displaySidebarSelection();
            setSidebarPref();
            setupAppDefaults();
            additionalValidation();
        },
        resetForm: function resetForm(form) {
            form.clearForm();
            form.validate().resetForm();
        },
        disableButton: function disableButton(btn) {
            btn.attr('disabled', true);
        },
        disableButtonWithLoading: function disableButtonWithLoading(btn) {
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
        },
        enableButton: function enableButton(btn) {
            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
        },
        showLoadingInButton: function showLoadingInButton(btn) {
            btn.addClass('m-loader m-loader--right m-loader--light');
        },
        removeLoadingFromButton: function removeLoadingFromButton(btn) {
            btn.removeClass('m-loader m-loader--right m-loader--light');
        },
        displayToastr: function displayToastr(type, data, callback) {
            toastr[type](data.message, data.title, {
                timeOut: 1500,
                onHidden: function onHidden() {
                    callback();
                }
            });
        },
        displayToastrForSuccessAndFailure: function displayToastrForSuccessAndFailure(data, success_type, success, failure) {
            if (data.result == true) {
                this.displayToastr(success_type, data, success);
            } else {
                this.displayToastr('error', data, failure);
            }
        },
        displayResultAndReload: function displayResultAndReload(data) {
            this.displayToastrForSuccessAndFailure(data, 'info', function () {
                window.location.href = window.location.href;
            }, blankFunction);
        },
        displayResultAndRedirect: function displayResultAndRedirect(data, url) {
            this.displayToastrForSuccessAndFailure(data, 'info', function () {
                window.location.href = url;
            }, blankFunction);
        },
        displayFailedValidation: function displayFailedValidation(data) {
            if (data.status == 422) {
                $.each(data.responseJSON, function (key, value) {
                    for (var i = 0; i < value.length; i++) {
                        toastr.error(value[i], 'Please Note!');
                    }
                });
            } else if (data.status == 401) {
                toastr.error(data.responseJSON['message'], 'Attention!');
            } else {
                toastr.error(data.statusText, 'Attention!');
            }
        },
        displayResult: function displayResult(data) {
            this.displayToastrForSuccessAndFailure(data, 'info', blankFunction, blankFunction);
        },
        displayResultWithSuccessCallback: function displayResultWithSuccessCallback(data, callback) {
            this.displayToastrForSuccessAndFailure(data, 'success', callback, blankFunction);
        },
        displayResultWithCallback: function displayResultWithCallback(data, success, failure) {
            this.displayToastrForSuccessAndFailure(data, 'info', success, failure);
        },
        displayErrorMessage: function displayErrorMessage(msg) {
            toastr.error(msg, "Attention!", {
                timeOut: 2000
            });
        },
        displayResultForFailureWithCallback: function displayResultForFailureWithCallback(data, success, failure) {
            if (data.result == true) {
                success();
            } else {
                this.displayToastr('error', data, failure);
            }
        },
        blockUiAndButton: function blockUiAndButton(btn, el) {
            btn.attr('disabled', 'disabled');
            App.blockUI({
                target: el,
                textOnly: true
            });
        },
        unblockUiAndButton: function unblockUiAndButton(btn, el) {
            btn.removeAttr('disabled');
            App.unblockUI(el);
        }
    };
}();
$(document).ready(function () {
    DDPApp.init();
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body',
        trigger: 'hover'
    });
    $('.counterup').counterUp({
        delay: 10,
        time: 3500
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var BanUser = function () {

    var handleUserBan = function handleUserBan() {

        $(document).on('click', '.ats-ban-user', function (e) {
            var btn = $(this);
            btn.tooltip('hide');
            e.preventDefault();
            DDPApp.disableButton(btn);

            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                            btn.parentsUntil(".ats-list-item").parent().remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            handleUserBan();
        }
    };
}();

jQuery(document).ready(function () {
    BanUser.init();
});

var UnbanUser = function () {

    var handleUserUnban = function handleUserUnban() {

        $(document).on('click', '.ats-unban-user', function (e) {
            var btn = $(this);
            btn.tooltip('hide');
            e.preventDefault();

            DDPApp.disableButton(btn);

            $.post(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                            btn.parentsUntil(".ats-list-item").parent().remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            handleUserUnban();
        }
    };
}();

jQuery(document).ready(function () {
    UnbanUser.init();
});

var Login = function () {

    var login = $('#m_login');
    var displaySignInForm = function displaySignInForm() {
        if ($("#m_login_forget_password_submit").attr("disabled")) {
            return;
        }
        login.removeClass('m-login--forget-password');
        login.addClass('m-login--signin');
        login.find('.m-login__signin').animateClass('flipInX animated');
    };
    var displayForgetPasswordForm = function displayForgetPasswordForm() {
        if ($("#m_login_signin_submit").attr("disabled")) {
            return;
        }
        login.removeClass('m-login--signin');
        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    };
    var clearLoginForm = function clearLoginForm() {
        $("#loginForm").clearForm();
        $("#loginForm").validate().resetForm();
    };
    var clearForgotPasswordForm = function clearForgotPasswordForm() {
        $("#forgotPasswordForm").clearForm();
        $("#forgotPasswordForm").validate().resetForm();
    };

    var handleFormSwitch = function handleFormSwitch() {
        $('#m_login_forget_password').click(function (e) {
            e.preventDefault();
            displayForgetPasswordForm();
            clearForgotPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
            clearLoginForm();
        });
    };

    var initSignInForm = function initSignInForm() {

        $("#loginForm").validate({
            rules: {
                username: {
                    required: true,
                    checkForWhiteSpace: true
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                username: {
                    required: "Mobile Number / Email Address is required."
                },
                password: {
                    required: "Password is required."
                }
            }
        });
    };

    var initForgotPasswordForm = function initForgotPasswordForm() {
        $("#forgotPasswordForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                email: {
                    required: 'Recovery email address is required.',
                    email: 'Please enter valid email address.'
                }
            }
        });
    };

    var handleSignInFormSubmit = function handleSignInFormSubmit() {

        $('#m_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.removeLoadingFromButton(btn);
                DDPApp.displayResultWithCallback(data, function () {
                    window.location.href = form.attr("data-redirect-url");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    var handleForgetPasswordFormSubmit = function handleForgetPasswordFormSubmit() {

        $('#m_login_forget_password_submit').click(function (e) {

            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    clearForgotPasswordForm();
                    DDPApp.enableButton(btn);
                    displaySignInForm();
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            handleFormSwitch();
            initSignInForm();
            initForgotPasswordForm();
            handleSignInFormSubmit();
            handleForgetPasswordFormSubmit();
        }
    };
}();

jQuery(document).ready(function () {
    Login.init();
});

var resetPassword = function () {

    var login = $('#m_login');

    var initResetPasswordForm = function initResetPasswordForm() {

        $("#resetPasswordForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 8,
                    checkForWhiteSpace: true
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                }
            },
            messages: {
                password: {
                    required: "New password is required.",
                    minlength: "Minimum length of password is 8"
                },
                password_confirmation: {
                    required: "Password confirmation is required",
                    equalTo: "Password should match"
                }
            }
        });
    };

    var handleResetPasswordForm = function handleResetPasswordForm() {

        $('#resetPasswordBtn').click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    window.location.href = form.attr("data-redirect-url");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            initResetPasswordForm();
            handleResetPasswordForm();
        }
    };
}();

jQuery(document).ready(function () {
    resetPassword.init();
});

var ChangePassword = function () {

    var initChangePassword = function initChangePassword() {

        $("#changePasswordForm").validate({
            rules: {
                current_password: {
                    required: true,
                    checkForWhiteSpace: true
                },
                password: {
                    required: true,
                    minlength: 8,
                    checkForWhiteSpace: true
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                }
            },
            messages: {
                current_password: {
                    required: "Current password is required."
                },
                password: {
                    required: "New password is required.",
                    minlength: "New password must be 8 characters long."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                }
            }
        });
    };

    var handleChangePassword = function handleChangePassword() {

        $("#changePasswordResetBtn").click(function () {

            $("#changePasswordForm").clearForm();
            $("#changePasswordForm").validate().resetForm();
        });

        $("#changePasswordBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    window.location.href = form.attr("data-redirect-url");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            initChangePassword();
            handleChangePassword();
        }
    };
}();

jQuery(document).ready(function () {
    ChangePassword.init();
});

var GeneratePassword = function () {

    var openGeneratePasswordModal = function openGeneratePasswordModal() {

        $(document).on('click', '.ats-generate-password', function (e) {

            $("#generatePasswordForm").attr("data-action", $(this).attr("data-url"));
            $("#username").html($(this).attr("data-name"));
            $("#generatePasswordModal").modal('show');
        });

        $("#generatePasswordModal").on('hidden.bs.modal', function () {
            $("#generatePasswordForm")[0].reset();
            $("#generatePasswordForm").validate().resetForm();
        });
    };

    var handleGeneratePassword = function handleGeneratePassword() {
        $("#generatePasswordForm").validate({
            rules: {
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                }
            },
            messages: {
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                }
            }
        });

        $('#generatePasswordBtn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $("#generatePasswordForm");

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.enableButton(btn);
                    $("#generatePasswordModal").modal("toggle");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            openGeneratePasswordModal();
            handleGeneratePassword();
        }
    };
}();

jQuery(document).ready(function () {
    GeneratePassword.init();
});

var TeamMemberCreate = function () {
    var teamMemberCreateValidation = function teamMemberCreateValidation() {
        $("#profile_photo").fileinput().on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#profile_photo').fileinput('destroy');
            $("#profile_photo").fileinput({
                showUpload: false,
                showRemove: true,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                msgPlaceholder: 'Select Profile Photo',
                browseLabel: 'Browse',
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                initialPreview: ["no image"],
                initialCaption: "Select Profile Photo"
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        });
        $("#profile_photo").trigger('fileclear');
    };
    var initTeamMemberCreate = function initTeamMemberCreate() {
        $("#createTeamMemberForm").validate({
            rules: {
                employee_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                department: {
                    required: true
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                },
                designation: {
                    required: true
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                employee_id: {
                    required: "Employee ID is required."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                department: {
                    required: "Department is required."
                },
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                },
                designation: {
                    required: "Designation is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleTeamMemberCreate = function handleTeamMemberCreate() {
        $("#createMemberResetBtn").click(function () {
            resetTeamMemberForm($("#createTeamMemberForm"));
        });
        $("#createMemberBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            var formData = new FormData();
            var image = document.getElementById('profile_photo');
            if (image.files && image.files[0]) {
                var extension = image.files[0].name.split('.');
                var temp2 = extension[extension.length - 1].toLowerCase();
                var size = parseFloat(image.files[0].size / 1024).toFixed(2);
                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('profile_photo', image.files[0]);
                }
            }
            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });
            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        resetTeamMemberForm(form);
                        DDPApp.enableButton(btn);
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };
    var resetTeamMemberForm = function resetTeamMemberForm(form) {
        $(".ats-department").val('').trigger("change");
        $(".ats-designation").val('').trigger("change");
        $(".ats-blood-group").val('').trigger("change");
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            teamMemberCreateValidation();
            initTeamMemberCreate();
            handleTeamMemberCreate();
        }
    };
}();
jQuery(document).ready(function () {
    TeamMemberCreate.init();
});

var ManageTeamMember = function () {
    var initTeamMemberManage = function initTeamMemberManage() {
        $('#manage_team_member_search_by_designation').select2({
            placeholder: "Filter by Designation"
        });
        $('.team-member-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleTeamMemberRedirect();
        });
        $("#manage_team_member_search_btn").click(function () {
            handleTeamMemberRedirect();
        });
        $("#manage_team_member_search_refresh_btn").click(function () {
            $("#manage_team_member_search_txt").val('');
            $("#manage_team_member_search_by_designation").val('').trigger('change');
            handleTeamMemberRedirect();
        });
        $('#all_team_member_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#team_member_selection').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#team_member_selection').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleTeamMemberRedirect = function handleTeamMemberRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".team-member-tabview").attr("data-status");
        if ($("#manage_team_member_search_by_designation").val() != '') {
            route += "&designation=" + $("#manage_team_member_search_by_designation").val();
        }
        if ($("#manage_team_member_search_txt").val().trim() != '') {
            route += '&search=' + $("#manage_team_member_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    var handleTeamMemberExport = function handleTeamMemberExport() {
        $("#manage_team_member_export_btn").click(function () {
            if ($('input[name="team_member_selection"]:checked').length != 0) {
                var tokens = $('input[name="team_member_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };
    return {
        init: function init() {
            initTeamMemberManage();
            handleTeamMemberExport();
        }
    };
}();
jQuery(document).ready(function () {
    ManageTeamMember.init();
});

var EditTeamMember = function () {

    var teamMemberEditValidation = function teamMemberEditValidation() {

        var preview = ["no image"];
        var previewCaption = "Select Profile Photo";
        var broweseLabel = "Browse";
        var showRemove = true;
        if ($("#edit_profile_photo").is("[data-profile-photo-url]")) {
            preview = ["<img src='" + $("#edit_profile_photo").attr("data-profile-photo-url") + "' height='180px' width='180px'>"];
            previewCaption = "1 Profile photo uploaded";
            broweseLabel = "Change";
            showRemove = false;
        }

        $("#edit_profile_photo").fileinput({
            showUpload: false,
            showRemove: showRemove,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            minFileSize: 1,
            //maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            browseLabel: broweseLabel,
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            initialPreview: preview,
            initialCaption: previewCaption
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_profile_photo').fileinput('destroy');
            $("#edit_profile_photo").fileinput({
                showUpload: false,
                showRemove: showRemove,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: broweseLabel,
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                //maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: previewCaption
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
        });
        $("#edit_profile_photo").trigger('fileclear');
    };

    var initTeamMemberEdit = function initTeamMemberEdit() {

        $("#editTeamMemberForm").validate({
            rules: {
                employee_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                department: {
                    required: true
                },
                designation: {
                    required: true
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                employee_id: {
                    required: "Employee ID is required."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                department: {
                    required: "Department is required."
                },
                designation: {
                    required: "Designation is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleTeamMemberEdit = function handleTeamMemberEdit() {

        $("#updateTeamMemberBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var formData = new FormData();

            var image = document.getElementById('edit_profile_photo');

            if (image.files && image.files[0]) {

                var extension = image.files[0].name.split('.');

                var temp2 = extension[extension.length - 1].toLowerCase();

                var size = parseFloat(image.files[0].size / 1024).toFixed(2);

                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('profile_photo', image.files[0]);
                }
            }

            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });

            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        var referrer = document.referrer;
                        if (!referrer) {
                            window.location.href = form.attr("data-redirect-url");
                        } else {
                            window.location.href = referrer;
                        }
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };

    return {
        init: function init() {
            teamMemberEditValidation();
            initTeamMemberEdit();
            handleTeamMemberEdit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    EditTeamMember.init();
});

var EditTeamMemberProfile = function () {

    var teamMemberProfileEditValidation = function teamMemberProfileEditValidation() {

        var preview = ["no image"];
        var previewCaption = "Select Profile Photo";
        var broweseLabel = "Browse";
        var showRemove = true;
        if ($("#edit_profile_photo").is("[data-profile-photo-url]")) {
            preview = ["<img src='" + $("#edit_profile_photo").attr("data-profile-photo-url") + "' height='180px' width='180px'>"];
            previewCaption = "1 Profile photo uploaded";
            broweseLabel = "Change";
            showRemove = false;
        }

        $("#edit_profile_photo").fileinput({
            showUpload: false,
            showRemove: showRemove,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            minFileSize: 1,
            maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            browseLabel: broweseLabel,
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            initialPreview: preview,
            initialCaption: previewCaption
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_profile_photo').fileinput('destroy');
            $("#edit_profile_photo").fileinput({
                showUpload: false,
                showRemove: showRemove,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: broweseLabel,
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: previewCaption
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
            toastr.error('File is invalid please select another file.', 'Invalid File');
        });
        $("#edit_profile_photo").trigger('fileclear');
    };
    var initTeamMemberProfileEdit = function initTeamMemberProfileEdit() {

        $("#editProfileForm").validate({
            rules: {
                employee_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                employee_id: {
                    required: "Employee ID is required."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                department: {
                    required: "Department is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleTeamMemberProfileEdit = function handleTeamMemberProfileEdit() {

        $("#updateProfileBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var formData = new FormData();

            var image = document.getElementById('edit_profile_photo');

            if (image.files && image.files[0]) {

                var extension = image.files[0].name.split('.');

                var temp2 = extension[extension.length - 1].toLowerCase();

                var size = parseFloat(image.files[0].size / 1024).toFixed(2);

                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('profile_photo', image.files[0]);
                }
            }

            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });

            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        window.location.href = form.attr("data-redirect-url");
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };

    return {
        init: function init() {
            teamMemberProfileEditValidation();
            initTeamMemberProfileEdit();
            handleTeamMemberProfileEdit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    EditTeamMemberProfile.init();
});

var TeamMemberBatteryWarrantyRequest = function () {
    var populateDealersForMember = function populateDealersForMember(data) {
        $("#dist_warranty_request_dealer_id option[value!='']").remove();
        if (data.length == 0) {
            $('#dist_warranty_request_dealer_id').select2({
                placeholder: 'No Dealers Found.'
            });
        } else {
            $('#dist_warranty_request_dealer_id').select2({
                data: data,
                placeholder: 'Select Dealer'
            });
        }
    };
    var initMemberBatteryWarrantyRequest = function initMemberBatteryWarrantyRequest() {
        if ($("#teamMemberWarrantyRequestForm").length) {
            $.get($("#teamMemberWarrantyRequestForm").attr("data-fetching-url")).done(function (data) {
                dealers = data.dealers;
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
            });
        }
        $("#dist_warranty_request_distributor_id").select2({
            placeholder: "Select Distributor"
        }).change(function () {
            if (!$(this).valid()) {
                return;
            }
            var dist = $(this).val();
            var filterd = $.map(dealers, function (item) {
                return item.distributor_id == dist ? item : null;
            });
            populateDealersForMember(filterd);
        });
        $("#dist_warranty_request_dealer_id").select2({
            placeholder: "Select Dealer"
        }).change(function () {
            $(this).valid();
        });
        $("#dist_warranty_request_dealer_id").select2({
            placeholder: "Select Dealer"
        }).change(function () {
            $(this).valid();
        });
        $("#teamMemberWarrantyRequestForm").validate({
            rules: {
                start_date: {
                    required: true
                },
                distributor_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                old_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                new_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                reason_for_replacement: {
                    required: true,
                    checkForWhiteSpace: true
                },
                reason_for_replacement_txt: {
                    required: function required(element) {
                        return $("[name='reason_for_replacement']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                },
                outcome: {
                    required: true
                },
                reason_for_rejection: {
                    required: function required(element) {
                        return $("input[type=radio][name=outcome]:checked").val() == 'rejected';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                start_date: {
                    required: "Warranty request date is required."
                },
                distributor_id: {
                    required: "Distributor name is required."
                },
                old_battery_serial_key: {
                    required: "Old battery serial key is required.",
                    digits: "Old battery serial key must contains only digits.",
                    minlength: "Old battery serial key must be 13 digits long.",
                    maxlength: "Old battery serial key must not long than 13 digits."
                },
                new_battery_serial_key: {
                    required: "New battery serial key is required.",
                    digits: "New battery serial key must contains only digits.",
                    minlength: "New battery serial key must be 13 digits long.",
                    maxlength: "New battery serial key must not long than 13 digits."
                },
                reason_for_replacement: {
                    required: "Reason for replacement is required."
                },
                reason_for_replacement_txt: {
                    required: "Reason for replacement is required."
                },
                outcome: {
                    required: "Warranty request status is required"
                },
                reason_for_rejection: {
                    required: "Reason for rejection is required."
                }
            }
        });
    };
    var handleMemberBatteryWarrantyRequest = function handleMemberBatteryWarrantyRequest() {
        $("#teamMemberWarrantyRequestResetBtn").click(function () {
            callHideAtsAlert();
            var $form = $("#teamMemberWarrantyRequestForm");
            $form.find("#dist_warranty_request_distributor_id").val('').trigger('change');
            $form.find("#dist_warranty_request_dealer_id").val('').trigger('change');
            $form.find("#reason_for_replacement").val('').trigger('change');
            $form.find("#warrantyRequestReasonForRejection").val('').trigger("change");
            $('input[type=radio][name=outcome][value=approved]').attr("checked", true).trigger('change');
            $('#old_battery_serial_key').attr('value', '');
            DDPApp.resetForm($form);
        });
        $("#teamMemberWarrantyRequestBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                var message = '';
                $.each(data.message, function (i) {
                    message += data.message[i] + "<br>";
                });
                displayAtsAlert(data.result, data.title, message);
                if (data.result) {
                    resetWarrantyRequestForm(form);
                    resetRejectionFields(form);
                }
                DDPApp.enableButton(btn);
            }).fail(function (data) {
                if (data.status == 422) {
                    var message = '';
                    $.each(data.responseJSON, function (key, value) {
                        for (var i = 0; i < value.length; i++) {
                            message += value[i] + "<br>";
                        }
                    });
                    displayAtsAlert(false, "Attention!", message);
                } else {
                    DDPApp.displayFailedValidation(data);
                }
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initMemberBatteryWarrantyRequest();
            handleMemberBatteryWarrantyRequest();
        }
    };
}();
jQuery(document).ready(function () {
    TeamMemberBatteryWarrantyRequest.init();
});

var ServiceEngineerCreate = function () {
    var serviceEngineerCreateValidation = function serviceEngineerCreateValidation() {
        $('#cities_id').multiselect({
            nonSelectedText: "Select City",
            enableClickableOptGroups: true,
            enableFiltering: true,
            numberDisplayed: 1,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            allSelectedText: 'India'
        }).change(function () {
            $(this).valid();
        });

        $("#engineer_photo").fileinput().on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#engineer_photo').fileinput('destroy');
            $("#engineer_photo").fileinput({
                showUpload: false,
                showRemove: true,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                msgPlaceholder: 'Select Profile Photo',
                browseLabel: 'Browse',
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                initialPreview: ["no image"],
                initialCaption: "Select Profile Photo"
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        });
        $("#engineer_photo").trigger('fileclear');
    };
    var initServiceEngineerCreate = function initServiceEngineerCreate() {
        $("#createServiceEngineerForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                service_engineer_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                engineer_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                service_engineer_id: {
                    required: "Service engineer ID is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                engineer_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                }
            }
        });
    };
    var handleServiceEngineerCreate = function handleServiceEngineerCreate() {
        $("#createEngineerResetBtn").click(function () {
            resetServiceEngineerForm($("#createServiceEngineerForm"));
        });
        $("#createEngineerBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            var formData = new FormData();
            var image = document.getElementById('engineer_photo');
            if (image.files && image.files[0]) {
                var extension = image.files[0].name.split('.');
                var temp2 = extension[extension.length - 1].toLowerCase();
                var size = parseFloat(image.files[0].size / 1024).toFixed(2);
                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('engineer_photo', image.files[0]);
                }
            }
            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });
            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        resetServiceEngineerForm(form);
                        DDPApp.enableButton(btn);
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };
    var resetServiceEngineerForm = function resetServiceEngineerForm(form) {
        $(".ats-blood-group").val('').trigger("change");
        $('#cities_id option:selected').each(function () {
            $(this).prop('selected', false);
        });
        $('#cities_id').multiselect('refresh');
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            serviceEngineerCreateValidation();
            initServiceEngineerCreate();
            handleServiceEngineerCreate();
        }
    };
}();
jQuery(document).ready(function () {
    ServiceEngineerCreate.init();
});

var ManageServiceEngineer = function () {
    var initServiceEngineerManage = function initServiceEngineerManage() {
        $('#manage_service_engineer_search_by_city').select2({
            placeholder: "Filter by Cities",
            closeOnSelect: true,
            maximumSelectionLength: 10,
            allowClear: true
        });
        $('.service-engineer-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleServiceEngineerRedirect();
        });
        $("#manage_service_engineer_search_btn").click(function () {
            handleServiceEngineerRedirect();
        });
        $("#manage_service_engineer_search_refresh_btn").click(function () {
            $("#manage_service_engineer_search_txt").val('');
            $('#manage_service_engineer_search_by_city').val('').trigger('change');
            handleServiceEngineerRedirect();
        });
        $('#all_service_engineer_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#service_engineer_selection').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#service_engineer_selection').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleServiceEngineerRedirect = function handleServiceEngineerRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".service-engineer-tabview").attr("data-status");
        if ($("#manage_service_engineer_search_by_city").val() != '') {
            route += "&cities_filter=" + $("#manage_service_engineer_search_by_city").val();
        }
        if ($("#manage_service_engineer_search_txt").val().trim() != '') {
            route += '&search=' + $("#manage_service_engineer_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    var handleServiceEngineerExport = function handleServiceEngineerExport() {
        $("#manage_service_engineer_export_btn").click(function () {
            if ($('input[name="service_engineer_selection"]:checked').length != 0) {
                var tokens = $('input[name="service_engineer_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };
    return {
        init: function init() {
            initServiceEngineerManage();
            handleServiceEngineerExport();
        }
    };
}();
jQuery(document).ready(function () {
    ManageServiceEngineer.init();
});

var EditServiceEngineer = function () {

    var serviceEngineerEditValidation = function serviceEngineerEditValidation() {
        $('#edit_cities_id').multiselect({
            nonSelectedText: "Select City",
            enableClickableOptGroups: true,
            enableFiltering: true,
            numberDisplayed: 1,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            allSelectedText: 'India'
        });

        var preview = ["no image"];
        var previewCaption = "Select Profile Photo";
        var broweseLabel = "Browse";
        var showRemove = true;
        if ($("#edit_engineer_photo").is("[data-profile-photo-url]")) {
            preview = ["<img src='" + $("#edit_engineer_photo").attr("data-profile-photo-url") + "' height='180px' width='180px'>"];
            previewCaption = "1 Profile photo uploaded";
            broweseLabel = "Change";
            showRemove = false;
        }

        $("#edit_engineer_photo").fileinput({
            showUpload: false,
            showRemove: showRemove,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            browseLabel: broweseLabel,
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            minFileSize: 1,
            maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            initialPreview: preview,
            initialCaption: previewCaption
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_engineer_photo').fileinput('destroy');
            $("#edit_engineer_photo").fileinput({
                showUpload: false,
                showRemove: showRemove,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: broweseLabel,
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: previewCaption
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
            toastr.error('File is invalid please select another file.', 'Invalid File');
        });
        $("#edit_engineer_photo").trigger('fileclear');
    };

    var initServiceEngineerEdit = function initServiceEngineerEdit() {

        $("#editServiceEngineerForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                service_engineer_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                service_engineer_id: {
                    required: "Service Engineer ID is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                }
            }
        });
    };
    var handleServiceEngineerEdit = function handleServiceEngineerEdit() {

        $("#updateServiceEngineerBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var formData = new FormData();

            var image = document.getElementById('edit_engineer_photo');

            if (image.files && image.files[0]) {

                var extension = image.files[0].name.split('.');

                var temp2 = extension[extension.length - 1].toLowerCase();

                var size = parseFloat(image.files[0].size / 1024).toFixed(2);

                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('engineer_photo', image.files[0]);
                }
            }

            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });

            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        var referrer = document.referrer;
                        if (!referrer) {
                            window.location.href = form.attr("data-redirect-url");
                        } else {
                            window.location.href = referrer;
                        }
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };

    return {
        init: function init() {
            serviceEngineerEditValidation();
            initServiceEngineerEdit();
            handleServiceEngineerEdit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    EditServiceEngineer.init();
});

var EditServiceEngineerProfile = function () {

    var serviceEngineerProfileEditValidation = function serviceEngineerProfileEditValidation() {

        var preview = ["no image"];
        var previewCaption = "Select Profile Photo";
        var broweseLabel = "Browse";
        var showRemove = true;
        if ($("#edit_engineer_profile_photo").is("[data-profile-photo-url]")) {
            preview = ["<img src='" + $("#edit_engineer_profile_photo").attr("data-profile-photo-url") + "' height='180px' width='180px'>"];
            previewCaption = "1 Profile photo uploaded";
            broweseLabel = "Change";
            showRemove = false;
        }

        $("#edit_engineer_profile_photo").fileinput({
            showUpload: false,
            showRemove: showRemove,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            browseLabel: broweseLabel,
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            minFileSize: 1,
            maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            initialPreview: preview,
            initialCaption: previewCaption
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_engineer_profile_photo').fileinput('destroy');
            $("#edit_engineer_profile_photo").fileinput({
                showUpload: false,
                showRemove: showRemove,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: broweseLabel,
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: previewCaption
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
            toastr.error('File is invalid please select another file.', 'Invalid File');
        });
        $("#edit_engineer_profile_photo").trigger('fileclear');
    };
    var initServiceEngineerProfileEdit = function initServiceEngineerProfileEdit() {

        $("#editEngineerProfileForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                service_engineer_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    required: true,
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                service_engineer_id: {
                    required: "Service Engineer ID is required."
                },
                email: {
                    required: "Email is required.",
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                }
            }
        });
    };
    var handleServiceEngineerProfileEdit = function handleServiceEngineerProfileEdit() {

        $("#updateEngineerProfileBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var formData = new FormData();

            var image = document.getElementById('edit_engineer_profile_photo');

            if (image.files && image.files[0]) {

                var extension = image.files[0].name.split('.');

                var temp2 = extension[extension.length - 1].toLowerCase();

                var size = parseFloat(image.files[0].size / 1024).toFixed(2);

                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('engineer_photo', image.files[0]);
                }
            }

            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });

            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        window.location.href = form.attr("data-redirect-url");
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };

    return {
        init: function init() {
            serviceEngineerProfileEditValidation();
            initServiceEngineerProfileEdit();
            handleServiceEngineerProfileEdit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    EditServiceEngineerProfile.init();
});

var EngineerBatteryWarrantyRequest = function () {
    var cities;
    var dealers;
    var all_distributors;
    var super_distributors;
    var distributors;
    var filteredDealers;
    var populateDealersForEngineer = function populateDealersForEngineer(data) {
        $("#eng_warranty_request_dealer_id option[value!='']").remove();
        if (data.length == 0) {
            $('#eng_warranty_request_dealer_id').select2({
                placeholder: 'No Dealers Found.'
            });
        } else {
            $('#eng_warranty_request_dealer_id').select2({
                data: data,
                placeholder: 'Select Dealer'
            });
        }
    };
    var populateDistributorsForEngineer = function populateDistributorsForEngineer(data) {
        $("#eng_warranty_request_distributor_id option[value!='']").remove();
        if (data.length == 0) {
            $('#eng_warranty_request_distributor_id').select2({
                placeholder: 'No Distributors Found.'
            });
        } else {
            $('#eng_warranty_request_distributor_id').select2({
                data: data,
                placeholder: 'Select Distributor'
            });
        }
    };
    var populateSuperDistributorForEngineer = function populateSuperDistributorForEngineer(data) {
        if (data.length == 0) {
            $('#eng_warranty_request_super_distributor_id').select2({
                placeholder: 'No C&F Found.'
            });
        } else {
            $('#eng_warranty_request_super_distributor_id').select2({
                data: data,
                placeholder: 'Select C&F'
            });
        }
    };
    var initEnginnerBatteryWarrantyRequest = function initEnginnerBatteryWarrantyRequest() {
        if ($("#addServiceEngineerWarrantyRequestForm").length) {
            $.get($("#addServiceEngineerWarrantyRequestForm").attr("data-fetching-url")).done(function (data) {
                cities = data.cities;
                all_distributors = data.all_distributors;
                super_distributors = data.super_distributors;
                distributors = data.distributors;
                dealers = data.dealers;
                filteredDealers = $.map(dealers, function (item) {
                    return jQuery.inArray(item.city_id, cities) !== -1 ? item : null;
                });
                populateDealersForEngineer(filteredDealers);
                populateDistributorsForEngineer(distributors);
                populateSuperDistributorForEngineer(super_distributors);
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
            });
        }
        $("#eng_warranty_request_super_distributor_id").select2({
            placeholder: "Select C&F"
        }).change(function () {
            if (!$(this).valid()) {
                return;
            }
            if ($(this).val() == '') {
                return;
            }
            var dist = $(this).val();
            var filterd = $.map(all_distributors, function (item) {
                return item.super_distributor_id == dist ? item : null;
            });
            populateDistributorsForEngineer(filterd);
        });
        $("#eng_warranty_request_distributor_id").select2({
            placeholder: "Select Distributor"
        }).change(function () {
            if (!$(this).valid()) {
                return;
            }
            if ($(this).val() == '') {
                return;
            }
            var dist = $(this).val();
            var filterd = $.map(dealers, function (item) {
                return item.distributor_id == dist ? item : null;
            });
            populateDealersForEngineer(filterd);
        });
        $("#eng_warranty_request_dealer_id").select2({
            placeholder: "Select Dealer"
        }).change(function () {
            $(this).valid();
        });
        $('input[type=radio][name=battery_collector]').change(function () {
            if ($(this).val() == 'super_distributor') {
                var $that = $("#eng_warranty_request_dealer_id");
                var $form = $that.closest("form");
                var validator = $form.validate();
                $("#eng_warranty_request_dealer_id option[value!='']").remove();
                $that.val('');
                $('#eng_warranty_request_dealer_id').select2({
                    placeholder: 'Select Distributor First'
                });
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $that[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($that[0]));
                var $that = $("#eng_warranty_request_distributor_id");
                $("#eng_warranty_request_distributor_id option[value!='']").remove();
                $that.val('');
                $('#eng_warranty_request_distributor_id').select2({
                    placeholder: 'Select C&F First'
                });
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $that[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($that[0]));
                $("#eng_warranty_request_super_distributor_id").parent().prev().children("span.required").show();
                $("#eng_warranty_request_distributor_id").parent().prev().children("span.required").hide();
                $("#eng_warranty_request_dealer_id").parent().prev().children("span.required").hide();
                $("#distContainer").show();
                $("#superDistContainer").show();
            }
            if ($(this).val() == 'distributor') {
                var $that = $("#eng_warranty_request_dealer_id");
                var $form = $that.closest("form");
                var validator = $form.validate();
                $("#eng_warranty_request_dealer_id option[value!='']").remove();
                $that.val('');
                $('#eng_warranty_request_dealer_id').select2({
                    placeholder: 'Select Distributor First'
                });
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $that[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($that[0]));
                $("#eng_warranty_request_distributor_id").parent().prev().children("span.required").show();
                $("#eng_warranty_request_dealer_id").parent().prev().children("span.required").hide();
                $("#superDistContainer").hide();
                $("#distContainer").show();
                $("#eng_warranty_request_super_distributor_id").val('').trigger("change");
                populateDistributorsForEngineer(distributors);
            }
            if ($(this).val() == 'dealer') {
                $("#eng_warranty_request_dealer_id").parent().prev().children("span.required").show();
                $("#superDistContainer").hide();
                $("#distContainer").hide();
                $("#eng_warranty_request_super_distributor_id").val('').trigger("change");
                $("#eng_warranty_request_distributor_id").val('').trigger("change");
                populateDealersForEngineer(filteredDealers);
            }
        });
    };
    var engineerBatteryWarrantyRequestValidation = function engineerBatteryWarrantyRequestValidation() {
        $("#addServiceEngineerWarrantyRequestForm").validate({
            rules: {
                start_date: {
                    required: true
                },
                dealer_id: {
                    required: function required(element) {
                        return $('input[type=radio][name=battery_collector]:checked').val() == 'dealer';
                    },
                    checkForWhiteSpace: true
                },
                distributor_id: {
                    required: function required(element) {
                        return $('input[type=radio][name=battery_collector]:checked').val() == 'distributor';
                    },
                    checkForWhiteSpace: true
                },
                super_distributor_id: {
                    required: function required(element) {
                        return $('input[type=radio][name=battery_collector]:checked').val() == 'super_distributor';
                    },
                    checkForWhiteSpace: true
                },
                old_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                new_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                reason_for_replacement: {
                    required: true,
                    checkForWhiteSpace: true
                },
                reason_for_replacement_txt: {
                    required: function required(element) {
                        return $("[name='reason_for_replacement']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                },
                outcome: {
                    required: true
                },
                reason_for_rejection: {
                    required: function required(element) {
                        return $("input[type=radio][name=outcome]:checked").val() == 'rejected';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                start_date: {
                    required: "Warranty request date is required."
                },
                dealer_id: {
                    required: "Dealer is required."
                },
                distributor_id: {
                    required: "Distributor is required."
                },
                super_distributor_id: {
                    required: "C&F is required."
                },
                old_battery_serial_key: {
                    required: "Old battery serial key is required.",
                    digits: "Old battery serial key must contains only digits.",
                    minlength: "Old battery serial key must be 13 digits long.",
                    maxlength: "Old battery serial key must not long than 13 digits."
                },
                new_battery_serial_key: {
                    required: "New battery serial key is required.",
                    digits: "New battery serial key must contains only digits.",
                    minlength: "New battery serial key must be 13 digits long.",
                    maxlength: "New battery serial key must not long than 13 digits."
                },
                reason_for_replacement: {
                    required: "Reason for replacement is required."
                },
                reason_for_replacement_txt: {
                    required: "Reason for replacement is required."
                },
                outcome: {
                    required: "Warranty request status is required"
                },
                reason_for_rejection: {
                    required: "Reason for rejection is required."
                }
            }
        });
    };
    var handleEngineerBatteryWarrantyRequest = function handleEngineerBatteryWarrantyRequest() {
        $("#engineerWarrantyRequestResetBtn").click(function () {
            callHideAtsAlert();
            var $form = $("#addServiceEngineerWarrantyRequestForm");
            $form.find("#eng_warranty_request_super_distributor_id").val('').trigger('change');
            $form.find("#eng_warranty_request_distributor_id").val('').trigger('change');
            $form.find("#eng_warranty_request_dealer_id").val('').trigger('change');
            $form.find("#reason_for_replacement").val('').trigger('change');
            $form.find("#warrantyRequestReasonForRejection").val('').trigger("change");
            $('input[type=radio][name=battery_collector][value=dealer]').attr("checked", true).trigger('change');
            $('input[type=radio][name=outcome][value=approved]').attr("checked", true).trigger('change');
            $('#old_battery_serial_key').attr('value', '');
            DDPApp.resetForm($form);
        });
        $("#engineerWarrantyRequestBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                var message = '';
                $.each(data.message, function (i) {
                    message += data.message[i] + "<br>";
                });
                displayAtsAlert(data.result, data.title, message);
                if (data.result) {
                    resetWarrantyRequestForm(form);
                    resetRejectionFields(form);
                }
                DDPApp.enableButton(btn);
            }).fail(function (data) {
                if (data.status == 422) {
                    var message = '';
                    $.each(data.responseJSON, function (key, value) {
                        for (var i = 0; i < value.length; i++) {
                            message += value[i] + "<br>";
                        }
                    });
                    displayAtsAlert(false, "Attention!", message);
                } else {
                    DDPApp.displayFailedValidation(data);
                }
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initEnginnerBatteryWarrantyRequest();
            engineerBatteryWarrantyRequestValidation();
            handleEngineerBatteryWarrantyRequest();
        }
    };
}();
jQuery(document).ready(function () {
    EngineerBatteryWarrantyRequest.init();
});

var BanSuperDistributor = function () {
    var ban_super_distributor_row;
    var ban_super_distributor_modal = $("#banSuperDistributorModal");
    var ban_super_distributor_url;
    var ban_super_distributor_id;
    var openBanSuperDistributorModal = function openBanSuperDistributorModal() {
        $(document).on('click', '.ban-super-distributor', function (e) {
            var btn = $(this);
            ban_super_distributor_row = btn.closest(".ats-list-item");
            ban_super_distributor_url = btn.attr("data-action");
            $(".modal-body > .alert > strong").html($(this).attr("data-firm-name"));
            ban_super_distributor_id = btn.attr("data-id");
            $("#ban_super_distributor_id option[value=" + ban_super_distributor_id + "]").attr("disabled", "disabled");
            $("#ban_super_distributor_id").select2({
                dropdownParent: $(ban_super_distributor_modal),
                placeholder: 'Select Super Distributor'
            });
            ban_super_distributor_modal.modal("show");
        });
        $("#ban_super_distributor_id").select2({
            dropdownParent: $(ban_super_distributor_modal),
            placeholder: 'Select Super Distributor'
        }).change(function () {
            $(this).valid();
        });
        $("#banSuperDistributorForm").validate({
            rules: {
                super_distributor_id: {
                    required: true
                }
            },
            messages: {
                super_distributor_id: {
                    required: "Super Distributor is required."
                }
            }
        });
    };
    var registerBanSuperDistributorModalEvent = function registerBanSuperDistributorModalEvent() {
        ban_super_distributor_modal.on('hidden.bs.modal', function (e) {
            ban_super_distributor_url = null;
            $("#ban_super_distributor_id option[value=" + ban_super_distributor_id + "]").removeAttr("disabled", "disabled");
            $("#ban_super_distributor_id").val('').trigger("change");
            DDPApp.resetForm($("#banSuperDistributorForm"));
            DDPApp.enableButton($(".ats-submit"));
        });
    };
    var handleBanSuperDistributor = function handleBanSuperDistributor() {
        $("#banSuperDistributorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest("form");
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButton(btn);
            $.delete(ban_super_distributor_url, form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    ban_super_distributor_modal.modal("hide");
                    $("#ban_super_distributor_id option[value=" + ban_super_distributor_id + "]").remove();
                    if ($(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        ban_super_distributor_row.fadeOut(500).promise().done(function () {
                            ban_super_distributor_row.remove();
                            ban_super_distributor_row = null;
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            openBanSuperDistributorModal();
            handleBanSuperDistributor();
            registerBanSuperDistributorModalEvent();
        }
    };
}();
jQuery(document).ready(function () {
    BanSuperDistributor.init();
});

var CFABatteryWarrantyRequest = function () {
    var dealers;
    var distributors;
    var super_distributor_id;
    var filteredDealers;
    var populateDealersForCFA = function populateDealersForCFA(super_distributor_id) {
        $("#cfa_warranty_request_dealer_id option[value!='']").remove();
        filteredDealers = $.map(dealers, function (item) {
            return item.distributor_id == super_distributor_id ? item : null;
        });
        if (filteredDealers.length == 0) {
            $('#cfa_warranty_request_dealer_id').select2({
                placeholder: 'No Dealers Found.'
            });
        } else {
            $('#cfa_warranty_request_dealer_id').select2({
                data: filteredDealers,
                placeholder: 'Select Dealer'
            });
        }
    };
    var populateDistributorsForCFA = function populateDistributorsForCFA(distributors) {
        $("#cfa_warranty_request_distributor_id option[value!='']").remove();
        if (distributors.length == 0) {
            $('#cfa_warranty_request_distributor_id').select2({
                placeholder: 'No Distributors Found.'
            });
        } else {
            $('#cfa_warranty_request_distributor_id').select2({
                data: distributors,
                placeholder: 'Select Distributor'
            });
        }
    };
    var initCFABatteryWarrantyRequest = function initCFABatteryWarrantyRequest() {
        if ($("#addCFAWarrantyRequestForm").length) {
            $.get($("#addCFAWarrantyRequestForm").attr("data-fetching-url")).done(function (data) {
                super_distributor_id = data.super_distributor_id;
                distributors = data.distributors;
                dealers = data.dealers;
                populateDealersForCFA(super_distributor_id);
                populateDistributorsForCFA(distributors);
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
            });
        }
        $("#cfa_warranty_request_distributor_id").select2({
            placeholder: "Select Distributor"
        }).change(function () {
            if (!$(this).valid()) {
                return;
            }
            var dist = $(this).val();
            populateDealersForCFA(dist);
        });
        $("#cfa_warranty_request_dealer_id").select2({
            placeholder: "Select Dealer"
        }).change(function () {
            $(this).valid();
        });
        $('input[type=radio][name=cfa_battery_collector]').change(function () {
            if ($(this).val() == 'self') {
                $("#dealerDistContainer").hide();
                $("#cfa_warranty_request_distributor_id").val('').trigger("change");
                $("#cfa_warranty_request_dealer_id").val('').trigger("change");
            }
            if ($(this).val() == 'distributor') {
                var $that = $("#cfa_warranty_request_dealer_id");
                var $form = $that.closest("form");
                var validator = $form.validate();
                $("#cfa_warranty_request_dealer_id option[value!='']").remove();
                $that.val('');
                $('#cfa_warranty_request_dealer_id').select2({
                    placeholder: 'Select Distributor First'
                });
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $that[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($that[0]));
                $("#cfa_warranty_request_dealer_id").parent().prev().children("span.required").hide();
                $("#distContainer").show();
                $("#dealerDistContainer").show();
            }
            if ($(this).val() == 'dealer') {
                $("#distContainer").hide();
                $("#cfa_warranty_request_dealer_id").parent().prev().children("span.required").show();
                $("#cfa_warranty_request_distributor_id").val('').trigger("change");
                populateDealersForCFA(super_distributor_id);
                $("#dealerDistContainer").show();
            }
        });
    };
    var cfaBatteryWarrantyRequestValidation = function cfaBatteryWarrantyRequestValidation() {
        $("#addCFAWarrantyRequestForm").validate({
            rules: {
                start_date: {
                    required: true
                },
                dealer_id: {
                    required: function required(element) {
                        return $('input[type=radio][name=cfa_battery_collector]:checked').val() == 'dealer';
                    },
                    checkForWhiteSpace: true
                },
                distributor_id: {
                    required: function required(element) {
                        return $('input[type=radio][name=cfa_battery_collector]:checked').val() == 'distributor';
                    },
                    checkForWhiteSpace: true
                },
                old_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                new_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                reason_for_replacement: {
                    required: true,
                    checkForWhiteSpace: true
                },
                reason_for_replacement_txt: {
                    required: function required(element) {
                        return $("[name='reason_for_replacement']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                },
                outcome: {
                    required: true
                },
                reason_for_rejection: {
                    required: function required(element) {
                        return $("input[type=radio][name=outcome]:checked").val() == 'rejected';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                start_date: {
                    required: "Warranty request date is required."
                },
                dealer_id: {
                    required: "Dealer name is required."
                },
                distributor_id: {
                    required: "Distributor name is required."
                },
                old_battery_serial_key: {
                    required: "Old battery serial key is required.",
                    digits: "Old battery serial key must contains only digits.",
                    minlength: "Old battery serial key must be 13 digits long.",
                    maxlength: "Old battery serial key must not long than 13 digits."
                },
                new_battery_serial_key: {
                    required: "New battery serial key is required.",
                    digits: "New battery serial key must contains only digits.",
                    minlength: "New battery serial key must be 13 digits long.",
                    maxlength: "New battery serial key must not long than 13 digits."
                },
                reason_for_replacement: {
                    required: "Reason for replacement is required."
                },
                reason_for_replacement_txt: {
                    required: "Reason for replacement is required."
                },
                outcome: {
                    required: "Warranty request status is required"
                },
                reason_for_rejection: {
                    required: "Reason for rejection is required."
                }
            }
        });
    };
    var handleCFABatteryWarrantyRequest = function handleCFABatteryWarrantyRequest() {
        $("#cfaWarrantyRequestResetBtn").click(function () {
            callHideAtsAlert();
            var $form = $("#addCFAWarrantyRequestForm");
            $form.find("#cfa_warranty_request_distributor_id").val('').trigger('change');
            $form.find("#cfa_warranty_request_dealer_id").val('').trigger('change');
            $form.find("#reason_for_replacement").val('').trigger('change');
            $form.find("#warrantyRequestReasonForRejection").val('').trigger("change");
            $('input[type=radio][name=cfa_battery_collector][value=self]').attr("checked", true).trigger('change');
            $('input[type=radio][name=outcome][value=approved]').attr("checked", true).trigger('change');
            $('#old_battery_serial_key').attr('value', '');
            DDPApp.resetForm($form);
        });
        $("#cfaWarrantyRequestBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                var message = '';
                $.each(data.message, function (i) {
                    message += data.message[i] + "<br>";
                });
                displayAtsAlert(data.result, data.title, message);
                if (data.result) {
                    resetWarrantyRequestForm(form);
                    resetRejectionFields(form);
                }
                DDPApp.enableButton(btn);
            }).fail(function (data) {
                if (data.status == 422) {
                    var message = '';
                    $.each(data.responseJSON, function (key, value) {
                        for (var i = 0; i < value.length; i++) {
                            message += value[i] + "<br>";
                        }
                    });
                    displayAtsAlert(false, "Attention!", message);
                } else {
                    DDPApp.displayFailedValidation(data);
                }
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initCFABatteryWarrantyRequest();
            cfaBatteryWarrantyRequestValidation();
            handleCFABatteryWarrantyRequest();
        }
    };
}();
jQuery(document).ready(function () {
    CFABatteryWarrantyRequest.init();
});

var CreateDistributor = function () {
    var initSuperDistributorSelection = function initSuperDistributorSelection(data) {
        var options = {
            placeholder: "Select Super Distributor",
            allowClear: true
        };
        if (data) {
            options['data'] = data;
        }
        $("#super_distributor_id").select2(options).change(function () {
            $(this).valid();
        });
    };
    var toggleIsSuperDistributor = function toggleIsSuperDistributor() {
        $("input[name=is_super_distributor]").change(function () {
            if ($(this).is(':checked')) {
                $("#super_distributor_container").hide();
                $("#super_distributor_id").val('').trigger("change");
            } else {
                $("#super_distributor_container").show();
            }
        });
    };
    var validationCreateDistributor = function validationCreateDistributor() {
        $("#addDistributorForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    checkForWhiteSpace: true,
                    equalTo: '#password'
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                },
                cc_emails: {
                    comma_separated_emails: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };
    var handleCreateDistributor = function handleCreateDistributor() {
        $("#addDistributorResetBtn").click(function () {
            resetCreateDistributorForm($("#addDistributorForm"));
        });
        $("#addDistributorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if (data.super_distributors) {
                        initSuperDistributorSelection(data.super_distributors);
                    }
                    resetCreateDistributorForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var resetCreateDistributorForm = function resetCreateDistributorForm(form) {
        $("#super_distributor_id").val('').trigger("change");
        $("#city_id").val('').trigger("change");
        DDPApp.resetForm(form);
        $("#super_distributor_container").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            initSuperDistributorSelection();
            toggleIsSuperDistributor();
            validationCreateDistributor();
            handleCreateDistributor();
        }
    };
}();
jQuery(document).ready(function () {
    CreateDistributor.init();
});

var ManageDistributors = function () {
    var initDistributorManage = function initDistributorManage() {
        $('#manage_distributor_search_by_city').select2({
            placeholder: "Filter by Cities",
            closeOnSelect: true,
            maximumSelectionLength: 10,
            allowClear: true
        });
        $('.distributor-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleDistributorRedirect();
        });
        $("#manage_distributor_search_btn").click(function () {
            handleDistributorRedirect();
        });
        $("#manage_distributor_search_refresh_btn").click(function () {
            $("#manage_distributor_search_txt").val('');
            $("#manage_distributor_search_by_city").val('').trigger('change');
            handleDistributorRedirect();
        });
        $('#all_distributor_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#distributor_selection').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#distributor_selection').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleDistributorRedirect = function handleDistributorRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".distributor-tabview").attr("data-status");
        if ($("#manage_distributor_search_by_city").val() != '') {
            route += "&cities_filter=" + $("#manage_distributor_search_by_city").val();
        }
        if ($("#manage_distributor_search_txt").val().trim() != '') {
            route += '&search=' + $("#manage_distributor_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    var handleDistributorExport = function handleDistributorExport() {
        $("#manage_distributor_export_btn").click(function () {
            if ($('input[name="distributor_selection"]:checked').length != 0) {
                var tokens = $('input[name="distributor_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };
    return {
        init: function init() {
            initDistributorManage();
            handleDistributorExport();
        }
    };
}();
jQuery(document).ready(function () {
    ManageDistributors.init();
});

var ShowDistributor = function () {
    var initDistributorShow = function initDistributorShow() {
        $('form.distributor-replacement-battery-form').each(function (key, form) {
            $(form).validate({
                rules: {
                    serial_key: {
                        required: true,
                        checkForWhiteSpace: true,
                        digits: true,
                        minlength: 13,
                        maxlength: 13,
                        valid_serial_key: true
                    }
                },
                messages: {
                    serial_key: {
                        required: "Serial key is required.",
                        digits: "Serial key must contains only digits.",
                        minlength: "Serial key must be 13 digits long.",
                        maxlength: "Serial key must not long than 13 digits."
                    }
                }
            });
        });
        $('.distributor-pending-replacement-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleDistributorShowRedirect();
        });
    };
    var handleDistributorShowRedirect = function handleDistributorShowRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        window.location.href = route;
    };
    var handleDistributorBatteryReplacement = function handleDistributorBatteryReplacement() {
        $(".add-replacement-to-distributor").click(function () {
            var btn = $(this);
            var row = btn.closest("tr");
            if (!btn.closest("form").valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(btn.attr("data-action"), {
                'serial_key': row.find('.serial-key').val(),
                '_method': 'PUT'
            }).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    row.fadeOut(500).promise().done(function () {
                        row.remove();
                        updateDistributorBatteryReplacementWidget();
                    });
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var updateDistributorBatteryReplacementWidget = function updateDistributorBatteryReplacementWidget() {
        var pending_widget = $(".pending-replacement-counter");
        var closed_widget = $(".closed-replacement-counter");
        pending_widget.attr("data-value", parseInt(pending_widget.attr("data-value")) - 1);
        pending_widget.html(pending_widget.attr("data-value"));
        closed_widget.attr("data-value", parseInt(closed_widget.attr("data-value")) + 1);
        closed_widget.html(closed_widget.attr("data-value"));
    };
    return {
        init: function init() {
            initDistributorShow();
            handleDistributorBatteryReplacement();
        }
    };
}();
jQuery(document).ready(function () {
    ShowDistributor.init();
});

var EditDistributor = function () {
    var validationEditDistributor = function validationEditDistributor() {
        $("#updateDistributorForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                },
                cc_emails: {
                    comma_separated_emails: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };
    var handleEditDistributor = function handleEditDistributor() {
        $("#updateDistributorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            validationEditDistributor();
            handleEditDistributor();
        }
    };
}();
jQuery(document).ready(function () {
    EditDistributor.init();
});

var EditDistributorProfile = function () {

    var distributorProfileEditValidation = function distributorProfileEditValidation() {

        $("#updateDistributorProfileForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };

    var handleDistributorProfileEdit = function handleDistributorProfileEdit() {

        $("#updateDistributorProfileBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    window.location.href = form.attr("data-redirect-url");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            distributorProfileEditValidation();
            handleDistributorProfileEdit();
        }
    };
}();

jQuery(document).ready(function () {
    EditDistributorProfile.init();
});

var DistributorBatteryWarrantyRequest = function () {

    var distributorBatteryWarrantyRequestValidation = function distributorBatteryWarrantyRequestValidation() {
        $("#dealer_id").select2({
            placeholder: 'Select Dealer'
        }).change(function () {
            $(this).valid();
        });

        $("#addDistributorWarrantyRequestForm").validate({
            rules: {
                start_date: {
                    required: true
                },
                old_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                new_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                reason_for_replacement: {
                    required: true,
                    checkForWhiteSpace: true
                },
                reason_for_replacement_txt: {
                    required: function required(element) {
                        return $("[name='reason_for_replacement']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                },
                outcome: {
                    required: true
                },
                reason_for_rejection: {
                    required: function required(element) {
                        return $("input[type=radio][name=outcome]:checked").val() == 'rejected';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                start_date: {
                    required: "Warranty request date is required."
                },
                old_battery_serial_key: {
                    required: "Old battery serial key is required.",
                    digits: "Old battery serial key must contains only digits.",
                    minlength: "Old battery serial key must be 13 digits long.",
                    maxlength: "Old battery serial key must not long than 13 digits."
                },
                new_battery_serial_key: {
                    required: "New battery serial key is required.",
                    digits: "New battery serial key must contains only digits.",
                    minlength: "New battery serial key must be 13 digits long.",
                    maxlength: "New battery serial key must not long than 13 digits."
                },
                reason_for_replacement: {
                    required: "Reason for replacement is required."
                },
                reason_for_replacement_txt: {
                    required: "Reason for replacement is required."
                },
                outcome: {
                    required: "Warranty request status is required"
                },
                reason_for_rejection: {
                    required: "Reason for rejection is required."
                }
            }
        });
    };

    var handleDistributorBatteryWarrantyRequest = function handleDistributorBatteryWarrantyRequest() {

        $("#distributorWarrantyRequestResetBtn").click(function () {
            callHideAtsAlert();
            form = $("#addDistributorWarrantyRequestForm");
            form.find("#dealer_id").val('').trigger("change");
            form.find("#reason_for_replacement").val('').trigger("change");
            form.find("#warrantyRequestReasonForRejection").val('').trigger("change");
            $('#old_battery_serial_key').attr('value', '');
            resetWarrantyRequestForm(form);
            resetRejectionFields(form);
            $('input[type=radio][name=outcome]:first').click();
            DDPApp.resetForm(form);
        });

        $("#distributorWarrantyRequestBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                var message = '';
                $.each(data.message, function (i) {
                    message += data.message[i] + "<br>";
                });
                displayAtsAlert(data.result, data.title, message);
                if (data.result) {
                    resetWarrantyRequestForm(form);
                    resetRejectionFields(form);
                }
                DDPApp.enableButton(btn);
            }).fail(function (data) {
                if (data.status == 422) {
                    var message = '';
                    $.each(data.responseJSON, function (key, value) {
                        for (var i = 0; i < value.length; i++) {
                            message += value[i] + "<br>";
                        }
                    });
                    displayAtsAlert(false, "Attention!", message);
                } else {
                    DDPApp.displayFailedValidation(data);
                }
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            distributorBatteryWarrantyRequestValidation();
            handleDistributorBatteryWarrantyRequest();
        }
    };
}();

jQuery(document).ready(function () {
    DistributorBatteryWarrantyRequest.init();
});

var ShowClosedDistributorReplacement = function () {
    var initClosedDistributorReplacementShow = function initClosedDistributorReplacementShow() {
        $('.distributor-closed-battery-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleClosedDistributorReplacementShowRedirect();
        });
    };
    var handleClosedDistributorReplacementShowRedirect = function handleClosedDistributorReplacementShowRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        window.location.href = route;
    };
    return {
        init: function init() {
            initClosedDistributorReplacementShow();
        }
    };
}();
jQuery(document).ready(function () {
    ShowClosedDistributorReplacement.init();
});

var BanDistributor = function () {
    var ban_distributor_row;
    var ban_distributor_modal = $("#banDistributorModal");
    var ban_distributor_url;
    var ban_distributor_id;
    var openBanDistributorModal = function openBanDistributorModal() {
        $(document).on('click', '.ban-distributor', function (e) {
            var btn = $(this);
            var dealer_count = parseInt(btn.attr('data-dealer-count'));
            if (dealer_count == 0) {
                ban_distributor_row = btn.closest(".ats-list-item");
                ban_distributor_url = btn.attr("data-action");
                ban_distributor_id = btn.attr("data-id");
                DDPApp.disableButton(btn);
                $.delete(ban_distributor_url, { 'distributor_id': null }).done(function (data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        $("#ban_distributor_id option[value=" + ban_distributor_id + "]").remove();
                        if ($(".ats-list-item").length == 1) {
                            window.location.href = window.location.href;
                        } else {
                            ban_distributor_row.fadeOut(500).promise().done(function () {
                                ban_distributor_row.remove();
                                ban_distributor_row = null;
                            });
                        }
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                }).fail(function (data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                });
            } else {
                ban_distributor_row = btn.closest(".ats-list-item");
                ban_distributor_url = btn.attr("data-action");
                $(".modal-body > .alert > strong").html($(this).attr("data-firm-name"));
                ban_distributor_id = btn.attr("data-id");
                $("#ban_distributor_id option[value=" + ban_distributor_id + "]").attr("disabled", "disabled");
                $("#ban_distributor_id").select2({
                    dropdownParent: $(ban_distributor_modal),
                    placeholder: 'Select Distributor'
                });
                ban_distributor_modal.modal("show");
            }
        });
        $("#ban_distributor_id").select2({
            dropdownParent: $(ban_distributor_modal),
            placeholder: 'Select Distributor'
        }).change(function () {
            $(this).valid();
        });
        $("#banDistributorForm").validate({
            rules: {
                distributor_id: {
                    required: true
                }
            },
            messages: {
                distributor_id: {
                    required: "Distributor is required."
                }
            }
        });
    };
    var registerBanDistributorModalEvent = function registerBanDistributorModalEvent() {
        ban_distributor_modal.on('hidden.bs.modal', function (e) {
            ban_distributor_url = null;
            $("#ban_distributor_id option[value=" + ban_distributor_id + "]").removeAttr("disabled", "disabled");
            $("#ban_distributor_id").val('').trigger("change");
            DDPApp.resetForm($("#banDistributorForm"));
            DDPApp.enableButton($(".ats-submit"));
        });
    };
    var handleBanDistributor = function handleBanDistributor() {
        $("#banDistributorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest("form");
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButton(btn);
            $.delete(ban_distributor_url, form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    ban_distributor_modal.modal("hide");
                    $("#ban_distributor_id option[value=" + ban_distributor_id + "]").remove();
                    if ($(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        ban_distributor_row.fadeOut(500).promise().done(function () {
                            ban_distributor_row.remove();
                            ban_distributor_row = null;
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            openBanDistributorModal();
            handleBanDistributor();
            registerBanDistributorModalEvent();
        }
    };
}();
jQuery(document).ready(function () {
    BanDistributor.init();
});

var CreateDealer = function () {
    var dealerCreateValidation = function dealerCreateValidation() {
        $("#addDealerForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    checkForWhiteSpace: true,
                    equalTo: '#password'
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                distributor_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                distributor_id: {
                    required: "Distributor is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };
    var handleDealerCreate = function handleDealerCreate() {
        $("#addDealerResetBtn").click(function () {
            resetDealerForm($("#addDealerForm"));
        });
        $("#addDealerBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    resetDealerForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var resetDealerForm = function resetDealerForm(form) {
        $("#city_id").val('').trigger("change");
        $("#distributor_id").val('').trigger("change");
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            dealerCreateValidation();
            handleDealerCreate();
        }
    };
}();
jQuery(document).ready(function () {
    CreateDealer.init();
});

var ManageDealers = function () {
    var initDealerManage = function initDealerManage() {
        $('#manage_dealer_search_by_distributor').select2({
            placeholder: "Filter by Distributors",
            closeOnSelect: true,
            maximumSelectionLength: 10,
            language: {
                maximumSelected: function maximumSelected(e) {
                    var t = "You can only select " + e.maximum + " distributors";
                    return t;
                }
            },
            allowClear: true
        });
        $('#manage_dealer_search_by_city').select2({
            placeholder: "Filter by cities",
            closeOnSelect: true,
            maximumSelectionLength: 10,
            allowClear: true
        });
        $('.dealer-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleDealerRedirect();
        });
        $('#manage_dealer_search_btn').click(function () {
            handleDealerRedirect();
        });
        $('#manage_dealer_search_refresh_btn').click(function () {
            $("#manage_dealer_search_txt").val('');
            $("#manage_dealer_search_by_distributor").val('').trigger('change');
            $("#manage_dealer_search_by_city").val('').trigger('change');
            handleDealerRedirect();
        });
        $('#all_dealer_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#dealer_selection').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#dealer_selection').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleDealerRedirect = function handleDealerRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".dealer-tabview").attr('data-status');
        if ($("#manage_dealer_search_by_distributor").length != 0 && $("#manage_dealer_search_by_distributor").val() != '') {
            route += '&distributor_filter=' + $("#manage_dealer_search_by_distributor").val();
        }
        if ($("#manage_dealer_search_by_city").length != 0 && $("#manage_dealer_search_by_city").val() != '') {
            route += '&cities_filter=' + $("#manage_dealer_search_by_city").val();
        }
        if ($("#manage_dealer_search_txt").val().trim() != '') {
            route += '&search=' + $("#manage_dealer_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    var handleDealerExport = function handleDealerExport() {
        $("#manage_dealer_export_btn").click(function () {
            if ($('input[name="dealer_selection"]:checked').length != 0) {
                var tokens = $('input[name="dealer_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };
    return {
        init: function init() {
            initDealerManage();
            handleDealerExport();
        }
    };
}();
jQuery(document).ready(function () {
    ManageDealers.init();
});

var ShowDealer = function () {
    var initDealerShow = function initDealerShow() {
        $('form.dealer-replacement-battery-form').each(function (key, form) {
            $(form).validate({
                rules: {
                    serial_key: {
                        required: true,
                        checkForWhiteSpace: true,
                        digits: true,
                        minlength: 13,
                        maxlength: 13,
                        valid_serial_key: true
                    }
                },
                messages: {
                    serial_key: {
                        required: "Serial key is required.",
                        digits: "Serial key must contains only digits.",
                        minlength: "Serial key must be 13 digits long.",
                        maxlength: "Serial key must not long than 13 digits."
                    }
                }
            });
        });
        $('.dealer-pending-battery-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleDealerShowRedirect();
        });
    };
    var handleDealerShowRedirect = function handleDealerShowRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        window.location.href = route;
    };
    var handleDealerBatteryReplacement = function handleDealerBatteryReplacement() {
        $(".add-replacement-to-dealer").click(function () {
            var btn = $(this);
            var row = btn.closest("tr");
            var table = row.closest("table");
            if (!btn.closest("form").valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(btn.attr("data-action"), { 'serial_key': row.find('.serial-key').val(), '_method': 'PUT' }).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    if (table.find("tbody > tr").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        row.fadeOut(500).promise().done(function () {
                            row.remove();
                            updateDealerBatteryReplacementWidget();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var updateDealerBatteryReplacementWidget = function updateDealerBatteryReplacementWidget() {
        var pending_widget = $(".pending-replacement-counter");
        var closed_widget = $(".closed-replacement-counter");
        pending_widget.attr("data-value", parseInt(pending_widget.attr("data-value")) - 1);
        pending_widget.html(pending_widget.attr("data-value"));
        closed_widget.attr("data-value", parseInt(closed_widget.attr("data-value")) + 1);
        closed_widget.html(closed_widget.attr("data-value"));
    };
    return {
        init: function init() {
            initDealerShow();
            handleDealerBatteryReplacement();
        }
    };
}();
jQuery(document).ready(function () {
    ShowDealer.init();
});

var EditDealer = function () {

    var dealerEditValidation = function dealerEditValidation() {

        $("#updateDealerForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                distributor_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                distributor_id: {
                    required: "Distributor is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };

    var handleDealerEdit = function handleDealerEdit() {

        $("#updateDealerBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            dealerEditValidation();
            handleDealerEdit();
        }
    };
}();

jQuery(document).ready(function () {
    EditDealer.init();
});

var EditDealerProfile = function () {

    var dealerProfileEditValidation = function dealerProfileEditValidation() {

        $("#editDealerProfileForm").validate({
            rules: {
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                email: {
                    checkForWhiteSpace: true,
                    EMAIL: true
                },
                phone_number: {
                    required: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                address: {
                    required: true,
                    checkForWhiteSpace: true
                },
                city_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                zipcode: {
                    required: true,
                    checkForWhiteSpace: true
                },
                firm_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                gst_number: {
                    checkForWhiteSpace: true,
                    gst_number: true
                },
                pan_number: {
                    checkForWhiteSpace: true,
                    pan_number: true
                },
                aadhar_number: {
                    checkForWhiteSpace: true,
                    aadhar_number: true
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                email: {
                    EMAIL: "Email address is invalid."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Phone number must be in digits.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                },
                address: {
                    required: "Address is required."
                },
                city_id: {
                    required: "City is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                },
                firm_name: {
                    required: "Firm name is required."
                }
            }
        });
    };

    var handleDealerProfileEdit = function handleDealerProfileEdit() {

        $("#updateDealerProfileBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    window.location.href = form.attr("data-redirect-url");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            dealerProfileEditValidation();
            handleDealerProfileEdit();
        }
    };
}();

jQuery(document).ready(function () {
    EditDealerProfile.init();
});

var DealerBatteryWarrantyRequest = function () {

    var dealerBatteryWarrantyRequestValidation = function dealerBatteryWarrantyRequestValidation() {

        $("#addDealerWarrantyRequestForm").validate({
            rules: {
                old_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                new_battery_serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                },
                reason_for_replacement: {
                    required: true,
                    checkForWhiteSpace: true
                },
                reason_for_replacement_txt: {
                    required: function required(element) {
                        return $("[name='reason_for_replacement']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                old_battery_serial_key: {
                    required: "Old battery serial key is required.",
                    digits: "Old battery serial key must contains only digits.",
                    minlength: "Old battery serial key must be 13 digits long.",
                    maxlength: "Old battery serial key must not long than 13 digits."
                },
                new_battery_serial_key: {
                    required: "New battery serial key is required.",
                    digits: "New battery serial key must contains only digits.",
                    minlength: "New battery serial key must be 13 digits long.",
                    maxlength: "New battery serial key must not long than 13 digits."
                },
                reason_for_replacement: {
                    required: "Reason for replacement is required."
                },
                reason_for_replacement_txt: {
                    required: "Reason for replacement is required."
                }
            }
        });
    };

    var handleDealerBatteryWarrantyRequest = function handleDealerBatteryWarrantyRequest() {

        $("#dealerWarrantyRequestResetBtn").click(function () {
            callHideAtsAlert();
            form = $("#addDealerWarrantyRequestForm");
            form.find("#reason_for_replacement").val('').trigger("change");
            $('#old_battery_serial_key').attr('value', '');
            DDPApp.resetForm(form);
        });

        $("#dealerWarrantyRequestBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                var message = '';
                $.each(data.message, function (i) {
                    message += data.message[i] + "<br>";
                });
                displayAtsAlert(data.result, data.title, message);
                if (data.result) {
                    resetWarrantyRequestForm(form);
                }
                DDPApp.enableButton(btn);
            }).fail(function (data) {
                if (data.status == 422) {
                    var message = '';
                    $.each(data.responseJSON, function (key, value) {
                        for (var i = 0; i < value.length; i++) {
                            message += value[i] + "<br>";
                        }
                    });
                    displayAtsAlert(false, "Attention!", message);
                } else {
                    DDPApp.displayFailedValidation(data);
                }
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            dealerBatteryWarrantyRequestValidation();
            handleDealerBatteryWarrantyRequest();
        }
    };
}();

jQuery(document).ready(function () {
    DealerBatteryWarrantyRequest.init();
});

var ShowClosedReplacement = function () {
    var initClosedReplacementShow = function initClosedReplacementShow() {
        $('.dealer-closed-battery-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleClosedReplacementShowRedirect();
        });
    };
    var handleClosedReplacementShowRedirect = function handleClosedReplacementShowRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        window.location.href = route;
    };
    return {
        init: function init() {
            initClosedReplacementShow();
        }
    };
}();
jQuery(document).ready(function () {
    ShowClosedReplacement.init();
});

var BatteryCheckWarranty = function () {

    var checkWarrantyValidation = function checkWarrantyValidation() {

        $("#checkWarrantyCardContainer").hide();

        $("#checkWarrantyForm").validate({
            rules: {
                serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                }
            },
            messages: {
                serial_key: {
                    required: "Serial key is required.",
                    digits: "Serial key must contains only digits.",
                    minlength: "Serial key must be 13 digits long.",
                    maxlength: "Serial key must not long than 13 digits."
                }
            }
        });
    };

    var handleBatteryCheckWarranty = function handleBatteryCheckWarranty() {

        $("#checkWarrantyResetBtn").click(function () {

            DDPApp.resetForm($("#checkWarrantyForm"));
        });

        $("#checkWarrantyBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                $("#checkWarrantyCardContainer").append(data.view);
                $('#checkWarrantyFormContainer').animateCss('flipOutX', function () {
                    $("#checkWarrantyFormContainer").hide();
                    $("#checkWarrantyCardContainer").show().animateCss('flipInX');
                    //$("#msgContainer").attr("data-toggle","tooltip");
                    //$("#msgContainer").attr("title",data.message);
                });
                setTimeout(function () {
                    DDPApp.enableButton(btn);
                }, 2000);
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });

        $(document).on("click", "#checkWarrantyBackBtn", function () {

            $('#checkWarrantyCardContainer').animateCss('flipOutX', function () {
                $("#checkWarrantyCardContainer").hide();
                $("#checkWarrantyFormContainer").show().animateCss('flipInX');
                $("#checkWarrantyCardContainer").empty();
            });
        });
    };

    return {
        init: function init() {
            checkWarrantyValidation();
            handleBatteryCheckWarranty();
        }
    };
}();

jQuery(document).ready(function () {
    BatteryCheckWarranty.init();
});

var CreateBatteryType = function () {
    var batteryTypeCreateValidation = function batteryTypeCreateValidation() {
        $("#battery_photo").fileinput().on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#battery_photo').fileinput('destroy');
            $("#battery_photo").fileinput({
                showUpload: false,
                showRemove: true,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                msgPlaceholder: 'Select Battery Photo',
                browseLabel: 'Browse',
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                initialPreview: ["no image"],
                initialCaption: "Select Battery Photo"
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        });
        $("#battery_photo").trigger('fileclear');
        $('#with_effect_from').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            defaultViewDate: {
                year: 2016,
                month: '00',
                day: '01'
            },
            startDate: new Date(2016, 0, 1)
        });
    };
    var initBatteryTypeCreate = function initBatteryTypeCreate() {
        $("#addBatteryTypeForm").validate({
            rules: {
                battery_photo: {
                    required: true,
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                category_id: {
                    required: true,
                    checkForWhiteSpace: true
                },
                type: {
                    required: true,
                    checkForWhiteSpace: true
                },
                serial_code: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 2,
                    min: 1,
                    max: 99
                },
                voltage: {
                    checkForWhiteSpace: true,
                    number: true,
                    min: 1,
                    max: 999.99
                },
                capacity: {
                    checkForWhiteSpace: true,
                    number: true,
                    min: 1,
                    max: 999.99
                },
                description: {
                    checkForWhiteSpace: true
                },
                with_effect_from: {
                    required: true,
                    checkForWhiteSpace: true
                },
                warranty_period: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    min: 1,
                    max: 1264
                },
                grace_period: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    min: 1,
                    max: 1264
                }
            },
            messages: {
                battery_photo: {
                    required: "Battery photo is required.",
                    filesize: "Battery photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Battery photo must be in .jpg,.jpeg,.png format."
                },
                category_id: {
                    required: "Battery category is required."
                },
                type: {
                    required: "Model number is required."
                },
                serial_code: {
                    required: "Serial code is required.",
                    digits: "Serial code must be number.",
                    minlength: "Serial code must be 2 digits.",
                    min: "Minimum value for serial code is 1.",
                    max: "Maximum value for serial code is 99."
                },
                voltage: {
                    number: "Voltage must be a number.",
                    min: "Minimum value for voltage is 1.",
                    max: "Maximum value for voltage is 999.99."
                },
                capacity: {
                    number: "Capacity must be a number",
                    min: "Minimum value for capacity is 1.",
                    max: "Maximum value for capacity is 999.99."
                },
                with_effect_from: {
                    required: "With effect from date is required."
                },
                warranty_period: {
                    required: "Warranty period is required.",
                    digits: "Warranty period must be number.",
                    min: "Minimum value for warranty period is 1 month.",
                    max: "Maximum value for warranty period is 1264 month."
                },
                grace_period: {
                    required: "Grace period is required.",
                    digits: "Grace period must be number.",
                    min: "Minimum value for grace period is 1 month.",
                    max: "Maximum value for grace period is 1264 month."
                }
            }
        });
    };
    var handleBatteryTypeCreate = function handleBatteryTypeCreate() {
        $("#addBatteryTypeResetBtn").click(function () {
            resetBatteryTypeForm($("#addBatteryTypeForm"));
        });
        $("#addBatteryTypeBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            var formData = new FormData();
            var image = document.getElementById('battery_photo');
            if (image.files && image.files[0]) {
                var extension = image.files[0].name.split('.');
                var temp2 = extension[extension.length - 1].toLowerCase();
                var size = parseFloat(image.files[0].size / 1024).toFixed(2);
                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('battery_photo', image.files[0]);
                }
            }
            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });
            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        $("#pro_rata_portlet").html(data.form);
                        $("#battery_type_footer").remove();
                        initCreateProRata();
                        DDPApp.enableButton(btn);
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };
    var resetBatteryTypeForm = function resetBatteryTypeForm(form) {
        $("#category_id").val('').trigger("change");
        DDPApp.resetForm(form);
    };
    return {
        init: function init() {
            batteryTypeCreateValidation();
            initBatteryTypeCreate();
            handleBatteryTypeCreate();
        }
    };
}();
jQuery(document).ready(function () {
    CreateBatteryType.init();
});

var ManageBatteryTypes = function () {

    var initBatteryTypeManage = function initBatteryTypeManage() {

        $('.battery-type-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleBatteryTypeRedirect();
        });

        $("#batteryTypeSearchBtn").click(function () {
            handleBatteryTypeRedirect();
        });

        $("#batteryTypeSearchRefreshBtn").click(function () {
            $("#batteryTypeSearchText").val('');
            handleBatteryTypeRedirect();
        });
    };

    var handleBatteryTypeRedirect = function handleBatteryTypeRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#batteryTypeSearchText").val().trim() != '') {
            route += '&search=' + $("#batteryTypeSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };

    return {
        init: function init() {
            initBatteryTypeManage();
        }
    };
}();

jQuery(document).ready(function () {
    ManageBatteryTypes.init();
});

var EditBatteryType = function () {
    var batteryTypeEditValidation = function batteryTypeEditValidation() {
        preview = ["<img src='" + $("#edit_battery_photo").attr("data-battery-photo-url") + "' height='150px' width='150px'>"];
        $("#edit_battery_photo").fileinput({
            showUpload: false,
            showRemove: false,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            minFileSize: 1,
            maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            browseLabel: 'Change',
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            initialPreview: preview,
            initialCaption: '1 battery photo uploaded'
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_battery_photo').fileinput('destroy');
            $("#edit_battery_photo").fileinput({
                showUpload: false,
                showRemove: false,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: 'Change',
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: '1 battery photo uploaded'
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
            toastr.error('File is invalid please select another file.', 'Invalid File');
        });
        $("#edit_battery_photo").trigger('fileclear');
    };
    var initBatteryTypeEdit = function initBatteryTypeEdit() {
        $("#updateBatteryTypeForm").validate({
            rules: {
                edit_battery_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                type: {
                    required: true,
                    checkForWhiteSpace: true
                },
                voltage: {
                    checkForWhiteSpace: true,
                    number: true,
                    min: 1,
                    max: 999.99
                },
                capacity: {
                    checkForWhiteSpace: true,
                    number: true,
                    min: 1,
                    max: 999.99
                },
                description: {
                    checkForWhiteSpace: true
                }
            },
            messages: {
                edit_battery_photo: {
                    filesize: "Battery photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Battery photo must be in .jpg,.jpeg,.png format."
                },
                type: {
                    required: "Model number is required."
                },
                voltage: {
                    number: "Voltage of battery must be in digits.",
                    min: "Minimum value for voltage is 1.",
                    max: "Maximum value for voltage is 999.99."
                },
                capacity: {
                    number: "Capacity of battery must be in digits.",
                    min: "Minimum value for capacity is 1.",
                    max: "Maximum value for capacity is 999.99."
                }
            }
        });
    };
    var handleBatteryTypeEdit = function handleBatteryTypeEdit() {
        $("#updateBatteryTypeBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            var formData = new FormData();
            var image = document.getElementById('edit_battery_photo');
            if (image.files && image.files[0]) {
                var extension = image.files[0].name.split('.');
                var temp2 = extension[extension.length - 1].toLowerCase();
                var size = parseFloat(image.files[0].size / 1024).toFixed(2);
                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('edit_battery_photo', image.files[0]);
                }
            }
            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });
            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        var referrer = document.referrer;
                        if (!referrer) {
                            window.location.href = form.attr("data-redirect-url");
                        } else {
                            window.location.href = referrer;
                        }
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };
    return {
        init: function init() {
            batteryTypeEditValidation();
            initBatteryTypeEdit();
            handleBatteryTypeEdit();
        }
    };
}();

jQuery(document).ready(function () {
    EditBatteryType.init();
});

var CreateWarrantyPeriod = function () {
    var initCreateWarrantyPeriod = function initCreateWarrantyPeriod() {
        $("#add_warranty_period_form").validate({
            rules: {
                with_effect_from: {
                    required: true,
                    checkForWhiteSpace: true
                },
                warranty_period: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    min: 1,
                    max: 1264
                },
                grace_period: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    min: 1,
                    max: 1264
                }
            },
            messages: {
                with_effect_from: {
                    required: "With effect from date is required."
                },
                warranty_period: {
                    required: "Warranty period is required.",
                    digits: "Warranty period must be number.",
                    min: "Warranty period must be atleast 1 month.",
                    max: "Maximum value for warranty period is 1264 month."
                },
                grace_period: {
                    required: "Grace period is required.",
                    digits: "Grace period must be number.",
                    min: "Grace period must be atleast 1 month.",
                    max: "Maximum value for grace period is 1264 month."
                }
            }
        });
    };
    var handleCreateWarrantyPeriod = function handleCreateWarrantyPeriod() {
        $(document).on('click', '#add_warranty_period_btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    $("#warranty_period_table > tbody > tr.no-data-block").hide();
                    $("#warranty_period_table > tbody").prepend(data.row);
                    DDPApp.resetForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initCreateWarrantyPeriod();
            handleCreateWarrantyPeriod();
        }
    };
}();
jQuery(document).ready(function () {
    CreateWarrantyPeriod.init();
});

var DestroyWarrantyPeriod = function () {
    var handleDestroyWarrantyPeriod = function handleDestroyWarrantyPeriod() {
        $(document).on('click', '.ats-destroy-warranty-period-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButton(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    btn.closest("tr").fadeOut(500).promise().done(function () {
                        if ($("#warranty_period_table > tbody > tr:not(.no-data-block)").length == 1) {
                            btn.closest("tr").remove();
                            $("#pro_rata_portlet").empty();
                            $("#warranty_period_table > tbody > tr.no-data-block").show();
                        } else {
                            btn.closest("tr").remove();
                        }
                    });
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyWarrantyPeriod();
        }
    };
}();

jQuery(document).ready(function () {
    DestroyWarrantyPeriod.init();
});

var ShowWarrantyPeriod = function () {
    var loadProRata = function loadProRata() {
        $(document).on('dblclick', 'tr.ats-load-pro-rata', function (e) {
            e.preventDefault();
            var row = $(this);
            if ($("tr.m-table__row--brand").index() == row.index()) {
                $("tr.ats-load-pro-rata").removeClass("m-table__row--brand");
                $("#pro_rata_portlet").empty();
                return;
            }
            $("tr.ats-load-pro-rata").removeClass("m-table__row--brand");
            row.addClass("m-table__row--brand");
            DDPApp.disableButton(row);
            $.get(row.attr("data-action")).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    $("#pro_rata_portlet").html(data.view);
                    initCreateProRata();
                }, function () {});
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
            });
        });
    };
    return {
        init: function init() {
            loadProRata();
        }
    };
}();

jQuery(document).ready(function () {
    ShowWarrantyPeriod.init();
});

var initCreateProRata = function initCreateProRata() {
    $("#create_pro_rata_form").validate({
        rules: {
            percentage: {
                required: true,
                checkForWhiteSpace: true,
                digits: true,
                min: 1,
                max: 100
            },
            no_of_months: {
                required: true,
                checkForWhiteSpace: true,
                digits: true,
                min: 1
            }
        },
        messages: {
            percentage: {
                required: "Percentage is required.",
                digits: "Percentage must be number"
            },
            no_of_months: {
                required: "No of months is required.",
                digits: "No of months must be number.",
                min: "No of months must be atleast 1 month."
            }
        }
    });
};
var CreateProRata = function () {
    var handleCreateProRata = function handleCreateProRata() {
        $(document).on('click', '#create_pro_rata_btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    $("#pro_rata_table > tbody > tr.no-data-block").hide();
                    $("#pro_rata_table > tbody").append(data.row);
                    DDPApp.resetForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initCreateProRata();
            handleCreateProRata();
        }
    };
}();
jQuery(document).ready(function () {
    CreateProRata.init();
});

var DestroyProRata = function () {
    var handleDestroyProRata = function handleDestroyProRata() {
        $(document).on('click', '.ats-destroy-pro-rata', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButton(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    btn.closest("tr").fadeOut(500).promise().done(function () {
                        if ($("#pro_rata_table > tbody > tr:not(.no-data-block)").length == 1) {
                            btn.closest("tr").remove();
                            $("#pro_rata_table > tbody > tr.no-data-block").show();
                        } else {
                            btn.closest("tr").remove();
                        }
                    });
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyProRata();
        }
    };
}();

jQuery(document).ready(function () {
    DestroyProRata.init();
});

var BatteryDetails = function () {
    var initBatteryDetails = function initBatteryDetails() {
        $("#getBatteryDetailsForm").validate({
            rules: {
                serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                }
            },
            messages: {
                serial_key: {
                    required: "Serial key is required.",
                    digits: "Serial key must contains only digits.",
                    minlength: "Serial key must be 13 digits long.",
                    maxlength: "Serial key must not long than 13 digits."
                }
            }
        });
    };
    var handleBatteryDetails = function handleBatteryDetails() {
        $("#getBatteryDetailsBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest('form');
            if (!form.valid()) {
                return;
            }
            $("#tabview").empty();
            DDPApp.disableButtonWithLoading(btn);
            $.get(form.attr('data-action'), form.serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    $("#tabview").html(data.tabview);
                    $("#tabview_container").show();
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayErrorMessage(data);
                DDPApp.enableButton(btn);
            });
        });
        $("#batteryDetailsResetBtn").click(function (e) {
            $("#tabview").empty();
            $("#tabview_container").hide();
            DDPApp.resetForm($("#getBatteryDetailsForm"));
            DDPApp.enableButton(btn);
        });
    };
    return {
        init: function init() {
            initBatteryDetails();
            handleBatteryDetails();
        }
    };
}();
jQuery(document).ready(function () {
    BatteryDetails.init();
});

var BatteryWarrantyExtension = function () {

    var checkBatteryWarrantyValidation = function checkBatteryWarrantyValidation() {

        $("#checkWarrantyExtensionCardContainer").hide();

        $("#checkSerialKeyForm").validate({
            rules: {
                serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                }
            },
            messages: {
                serial_key: {
                    required: "Battery serial key is required.",
                    digits: "Serial key must contains only digits.",
                    minlength: "Serial key must be 13 digits long.",
                    maxlength: "Serail key must not long than 13 digits."
                }
            }
        });
    };

    var handleBatteryCheckWarrantyRevalidation = function handleBatteryCheckWarrantyRevalidation() {

        $(document).on('click', '#checkSerialKeyBtn', function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                $("#checkWarrantyExtensionCardContainer").append(data.view);
                $('#checkWarrantyExtensionFormContainer').animateCss('flipOutX', function () {
                    $("#checkWarrantyExtensionFormContainer").hide();
                    $("#checkWarrantyExtensionCardContainer").show().animateCss('flipInX');
                });
                setTimeout(function () {
                    DDPApp.enableButton(btn);
                }, 2000);
                showBatteryRevalidationForm(data.form);
                showBatteryRevalidationTable(data.table);
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });

            $(document).on("click", "#checkWarrantyExtensionBackBtn", function () {

                $('#checkWarrantyExtensionCardContainer').animateCss('flipOutX', function () {
                    $("#checkWarrantyExtensionCardContainer").hide();
                    $("#checkWarrantyExtensionFormContainer").show().animateCss('flipInX');
                    $("#checkWarrantyExtensionCardContainer").empty();
                });
            });
        });
    };

    var showBatteryRevalidationForm = function showBatteryRevalidationForm(form) {

        $("#batteryRevalidationForm").html(form);
        handleBatteryRevalidationInsert();
    };

    var handleBatteryRevalidationInsert = function handleBatteryRevalidationInsert() {

        $('[name="reason"]').select2({
            placeholder: "Select Battery Extension Reason"
        }).change(function () {
            $(this).valid();
            var form = $(this).closest('form');
            var data = form.find('input[name="reason"]');
            var dataTextarea = form.find('textarea[name="reason_txt"]');
            if (dataTextarea) {
                if ($(this).val() == 'Other') {
                    form.find("#reason_txt").css('display', 'block');
                    dataTextarea.focus();
                } else {
                    dataTextarea.val('').trigger("change");
                    form.find("#reason_txt").css('display', 'none');
                }
            }
        }).on('click', function (event) {
            var form = $(this).closest('form');
            var data = $(this).element['reason'];
            var dataTextarea = $(this).element['reason_txt'];

            data.value = data.value == 'Other' ? dataTextarea.value : data.value;

            $(this).valid();
        });

        $('#revalidateBatteryWarrantyForm').validate({
            rules: {
                no_of_days: {
                    required: true,
                    checkForWhiteSpace: true,
                    step: 1
                },
                reason: {
                    required: true
                },
                reason_txt: {
                    required: function required(element) {
                        return $("[name='reason']").val() == 'Other';
                    },
                    checkForWhiteSpace: true
                }
            },
            messages: {
                no_of_days: {
                    required: 'Number of days are required.',
                    step: 'Number of days should be an integer.'
                },
                reason: {
                    required: "Battery extension reason is required."
                },
                reason_txt: {
                    required: "Battery extension reason text is required."
                }
            }
        });

        $(document).on('click', "#saveRevalidateBatteryWarrantyBtn", function () {

            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            $.post($('#revalidateBatteryWarrantyForm').attr('data-action'), $('#revalidateBatteryWarrantyForm').serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    resetWarrantyRevalidationForm(form);
                    window.location.href = form.attr("data-redirect-url");
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
            });
        });
    };

    var showBatteryRevalidationTable = function showBatteryRevalidationTable(html) {

        $('#batteryRevalidationTableRow').html(html);
    };

    var resetWarrantyRevalidationForm = function resetWarrantyRevalidationForm(form) {

        form.find("#reason_txt").val('').trigger("change");
        form.find("#reason_txt").css('display', 'none');
        $("#reason").val('').trigger("change");
        DDPApp.resetForm(form);
    };

    return {
        init: function init() {
            checkBatteryWarrantyValidation();
            handleBatteryCheckWarrantyRevalidation();
        }
    };
}();

jQuery(document).ready(function () {
    BatteryWarrantyExtension.init();
});

var ManageWarrantyRequests = function () {
    var start = moment().subtract(29, 'days');
    var end = moment();
    if ($('#warranty_start_date .form-control').attr("data-start-date")) {
        var _$$attr$split = $('#warranty_start_date .form-control').attr("data-start-date").split("/"),
            _$$attr$split2 = _slicedToArray(_$$attr$split, 3),
            day = _$$attr$split2[0],
            month = _$$attr$split2[1],
            year = _$$attr$split2[2];

        var f = new Date(year, month - 1, day);
        start = moment(f);
    }
    if ($('#warranty_start_date .form-control').attr("data-end-date")) {
        var _$$attr$split3 = $('#warranty_start_date .form-control').attr("data-end-date").split("/"),
            _$$attr$split4 = _slicedToArray(_$$attr$split3, 3),
            _day = _$$attr$split4[0],
            _month = _$$attr$split4[1],
            _year = _$$attr$split4[2];

        var f = new Date(_year, _month - 1, _day);
        end = moment(f);
    }
    var initWarrantyRequestManage = function initWarrantyRequestManage() {
        $('#warranty_start_date').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-brand',
            cancelClass: 'btn-danger',
            startDate: start,
            endDate: end,
            locale: {
                cancelLabel: 'Cancel',
                format: 'DD/MM/YYYY'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#warranty_start_date .form-control').val(moment(start).format('DD/MM/YYYY') + ' - ' + moment(end).format('DD/MM/YYYY'));
        });
        $("#warranty_start_date").on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
        });
        $("#warranty_start_date").on('cancel.daterangepicker', function (ev, picker) {
            $('#warranty_start_date .form-control').val('');
            start = '';
            end = '';
        });
        $('.warranty-request-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleWarrantyRequestRedirect();
        });
        $("#manage_warranty_request_search_btn").click(function () {
            handleWarrantyRequestRedirect();
        });
        $("#manage_warranty_request_search_refresh_btn").click(function () {
            $("#warranty_request_search_txt").val('');
            $("#warranty_start_date .form-control").val('');
            handleWarrantyRequestRedirect();
        });
        $('#all_warranty_request_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#warrantyRequestTable').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#warrantyRequestTable').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleWarrantyRequestRedirect = function handleWarrantyRequestRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#warranty_records").val();
        route += "&outcome=" + $(".warranty-request-tabview").attr("data-status");
        if ($("#warranty_start_date .form-control").val() != '') {
            route += '&from=' + moment(start).format('DD/MM/YYYY') + '&to=' + moment(end).format('DD/MM/YYYY');
        }
        if ($("#warranty_request_search_txt").val().trim() != '') {
            route += '&search=' + $("#warranty_request_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    var handleWarrantyRequestExport = function handleWarrantyRequestExport() {
        $("#manage_warranty_request_export_btn").click(function () {
            if ($('input[name="warranty_request_selection"]:checked').length != 0) {
                var tokens = $('input[name="warranty_request_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };
    return {
        init: function init() {
            initWarrantyRequestManage();
            handleWarrantyRequestExport();
        }
    };
}();
jQuery(document).ready(function () {
    ManageWarrantyRequests.init();
});

var ApproveWarrantyRequest = function () {
    var handleWarrantyRequestApproval = function handleWarrantyRequestApproval() {
        $(document).on('click', '.approve-warranty-request', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButton(btn);
            DDPApp.disableButton(btn.next(".reject-warranty-request"));
            $.post(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    window.location.href = btn.attr("data-redirect-url");
                    //                        window.open(btn.closest("tr").attr("data-redirect-url"),'_blank');
                    //                        DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleWarrantyRequestApproval();
        }
    };
}();
jQuery(document).ready(function () {
    ApproveWarrantyRequest.init();
});

var RejectWarrantyRequest = function () {
    var rejection_modal = $("#rejectWarrantyRequestModal");
    var rejection_url;
    var redirect_url;
    var openWarrantyRejectionModal = function openWarrantyRejectionModal() {
        $(document).on('click', '.reject-warranty-request', function (e) {
            if (fillDataInModal($(this))) {
                rejection_modal.modal("show");
            }
        });
        $("#rejectWarrantyRequestForm").validate({
            rules: {
                reason_for_rejection: {
                    required: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                reason_for_rejection: {
                    required: "Reason for rejection is required."
                }
            }
        });
        $("#reasonForRejection").select2({
            dropdownParent: $(rejection_modal),
            placeholder: 'Select reason for rejection'
        }).change(function () {
            $(this).valid();
            if ($(this).val() == 'Other') {
                $(this).removeAttr("name");
                $("#reasonForRejectionContainer textarea").attr("name", "reason_for_rejection");
                $("#reasonForRejectionContainer").show();
            } else {
                $("#reasonForRejectionContainer textarea").removeAttr("name");
                $(this).attr("name", "reason_for_rejection");
                $("#reasonForRejectionContainer").hide();
            }
        });
    };
    var registerWarrantyRejectionModalEvent = function registerWarrantyRejectionModalEvent() {
        rejection_modal.on('hidden.bs.modal', function (e) {
            refreshModal();
        });
    };
    var handleWarrantyRequestRejection = function handleWarrantyRequestRejection() {
        $("#rejectWarrantyRequestBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest("form");
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButton(btn);
            $.post(rejection_url, form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    $("#reasonForRejection").val('').trigger('change.select2');
                    var redirection = redirect_url;
                    //                        rejection_modal.modal("hide");
                    window.location.href = redirection;
                    //                        window.open(redirection, '_blank');
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var fillDataInModal = function fillDataInModal(btn) {
        rejection_url = btn.attr("data-action");
        redirect_url = btn.attr("data-redirect-url");
        $("#old_battery_serial_key_container").html(btn.attr("data-old-battery"));
        $("#new_battery_serial_key_container").html(btn.attr("data-new-battery"));
        $("#reason_for_replacement_container").html(btn.attr("data-reason-for-replacment"));
        return true;
    };
    var refreshModal = function refreshModal() {
        rejection_url = null;
        redirect_url = null;
        $("#old_battery_serial_key_container").empty();
        $("#new_battery_serial_key_container").empty();
        $("#reason_for_replacement_container").empty();
        $("#reasonForRejection").val('').trigger("change");
        DDPApp.resetForm($("#rejectWarrantyRequestForm"));
        DDPApp.enableButton($(".ats-submit"));
    };
    return {
        init: function init() {
            openWarrantyRejectionModal();
            handleWarrantyRequestRejection();
            registerWarrantyRejectionModalEvent();
        }
    };
}();
jQuery(document).ready(function () {
    RejectWarrantyRequest.init();
});

var resetRejectionFields = function resetRejectionFields($form) {
    $rejection = $form.find("#warrantyRequestReasonForRejection");
    var validator = $form.validate();
    $("#warrantyRequestReasonForRejectionContainer textarea").val('');
    $rejection.val('').trigger('change');
    if (validator.settings.unhighlight) {
        validator.settings.unhighlight.call(validator, $rejection[0], validator.settings.errorClass, validator.settings.validClass);
    }
    validator.hideThese(validator.errorsFor($rejection[0]));
};
var ReasonForRejection = function () {
    var registerWarrantyRequestStatusChangeEvent = function registerWarrantyRequestStatusChangeEvent() {
        $('input[type=radio][name=outcome]').change(function () {
            var form = $(this).closest('form');
            if ($(this).val() == 'rejected') {
                $("#rejectionContainer").show();
            } else {
                $("#rejectionContainer").hide();
                resetRejectionFields(form);
            }
        });
        $("#warrantyRequestReasonForRejection").select2({
            placeholder: 'Select reason for rejection'
        }).change(function () {
            $(this).valid();
            if ($(this).val() == 'Other') {
                $(this).removeAttr("name");
                $("#warrantyRequestReasonForRejectionContainer textarea").attr("name", "reason_for_rejection");
                $("#warrantyRequestReasonForRejectionContainer").show();
            } else {
                $("#warrantyRequestReasonForRejectionContainer textarea").removeAttr("name");
                $(this).attr("name", "reason_for_rejection");
                $("#warrantyRequestReasonForRejectionContainer").hide();
            }
        });
    };
    return {
        init: function init() {
            registerWarrantyRequestStatusChangeEvent();
        }
    };
}();
jQuery(document).ready(function () {
    ReasonForRejection.init();
});

var DispatchToDistributor = function () {
    var wizard;
    var initWizard = function initWizard() {
        var wizardEl = $('#distributor_dispatch_wizard');
        wizard = wizardEl.mWizard({
            startStep: 1,
            manualStepForward: true
        });
        wizard.on('beforeNext', function (wizard) {
            if (wizard.getStep() == 1) {
                if ($("#addOrderForm").attr("data-action") == null || $("#addOrderForm").attr("data-action") == '') {
                    return false;
                }
            } else if (wizard.getStep() == 2) {
                $('.ats-dispatch-type').select2({
                    placeholder: "Select Dispatch Type"
                }).change(function () {
                    $(this).valid();
                }).val('regular').trigger('change.select2');
                if ($("#addDispatchBatteryForm").attr("data-action") == null || $("#addDispatchBatteryForm").attr("data-action") == '') {
                    return false;
                }
            }
        });
    };
    var initDistributorDispatchStep1 = function initDistributorDispatchStep1() {
        var isCompany = $('#isCompany').val();
        var result = isCompany == "true";
        $('.ats-dispatch-date').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            setDate: 'now',
            enableOnReadonly: false
        });
        $('.ats-invoice-date').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            autoclose: true,
            enableOnReadonly: false
        });
        $("#addDistributorDispatchWizardStep1Form").validate({
            rules: {
                distributor_id: {
                    required: true
                },
                dispatch_date: {
                    required: true
                },
                transporter_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                contact_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                lr_no: {
                    checkForWhiteSpace: true,
                    lr_no: result
                },
                po_no: {
                    checkForWhiteSpace: true,
                    po_no: result
                },
                vehicle_number: {
                    checkForWhiteSpace: true
                },
                driver_name: {
                    checkForWhiteSpace: true
                },
                invoice_number: {
                    checkForWhiteSpace: true
                },
                invoice_amount: {
                    checkForWhiteSpace: true
                },
                total_no_of_boxes: {
                    digits: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                distributor_id: {
                    required: "Distributor name is required."
                },
                dispatch_date: {
                    required: "Battery dispatch date is required."
                },
                transporter_name: {
                    required: "Transporter name is required."
                },
                contact_number: {
                    required: "Contact number is required.",
                    digits: "Contact number must be in digits.",
                    rangelength: "Contact number must be between 10 to 12 digits."
                },
                total_quantity: {
                    required: "Total quantity of batteries are required."
                },
                total_no_of_boxes: {
                    digits: "Total number of boxes must be in digits."
                }
            }
        });
    };
    var handleDistributorDispatchStep1 = function handleDistributorDispatchStep1() {
        $(document).on('click', '#addDistributorDispatchWizardStep1Btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    populateModelsInBatteryTypeSelection(data.models);
                    urlToDispatchForm(data);
                    // remove distributor select2
                    if ($('#distributor_label_is_exists').val() == '0') {
                        var distributor_with_company = $.trim($("#distributor_id option:selected").html());
                        var distributor_id = $("#distributor_id").val();
                        $('#distributor_select2_div').remove();
                        $('#distributor_id_div').append('<input type="hidden" name="distributor_id" value="' + distributor_id + '">').append('<input type="text" disabled="disabled" autocomplete="off" class="form-control m-input--air" placeholder="Distributor Name" value="' + distributor_with_company + '">');
                        $('#distributor_label_is_exists').val('1');
                    }
                    DDPApp.enableButton(btn);
                    wizard.goNext();
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var urlToDispatchForm = function urlToDispatchForm(data) {
        if (data.update_dispatch_route) {
            $("#addDistributorDispatchWizardStep1Form").attr("data-action", data.update_dispatch_route);
        }
        $("#addDistributorDispatchWizardStep1Form").append(data.method);
        $("#addOrderForm").attr('data-action', data.add_battery_models_route);
        $("#addDispatchBatteryForm").attr('data-action', data.add_batteries_route);
        $("#multiple_dispatch_battery_btn").attr('data-action', data.delete_batteries_route);
        if (data.update_status_route) {
            $(".ats-update-dispatch-status-btn").attr('data-action', data.update_status_route);
        }
    };
    return {
        init: function init() {
            initWizard();
            initDistributorDispatchStep1();
            handleDistributorDispatchStep1();
        }
    };
}();
jQuery(document).ready(function () {
    DispatchToDistributor.init();
});

var ManageBatteryDispatches = function () {
    var lr_no_row;
    var lr_no_modal = $("#editLRNumberModal");
    var update_lr_no_url;
    var start = moment().subtract(29, 'days');
    var end = moment();
    if ($('#manage_dispatch_order_date_filter .form-control').attr("data-start-date")) {
        var _$$attr$split5 = $('#manage_dispatch_order_date_filter .form-control').attr("data-start-date").split("/"),
            _$$attr$split6 = _slicedToArray(_$$attr$split5, 3),
            day = _$$attr$split6[0],
            month = _$$attr$split6[1],
            year = _$$attr$split6[2];

        var f = new Date(year, month - 1, day);
        start = moment(f);
    }
    if ($('#manage_dispatch_order_date_filter .form-control').attr("data-end-date")) {
        var _$$attr$split7 = $('#manage_dispatch_order_date_filter .form-control').attr("data-end-date").split("/"),
            _$$attr$split8 = _slicedToArray(_$$attr$split7, 3),
            _day2 = _$$attr$split8[0],
            _month2 = _$$attr$split8[1],
            _year2 = _$$attr$split8[2];

        var f = new Date(_year2, _month2 - 1, _day2);
        end = moment(f);
    }
    var initBatteryDispatchManage = function initBatteryDispatchManage() {
        $('#manage_dispatch_order_search_by_distributor').select2({
            placeholder: "Filter by Distributors",
            closeOnSelect: true,
            allowClear: true,
            maximumSelectionLength: 10,
            language: {
                maximumSelected: function maximumSelected(e) {
                    var t = "You can only select " + e.maximum + " distributors";
                    return t;
                }
            }
        });
        $('#manage_dispatch_order_date_filter').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-brand',
            cancelClass: 'btn-danger',
            startDate: start,
            endDate: end,
            locale: {
                cancelLabel: 'Cancel',
                format: 'DD/MM/YYYY'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#manage_dispatch_order_date_filter .form-control').val(moment(start).format('DD/MM/YYYY') + ' - ' + moment(end).format('DD/MM/YYYY'));
        });
        $("#manage_dispatch_order_date_filter").on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
        });
        $("#manage_dispatch_order_date_filter").on('cancel.daterangepicker', function (ev, picker) {
            $('#manage_dispatch_order_date_filter .form-control').val('');
            start = '';
            end = '';
        });
        $('.dispatch-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleBatteryDispatchRedirect();
        });
        $("#batteryDispatchSearchBtn").click(function () {
            handleBatteryDispatchRedirect();
        });
        $("#batteryDispatchSearchRefreshBtn").click(function () {
            $("#batteryDispatchSearchText").val('');
            $("#manage_dispatch_order_search_by_distributor").val('').trigger('change');
            $("#manage_dispatch_order_date_filter .form-control").val('');
            handleBatteryDispatchRedirect();
        });
    };
    var handleBatteryDispatchRedirect = function handleBatteryDispatchRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".dispatches-tabview").attr("data-status");
        if ($("#manage_dispatch_order_search_by_distributor").length != 0 && $("#manage_dispatch_order_search_by_distributor").val() != '') {
            route += '&distributor_filter=' + $("#manage_dispatch_order_search_by_distributor").val();
        }
        if ($("#manage_dispatch_order_date_filter .form-control").val() != '') {
            route += '&from=' + moment(start).format('DD/MM/YYYY') + '&to=' + moment(end).format('DD/MM/YYYY');
        }
        if ($("#batteryDispatchSearchText").val().trim() != '') {
            route += '&search=' + $("#batteryDispatchSearchText").val().replace(/<[^>]+>/g, '').trim();
        }

        window.location.href = route;
    };

    // for update lr number

    var openLRNumberModal = function openLRNumberModal() {
        $(document).on('click', '.ats-edit-lr-no', function (e) {
            var btn = $(this);
            lr_no_row = btn.closest(".ats-list-item");
            update_lr_no_url = btn.attr("data-action");
            lr_no_modal.find(".modal-body > .alert > strong").html($(this).attr("data-dispatch-date"));
            lr_no_modal.find('[name=lr_no]').val(btn.attr("data-lr-no"));
            lr_no_modal.modal("show");
        });
        $("#updateLRNumberForm").validate({
            rules: {
                lr_no: {
                    required: true,
                    checkForWhiteSpace: true,
                    lr_no: true
                }
            },
            messages: {
                lr_no: {
                    required: "LR number is required."
                }
            }
        });
    };
    var registerLRNumberModalEvent = function registerLRNumberModalEvent() {
        lr_no_modal.on('hidden.bs.modal', function (e) {
            update_lr_no_url = null;
            DDPApp.resetForm($("#updateLRNumberForm"));
            DDPApp.enableButton($(".ats-submit"));
        });
    };
    var handleLRNumber = function handleLRNumber() {
        $("#update_lr_no_btn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest("form");
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButton(btn);
            $.post(update_lr_no_url, form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    lr_no_row.replaceWith(data.row);
                    lr_no_modal.modal("hide");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initBatteryDispatchManage();
            openLRNumberModal();
            handleLRNumber();
            registerLRNumberModalEvent();
        }
    };
}();
jQuery(document).ready(function () {
    ManageBatteryDispatches.init();
});

var EditDispatchOrder = function () {
    var dispatch_order_modal = $("#editDispatchOrderModal");
    var orderRow = null;
    var handleDispatchOrderEdit = function handleDispatchOrderEdit() {
        $(document).on('click', '.ats-edit-dispatch-order-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            orderRow = btn.closest("tr");
            DDPApp.disableButton(btn);
            $.get(btn.attr("data-action")).done(function (data) {
                dispatch_order_modal.find(".modal-body").html(data.form);
                dispatch_order_modal.find(".modal-title > span").html(orderRow.attr("data-battery-type"));
                dispatch_order_modal.modal("toggle");
                validateUpdateOrderForm();
                DDPApp.enableButton(btn);
            }).fail(function () {
                DDPApp.enableButton(btn);
            });
        });
    };
    var registerUpdateOrderModalEvent = function registerUpdateOrderModalEvent() {
        dispatch_order_modal.on('hidden.bs.modal', function () {
            dispatch_order_modal.find(".modal-body").empty();
            dispatch_order_modal.find(".modal-title > span").empty();
            orderRow = null;
        });
    };
    var validateUpdateOrderForm = function validateUpdateOrderForm() {
        $('#updateOrderForm').validate({
            rules: {
                regular_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                },
                foc_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                },
                replacement_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                }
            },
            messages: {
                regular_battery_count: {
                    digits: "No. of regular battery must contains only digits.",
                    require_from_group: "One of these must be required."
                },
                foc_battery_count: {
                    digits: "No. of foc battery must contains only digits.",
                    require_from_group: "One of these must be required."
                },
                replacement_battery_count: {
                    digits: "No. of replaced battery must contains only digits.",
                    require_from_group: "One of these must be required."
                }
            }
        });
    };
    var handleOrderUpdate = function handleOrderUpdate() {
        $(document).on('click', '#updateOrderBtn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = btn.closest("form");
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButton(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    orderRow.replaceWith(data.row);
                    dispatch_order_modal.modal("hide");
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDispatchOrderEdit();
            registerUpdateOrderModalEvent();
            handleOrderUpdate();
        }
    };
}();
jQuery(document).ready(function () {
    EditDispatchOrder.init();
});

var DestroyDispatch = function () {
    var handleDestroyDispatch = function handleDestroyDispatch() {
        $(document).on('click', '.ats-destroy-dispatch-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButtonWithLoading(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                            btn.parentsUntil(".ats-list-item").parent().remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyDispatch();
        }
    };
}();
jQuery(document).ready(function () {
    DestroyDispatch.init();
});

var DispatchStatus = function () {
    var handleUpdateStatus = function handleUpdateStatus() {
        $(document).on('click', '.ats-update-dispatch-status-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButtonWithLoading(btn);
            $.post(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find("#dispatch_content").length == 0) {
                        window.location.href = btn.attr("data-redirect-url");
                    } else {
                        if ($(document).find(".ats-list-item").length == 1) {
                            window.location.href = window.location.href;
                        } else {
                            btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                btn.parentsUntil(".ats-list-item").parent().remove();
                            });
                        }
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleUpdateStatus();
        }
    };
}();

jQuery(document).ready(function () {
    DispatchStatus.init();
});

var populateModelsInBatteryTypeSelection = function populateModelsInBatteryTypeSelection(models) {
    $('.ats-battery-types').find('option:not(:first)').remove().end();
    $.each(models, function (key, value) {
        $('.ats-battery-types').append($("<option></option>").attr("value", value.id).attr("data-max", value.replacement_to_distributor_count).text(value.text));
    });
};
var DispatchOrder = function () {
    var initBatteryTypeSelection = function initBatteryTypeSelection() {
        $('.ats-battery-types').select2({
            placeholder: "Select Battery Type"
        }).on('change', function () {
            $form = $(this).closest('form');
            $field = $form.find('[name="replacement_battery_count"]');
            if ($(this).valid()) {
                var max = $('.ats-battery-types option[value!=""]:selected').attr('data-max');
                if (max == 0) {
                    $field.val('').removeAttr("max").attr("disabled", true);
                    var validator = $form.validate();
                    if (validator.settings.unhighlight) {
                        validator.settings.unhighlight.call(validator, $field[0], validator.settings.errorClass, validator.settings.validClass);
                    }
                    validator.hideThese(validator.errorsFor($field[0]));
                    $('span.m-form__help').empty();
                } else {
                    $field.attr("max", max).val('').attr("disabled", false);
                    $('span.m-form__help').html("Max count: " + max);
                }
            } else {
                $field.val('').removeAttr("max").attr("disabled", true);
                var validator = $form.validate();
                if (validator.settings.unhighlight) {
                    validator.settings.unhighlight.call(validator, $field[0], validator.settings.errorClass, validator.settings.validClass);
                }
                validator.hideThese(validator.errorsFor($field[0]));
                $('span.m-form__help').empty();
            }
        });
    };
    var initDistributorDispatchStep2 = function initDistributorDispatchStep2() {
        $('#addOrderForm').validate({
            rules: {
                battery_type_id: {
                    required: true
                },
                regular_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                },
                foc_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                },
                replacement_battery_count: {
                    checkForWhiteSpace: true,
                    digits: true,
                    require_from_group: [1, '.batteries-count'],
                    greater_than_number: [0, '.batteries-count']
                }
            },
            messages: {
                battery_type_id: {
                    required: "Please select battery type."
                },
                regular_battery_count: {
                    digits: "No. of regular battery must contains only digits.",
                    require_from_group: "One of these must be required."
                },
                foc_battery_count: {
                    digits: "No. of foc battery must contains only digits.",
                    require_from_group: "One of these must be required."
                },
                replacement_battery_count: {
                    digits: "No. of replaced battery must contains only digits.",
                    require_from_group: "One of these must be required."
                }
            }
        });
    };
    var handleDistributorDispatchStep2 = function handleDistributorDispatchStep2() {
        $('.dispatch-back-btn').click(function () {
            var form = $('.dispatch-back-btn').closest('form');
            // $('#battery_type_id option:selected').remove();
            $("#battery_type_id").val('').trigger("change");
            $('#replacement_battery_form_group').removeClass('has-danger');
            $('span.m-form__help').html(" ");
            DDPApp.resetForm(form);
        });

        $(document).on('click', "#addDistributorDispatchWizardStep2Btn", function () {
            var btn = $(this);
            var form = $(this).closest('form');
            if (form.attr("data-action") == null) {
                return;
            }
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr('data-action'), form.serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    $("#dispatch_order_table tbody .no-data-block").hide();
                    $("#dispatch_order_table tbody").prepend(data.row);
                    $('#battery_type_id option:selected').remove();
                    $("#battery_type_id").val('').trigger("change");
                    DDPApp.resetForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initBatteryTypeSelection();
            initDistributorDispatchStep2();
            handleDistributorDispatchStep2();
        }
    };
}();
jQuery(document).ready(function () {
    DispatchOrder.init();
});

var ManageDispatchOrderBattery = function () {
    var initDispatchOrderBattery = function initDispatchOrderBattery() {
        $('.dispatch-battery-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleDispatchOrderBatteryRedirect();
        });
        $("#battery_search_btn").click(function () {
            handleDispatchOrderBatteryRedirect();
        });

        $("#battery_search_refresh_btn").click(function () {
            $("#battery_search_text").val('');
            handleDispatchOrderBatteryRedirect();
        });
    };
    var handleDispatchOrderBatteryRedirect = function handleDispatchOrderBatteryRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#battery_search_text").val().trim() != '') {
            route += '&search=' + $("#battery_search_text").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    return {
        init: function init() {
            initDispatchOrderBattery();
        }
    };
}();
jQuery(document).ready(function () {
    ManageDispatchOrderBattery.init();
});

var DestroyDispatchOrder = function () {
    var handleDestroyDispatchOrder = function handleDestroyDispatchOrder() {
        $(document).on('click', ".ats-destroy-dispatch-order-btn", function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButtonWithLoading(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    if ($("#dispatch_order_table tbody tr:not(.no-data-block)").length == 1) {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                            $("#dispatch_order_table tbody .no-data-block").show();
                        });
                    } else {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                        });
                    }
                    populateModelsInBatteryTypeSelection(data.models);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyDispatchOrder();
        }
    };
}();
jQuery(document).ready(function () {
    DestroyDispatchOrder.init();
});

var DispatchBatteries = function () {
    var initDistributorDispatchStep3 = function initDistributorDispatchStep3() {
        $('#addDispatchBatteryForm').validate({
            rules: {
                serial_key: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    minlength: 13,
                    maxlength: 13,
                    valid_serial_key: true
                }
            },
            messages: {
                serial_key: {
                    required: "Serial key is required.",
                    digits: "Serial key must contains only digits.",
                    minlength: "Serial key must be 13 digits long.",
                    maxlength: "Serial key must not long than 13 digits."
                }
            }
        });
    };
    var handleDistributorDispatchStep3 = function handleDistributorDispatchStep3() {
        $(document).on('click', "#addDispatchBatteryBtn", function () {
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post($('#addDispatchBatteryForm').attr('data-action'), $('#addDispatchBatteryForm').serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    $(".ats-list-group .battery-no-data-block").hide();
                    $(".ats-list-group").prepend(data.row);
                    $("#serial_key").val('').trigger("change");
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var handleDistributorDispatchRedirection = function handleDistributorDispatchRedirection() {
        $(document).on('click', ".dispatch-save-btn", function () {
            var referrer = document.referrer;
            if (!referrer) {
                window.location.href = $(this).attr("data-redirect-url");
            } else {
                window.location.href = referrer;
            }
        });
    };
    return {
        init: function init() {
            initDistributorDispatchStep3();
            handleDistributorDispatchStep3();
            handleDistributorDispatchRedirection();
        }
    };
}();
jQuery(document).ready(function () {
    DispatchBatteries.init();
});

var old_val = null;
var EditDispatchBatteryType = function () {
    var intializeDispatchBatteryType = function intializeDispatchBatteryType() {
        $('.ats-dispatch-battery-type-edit').select2().enable(false);
        $('.ats-dispatch-battery-type-edit').select2({
            minimumResultsForSearch: -1
        }).on('select2:selecting', function () {
            old_val = $(this).val();
        }).change(function () {
            updateDispatchTypeOfBattery($(this));
        });
        $(".ats-enable-battery-dispatch-type-edit").click(function () {
            $(this).closest("tr").find(".ats-dispatch-battery-type-edit").select2().enable(true);
        });
    };
    var updateDispatchTypeOfBattery = function updateDispatchTypeOfBattery(field) {
        $.post(field.attr('data-action'), {
            _method: 'PUT',
            dispatch_type: field.val()
        }).done(function (data) {
            DDPApp.displayResultForFailureWithCallback(data, function () {
                field.select2().enable(false);
                old_val = null;
            }, function () {
                if (old_val != null) {
                    field.val(old_val);
                }
                field.select2().enable(false);
                old_val = null;
            });
        }).fail(function (data) {
            DDPApp.displayFailedValidation(data);
            if (old_val != null) {
                field.val(old_val);
            }
            field.select2().enable(false);
            old_val = null;
        });
    };
    return {
        init: function init() {
            intializeDispatchBatteryType();
        }
    };
}();

jQuery(document).ready(function () {
    EditDispatchBatteryType.init();
});

var DestroyDispatchBattery = function () {
    var initDestroyBattery = function initDestroyBattery() {
        $(document).on('change', 'input[name="dispatched_battery_selection"]', function (e) {
            if ($("input[name='dispatched_battery_selection']:checked").length != 0) {
                $('#multiple_dispatch_battery_btn').attr('disabled', false);
            } else {
                $('#multiple_dispatch_battery_btn').attr('disabled', true);
            }
        });
    };
    var handleDestroyDispatchBattery = function handleDestroyDispatchBattery() {
        $(document).on('click', '.ats-destroy-dispatch-battery-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var currentElement = btn.parentsUntil(".ats-list-item").parent().find('input[name=dispatched_battery_selection]');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                confirmButtonClass: 'btn btn-brand',
                //                confirmButtonColor: '#223965',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    DDPApp.disableButtonWithLoading(btn);
                    $.delete(btn.attr("data-action")).done(function (data) {
                        if (data.result) {
                            swal(data.title, data.message, 'success').then(function (result) {
                                if ($(document).find(".ats-list-item").length == 1) {
                                    btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                        btn.parentsUntil(".ats-list-item").parent().remove();
                                        $(".battery-no-data-block").show();
                                    });
                                } else {
                                    btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                        btn.parentsUntil(".ats-list-item").parent().remove();
                                    });
                                }
                            });
                        }
                        if ($("input[name='dispatched_battery_selection']:checked").not(currentElement).length >= 1) {
                            $('#multiple_dispatch_battery_btn').attr('disabled', false);
                        } else {
                            $('#multiple_dispatch_battery_btn').attr('disabled', true);
                        }
                    }).fail(function (data) {
                        DDPApp.displayFailedValidation(data);
                        DDPApp.enableButton(btn);
                        if ($("input[name='dispatched_battery_selection']:checked").length >= 1) {
                            $('#multiple_dispatch_battery_btn').attr('disabled', false);
                        } else {
                            $('#multiple_dispatch_battery_btn').attr('disabled', true);
                        }
                    });
                }
            });
        });
    };
    var handleDestroyDispatchBatteries = function handleDestroyDispatchBatteries() {
        $('#multiple_dispatch_battery_btn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    DDPApp.disableButtonWithLoading(btn);
                    $.delete(btn.attr("data-action"), {
                        batteries: $('[name=dispatched_battery_selection]:checked').map(function () {
                            return this.value;
                        }).get().join(',')
                    }).done(function (data) {
                        DDPApp.enableButton(btn);
                        if (data.result) {
                            swal(data.title, data.message, 'success').then(function (result) {
                                $("[name=dispatched_battery_selection]:checked").each(function () {
                                    $(this).parentsUntil(".ats-list-item").parent().remove();
                                });
                                if ($(document).find(".ats-list-item").length == 0) {
                                    $(".battery-no-data-block").show();
                                }
                                btn.attr('disabled', true);
                            });
                        }
                    }).fail(function (data) {
                        DDPApp.displayFailedValidation(data);
                        DDPApp.enableButton(btn);
                    });
                }
            });
        });
    };
    return {
        init: function init() {
            initDestroyBattery();
            handleDestroyDispatchBattery();
            handleDestroyDispatchBatteries();
        }
    };
}();
jQuery(document).ready(function () {
    DestroyDispatchBattery.init();
});

var ManageWarrantyRejections = function () {
    var initWarrantyRejectionManage = function initWarrantyRejectionManage() {
        $('.warranty-rejection-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleWarrantyRejectionRedirect();
        });
        $("#warranty_rejection_search_btn").click(function () {
            handleWarrantyRejectionRedirect();
        });
        $("#warranty_rejection_search_refresh_btn").click(function () {
            $("#warranty_rejection_search_txt").val('');
            handleWarrantyRejectionRedirect();
        });
    };
    var handleWarrantyRejectionRedirect = function handleWarrantyRejectionRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#warranty_rejection_records").val();
        if ($("#warranty_rejection_search_txt").val().trim() != '') {
            route += '&search=' + $("#warranty_rejection_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    return {
        init: function init() {
            initWarrantyRejectionManage();
        }
    };
}();
jQuery(document).ready(function () {
    ManageWarrantyRejections.init();
});

var DispatchToOem = function () {
    var wizard;
    var initOemWizard = function initOemWizard() {
        var wizardEl = $('#oem_dispatch_wizard');
        wizard = wizardEl.mWizard({
            startStep: 1,
            manualStepForward: true
        });
        wizard.on('beforeNext', function (wizard) {
            if (wizard.getStep() == 1) {
                $('.ats-dispatch-type').select2({
                    placeholder: "Select Dispatch Type"
                }).change(function () {
                    $(this).valid();
                }).val('regular').trigger('change.select2');
                if ($("#addOemDispatchBatteryForm").attr("data-action") == null || $("#addOemDispatchBatteryForm").attr("data-action") == '') {
                    return false;
                }
            }
        });
    };
    var initOemDispatchStep1 = function initOemDispatchStep1() {
        $('.ats-dispatch-date').datepicker({
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            setDate: 'now',
            enableOnReadonly: false
        });

        $('#oem_vendor_id').select2({
            placeholder: "Select OEM Vendor"
        }).change(function () {
            $(this).valid();
        });

        $("#addOemDispatchWizardStep1Form").validate({
            rules: {
                oem_vendor_id: {
                    required: true
                },
                dispatch_date: {
                    required: true
                },
                oem_order_no: {
                    required: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                oem_vendor_id: {
                    required: "OEM vendor name is required."
                },
                dispatch_date: {
                    required: "Battery dispatch date is required."
                },
                oem_order_no: {
                    required: "OEM Dispatch order no. is required."
                }
            }
        });
    };
    var handleOemDispatchStep1 = function handleOemDispatchStep1() {
        $(document).on('click', '#addOemDispatchWizardStep1Btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    urlToOemDispatchForm(data);
                    DDPApp.enableButton(btn);
                    wizard.goNext();
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var urlToOemDispatchForm = function urlToOemDispatchForm(data) {
        if (data.update_dispatch_route) {
            $("#addOemDispatchWizardStep1Form").attr("data-action", data.update_dispatch_route);
        }
        $("#addOemDispatchWizardStep1Form").append(data.method);
        $("#addOemDispatchBatteryForm").attr('data-action', data.add_batteries_route);
        $("#multiple_oem_dispatch_battery_btn").attr('data-action', data.delete_batteries_route);
        if (data.update_status_route) {
            $(".ats-update-oem-dispatch-status-btn").attr('data-action', data.update_status_route);
        }
    };
    return {
        init: function init() {
            initOemWizard();
            initOemDispatchStep1();
            handleOemDispatchStep1();
        }
    };
}();
jQuery(document).ready(function () {
    DispatchToOem.init();
});

var ManageOemBatteryDispatches = function () {
    var start = moment().subtract(29, 'days');
    var end = moment();
    if ($('#manage_dispatch_to_oem_order_date_filter .form-control').attr("data-start-date")) {
        var _$$attr$split9 = $('#manage_dispatch_to_oem_order_date_filter .form-control').attr("data-start-date").split("/"),
            _$$attr$split10 = _slicedToArray(_$$attr$split9, 3),
            day = _$$attr$split10[0],
            month = _$$attr$split10[1],
            year = _$$attr$split10[2];

        var f = new Date(year, month - 1, day);
        start = moment(f);
    }
    if ($('#manage_dispatch_to_oem_order_date_filter .form-control').attr("data-end-date")) {
        var _$$attr$split11 = $('#manage_dispatch_to_oem_order_date_filter .form-control').attr("data-end-date").split("/"),
            _$$attr$split12 = _slicedToArray(_$$attr$split11, 3),
            _day3 = _$$attr$split12[0],
            _month3 = _$$attr$split12[1],
            _year3 = _$$attr$split12[2];

        var f = new Date(_year3, _month3 - 1, _day3);
        end = moment(f);
    }
    var initBatteryDispatchToOemManage = function initBatteryDispatchToOemManage() {
        $('#manage_dispatch_order_search_by_oem').select2({
            placeholder: "Filter by OEM Vendors",
            closeOnSelect: true,
            allowClear: true,
            maximumSelectionLength: 10,
            language: {
                maximumSelected: function maximumSelected(e) {
                    var t = "You can only select " + e.maximum + " OEM vendors";
                    return t;
                }
            }
        });
        $('#manage_dispatch_to_oem_order_date_filter').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-brand',
            cancelClass: 'btn-danger',
            startDate: start,
            endDate: end,
            locale: {
                cancelLabel: 'Cancel',
                format: 'DD/MM/YYYY'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function (start, end, label) {
            $('#manage_dispatch_to_oem_order_date_filter .form-control').val(moment(start).format('DD/MM/YYYY') + ' - ' + moment(end).format('DD/MM/YYYY'));
        });
        $("#manage_dispatch_to_oem_order_date_filter").on('apply.daterangepicker', function (ev, picker) {
            start = picker.startDate;
            end = picker.endDate;
        });
        $("#manage_dispatch_to_oem_order_date_filter").on('cancel.daterangepicker', function (ev, picker) {
            $('#manage_dispatch_to_oem_order_date_filter .form-control').val('');
            start = '';
            end = '';
        });
        $('.oem-dispatch-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleBatteryDispatchToOemRedirect();
        });
        $("#batteryDispatchToOemSearchBtn").click(function () {
            handleBatteryDispatchToOemRedirect();
        });
        $("#batteryDispatchToOemSearchRefreshBtn").click(function () {
            $("#manage_dispatch_order_search_by_oem").val('').trigger('change');
            $("#batteryDispatchToOemSearchText").val('').trigger('change');
            $("#manage_dispatch_to_oem_order_date_filter .form-control").val('');
            handleBatteryDispatchToOemRedirect();
        });
    };
    var handleBatteryDispatchToOemRedirect = function handleBatteryDispatchToOemRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".oem-dispatches-tabview").attr("data-status");
        if ($("#batteryDispatchToOemSearchText").val().trim() != '') {
            route += '&search=' + $("#batteryDispatchToOemSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        if ($("#manage_dispatch_order_search_by_oem").length != 0 && $("#manage_dispatch_order_search_by_oem").val() != '') {
            route += '&oem_filter=' + $("#manage_dispatch_order_search_by_oem").val();
        }
        if ($("#manage_dispatch_to_oem_order_date_filter .form-control").val() != '') {
            route += '&from=' + moment(start).format('DD/MM/YYYY') + '&to=' + moment(end).format('DD/MM/YYYY');
        }
        window.location.href = route;
    };

    return {
        init: function init() {
            initBatteryDispatchToOemManage();
        }
    };
}();
jQuery(document).ready(function () {
    ManageOemBatteryDispatches.init();
});

var DestroyOemDispatch = function () {

    var handleDestroyOemDispatch = function handleDestroyOemDispatch() {
        $(document).on('click', '.ats-destroy-oem-dispatch-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButtonWithLoading(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find(".ats-list-item").length == 1) {
                        window.location.href = window.location.href;
                    } else {
                        btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                            btn.parentsUntil(".ats-list-item").parent().remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyOemDispatch();
        }
    };
}();
jQuery(document).ready(function () {
    DestroyOemDispatch.init();
});

var OemDispatchStatus = function () {
    var handleUpdateOemStatus = function handleUpdateOemStatus() {
        $(document).on('click', '.ats-update-oem-dispatch-status-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButtonWithLoading(btn);
            $.post(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($(document).find("#oem_dispatch_content").length == 0) {
                        window.location.href = btn.attr("data-redirect-url");
                    } else {
                        if ($(document).find(".ats-list-item").length == 1) {
                            window.location.href = window.location.href;
                        } else {
                            btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                btn.parentsUntil(".ats-list-item").parent().remove();
                            });
                        }
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleUpdateOemStatus();
        }
    };
}();

jQuery(document).ready(function () {
    OemDispatchStatus.init();
});

var ManageOemDispatchOrderBattery = function () {
    var initOemDispatchOrderBattery = function initOemDispatchOrderBattery() {
        $('.dispatch-oem-battery-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleOemDispatchOrderBatteryRedirect();
        });
        $("#oem_battery_search_btn").click(function () {
            handleOemDispatchOrderBatteryRedirect();
        });

        $("#oem_battery_search_refresh_btn").click(function () {
            $("#oem_battery_search_text").val('');
            handleOemDispatchOrderBatteryRedirect();
        });
    };
    var handleOemDispatchOrderBatteryRedirect = function handleOemDispatchOrderBatteryRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#oem_battery_search_text").val().trim() != '') {
            route += '&search=' + $("#oem_battery_search_text").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    return {
        init: function init() {
            initOemDispatchOrderBattery();
        }
    };
}();
jQuery(document).ready(function () {
    ManageOemDispatchOrderBattery.init();
});

var OemDispatchBatteries = function () {
    var initOemDispatchStep2 = function initOemDispatchStep2() {
        $('#addOemDispatchBatteryForm').validate({
            rules: {
                serial_key: {
                    required: true,
                    digits: true,
                    rangelength: [12, 14],
                    checkForWhiteSpace: true
                }
            },
            messages: {
                serial_key: {
                    required: "Serial key is required.",
                    digits: "Enter valid serial key.",
                    rangelength: "Serial key must be between 12 to 14 digits."
                }
            }
        });
    };
    var handleOemDispatchStep2 = function handleOemDispatchStep2() {
        $(document).on('click', "#addOemDispatchBatteryBtn", function () {
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post($('#addOemDispatchBatteryForm').attr('data-action'), $('#addOemDispatchBatteryForm').serialize()).done(function (data) {
                DDPApp.displayResultForFailureWithCallback(data, function () {
                    $(".ats-list-group .no-data-block").hide();
                    $(".ats-list-group").prepend(data.row);
                    $("#serial_key").val('').trigger("change");
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var handleOemDispatchRedirection = function handleOemDispatchRedirection() {
        $(document).on('click', "#oem-dispatch-save-btn", function () {
            var referrer = document.referrer;
            if (!referrer) {
                window.location.href = $(this).attr("data-redirect-url");
            } else {
                window.location.href = referrer;
            }
        });
    };
    return {
        init: function init() {
            initOemDispatchStep2();
            handleOemDispatchStep2();
            handleOemDispatchRedirection();
        }
    };
}();
jQuery(document).ready(function () {
    OemDispatchBatteries.init();
});

var old_val = null;
var EditOemDispatchBatteryType = function () {
    var intializeOemDispatchBatteryType = function intializeOemDispatchBatteryType() {
        $('.ats-dispatch-to-oem-battery-type-edit').select2().enable(false);
        $('.ats-dispatch-to-oem-battery-type-edit').select2({
            minimumResultsForSearch: -1
        }).on('select2:selecting', function () {
            old_val = $(this).val();
        }).change(function () {
            updateOemDispatchTypeOfBattery($(this));
        });
        $(".ats-enable-oem-battery-dispatch-type-edit").click(function () {
            $(this).closest("tr").find(".ats-dispatch-to-oem-battery-type-edit").select2().enable(true);
        });
    };
    var updateOemDispatchTypeOfBattery = function updateOemDispatchTypeOfBattery(field) {
        $.post(field.attr('data-action'), {
            _method: 'PUT',
            dispatch_type: field.val()
        }).done(function (data) {
            DDPApp.displayResultForFailureWithCallback(data, function () {
                field.select2().enable(false);
                old_val = null;
            }, function () {
                if (old_val != null) {
                    field.val(old_val);
                }
                field.select2().enable(false);
                old_val = null;
            });
        }).fail(function (data) {
            DDPApp.displayFailedValidation(data);
            if (old_val != null) {
                field.val(old_val);
            }
            field.select2().enable(false);
            old_val = null;
        });
    };
    return {
        init: function init() {
            intializeOemDispatchBatteryType();
        }
    };
}();

jQuery(document).ready(function () {
    EditOemDispatchBatteryType.init();
});

var DestroyOemDispatchBattery = function () {
    var initDestroyOemBattery = function initDestroyOemBattery() {
        $(document).on('change', 'input[name="oem_dispatched_battery_selection"]', function (e) {
            if ($("input[name='oem_dispatched_battery_selection']:checked").length != 0) {
                $('#multiple_oem_dispatch_battery_btn').attr('disabled', false);
            } else {
                $('#multiple_oem_dispatch_battery_btn').attr('disabled', true);
            }
        });
    };
    var handleDestroyOemDispatchBattery = function handleDestroyOemDispatchBattery() {
        $(document).on('click', '.ats-destroy-oem-dispatch-battery-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                confirmButtonClass: 'btn btn-brand',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    btn.parentsUntil(".ats-list-item").find("input[name='oem_dispatched_battery_selection']").trigger("click");
                    DDPApp.disableButtonWithLoading(btn);
                    $.delete(btn.attr("data-action")).done(function (data) {
                        if (data.result) {
                            swal(data.title, data.message, 'success').then(function (result) {
                                if ($(document).find(".ats-list-item").length == 1) {
                                    btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                        btn.parentsUntil(".ats-list-item").parent().remove();
                                        $(".no-data-block").show();
                                    });
                                } else {
                                    btn.parentsUntil(".ats-list-item").parent().fadeOut(500).promise().done(function () {
                                        btn.parentsUntil(".ats-list-item").parent().remove();
                                    });
                                }
                            });
                        }
                    }).fail(function (data) {
                        DDPApp.displayFailedValidation(data);
                        DDPApp.enableButton(btn);
                    });
                }
            });
        });
    };
    var handleDestroyOemDispatchBatteries = function handleDestroyOemDispatchBatteries() {
        $('#multiple_oem_dispatch_battery_btn').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            swal({
                title: 'Are you sure remove the battery?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    DDPApp.disableButtonWithLoading(btn);
                    $.delete(btn.attr("data-action"), {
                        batteries: $('[name=oem_dispatched_battery_selection]:checked').map(function () {
                            return this.value;
                        }).get().join(',')
                    }).done(function (data) {
                        DDPApp.enableButton(btn);
                        if (data.result) {
                            swal(data.title, data.message, 'success').then(function (result) {
                                $("[name=oem_dispatched_battery_selection]:checked").each(function () {
                                    $(this).parentsUntil(".ats-list-item").parent().remove();
                                });
                                if ($(document).find(".ats-list-item").length == 0) {
                                    $(".no-data-block").show();
                                }
                                btn.attr('disabled', true);
                            });
                        }
                    }).fail(function (data) {
                        DDPApp.displayFailedValidation(data);
                        DDPApp.enableButton(btn);
                    });
                }
            });
        });
    };
    return {
        init: function init() {
            initDestroyOemBattery();
            handleDestroyOemDispatchBattery();
            handleDestroyOemDispatchBatteries();
        }
    };
}();
jQuery(document).ready(function () {
    DestroyOemDispatchBattery.init();
});

var VehicleCreate = function () {

    var intializeElements = function intializeElements() {
        $('#vehicle_type').select2({
            placeholder: "Select Vehicle Type"
        }).change(function () {
            $(this).valid();
        });

        $('select[name=company_id]').select2({
            placeholder: "Select Vehicle Company"
        }).change(function () {
            $(this).valid();
        });
    };

    var validateVehicleForm = function validateVehicleForm() {
        $("#createVehicleForm").validate({
            rules: {
                vehicle_model: {
                    required: true,
                    checkForWhiteSpace: true
                },
                vehicle_type: {
                    required: true
                },
                company_id: {
                    required: true
                }
            },
            messages: {
                vehicle_model: {
                    required: "Vehicle model is required."
                },
                vehicle_type: {
                    required: "Vehicle type is required."
                },
                company_id: {
                    required: "Vehicle company is required."
                }
            }
        });
    };

    var handleVehicleCreate = function handleVehicleCreate() {
        $("#createVehicleResetBtn").click(function () {
            resetVehicleForm($("#createVehicleForm"));
        });
        $("#createVehicleBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);

            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    resetVehicleForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    var resetVehicleForm = function resetVehicleForm(form) {
        $("#vehicle_type").val('').trigger("change");
        $('select[name=company_id]').val('').trigger("change");
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            intializeElements();
            validateVehicleForm();
            handleVehicleCreate();
        }
    };
}();
jQuery(document).ready(function () {
    VehicleCreate.init();
});

var ManageVehicles = function () {

    var initVehicleManage = function initVehicleManage() {

        $('.vehicle-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleVehicleRedirect();
        });

        $("#vehicleSearchBtn").click(function () {
            handleVehicleRedirect();
        });

        $("#vehicleSearchRefreshBtn").click(function () {
            $("#vehicleSearchText").val('');
            handleVehicleRedirect();
        });
    };

    var handleVehicleRedirect = function handleVehicleRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#vehicleSearchText").val().trim() != '') {
            route += '&search=' + $("#vehicleSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };

    return {
        init: function init() {
            initVehicleManage();
        }
    };
}();

jQuery(document).ready(function () {
    ManageVehicles.init();
});

var EditVehicle = function () {
    var intializeElements = function intializeElements() {
        $('#vehicle_type').select2({
            placeholder: "Select Vehicle Type"
        }).change(function () {
            $(this).valid();
        });

        $('select[name=company_id]').select2({
            placeholder: "Select Vehicle Company"
        }).change(function () {
            $(this).valid();
        });
    };

    var validateVehicleForm = function validateVehicleForm() {
        $("#updateVehicleForm").validate({
            rules: {
                vehicle_model: {
                    required: true,
                    checkForWhiteSpace: true
                },
                vehicle_type: {
                    required: true
                },
                company_id: {
                    required: true
                }
            },
            messages: {
                vehicle_model: {
                    required: "Vehicle model is required."
                },
                vehicle_type: {
                    required: "Vehicle type is required."
                },
                company_id: {
                    required: "Vehicle company is required."
                }
            }
        });
    };

    var handleVehicleEdit = function handleVehicleEdit() {
        $("#updateVehicleBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.put(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            intializeElements();
            validateVehicleForm();
            handleVehicleEdit();
        }
    };
}();
jQuery(document).ready(function () {
    EditVehicle.init();
});

var DestroyVehicle = function () {
    var handleDestroyVehicle = function handleDestroyVehicle() {
        $(document).on('click', '.ats-destroy-vehicle-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButton(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($("#vehicleTable tbody tr:not(.no-data-block)").length == 1) {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                            $("#vehicleTable tbody .no-data-block").show();
                        });
                    } else {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyVehicle();
        }
    };
}();

jQuery(document).ready(function () {
    DestroyVehicle.init();
});

var CreateVehicleFinder = function () {
    var intializeSelect2 = function intializeSelect2() {
        $('#company').select2({
            placeholder: "Select Vehicle Company"
        });
        $('#vehicle').select2({
            placeholder: "Select Vehicle"
        });

        $('#battery_type_id').select2({
            placeholder: "Select Battery Type"
        });
    };

    var validationCreateVehicleFinder = function validationCreateVehicleFinder() {
        $("#addVehicleFinderForm").validate({
            rules: {
                company: {
                    required: true
                },
                vehicle: {
                    required: true
                },
                battery_type_id: {
                    required: true
                }
            },
            messages: {
                company: {
                    required: "Vehicle company is required."
                },
                vehicle: {
                    required: "Vehicle is required."
                },
                battery_type_id: {
                    required: "Battery type is required."
                }
            }
        });
    };
    var handleCreateVehicleFinder = function handleCreateVehicleFinder() {
        $("#addVehicleFinderResetBtn").click(function () {
            resetVehicleFinderForm($("#addVehicleFinderForm"));
        });
        $("#addVehicleFinderBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    resetVehicleFinderForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var resetVehicleFinderForm = function resetVehicleFinderForm(form) {
        $("#company").val('').trigger("change.select2");
        $("#vehicle").val('').attr('disabled', 'disabled').trigger("change.select2");
        $("#vehicle_form_group").removeClass('has-danger');
        $("#battery_type_id").val('').attr('disabled', 'disabled').trigger("change.select2");
        $("#battery_type_form_group").removeClass('has-danger');
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    var loadVehicleData = function loadVehicleData() {
        var action = $('#company').attr('data-vehicles-load-action');
        $('#company').on('change', function () {
            if ($(this).val() != '' && $(this).val() != null) {
                // get request for loading vehicle data of a particular company
                $.get(action, { 'company': $(this).val() }).done(function (data) {
                    if (!$.isEmptyObject(data.vehicles)) {
                        $('#vehicle').html('');
                        $('#vehicle').append('<option value=""></option>');
                        $.each(data.vehicles, function (i, val) {
                            var id = val.id;
                            var vehicle = val.vehicle_model;
                            var type = val.vehicle_type;
                            $('#vehicle').append('<option value="' + id + '">' + vehicle + ' | ' + type + '</option>');
                        });
                        $('#vehicle').removeAttr('disabled').trigger('change.select2');
                    } else {
                        DDPApp.displayErrorMessage('No vehicles added for this company');
                        $("#vehicle").val('').attr('disabled', 'disabled').trigger("change.select2");
                        $("#battery_type_id").val('').attr('disabled', 'disabled').trigger("change.select2");
                    }
                });
            } else {
                $("#vehicle").val('').attr('disabled').trigger("change");
            }
        });
    };
    var loadBatteryData = function loadBatteryData() {
        $('#vehicle').on('change', function () {
            if ($(this).val() != '' && $(this).val() != null) {
                $('#battery_type_id').removeAttr('disabled').trigger('change.select2');
            } else {
                $("#battery_type_id").val('').attr('disabled').trigger("change");
            }
        });
    };
    return {
        init: function init() {
            intializeSelect2();
            validationCreateVehicleFinder();
            handleCreateVehicleFinder();
            loadVehicleData();
            loadBatteryData();
        }
    };
}();
jQuery(document).ready(function () {
    CreateVehicleFinder.init();
});

var ManageVehicleFinders = function () {

    var initVehicleFinderManage = function initVehicleFinderManage() {

        $('.vehicle-finder-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleVehicleFinderRedirect();
        });

        $("#vehicleFinderSearchBtn").click(function () {
            handleVehicleFinderRedirect();
        });

        $("#vehicleFinderSearchRefreshBtn").click(function () {
            $("#vehicleFinderSearchText").val('');
            handleVehicleFinderRedirect();
        });
    };

    var handleVehicleFinderRedirect = function handleVehicleFinderRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#vehicleFinderSearchText").val().trim() != '') {
            route += '&search=' + $("#vehicleFinderSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };

    return {
        init: function init() {
            initVehicleFinderManage();
        }
    };
}();

jQuery(document).ready(function () {
    ManageVehicleFinders.init();
});

var EditVehicleFinder = function () {
    var intializeEditSelect2 = function intializeEditSelect2() {
        $('#company').select2({
            placeholder: "Select Vehicle Company"
        }).change(function () {
            $(this).valid();
        });

        $('#vehicle').select2({
            placeholder: "Select Vehicle"
        }).change(function () {
            $(this).valid();
        });

        $('#battery_type_id').select2({
            placeholder: "Select Battery Type"
        }).change(function () {
            $(this).valid();
        });
    };
    var validationEditVehicleFinder = function validationEditVehicleFinder() {
        $("#updateVehicleFinderForm").validate({
            rules: {
                company: {
                    required: true
                },
                vehicle: {
                    required: true
                },
                battery_type_id: {
                    required: true
                }
            },
            messages: {
                company: {
                    required: "Vehicle company is required."
                },
                vehicle: {
                    required: "Vehicle is required."
                },
                battery_type_id: {
                    required: "Battery type is required."
                }
            }
        });
    };
    var handleEditVehicleFinder = function handleEditVehicleFinder() {
        $("#updateVehicleFinderBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.put(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            intializeEditSelect2();
            validationEditVehicleFinder();
            handleEditVehicleFinder();
        }
    };
}();
jQuery(document).ready(function () {
    EditVehicleFinder.init();
});

var DestroyVehicleFinder = function () {
    var handleDestroyVehicleFinder = function handleDestroyVehicleFinder() {
        $(document).on('click', '.ats-destroy-vehicle-finder-btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            DDPApp.disableButton(btn);
            $.delete(btn.attr("data-action")).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    if ($("#batteryFinderTable tbody tr:not(.no-data-block)").length == 1) {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                            $("#batteryFinderTable tbody .no-data-block").show();
                        });
                    } else {
                        btn.closest('tr').fadeOut(1000).promise().done(function () {
                            btn.closest("tr").remove();
                        });
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            handleDestroyVehicleFinder();
        }
    };
}();

jQuery(document).ready(function () {
    DestroyVehicleFinder.init();
});

var BatteryFinder = function () {
    var GetAllBrand = function GetAllBrand() {
        var checkVehicleType = $("input[type=radio][name='vehicle_type']:checked").attr('data-action');
        if (checkVehicleType !== "") {
            getBrandData(checkVehicleType);
        }
        $('input[type=radio][name=vehicle_type]').change(function () {
            var checkVehicleType = $("input[type=radio][name='vehicle_type']:checked").attr('data-action');
            if (checkVehicleType !== '') {
                $('#model_id').html('');
                $('#brand_id').html('');
                $("#checkBatteryFinderCardContainer").html('');
                getBrandData(checkVehicleType);
            }
        });
    };

    var getBrandData = function getBrandData(url) {
        $.ajax({
            url: url,
            type: "GET"
        }).done(function (data) {
            if (data.result) {
                $("#brand_id option[value!='']").remove();
                if (data.brands.length !== 0) {
                    $('#model_id').select2({
                        placeholder: "Select Vehicle Model"
                    }).change(function () {
                        $(this).valid();
                    });
                    $('#brand_id').select2({
                        placeholder: "Select Vehicle Company"
                    }).change(function () {
                        $(this).valid();
                    });

                    $('#brand_id').html('');
                    $('#brand_id').append('<option value=""></option>');
                    $.each(data.brands, function (key, company) {
                        $('#brand_id').append('<option value="' + company['brand_id_token'] + '">' + company['brand_name'] + '</option>');
                    });
                } else {
                    $('#model_id').select2({
                        placeholder: "No Vehicle Models Found"
                    }).change(function () {
                        $(this).valid();
                    });
                    $('#brand_id').select2({
                        placeholder: "No Vehicle Company Found"
                    }).change(function () {
                        $(this).valid();
                    });
                }
            }
        }).fail(function (data) {
            DDPApp.displayFailedValidation(data);
            DDPApp.enableButton(btn);
        });
    };

    var getVehicleModelData = function getVehicleModelData() {
        $("#brand_id").on("change", function (e) {
            $('#model_id').html("").append('<option disabled selected>Select Vehicle Model</option>');
            var checkVehicleType = $("input[name='vehicle_type']:checked").val();
            var theCompanyID = $('#brand_id').select2("val");
            $.ajax({
                url: "/api/ddp/v1/find-my-battery/get-all-model/" + checkVehicleType + "/" + theCompanyID,
                type: "GET"
            }).done(function (data) {
                if (data.result) {
                    $("#model_id option[value!='']").remove();
                    if (data.models.length !== 0) {
                        $('#model_id').html('');
                        $('#model_id').append('<option value=""></option>');
                        $.each(data.models, function (key, company) {
                            $('#model_id').append('<option value="' + company['vehicle_model_id_token'] + '">' + company['vehicle_model_name'] + '</option>');
                        });
                    } else {
                        DDPApp.displayErrorMessage('Unable to load vehicle data');
                    }
                }
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    var checkBatteryFinderValidation = function checkBatteryFinderValidation() {

        $("#checkBatteryFinderForm").validate({
            rules: {
                company: {
                    required: true
                },
                vehicle: {
                    required: true
                }
            },
            messages: {
                company: {
                    required: "Vehicle company is required."
                },
                vehicle: {
                    required: "Vehicle model is required."
                }
            }
        });
    };

    var handleBatteryFinder = function handleBatteryFinder() {
        $(document).on('click', '#addBatteryFinderBtn', function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var theBrandID = $('#brand_id').select2("val");
            var theModelID = $('#model_id').select2("val");

            $.ajax({
                url: "/api/ddp/v1/find-my-battery/get-all-battery/" + theBrandID + "/" + theModelID,
                type: "get"
            }).done(function (data) {
                if (data.result == true) {
                    if (data.batteries.length > 0) {
                        setTimeout(function () {
                            DDPApp.enableButton(btn);
                        }, 500);
                        showBatteryFinderCard(data);
                    } else {
                        var data = "No battery found.";
                        var img = $('#no_img_url').val();
                        DDPApp.enableButton(btn);
                        showNoBatteryAlert(img);
                    }
                }
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    var showBatteryFinderCard = function showBatteryFinderCard(data) {
        $("#checkBatteryFinderCardContainer").html('');
        $.each(data.batteries, function (key, battery) {
            $("#checkBatteryFinderCardContainer").append('<div class="col-md-6 col-sm-6 col-12">' + '<div>' + '<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force" data-placement="top">' + '<div class="m-portlet__head m-portlet__head--fit" style="padding: 0 1.1rem;background-color: transparent;">' + '<div class="m-portlet__head-caption">' + '<div class="m-portlet__head-action">' + '</div>' + '</div>' + '</div>' + '<div class="m-portlet__body" style="padding-bottom: 10px;">' + '<div class="m-widget19">' + '<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">' + '<img src="' + battery['battery_type_thumbnail'] + '" alt="Battery Photo" style="height: 200px;">' + '<h3 class="m-widget19__title m--font-light" style="padding-left: 1.1rem;padding-bottom: 0.5rem;">' + battery['battery_type'] + '</h3>' + '<div class="m-widget19__shadow" style="background: linear-gradient(to bottom, transparent 5%, rgba(0, 0, 0, 0.5) 40%, rgba(0, 0, 0, 0.6) 90%) no-repeat scroll 0 0;">' + '</div>' + '</div>' + '<div class="m-widget19__content" style="margin-bottom: 0px;padding:10px 0px 0px 0px;">' + '<div class="row">' + '<div class="col-6">' + '<div class="m-widget28__tab-item">' + '<small class="m--font-transform-u">Rated Capacity</small><br>' + (battery['battery_rated_capacity'] == null ? '<strong class="m--font-brand">N/A</strong>' : '<strong class="m--font-brand">' + battery['battery_rated_capacity'] + ' AH</strong>') + '</div>' + '</div>' + '<div class="col-6">' + '<div class="m-widget28__tab-item">' + '<small class="m--font-transform-u">Voltage</small><br>' + (battery['battery_voltage'] == null ? '<strong class="m--font-brand">N/A</strong>' : '<strong class="m--font-brand">' + battery['battery_voltage'] + ' V</strong>') + '</div>' + '</div>' + '</div>' + '<div class="row">' + '<div class="col-6">' + '<div class="m-widget28__tab-item">' + '<small class="m--font-transform-u">Warranty Period</small><br>' + '<strong class="m--font-brand">' + battery['warranty_period'] + '</strong>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>');
        });
    };

    var showNoBatteryAlert = function showNoBatteryAlert(img) {
        $("#checkBatteryFinderCardContainer").html('');
        $("#checkBatteryFinderCardContainer").append('<div class="col-md-6 col-sm-6 col-12">' + '<div>' + '<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force" data-placement="top">' + '<div class="m-portlet__head m-portlet__head--fit" style="padding: 0 1.1rem;background-color: transparent;">' + '<div class="m-portlet__head-caption">' + '<div class="m-portlet__head-action">' + '</div>' + '</div>' + '</div>' + '<div class="m-portlet__body" style="padding-bottom: 0px;">' + '<div class="m-widget19">' + '<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">' + '<img src="' + img + '" alt="Battery Photo" style="height: 200px;">' + '<h3 class="m-widget19__title m--font-light" style="padding-left: 1.1rem;padding-bottom: 0.5rem;">' + '<i class="fa fa-lg fa-fw fa-times-circle m--font-danger" style="font-size:1.3333em"></i>' + 'No Battery Found' + '</h3>' + '<div class="m-widget19__shadow" style="background: linear-gradient(to bottom, transparent 5%, rgba(0, 0, 0, 0.5) 40%, rgba(0, 0, 0, 0.6) 90%) no-repeat scroll 0 0;">' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>');
    };

    return {
        init: function init() {
            getVehicleModelData();
            checkBatteryFinderValidation();
            handleBatteryFinder();
            GetAllBrand();
        }
    };
}();

jQuery(document).ready(function () {
    BatteryFinder.init();
});

var Vendor = function () {

    var validateVendorForm = function validateVendorForm() {
        $("#createVendorForm").validate({
            rules: {
                vendor_code: {
                    required: true,
                    digits: true,
                    min: 2,
                    max: 99,
                    minlength: 2,
                    maxlength: 2,
                    checkForWhiteSpace: true
                },
                name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                vendor_email: {
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                vendor_contact_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                cc_emails: {
                    comma_separated_emails: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                vendor_code: {
                    required: "Vendor code is required.",
                    digits: "Vendor code must be in digit.",
                    min: "Enter vendor code greater than or equal to 02.",
                    max: "Enter vendor code less than or equal to 99.",
                    minlength: "Vendor code should contain 2 digits.",
                    maxlength: "Vendor code should contain 2 digits."
                },
                name: {
                    required: "Vendor name is required."
                },
                vendor_email: {
                    EMAIL: "Enter email address in proper format."
                },
                vendor_contact_number: {
                    required: "Contact number is required.",
                    digits: "Enter valid contact number.",
                    rangelength: "Contact number must be between 10 to 12 digits."
                }
            }
        });
    };

    var handleVendorCreate = function handleVendorCreate() {
        $("#createVendorResetBtn").click(function () {
            DDPApp.resetForm($("#createVendorForm"));
        });
        $("#createVendorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.resetForm(form);
                    if (data.vendorCount == 99 || data.vendorCount > 99) {
                        $('#vendorFooterContainer').remove();
                        $("#vendorMsgContainer").show();
                    }
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            validateVendorForm();
            handleVendorCreate();
        }
    };
}();

jQuery(document).ready(function () {
    Vendor.init();
});

var ManageVendors = function () {

    var initVendorManage = function initVendorManage() {

        $('.vendor-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleVendorRedirect();
        });

        $("#vendorSearchBtn").click(function () {
            handleVendorRedirect();
        });

        $("#vendorSearchRefreshBtn").click(function () {
            $("#vendorSearchText").val('');
            handleVendorRedirect();
        });

        $('#all_vendor_selection').click(function (event) {
            if (this.checked) {
                $(':checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                });
            }
        });
    };

    var handleVendorRedirect = function handleVendorRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#vendorSearchText").val().trim() != '') {
            route += '&search=' + $("#vendorSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };

    var handleVendorExport = function handleVendorExport() {
        $("#manage_vendor_export_btn").click(function () {
            if ($('input[name="vendor_selection"]:checked').length != 0) {
                var tokens = $('input[name="vendor_selection"]:checked').map(function () {
                    return this.value;
                }).get().join(',');
                window.location.href = $(this).attr("data-action") + '?exports=' + tokens;
            } else {
                window.location.href = $(this).attr("data-action");
            }
        });
    };

    return {
        init: function init() {
            initVendorManage();
            handleVendorExport();
        }
    };
}();

jQuery(document).ready(function () {
    ManageVendors.init();
});

var EditVendor = function () {
    var initVendorEdit = function initVendorEdit() {
        $("#updateVendorForm").validate({
            rules: {
                name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                vendor_email: {
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                vendor_contact_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                },
                cc_emails: {
                    comma_separated_emails: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                name: {
                    required: "Vendor name is required."
                },
                vendor_email: {
                    EMAIL: "Enter email address in proper format."
                },
                vendor_contact_number: {
                    required: "Contact number is required.",
                    digits: "Enter valid contact number.",
                    rangelength: "Contact number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleVendorEdit = function handleVendorEdit() {
        $("#updateVendorBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.put(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    return {
        init: function init() {
            initVendorEdit();
            handleVendorEdit();
        }
    };
}();

jQuery(document).ready(function () {
    EditVendor.init();
});

var ManageState = function () {
    var initStateManage = function initStateManage() {
        $('.state-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleStateRedirect();
        });
        $("#stateSearchBtn").click(function () {
            handleStateRedirect();
        });
        $("#stateSearchRefreshBtn").click(function () {
            $("#stateSearchText").val('');
            handleStateRedirect();
        });
    };
    var handleStateRedirect = function handleStateRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#stateSearchText").val().trim() != '') {
            route += '&search=' + $("#stateSearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    return {
        init: function init() {
            initStateManage();
        }
    };
}();
jQuery(document).ready(function () {
    ManageState.init();
});

var ManageCities = function () {
    var initCityManage = function initCityManage() {

        $('.city-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleCityRedirect();
        });

        $("#citySearchBtn").click(function () {
            handleCityRedirect();
        });

        $("#citySearchRefreshBtn").click(function () {
            $("#citySearchText").val('');
            handleCityRedirect();
        });
    };

    var handleCityRedirect = function handleCityRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        if ($("#citySearchText").val().trim() != '') {
            route += '&search=' + $("#citySearchText").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };

    return {
        init: function init() {
            initCityManage();
        }
    };
}();

jQuery(document).ready(function () {
    ManageCities.init();
});

var CreateCity = function () {

    var validationCreateCity = function validationCreateCity() {
        $("#addCityForm").validate({
            rules: {
                state: {
                    required: true
                },
                city: {
                    required: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                state: {
                    required: "State is required."
                },
                city: {
                    required: "City is required."
                }
            }
        });
    };
    var handleCreateCity = function handleCreateCity() {
        $("#addCityResetBtn").click(function () {
            resetCreateCityForm($("#addCityForm"));
        });
        $("#addCityBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    resetCreateCityForm(form);
                    DDPApp.enableButton(btn);
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };
    var resetCreateCityForm = function resetCreateCityForm(form) {
        $("#state").val('').trigger("change.select2");
        DDPApp.resetForm(form);
    };

    return {
        init: function init() {
            validationCreateCity();
            handleCreateCity();
        }
    };
}();
jQuery(document).ready(function () {
    CreateCity.init();
});

var EditCity = function () {
    var validationEditCity = function validationEditCity() {
        $("#updateCityForm").validate({
            rules: {
                state: {
                    required: true
                },
                city: {
                    required: true,
                    checkForWhiteSpace: true
                }
            },
            messages: {
                state: {
                    required: "State is required."
                },
                city: {
                    required: "City is required."
                }
            }
        });
    };
    var handleEditCity = function handleEditCity() {
        $("#updateCityBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            $.post(form.attr("data-action"), form.serialize()).done(function (data) {
                DDPApp.displayResultWithCallback(data, function () {
                    DDPApp.removeLoadingFromButton(btn);
                    var referrer = document.referrer;
                    if (!referrer) {
                        window.location.href = form.attr("data-redirect-url");
                    } else {
                        window.location.href = referrer;
                    }
                }, function () {
                    DDPApp.enableButton(btn);
                });
            }).fail(function (data) {
                DDPApp.displayFailedValidation(data);
                DDPApp.enableButton(btn);
            });
        });
    };

    return {
        init: function init() {
            validationEditCity();
            handleEditCity();
        }
    };
}();
jQuery(document).ready(function () {
    EditCity.init();
});

var GatekeeperCreate = function () {
    var gatekeeperCreateValidation = function gatekeeperCreateValidation() {
        $("#profile_photo").fileinput().on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#profile_photo').fileinput('destroy');
            $("#profile_photo").fileinput({
                showUpload: false,
                showRemove: true,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                minFileSize: 1,
                maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                msgPlaceholder: 'Select Profile Photo',
                browseLabel: 'Browse',
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                initialPreview: ["no image"],
                initialCaption: "Select Profile Photo"
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        });
        $("#profile_photo").trigger('fileclear');
    };
    var initGatekeeperCreate = function initGatekeeperCreate() {
        $("#createGatekeeperForm").validate({
            rules: {
                employee_id: {
                    checkForWhiteSpace: true
                },
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                password: {
                    required: true,
                    checkForWhiteSpace: true,
                    spaceNotAllowed: true,
                    rangelength: [8, 150]
                },
                password_confirmation: {
                    required: true,
                    equalTo: '#password',
                    checkForWhiteSpace: true
                },
                email: {
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                password: {
                    required: "Password is required.",
                    rangelength: "Password must be between 8 to 150 characters."
                },
                password_confirmation: {
                    required: "Password confirmation is required.",
                    equalTo: "Password should match."
                },
                email: {
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleGatekeeperCreate = function handleGatekeeperCreate() {
        $("#createGatekeeperResetBtn").click(function () {
            resetTeamMemberForm($("#createGatekeeperForm"));
        });
        $("#createGatekeeperBtn").click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            if (!form.valid()) {
                return;
            }
            DDPApp.disableButtonWithLoading(btn);
            var formData = new FormData();
            var image = document.getElementById('profile_photo');
            if (image.files && image.files[0]) {
                var extension = image.files[0].name.split('.');
                var temp2 = extension[extension.length - 1].toLowerCase();
                var size = parseFloat(image.files[0].size / 1024).toFixed(2);
                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('profile_photo', image.files[0]);
                }
            }
            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });
            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        resetGatekeeperForm(form);
                        DDPApp.enableButton(btn);
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };
    var resetGatekeeperForm = function resetGatekeeperForm(form) {
        $(".ats-blood-group").val('').trigger("change");
        DDPApp.resetForm(form);
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    return {
        init: function init() {
            gatekeeperCreateValidation();
            initGatekeeperCreate();
            handleGatekeeperCreate();
        }
    };
}();
jQuery(document).ready(function () {
    GatekeeperCreate.init();
});

var ManageGatekeeper = function () {
    var initGatekeeperManage = function initGatekeeperManage() {
        $('.gatekeeper-attrib-box').select2({
            minimumResultsForSearch: -1
        }).change(function () {
            handleGatekeeperRedirect();
        });
        $("#manage_gatekeeper_search_btn").click(function () {
            handleGatekeeperRedirect();
        });
        $("#manage_gatekeeper_search_refresh_btn").click(function () {
            $("#manage_gatekeeper_search_txt").val('');
            handleGatekeeperRedirect();
        });
        $('#all_gatekeeper_selection').click(function () {
            if ($(this).hasClass('all-checked')) {
                $('input[type="checkbox"]', '#gatekeeper_selection').prop('checked', false);
            } else {
                $('input[type="checkbox"]', '#gatekeeper_selection').prop('checked', true);
            }
            $(this).toggleClass('all-checked');
        });
    };
    var handleGatekeeperRedirect = function handleGatekeeperRedirect() {
        var route = [location.protocol, '//', location.host, location.pathname].join('');
        route += "?records=" + $("#records").val();
        route += "&status=" + $(".gatekeeper-tabview").attr("data-status");
        if ($("#manage_gatekeeper_search_txt").val().trim() != '') {
            route += '&search=' + $("#manage_gatekeeper_search_txt").val().replace(/<[^>]+>/g, '').trim();
        }
        window.location.href = route;
    };
    return {
        init: function init() {
            initGatekeeperManage();
        }
    };
}();
jQuery(document).ready(function () {
    ManageGatekeeper.init();
});

var EditGatekeeper = function () {

    var gatekeeperEditValidation = function gatekeeperEditValidation() {

        var preview = ["no image"];
        var previewCaption = "Select Profile Photo";
        var broweseLabel = "Browse";
        var showRemove = true;
        if ($("#edit_profile_photo").is("[data-profile-photo-url]")) {
            preview = ["<img src='" + $("#edit_profile_photo").attr("data-profile-photo-url") + "' height='180px' width='180px'>"];
            previewCaption = "1 Profile photo uploaded";
            broweseLabel = "Change";
            showRemove = false;
        }

        $("#edit_profile_photo").fileinput({
            showUpload: false,
            showRemove: showRemove,
            previewFileType: 'image',
            minFileCount: 1,
            maxFileCount: 1,
            allowedFileExtensions: ['jpeg', 'jpg', 'png'],
            msgErrorClass: 'file-error-message d-none',
            minFileSize: 1,
            //maxFileSize: 2000,
            msgInvalidFileExtension: 'Invalid File Format',
            browseLabel: broweseLabel,
            browseClass: 'btn btn-sm btn-brand m-btn--air',
            removeClass: 'btn btn-sm btn-danger m-btn--air',
            initialPreview: preview,
            initialCaption: previewCaption
        }).on('change', function (event) {
            $(this).valid();
        }).on('fileclear', function (event) {
            $('#edit_profile_photo').fileinput('destroy');
            $("#edit_profile_photo").fileinput({
                showUpload: false,
                showRemove: showRemove,
                previewFileType: 'image',
                minFileCount: 1,
                maxFileCount: 1,
                allowedFileExtensions: ['jpeg', 'jpg', 'png'],
                msgErrorClass: 'file-error-message d-none',
                browseLabel: broweseLabel,
                browseClass: 'btn btn-sm btn-brand m-btn--air',
                removeClass: 'btn btn-sm btn-danger m-btn--air',
                minFileSize: 1,
                //maxFileSize: 2000,
                msgInvalidFileExtension: 'Invalid File Format',
                initialPreview: preview,
                initialCaption: previewCaption
            });
        }).on('filecleared', function (event) {
            $(this).valid();
        }).on('fileerror', function (event, data, msg) {
            $(this).trigger('fileclear');
        });
        $("#edit_profile_photo").trigger('fileclear');
    };

    var initGatekeeperEdit = function initGatekeeperEdit() {

        $("#editGatekeeperFormss").validate({
            rules: {
                employee_id: {
                    checkForWhiteSpace: true
                },
                first_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                last_name: {
                    required: true,
                    checkForWhiteSpace: true
                },
                middle_name: {
                    checkForWhiteSpace: true
                },
                profile_photo: {
                    filesize: 2048,
                    accept: "image/jpg,jpeg,png",
                    extension: "jpg|jpeg|png"
                },
                email: {
                    EMAIL: true,
                    checkForWhiteSpace: true
                },
                phone_number: {
                    required: true,
                    checkForWhiteSpace: true,
                    digits: true,
                    rangelength: [10, 12]
                }
            },
            messages: {
                first_name: {
                    required: "First name is required."
                },
                last_name: {
                    required: "Last name is required."
                },
                profile_photo: {
                    filesize: "Profile photo must be under 2 MB of size.",
                    accept: "Please select JPG, JPEG or PNG file.",
                    extension: "Profile photo must be in .jpg,.jpeg,.png format."
                },
                email: {
                    EMAIL: "Enter email address in proper format."
                },
                phone_number: {
                    required: "Phone number is required.",
                    digits: "Enter valid phone number.",
                    rangelength: "Phone number must be between 10 to 12 digits."
                }
            }
        });
    };
    var handleGatekeeperEdit = function handleGatekeeperEdit() {

        $("#updateGatekeeperBtn").click(function (e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            if (!form.valid()) {
                return;
            }

            DDPApp.disableButtonWithLoading(btn);

            var formData = new FormData();

            var image = document.getElementById('edit_profile_photo');

            if (image.files && image.files[0]) {

                var extension = image.files[0].name.split('.');

                var temp2 = extension[extension.length - 1].toLowerCase();

                var size = parseFloat(image.files[0].size / 1024).toFixed(2);

                if (size > 2048) {
                    toastr.warning("Maximum upload file size is 2MB", "Size Alert");
                    return false;
                } else if (temp2 != "jpg" && temp2 != "jpeg" && temp2 != "png") {
                    toastr.warning("Allow only jpg, png format files", "Format Alert");
                    return false;
                } else {
                    formData.append('profile_photo', image.files[0]);
                }
            }

            form.serializeArray().forEach(function (field) {
                if (field.value.trim() != '' || field.value.trim() != null) {
                    formData.append(field.name, field.value);
                }
            });

            $.ajax({
                url: form.attr("data-action"),
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function success(data) {
                    DDPApp.displayResultWithCallback(data, function () {
                        DDPApp.removeLoadingFromButton(btn);
                        var referrer = document.referrer;
                        if (!referrer) {
                            window.location.href = form.attr("data-redirect-url");
                        } else {
                            window.location.href = referrer;
                        }
                    }, function () {
                        DDPApp.enableButton(btn);
                    });
                },
                error: function error(data) {
                    DDPApp.displayFailedValidation(data);
                    DDPApp.enableButton(btn);
                }
            });
        });
    };

    return {
        init: function init() {
            gatekeeperEditValidation();
            initGatekeeperEdit();
            handleGatekeeperEdit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    EditGatekeeper.init();
});