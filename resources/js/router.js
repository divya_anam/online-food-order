import Vue from 'vue'
import VueRouter from 'vue-router'

import RestaurantHome from './components/Restaurant/RestaurantHome.vue';
import RestaurantDetails from './components/Restaurant/RestaurantDetails.vue';
import PaymentSummary from './components/Order/PaymentSummary.vue';
import PaymentSuccess from './components/Order/PaymentSuccess.vue';
import PaymentFailed from './components/Order/PaymentFailed.vue';
import Error404 from './components/Errors/Error404.vue';
import Error405 from './components/Errors/Error405.vue';
import Error503 from './components/Errors/Error503.vue';
import ErrorDefault from './components/Errors/ErrorDefault.vue';


Vue.use(VueRouter)

export const routes = [
    {
        path: '/',
        name: 'home',
        component: RestaurantHome,
    },
    { 
        path:'/restaurant-detail/:restaurant_id',
        component:RestaurantDetails,
        name:'restaurant-detail'
    },
    {
        path:'/payment/payment-success',
        component:PaymentSuccess,
        name:'payment-succes'
    },
    {
        path:'/payment/payment-failed',
        component:PaymentFailed,
        name:'payment-failed'
    },
    {
        path:'/order/:order_id/payment',
        component:PaymentSummary,
        name:'order-payment'
    },

    {
        path: '/404',
        name:'error-404',
        component:Error404,
    },
    {
        path: '/405',
        name:'error-405',
        component:Error405,
    },
    {
        path: '/503',
        name:'error-503',
        component:Error503,

    },
    {
        path:'/error',
        name:'error-default',
        component:ErrorDefault
    }
];

export default new VueRouter({mode: 'history', routes})