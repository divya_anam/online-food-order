import Vue from 'vue'
import Vuex from 'vuex'
import orderStore from './modules/orderStore'
import restaurantStore from './modules/restaurantStore'


Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        orderStore,
        restaurantStore
    }

})