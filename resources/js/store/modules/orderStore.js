import axios from '../../axios-default.js'
import vm from '../../app.js'

const mutations = {
    'CREATE_ORDER'(state,order){
        var url='/order'
        console.log(order)
        axios.post(url,order)
        .then(response => {
            if(response.status == 200 && response.data.result){
                vm.$router.push({ name: 'order-payment', params: { order_id: response.data.order_id } })
            }
        })
        .catch(error => {
            if(error.response.status == 404){
                vm.$router.push('/404')
            } 
            else if(error.response.status == 405){
                vm.$router.push('/405')
            }
            else if(error.response.status == 503){
                vm.$router.push('/503')
            }
            else{
                vm.$router.push('/error')
            }
        })
    },
    'MAKE_PAYMENT'(state,payment_details){
        var url='/order/make-payment'
        axios.post(url,payment_details)
        .then(response => {
            if(response.status == 200 && response.data.result){
                vm.$router.push('/payment/payment-success');
            }
            else if(response.status == 200 && !response.data.result){
                vm.$router.push('/payment/payment-failed');
            }
        })
        .catch(error => {
            console.log(error)
        })
    }

}
const actions = {
    create_order({commit},order){
        commit('CREATE_ORDER',order);
    },
    make_payment({commit},payment_details){
        commit('MAKE_PAYMENT',payment_details);
    }
}

export default {
    mutations,
    actions,
}