import axios from '../../axios-default.js'
import vm from '../../app.js'

const state = {
    restaurants:null,
    menu:null,
}

const mutations = {
    'GET_RESTAURANT'(state,search){
        var url='/restaurant?search='+search
        axios.get(url)
        .then(response => {
            state.restaurants=response.data.restaurants;
        })
        .catch(error => {
            if(error.response.status == 404){
                vm.$router.push('/404')
            } 
            else if(error.response.status == 405){
                vm.$router.push('/405')
            }
            else if(error.response.status == 503){
                vm.$router.push('/503')
            }
            else{
                vm.$router.push('/error')
            }
        })
    },
    'GET_MENU'(state,id){
        var url = '/restaurant/'+id
        axios.get(url)
        .then(response => {
            state.menu = response.data.menu_options;
        })
        .catch(error => {
            if(error.response.status == 404){
                vm.$router.push('/404')
            } 
            else if(error.response.status == 405){
                vm.$router.push('/405')
            }
            else if(error.response.status == 503){
                vm.$router.push('/503')
            }
            else{
                vm.$router.push('/error')
            }
        })
    }
}

const actions = {
    get_restaurant({commit},search){
        commit('GET_RESTAURANT',search)
    },
    get_menu({commit},restaurant_id){
        commit('GET_MENU',restaurant_id)
    }
}

const getters = {
    get_restaurant(state){
        return state.restaurants
    },
    get_menu(state){
        return state.menu
    }
}


export default {
    state,
    mutations,
    actions,
    getters
}