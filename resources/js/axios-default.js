import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://divya.twoiq.me/'
})

// instance.defaults.headers.common['SOMETHING'] = 'something'

export default instance