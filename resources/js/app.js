import Vue from 'vue'
import App from './App.vue'
import store from './store/store'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import router from './router.js'

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
const vm = new Vue({
    el: '#app',
    render: h => h(App),
    router:router,
    store:store,
});
export default vm