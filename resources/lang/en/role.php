<?php

return [
    'add_role' => 'A new role has been created',  
    'user_exists_in_role' => 'Users exists for this role',
    'update_role' => 'Role information has been updated.',
    'destroy_role' => 'Role has been deleted.',
    'role_not_found' => 'Role information not found.',
    'role_search_not_found' => 'No roles available in system!',

    'add_role_title' => 'Add a Role',
    'edit_role_title' => 'Edit a Role',
    'list_role_title' => 'Manage Roles',
    'role_label' => 'Role',
    'role_placeholder' => 'Enter Role Name',
];