<?php

return [
    'add_user' => 'A new user has been registered and activated.',
    'update_user' => 'User information has been updated.',
    'unauthorized_user' => 'Unauthorize request.',
    'destroy_user' => 'User has been deleted.',
    'user_search_not_found' => 'No users available in system!',

    'add_user_title' => 'Add an User',
    'edit_user_title' => 'Edit an User',
    'list_user_title' => 'Manage Users',


    'first_name_label' => 'First Name',
    'middle_name_label' => 'Middle Name',
    'last_name_label' => 'Last Name',
    'phone_number_label' => 'Phone Number',
    'email_label' => 'Email',
    'password_label' => 'Password',
    'confirm_password_label' => 'Confirm Password',

    'first_name_placeholder' => 'Enter First Name',
    'middle_name_placeholder' => 'Enter Middle Name',
    'last_name_placeholder' => 'Enter Last Name',
    'phone_number_placeholder' => 'Enter Phone Number',
    'email_placeholder' => 'Enter Email Address',
    'password_placeholder' => 'Enter Password',
    'confirm_password_placeholder' => 'Retype Password',

];
