<?php
return [
    //Login Screen
    'sign_in' => 'Sign In',
    'sign_out' => 'Sign Out',
    //Change Password Screen
    'change_password' => 'Change Password',
    //forget Password Screen
    'forgot_password_send_email' => 'Send Email',
    //Reset Password Screen
    'reset_password_reset_password' => 'Reset Password',
    //Form button
    'save' => 'Save',
    'reset' => 'Reset',
    'request' => 'Request',
    'cancel' => 'Cancel',
    'update' => 'Update',
];