<?php

return [
    'sorry_word' => 'Sorry!',
    'attention_word' => 'Attention!',
    'great_word' => 'Great!',
    
    'add_order' => 'Add Order',
    'update_order' => 'Update Order',
];