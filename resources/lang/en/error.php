<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'unauthorized' => 'Unauthorized to access.',
    'missing_token' => 'Please provide token.',
    '500' => 'Unable to :operation, Please contact system admin.',
    'account_not_found' => 'You\'re account has been not found.',
    'account_banned' => 'You\'re account has been banned in the system.',
    'login_expired' => 'You\'re login is expired. Please login again.',
    'login_invalid' => 'You\'re login is invalid. Please login again.',
    '404' => ':slug information not found',
    'unauthorized_dealer' => 'Unauthorized :slug',
    'notice_error_message' => 'Your reset password request could not found or expire, Please try again by sending a new request',
];