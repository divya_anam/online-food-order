<?php

return [
    'invalid_credantial' => ':user_name Invalid User Name/Email Address or Password. Incase of Multiple Attempts Please Contact System Admin.',
    'not_found_msg' => ':Module_name information not found.',
    'reminder_not_found_msg' => 'Your reset password request could not found or expire, Please try again by sending a new request.',
    'mail_not_registered' => 'This email address is not registered in the system.',
    'success_mail_for_forgot_password' => 'The Reset Password process has been initiated and you will receive an email shortly for the same.',
    'error_msg_while_sending_mail' => 'An error has occured on the server please try again later.',
    'reset_password_successfully' => 'Your Password Reset Successfully.',
];