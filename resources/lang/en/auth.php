<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    
    'already_logged_in' => 'You are already logged in another tab please refersh your current page.',
    'change_password' => 'Your password has been changed successfully.',
    'incorrect_password' => 'Current password you have entered is incorrect ! Please try again with correct password for change password',
    'unexcepted_response' => 'Sorry, could not process your request, Please contact your administrator.',
    'admin_logged_in' => 'Admin Logged into System.',
    'success_admin_login' => 'Admin :first_name :last_name Login Successfully From Server Ip=> :ip_address .',
    'welocme_msg' => "Welcome To :app_name </br> :full_name",
    'failed_admin_login' => 'Error at Login Time.',
    'invalid_login' => ':User_name Invalid Login Credential. Incase of Multiple Attempts Please Contact System Admin.',
    'incorrect_mail' => 'This email address is not registered in the system.',
    'under_process_reset_password' => 'Your reset password is under process you may have received an email please check spam folders.',
    'successfully_forgot_password' => 'The Reset Password process Has Been Initiated And You Will Recieve An Email Shortly For The Same.',

    //Server side validations
    'required_current_password' => 'Current passowrd is required',
    'required_password' => 'Password is required',
    'minimum_length_of_password' => 'Minimum length of password is 8',
    'password_should_match' => 'Password should match',
    'required_recovery_email' => 'Recovery email address is required',
    'email_validation' => 'Please enter valid email address',
    'email_exists_validation' => 'A user with given email does not exists',
    'required_username' => 'Username is required',

    //titles
    'reset_password_title' => 'Reset Password',
    'login_title' => 'Login',
    'change_password_title' => 'Change Password',

    //placeholders
    'new_password_placeholder' => 'Enter New Password',
    'confirm_password_placeholder' => 'Retype New Password',
    'username_placeholder' => 'Mobile Number/Email Address',
    'password_placeholder' => 'Password',
    'email_placeholder' => 'Email',
    'current_password_placeholder' => 'Enter Your Current Password',

    //Labels
    'current_password_label' => 'Current Password',
    'new_password_label' => 'New Password',
    'confirm_password_label' => 'Confirm New Password',
];