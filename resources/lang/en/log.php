<?php
return [
    'error_add_user' => 'Error while adding user',
    'error_update_user' => 'Error while editing user information',
    'error_delete_user' => 'Error while removing user',
    'error_list_user' => 'Error while listing the user information',
    
    'error_add_role' => 'Error while adding role',
    'error_update_role' => 'Error while editing role information',
    'error_delete_role' => 'Error while removing role',
    'error_list_role' => 'Error while listing the role information',
];