<?php

use App\Models\Restaurant\Restaurant;
use Faker\Generator as Faker;


$factory->define(Restaurant::class, function (Faker $faker) {
    $cuisine = ['Indian','Continental','Chinese','Italian','Snacks'];
    $attributes = [
        'name'=>$faker->company,
        'cuisine'=>$cuisine[array_rand($cuisine)] ,
        'location'=>$faker->streetName,
    ];
    return $attributes;
});
