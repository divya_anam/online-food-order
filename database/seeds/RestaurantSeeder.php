<?php

use Illuminate\Database\Seeder;
use App\Repositories\Restaurant\RestaurantRepository;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $restaurant_repo;
    public function __construct(RestaurantRepository $restaurant_repo){
		$this->restaurant_repo=$restaurant_repo;
    }
    
    public function run()
    {
        foreach(range(1,10) as $counter){
            $data = (factory(App\Models\Restaurant\Restaurant::class)->make()->getAttributes());
            $this->restaurant_repo->addRestaurant($data);
        }
    }
}
