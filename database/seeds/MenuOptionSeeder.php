<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Repositories\Restaurant\MenuOptionRepository;
use App\Models\Restaurant\Restaurant;

class MenuOptionSeeder extends Seeder
{
    protected $menu_repo;
    public function __construct(MenuOptionRepository $menu_repo){
		$this->menu_repo=$menu_repo;
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));
        $resturants = Restaurant::select('id')->get();
        foreach(range(1, 100) as $counter){
    		$restaurant = $resturants->random();
            $attributes = [
                'restaurant_id'=>$restaurant->id,
                'item_name'=>$faker->foodName(),
                'item_price'=>$faker->numberBetween(50,200)
            ];
            $this->menu_repo->addMenuOptions($attributes);
        }
    }
}
